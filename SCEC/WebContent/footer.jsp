<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>

	<div id = "footer" style="background-color:#dbdad8">
		<div class="container">
			<div class="row">
				<div class="span12" style="text-align:left">
				<ul class="list-inline" style="margin-left:0px">
                    <li><a href="#about">關於盛全</a> </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li><a href="#contact">聯絡</a> </li>
				</ul>
				<ul class="list-inline" style="margin-left:0px">
	    			<li>&copy;2013-2014 盛全企業管理顧問有限公司. All right reserved</li>
                    <li class="footer-menu-divider">|</li>
	    			<li>服務時間08:30~19:00</li>
                    <li class="footer-menu-divider">|</li>
	    			<li>手機: (日)0939-931223 (夜/假日)0936-901961 </li>
				</ul>
				</div>
			</div>
	    </div>
    </div><!-- end #footer -->