<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
Boolean included = (Boolean)request.getAttribute("included");
%>

<%-- 此頁練習採用 EL 的寫法取值 --%>

<jsp:useBean id="listEmpsByDeptId" scope="request" type="java.util.List" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />
<html>
<head>
<title>部門員工 </title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>
<script type="text/javascript">
	$(function () {
    	$("#empTable").tablesorter({widgets: ['zebra']});
	});
</script>
</head>

<body>

<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<% 
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>

<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li>${message}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>

		<table id="empTable" class="tablesorter">
			<thead>
			<tr>
				<th>員工編號</th>
				<th>員工姓名</th>
				<th>部門</th>
				<th>單位</th>
				<th>職位</th>
				<th>職系</th>
				<th>職等</th>
				<th>到職日期</th>
				<th>帳號</th>
				<!--
				<th>修改</th>
				<th>刪除</th>
				-->
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${listEmpsByDeptId}" >
				<tr align='center' valign='middle'>
					<td>${empVO.empId}</td>
					<td>${empVO.empName}</td>
					<td>${empVO.deptName}</td>
					<td>${empVO.sectionName}</td>
					<td>${empVO.titleName}</td>
					<td>${empVO.seriesName}</td>
					<td>${empVO.levelName}</td>
					<td>${empVO.onboard}</td>
					<td>${empVO.account}</td>
					<!--
					<td>
					  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/emp/emp.do">
					    <input type="submit" value="修改"> 
					    <input type="hidden" name="empId" value="${empVO.empId}">
					    <input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>">
					    <input type="hidden" name="action"	value="getOne_For_Update"></FORM>
					</td>
					<td>
					  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/emp/emp.do">
					    <input type="submit" value="刪除">
					    <input type="hidden" name="empId" value="${empVO.empId}">
					    <input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>">
					    <input type="hidden" name="action"value="delete"></FORM>
					</td>
					-->
				</tr>
			</c:forEach>
		</table>

	<c:if test="${debugMode != null}">
		<br>本網頁的路徑:<br><b>
   		<font color=blue>request.getServletPath():</font> <%= request.getServletPath()%><br>
   		<font color=blue>request.getRequestURI(): </font> <%= request.getRequestURI()%> </b>
   	</c:if>
</body>
</html>
