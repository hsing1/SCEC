<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.department.model.*"%>

<%  
// EmpService empSvc = new EmpService();
// List<EmpVO> list = empSvc.getAll();
// pageContext.setAttribute("list",list);
int deptId = 0;
Boolean included = (Boolean)request.getAttribute("included");
HashMap<Integer, Integer> deptMap = DepartmentService.numOfEmpDept;
HashMap <String, String> deptMap2 = new HashMap<String, String>();
deptMap2.put("1", "a");
deptMap2.put("2", "b");
deptMap2.put("3", "c");
request.setAttribute("deptMap", deptMap);
request.setAttribute("deptMap2", deptMap2);
%>
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />

<html>
<head>
<title>┮Τ场 </title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/style.css" />
<!--
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />
-->

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
			/*
				padding-top: 60px;
				padding-bottom: 40px;
				*/
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>
<script type="text/javascript">
	$(function () {
    	$("#deptTable").tablesorter({widgets: ['zebra']});
		$(".showDetail").fancybox();
	});
</script>

</head>

<body>

<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<% 
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>
	<div class="container">
		<div class="push"></div>
		<div class="row">
			<div class="span20">
				<ul class="breadcrumb">
					<li><a href="#">琩高</a> <span class="divider">&gt;</span></li>
					<li class="active">场舱麓</li>
				</ul>
			</div>
		</div>
		
		<table id="deptTable" class="tablesorter">
			<thead>
			<tr>
				<th><h4>场絪腹</h4></th>
				<th><h4>场嘿</h4></th>
				<th><h4>场计</h4></th>
				<!--
				<th>э</th>
				<th>埃<font color=red>(闽羛代刚籔ユ-み)</font></th>
				-->
			</tr>
			</thead>
			
			<c:forEach var="deptVO" items="${deptSvc.all}">
				<tr align='center' valign='middle'>
					<td><h5>${deptVO.deptId}</h5></td>
					<td><h5>${deptVO.name}</h5></td>
					<!--
					<td>${deptMap["${deptVO.deptId}"]}</td>
					<td>
					  	<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/dept/dept.do">
					    	<input type="submit" value="э" disabled="true"> 
					    	<input type="hidden" name="deptId" value="${deptVO.deptId}">
					    	<input type="hidden" name="action" value="getOne_For_Update_Dept">
						</FORM>
					</td>
					<td>
					  	<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/dept/dept.do">
						    <input type="submit" value="埃" disabled="true">
						    <input type="hidden" name="deptId" value="${deptVO.deptId}">
						    <input type="hidden" name="action" value="delete_Dept">
						</FORM>
					</td>
					-->
					<!--
					<td>
						<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/dept/dept.do">
							<c:choose>
								<c:when test="${not empty deptMap[deptVO.deptId]}">
								    <input type="submit" class="showDetail fancybox.ajax" value="${deptMap[deptVO.deptId]}"> 
								</c:when>
								<c:otherwise>
								    <input type="submit" class="showDetail fancybox.ajax" value="0"> 
								</c:otherwise>
							</c:choose>
						    <input type="hidden" name="deptId" value="${deptVO.deptId}">
						    <input type="hidden" name="action" value="listEmpsByDeptId_B">
						</FORM>
					</td>
					-->
					<td>
						<h5>
						<c:choose>
							<c:when test="${not empty deptMap[deptVO.deptId]}">
								<a href="<%=request.getContextPath()%>/dept/dept.do?action=listEmpsByDeptId_B&deptId=${deptVO.deptId}">${deptMap[deptVO.deptId]}</a>
							</c:when>
							<c:otherwise>
								0
							</c:otherwise>
						</c:choose>
						</h5>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<div class="secondary">
			<%if (request.getAttribute("listEmpsByDeptId")!=null) {%>
				<jsp:include page="/dept/listEmpsByDeptId.jsp"/>
			<%} %>
		</div>
	</div> <!-- end of container -->
</div> <!--  end of wrapper -->


</body>
</html>
