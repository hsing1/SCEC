var xhr1 = null;
var xhr = null;
var xhr2 = null;


function createXHR1(){	
	//alert("createXHR");
	if( window.XMLHttpRequest ){	
		xhr1 = new XMLHttpRequest();
	}else if( window.ActiveXObject ){		
		xhr1 = new ActiveXObject("Microsoft.XMLHTTP");
	} else
		xhr1 = null;    
	
	return xhr1;
}	

function updateSection(allSection){
	var sectionList = JSON.parse(allSection);
	var sectionSelector = document.getElementById("selectSection");
	var titleSelector = document.getElementById("selectTitle");
	var name;
	var id;
	var option = null, option1 = null;
	var url = "/SCEC/dept/dept.do";
	var action = "getTitleByDeptAndSec";
	var elementObj = {};
		
	sectionSelector.options.length = 0;
	sectionSelector.disabled = false;
	option = new Option("--請選擇--", -1);
	sectionSelector.options.add(option, null);
	
	if (sectionList.length == 2 && sectionList[1] == "none") {
		elementObj.deptElemId = "selectDept";
		elementObj.sectionElemId = "selectSection";
		id = sectionList[0];
		name = "--請選擇--";
		option = new Option(name, id, true, true);
		//option.selected = true;
		sectionSelector.options.add(option, null);
		sectionSelector.disabled = true;
		getTitle(url, action, elementObj, updateTitle);
		return;
	}
			
	if (titleSelector) {
		option1 = new Option("--請選擇--", -1);
		titleSelector.options.length = 0;
		titleSelector.options.add(option1, null);
	}
			
	for (var i=0; i< sectionList.length; i++){
		id = sectionList[i++];
		name = sectionList[i];
		option = new Option(name, id);
		sectionSelector.options.add(option, null);
	}				
}

function getSection(elementId) {
	var selectDept = document.getElementById(elementId);
	var opts = selectDept.options;
	//var deptId = selectDept.value;
	//var deptId = selectDept.selectedIndex;
	var url = '/SCEC/dept/dept.do';
	var selectedArr = [];
	var requestData = {
		action : "getSection",
		selectedArr : selectedArr,
	};
	
	var deptId = selectDept.value;
	
	selectedArr.push(deptId);
	
	$.ajax( {
		type : 'POST',
		url : url,
		data : requestData,
		success : updateSection
	});
}

function getSectionMulti() {
	var selectDept = document.getElementById("selectDept");
	var opts = selectDept.options;
	//var deptId = selectDept.value;
	//var deptId = selectDept.selectedIndex;
	var url = '/SCEC/dept/dept.do';
	var selectedArr = [];
	var requestData = {
		action : "getSection",
		selectedArr : selectedArr,
	};
	
	for (var i = 0; i < opts.length; i++) {
		if (opts[i].selected == true)
			selectedArr.push(opts[i].value);
	}
	
	$.ajax( {
		type : 'POST',
		url : url,
		data : requestData,
		success : updateSection
	});
}

function updateTitle(allTitle){
	var titleList = JSON.parse(allTitle);
	var titleSelector = document.getElementById("selectTitle");
	var name;
	var id;
	var option = null;
		
	titleSelector.options.length = 0;
	option = new Option("--請選擇--", -1);
	titleSelector.options.add(option, null);
			
	for (var i=0; i< titleList.length; i++){
		id = titleList[i++];
		name = titleList[i];
		option = new Option(name, id);
		titleSelector.options.add(option, null);
	}				
}

function updateTitle1Multi(allTitle){
	var titleList = JSON.parse(allTitle);
	var titleSelector = document.getElementById("selectTitle1");
	var empSelector = document.getElementById("selectEmp1");
	var name;
	var id;
	var option = null, option1 = null;
		
	titleSelector.options.length = 0;
	option = new Option("--請選擇--", -1);
	titleSelector.options.add(option, null);
			
	if (empSelector) {
		option1 = new Option("--請選擇--", -1);
		empSelector.options.length = 0;
		empSelector.options.add(option1, null);
	}
			
	for (var i=0; i< titleList.length; i++){
		id = titleList[i++];
		name = titleList[i];
		option = new Option(name, id);
		titleSelector.options.add(option, null);
	}				
}

function updateTitle1(){
	//alert("readyState=" + xhr1.readyState);
	if(xhr1.readyState == 4){
		if(xhr1.status == 200){
			var allTitle = xhr1.responseText;
			var titleList = JSON.parse(allTitle);
			var titleSelector = document.getElementById("selectTitle1");
			var empSelector = document.getElementById("selectEmp1");
			var name;
			var id;
			var option = null, option1 = null;
			
			titleSelector.options.length = 0;
			option = new Option("--請選擇--", -1);
			titleSelector.options.add(option, null);
			
			if (empSelector) {
				option1 = new Option("--請選擇--", -1);
				empSelector.options.length = 0;
				empSelector.options.add(option1, null);
			}
			
			for (var i=0; i< titleList.length; i++){
				id = titleList[i++];
				name = titleList[i];
				option = new Option(name, id);
				titleSelector.options.add(option, null);
			}				
		}
	}
}

function getTitle1Multi(){	
	var selectDept = document.getElementById("selectDept1");
	var opts = selectDept.options;
	//var deptId = selectDept.value;
	//var deptId = selectDept.selectedIndex;
	var url = '/SCEC/dept/dept.do';
	var selectedArr = [];
	var requestData = {
			action : "getTitle",
			selectedArr : selectedArr,
	};
	
	for (var i = 0; i < opts.length; i++) {
		if (opts[i].selected == true)
			selectedArr.push(opts[i].value);
	}
	
	$.ajax( {
		type : 'POST',
		url : url,
		data : requestData,
		success : updateTitle1Multi
	});
}

function getTitle(url, action, elementObj, success){	
	var deptElemId = elementObj.deptElemId;
	var sectionElemId = elementObj.sectionElemId;
	var deptId, sectionId;
	var deptArr = [], sectionArr = [];
	var requestData = {
			action : action,
			deptArr : deptArr,
			sectionArr : sectionArr
	};
	
	if (deptElemId) {
		deptId = document.getElementById(deptElemId).value;
		deptArr.push(deptId);
	} else {
		deptArr.push(0);
	}
	
	if (sectionElemId) {
		sectionId = document.getElementById(sectionElemId).value;
		sectionArr.push(sectionId);
	} else {
		sectionArr.push(0);
	}
	
	$.ajax({
		type : "POST",
		url : url,
		data : requestData,
		success : success 
	});
}

function getTitle1(){	
	var url = '/SCEC/dept/dept.do';
	var selectedArr = [];
	var requestData = {
			action : "getTitle",
			selectedArr : selectedArr,
	};
	
	var deptId = document.getElementById("selectDept1").value;
	selectedArr.push(deptId);
	
	$.ajax({
		type : "POST",
		url : url,
		data : requestData,
		success : updateTitle1Multi
	});
}

function getTitle1_org(){	
	xhr1 = createXHR1();
	//alert("getTown");
	if( xhr1 == null ){
		alert("no xhr creted");
		return;
	}		   
	  
	var selectDept = document.getElementById("selectDept1");
	//var deptId = selectDept.value;
	var deptId = selectDept.selectedIndex;
	//var deptId = $('#selectDept1').value();
	var url = '/SCEC/dept/dept.do';
	var requestData = 'action=getTitle&deptId=' + deptId;
	xhr1.open('POST', url, true);
	xhr1.onreadystatechange = updateTitle1;
	xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr1.send(requestData);
}

function updateEmp1Multi(allEmp) {  
	var empList = JSON.parse(allEmp);
	var empSelector = document.getElementById("selectEmp1");
	var option = new Option("--請選擇--", -1);
	empSelector.options.add(option, null);				
	var name;
	var id;
	empSelector.options.length = 0; 
	
	empSelector.options.add(option, null);
	for (var i=0; i< empList.length; i++){
		id = empList[i++];
		name = empList[i];
		option = new Option(name, id);
		empSelector.options.add(option, null);				
	}				
}

function updateEmp1(){  
	//alert("readyState=" + xhr.readyState);
	if(xhr.readyState == 4){
		if(xhr.status == 200){
			var allEmp = xhr.responseText;
			var empList = JSON.parse(allEmp);
			var empSelector = document.getElementById("selectEmp1");
			var option = new Option("--請選擇--", -1);
			empSelector.options.add(option, null);				
			var name;
			var id;
			empSelector.options.length = 0; 
			
			empSelector.options.add(option, null);
			for (var i=0; i< empList.length; i++){
				id = empList[i++];
				name = empList[i];
				option = new Option(name, id);
				empSelector.options.add(option, null);				
			}				
		}
	}
}

function getEmp1Multi() {	
	var selectDept = document.getElementById("selectDept1").options;
	var selectTitle = document.getElementById("selectTitle1").options;
	var url = '/SCEC/employee/emp.do';
	var deptArr = [], titleArr = [];
	var i = 0;
	var requestData = null;
	
	for (i = 0; i < selectDept.length; i++) {
		if (selectDept[i].selected == true)
			deptArr.push(selectDept[i].value);
	}
	
	for (i = 0; i < selectTitle.length; i++) {
		if (selectTitle[i].selected == true)
			titleArr.push(selectTitle[i].value);
	}
	
	requestData = {
		action : "listEmpByDeptAndTitleMulti",
		deptArr : deptArr,
		titleArr : titleArr
	};
	
	$.ajax({
		type : "POST",
		url : url,
		data : requestData,
		success : updateEmp1Multi
	});
}

function getEmp1_js() {	
	xhr = createXHR1();
	if( xhr == null ){
		alert("no xhr creted");
		return;
	}		   
	  
	var deptId = document.getElementById("selectDept1").value;
	var titleId = document.getElementById("selectTitle1").value;
	var html = '<input type="hidden" name="deptId" value=' + deptId + '>' +
			   '<input type="hidden" name="titleId" value=' + titleId + '>';
	
	var selectInfo = $('#selectedInfo');
	if (selectInfo != null)
		$('#selectedInfo').html(html);
	
	xhr.open('Get', '/SCEC/employee/emp.do?action=listEmpByDeptAndTitle&deptId='+deptId+'&titleId='+titleId, true);
	xhr.onreadystatechange = updateEmp1;
	xhr.send(null);
}


/*
 * process emp2
 */
function updateTitle2(allTitle){  
	var titleList = JSON.parse(allTitle);
	var titleSelector = document.getElementById("selectTitle2");
	titleSelector.options.length = 0;
	var name;
	var id;
			
	option = new Option("--請選擇--", -1);
	titleSelector.options.add(option, null);
	for (var i=0; i< titleList.length; i++){
		id = titleList[i++];
		name = titleList[i];
		option = new Option(name, id);
		titleSelector.options.add(option, null);
	}				
}

//TODO:check why it fail
function getTitle2_fail() {	
	xhr1 = createXHR1();
	var url = '/SCEC/dept/dept.do';
	var selectedArr = [];
	var requestData = {
			action : "getTitle",
			selectedArr : selectedArr,
	};
	
	if( xhr1 == null ){
		alert("no xhr creted");
		return;
	}		   
	var deptId = document.getElementById("selectDept2").value;
	selectedArr.push(deptId);
	
	xhr1.onreadystatechange = updateTitle2;
	xhr1.open('POST', url, true);
	xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr1.send(requestData);
}

function getTitle2(){	
	var url = '/SCEC/dept/dept.do';
	var selectedArr = [];
	var requestData = {
			action : "getTitle",
			selectedArr : selectedArr,
	};
	
	var deptId = document.getElementById("selectDept2").value;
	selectedArr.push(deptId);
	
	$.ajax({
		type : "POST",
		url : url,
		data : requestData,
		success : updateTitle2
	});
}


function updateEmp2(){  
	if(xhr.readyState == 4){
		if(xhr.status == 200){
			var allEmp = xhr.responseText;
			var empList = JSON.parse(allEmp);
			var empSelector = document.getElementById("selectEmp2");
			var option = new Option("--請選擇--", -1);
			empSelector.options.add(option, null);				
			var name;
			var id;
			empSelector.options.length = 0; 
			
			empSelector.options.add(option, null);
			for (var i=0; i< empList.length; i++){
				id = empList[i++];
				name = empList[i];
				option = new Option(name, id);
				empSelector.options.add(option, null);				
			}				
		}
	}
}

function getEmp2(){	
	xhr = createXHR1();
	if( xhr == null ){
		alert("no xhr creted");
		return;
	}		   
	  
	xhr.onreadystatechange = updateEmp2;
	var deptId = document.getElementById("selectDept2").value;
	var titleId = document.getElementById("selectTitle2").value;
	xhr.open('Get', '/SCEC/employee/emp.do?action=listEmpByDeptAndTitle&deptId='+deptId+'&titleId='+titleId, true);
	xhr.send(null);
}

function showTable(tableData, elementId){
	
	$('#' + elementId).html('<table class="table table-striped" id="output"> </table>');
	var table = document.getElementById('output');
	var table1 = document.getElementById('output');
	var headerArr = tableData[0];
	var header = null, row = null, cell = null, rowData = null;
	
	//table1.style.border = "thin solid #0000FF";
	/*
	 * need keep order: 1: create tbody 2: create thead, otherwise the tbody will lost and row2.parentnode = thead
	 */
	var row2 = null;
	for (var i = 1; i < tableData.length; i++) {
		row2 = table.insertRow(-1);
		rowData = tableData[i]; 
		for (var j = 0; j < rowData.length; j++) {
			cell = row2.insertCell(j);
			cell.innerHTML = rowData[j];
		}
	}
	/*
	 * create header
	 */
	header = table.createTHead();
	row = header.insertRow(0);
	for (var i = 0; i < headerArr.length; i++) {
		cell = row.insertCell(i);
		cell.innerHTML = headerArr[i];
	}
	
	//TODO:tablesorter has no effect on dynamically created table
    //$("#output").tablesorter({widgets: ['zebra']});
}

function showChart(){
	//alert("readyState=" + xhr2.readyState);
	
	if(xhr2.readyState == 4) {
		if(xhr2.status == 200){
			var allEmp = xhr2.responseText;
			var empList = JSON.parse(allEmp);
			var chartType;
			var emp1Name = empList[2], emp2Name = empList[5];
			var numOfCat = Object.size(empList[0]);
			var tableData = new Array(3);
			var cat = new Array(numOfCat);
			var companyAvg = empList[7]
			
			for (var i = 0; i < numOfCat; i++) {
				cat[i] = empList[0][i+1];
			}
			
			//tableData[0] = ['員工', cat[0], cat[1], cat[2], cat[3]];
			tableData[0] = new Array(numOfCat + 1);
			tableData[1] = new Array(numOfCat + 1);
			tableData[2] = new Array(numOfCat + 1);
			for (var i = 0; i < numOfCat; i++) {
				tableData[0][i + 1] = cat[i];
				tableData[1][i + 1] = empList[3][i];
				tableData[2][i + 1] = empList[6][i];
			}
			tableData[0][0] = "員工";
			tableData[1][0] = empList[2];
			tableData[2][0] = empList[5];
			//tableData[1] = [empList[2], empList[3][0], empList[3][1], empList[3][2], empList[3][3]];
			//tableData[2] = [empList[5], empList[6][0], empList[6][1], empList[6][2], empList[6][3]];
			showTable(tableData, 'table');	
			
			chartType = getChartType();
			if (chartType == 'radar') {
				var radarChartData1 = [];
				//var fillColor1 = 'rgba(220,220,250,0.5)';
				//var fillColor2 = 'rgba(220,220,220,0.5)';
				var fillColor1 = 'rgba(255,0,0,0.5)';
				var fillColor2 = 'rgba(0,0,255,0.5)';
				
				
				//radarChartData1[0] = {labels : [], datasets : []};
				radarChartData1 = {labels : new Array(numOfCat), datasets : new Array(2)};
				for (var i = 0; i < numOfCat; i++) {
					radarChartData1.labels[i] = cat[i];
				}
				
				//emp1
				radarChartData1.datasets[0] = {};
				radarChartData1.datasets[0].data = new Array(numOfCat);
				radarChartData1.datasets[0].fillColor = fillColor1;
				radarChartData1.datasets[0].strokeColor = "rgba(220,220,250,1)";
				radarChartData1.datasets[0].pointColor = "rgba(220,220,250,1)";
				radarChartData1.datasets[0].pointStrokeColor = "#fff";
				for (var i = 0; i < numOfCat; i++) {
					radarChartData1.datasets[0].data[i] = empList[3][i];
				}
				
				//emp2
				radarChartData1.datasets[1] = {};
				radarChartData1.datasets[1].data = new Array(4);
				radarChartData1.datasets[1].fillColor = fillColor2; 
				radarChartData1.datasets[1].strokeColor = "rgba(220,220,220,1)";
				radarChartData1.datasets[1].pointColor = "rgba(220,220,220,1)";
				radarChartData1.datasets[1].pointStrokeColor = "#fff";
				for (var i = 0; i < numOfCat; i++) {
					radarChartData1.datasets[1].data[i] = empList[6][i];
				}
				
				//showTable(empList);
				$('.secondary').html('<canvas id="chart_canvas" height="450" width="450"></canvas> <div id="note"></div>');
				drawRadarChart(radarChartData1, "chart_canvas", 4);
				
				var box1 = '<div style="display:inline-block; background-color:' + fillColor1 + '; width:20px; height:20px"> </div>' + emp1Name;
				var box2 = '<div style="display:inline-block; background-color:' + fillColor2 + '; width:20px; height:20px"> </div>' + emp2Name;
				
				$('#note').html(box1 + '&nbsp; &nbsp;' + box2);
				//$('#note').html('Color1 :' + emp1 + '<br> Color2 :' + emp2);
					
			} else if (chartType == 'pie') {
				//no used
				var pieChartData = [];
				pieChartData[0] = [];
				pieChartData[1] = [];
				pieChartData[0][0] = ['職能類別', '平均'];
				pieChartData[0][1] = ['核心職能', empList[2][0]];
				pieChartData[0][2] = ['一般職能', empList[2][1]];
				pieChartData[0][3] = ['專業職能', empList[2][2]];
				pieChartData[0][4] = ['管理職能', empList[2][3]];
				
				$('.secondary').html('<div id="chart_div" height="450" width="450"></div>');
				drawPieChart(pieChartData[0], "chart_div");
				
			} else if (chartType == 'bar') {
				var barChartData = new Array(numOfCat + 1);
			
				barChartData[0] =  ['職能類別', emp1Name, emp2Name];
				for (var i = 0; i < numOfCat; i++) {
					barChartData[i + 1] =  [cat[i], empList[3][i], empList[6][i]];
				}
				
				$('.secondary').html('<div id="chart_div" height="450" width="450"></div>');
				drawBarChart(barChartData, "chart_div", 4);
			} else if (chartType == 'combo') {
				var comboChartData = new Array(numOfCat + 1);
				comboChartData[0] = ['職能類別', empList[2], empList[5], "公司平均"];
				for (var i = 0; i < numOfCat; i++) {
					comboChartData[i + 1] = [cat[i], empList[3][i], empList[6][i], companyAvg[i]];
				}
				
				$('.secondary').html('<div id="chart_div" height="450" width="450"></div>');
				drawComboChart(comboChartData, "chart_div", 4);
			}
		}
	}
}


function showChartByEmpId(){
	var empId1 = document.getElementById("selectEmp1").value;
	var empId2 = document.getElementById("selectEmp2").value;
	var empList = new Array();
	var url = '/SCEC/employee/emp.do';
	var selectChart = document.getElementById("selectChart");
	
	//empList.put(empId1);
	//empList.put(empId2);
	xhr2 = createXHR1();
	
	if( xhr2 == null ){
		alert("no xhr creted");
		return;
	}		   
	
	var requestData = "action=getOneEmpJSON&empId1=" + escape(empId1) + "&empId2=" + escape(empId2);
	  
	//xhr2.open('Get', '/SCEC/employee/emp.do?action=getOneEmpJSON&empId1='+empId1+'&empId2='+empId2, true);
	xhr2.open('POST', url, true);
	xhr2.onreadystatechange = showChart;
	xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr2.send(requestData);
}

function getChartType(){
	var selectChart = $('#selectChart input');
	var radio;
	var type = 'radar';
	
	for (var i = 0; i < selectChart.length; i++) {
		radio = selectChart[i];
		if (radio.checked == true) {
			type = radio.value;
			break;
		}
	}
	
	return type;
}

function updateSubject(){  
	if(xhr.readyState == 4){
		if(xhr.status == 200){
			var allSubject = xhr.responseText;
			var subjectList = JSON.parse(allSubject);
			var subjectSelector = document.getElementById("selectSubject");
			var option = new Option("--請選擇--", -1);
			subjectSelector.options.add(option, null);				
			var name;
			var id;
			subjectSelector.options.length = 0; 
			
			subjectSelector.options.add(option, null);
			for (var i=0; i< subjectList.length; i++){
				id = subjectList[i++];
				name = subjectList[i];
				option = new Option(name, id);
				subjectSelector.options.add(option, null);				
			}				
		}
	}
}

function getSubject() {
	xhr = createXHR1();
	if( xhr == null ){
		alert("no xhr creted");
		return;
	}		   
	  
	xhr.onreadystatechange = updateSubject;
	var catId = document.getElementById("selectCat").value;
	xhr.open('Get', '/SCEC/subject/subject.do?action=listByCategoryIdJSON&catId='+catId, true);
	xhr.send(null);
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};