<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/upload.css" />
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/jquery-ui-1.10.3.custom.js"> </script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/style.css" />

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap-fileupload/bootstrap-fileupload.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap-fileupload/bootstrap-fileupload.js"> </script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script>
	$(function() {
		$(".upload-tip").popover( {
			content : '請選擇職能盤點報表檔案, 支援格式: xls, xlsx', 
			trigger:'hover'
			}
		);	
	});
</script>

<title>上傳檔案</title>
</head>
<body>

<div class="wrapper">
	<div id="header">
		<%@include file="../menu.jsp" %>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="span7">
				<div class="hero-unit">
		            <h1>上傳報表一次搞定！</h1>
		            <br>
		            <p>請上傳職能盤點報表, 支援檔案格式：.xls、.xlsx</p>
		          	<p><a href="#" class="btn btn-info btn-large">了解更多...</a></p>
				</div> <!-- end hero-unit -->
			</div> <!-- end span7 -->

			<div class="span5">
				<h2> 上傳檔案</h2>
				<form id="upload_excel" class="form-inline" METHOD="post" ACTION="<%=request.getContextPath()%>/util/init.do" enctype="multipart/form-data">
					<!-- 
					<label class="upload-tip label label-info" for="">上傳 Excel 檔案</label>
					-->
					<div class="fileupload fileupload-new" data-provides="fileupload">
			  			<div class="upload-tip input-append">
			    			<div class="uneditable-input span3">
			    				<i class="icon-file fileupload-exists"></i> 
			    				<span class="fileupload-preview"></span>
			    			</div>
			    			<span class="btn btn-file">
			    				<span class="fileupload-new">選擇檔案</span>
			    				<span class="fileupload-exists">修改</span>
			    				<input type="file" name="excel"/>
			    			</span>
			    			<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">移除</a>
						</div>

						<div style="margin-top:10px">
							<button class="btn btn-primary btn-large" type="submit"><span class="span3">確認</span> </button>
						</div>
					</div>
				
					<input type="hidden" name="action" value="readExcel">
					<input type="hidden" name="requestURL" value="<%=request.getParameter("requestURL")%>">
				</form>
			</div> <!--  end of span5 -->
		</div> <!--  end of row -->

	
		<c:if test="${!empty errorMsgs}">
			<div class="row">
				<div class="span5 offset7">
					<div class="alert alert-error" style="visibility:visible">  
	  					<!--  <a class="close" data-dismiss="alert">×</a>  -->
	  					<strong>Error!</strong>. 
	  					<c:forEach var="msg" items="${errorMsgs}">
	  						${msg}
	  					</c:forEach>
					</div> 
				</div>
			</div>
		</c:if>
	</div> <!-- end container -->
</div> <!-- wrapper -->
		
<%@include file="../footer.jsp" %>

</body>
</html>