
function drawChart(dataArr, elementId) {
	var chartType = document.getElementById("chartType").selectedIndex;
	switch (chartType) {
	case 0:
		drawPieChart(dataArr, elementId);
		break;
	case 1:
		drawPieChart(dataArr, elementId);
		break;
	}
}

function initPieChartData(data, index, a, b) {
	//var num = parseInt(b);
	//var num = Number(b);
	//var num = new Number(b);
	//var num = num.valueOf();
	var num = +b;
	data[index] = [a, num];
}

function drawPieChart(dataArr, elementId) {
    var options = {
      title: '職能平均'
    };
    
    var data = google.visualization.arrayToDataTable(dataArr);
    var chart = new google.visualization.PieChart(document.getElementById(elementId));
        
    chart.draw(data, options);
}

function drawBarChart(dataArr, elementId, maxValue) {
	var options = {
		title: '職能平均',
		vAxis: {title: '職能類別',  titleTextStyle: {color: 'red'}, direction : 1, minValue: 0, maxValue : maxValue},
		hAxis: {minValue : 0, maxValue : maxValue}
    };

    var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
    
    var data = google.visualization.arrayToDataTable(dataArr);
    var chart = new google.visualization.BarChart(document.getElementById(elementId));
        
    chart.draw(data, options);
}

function drawComboChart(dataArr, elementId, maxValue) {
	var options = {
		title: '職能平均',
		vAxis: {title: '職能類別',  titleTextStyle: {color: 'red'}, minValue: 0, maxValue : maxValue},
    	seriesType:"bars",
    	series:{2:{type:"line"}}
    };

    var data = google.visualization.arrayToDataTable(dataArr);
    var chart = new google.visualization.ComboChart(document.getElementById(elementId));
        
    chart.draw(data, options);
}

function showChartByEmp(chartData, chartType, maxValue) {
	if (chartType == 'pie') {
		$('.secondary').html('<div id="chart_div" height="450" width="450"></div>');
		drawPieChart(chartData, "chart_div");
	} else if (chartType == 'bar') {
		$('.secondary').html('<div id="chart_div" height="450" width="450"></div>');
		drawBarChart(chartData, "chart_div", maxValue);
	} else if (chartType == 'radar') {
		$('.secondary').html('<canvas id="chart_canvas" height="450" width="450"></canvas> <div id="note"></div>');
		drawRadarChart(chartData, "chart_canvas", maxValue);
	}
}
