
function drawRadarChart(data, elementId, maxValue) {
	var myChart = document.getElementById(elementId);
	var ctx = myChart.getContext("2d");
	var chart = new Chart(ctx);
	var opt = {
			scaleSteps : maxValue,
			scaleStartValue : 0,
			scaleStepWidth : 1,
			scaleOverLay : true,
			scaleOverride : true,
			scaleShowLabels : true, 
			pointLabelFontSize : 14,
			scaleFontColor : "#000",
			scaleFontColor : "#000",
			scaleLineColor : "#000"
	};
	
	var myNewChart = chart.Radar(data, opt);		
	//var myRadar = new Chart(document.getElementById("canvas").getContext("2d")).Radar(data, {scaleShowLabels : true, pointLabelFontSize : 10});
}

