<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	Boolean uploadStatus = (Boolean)request.getAttribute("uploadStatus");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/jquery-ui-1.10.3.custom.js"> </script>

<style>
	#upload {
		margin-top:100px;
		margin-right:30%;
		margin-left:30%;
	}
</style>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap-fileupload/bootstrap-fileupload.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap-fileupload/bootstrap-fileupload.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<title>Insert title here</title>

</head>

<body>

<div id="wrapper">
	<div id="header">
		<%@include file="../menu.jsp" %>
	</div>
	
	<div class="container">
		<div id="upload">
		<c:if test="${uploadStatus==true}">
			讀取 Excel 檔成功
		</c:if>
		<c:if test="${uploadStatus==false}">
			讀取 Excel 檔失敗
		</c:if>
		</div>
	</div>
</div>

</body>
</html>