<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
		
		<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	<!--
            	<i class="icon-bar"></i>
            -->
          	</button>
          			
          	<!--
          	<a class="brand" href="#"> 盛全職能發展管理系統 </a>
          	-->
          	
          	<div class="nav-collapse collapse">
          		<ul class="nav">
          		<!--
          			<li><img style="margin-top:45px" src="images/homepage/CDMS.png"></li>
          			<li><img src="<%=request.getContextPath() %>/images/homepage/logo.png"></li>
          		-->
          			<li class="active">
          				<p class="brand" style="color:#2693F0; font-weight:900">職能發展管理系統 CDMS</p>
          			</li>
          			<!--
          			<li class="dropdown">
          				<a href="#" class="brand dropdown-toggle" data-toggle="dropdown"> 職能發展管理系統 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
							<li><a href="<%=request.getContextPath()%>/util/readExcel.jsp">關於盛全 </a> </li>
          				</ul>
          			<li>
          			-->
          			<li class="active"><a href="<%=request.getContextPath()%>/index.jsp"><i class="icon-home icon"></i>首頁</a><li>
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file icon"></i> 檔案 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
							<li><a href="<%=request.getContextPath()%>/util/readExcel.jsp"> <i class="icon-upload icon-black"></i> 匯入資料檔</a> </li>
							<li class="disabled"><a href="<%=request.getContextPath()%>/util/writeExcel.jsp"><i class="icon-download icon-black"></i> 匯出資料檔</a> </li>
          				</ul>
          			<li>
          			
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user icon"></i>員工 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
							<li class="disabled"><a href="<%=request.getContextPath()%>/employee/addEmp.jsp">新增員工</a> </li>
							<li class="disabled"><a href="#">管理員工資料</a> </li>
          				</ul>
          			<li>
          			
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 訓練 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
							<li class="disabled"><a href="#">新增訓練記錄</a> </li>
							<li class="disabled"><a href="#">管理訓練記錄</a> </li>
							<li class="disabled"><a href="#">訓練體系/地圖</a> </li>
							<li><a href="<%=request.getContextPath()%>/training/training.do?action=showUnqualified">辦訓科目分析</a> </li>
          				</ul>
          			<li>
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 職能 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
							<li class="disabled"><a href="#">新增職能</a> </li>
							<li class="disabled"><a href="#">管理職能</a> </li>
          				</ul>
          			<li>
          			
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-list icon"></i> 報表 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
		    				<li><a href="<%=request.getContextPath()%>/employee/emp.do?action=listAllEmp"> 職能總表 </a></li>
		    				<c:if test="${priv <= 4}">
		    					<li><a href="<%=request.getContextPath()%>/employee/selectSubjectAndGrade.jsp"> 科目報表 </a></li>
		    				</c:if>
		    				<li><a href="<%=request.getContextPath()%>/employee/emp.do?action=selectEmpAndGrade"> 個人報表 </a></li>
          				</ul>
          			<li>
          			
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-search icon"></i> 查詢 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
						    <li><a href="<%=request.getContextPath()%>/standard/standard.do?action=selectCategory"> 職能標準 </a></li>
				 			<li><a href="<%=request.getContextPath()%>/employee/emp.do?action=selectEmpShowPromotion"> 職務異動記錄 </a></li>
				 			<li class="divider"></li>
						    <li><a href="<%=request.getContextPath()%>/standard/selectTitleToShowStandard.jsp"> 個人職務異動差距分析 </a></li>
						    <li class="disabled"><a href="<%=request.getContextPath()%>/employee/listAllEmpPromotionDiff.jsp"> 群組職務異動差距分析 </a></li>
						    <li class="divider"></li>
						    <li><a href="<%=request.getContextPath()%>/dept/listAllDept.jsp"> 部門組織  </a></li>
							<li class="disabled"><a href="#">能力查詢</a> </li>
							<li class="disabled"><a href="#">工作/職務說明書</a> </li>
          				</ul>
          			<li>
          			
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 評比 <b class="caret"></b></a>
          				<ul class="dropdown-menu">
		    				<li><a href="<%=request.getContextPath()%>/employee/listAvgByCatIdDeptId.jsp"> 部門評比 </a></li>
		    				<c:if test="${priv < 4}">
		    					<li><a href="<%=request.getContextPath()%>/employee/emp.do?action=compareEmps"> 員工評比 </a></li>
		    				</c:if>
          				</ul>
          			<li>
          			
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 說明 <b class="caret"></b></a>
          			</li>
          		</ul>
          		
          		<ul class="nav pull-right">	
          			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-asterisk icon"></i> ${myEmpVO.empName} <b class="caret"></b></a>
          				<ul class="dropdown-menu">
		    				<li><a href="<%=request.getContextPath()%>/employee/emp.do?action=getEmpInfo"> 帳號設定 </a></li>
          				</ul>
          			<li>
				   	<li> <a href='<%=request.getContextPath()%>/login/login.do?action=Logout'> <i class="icon-off icon"></i> 登出 </a> </li>
          		</ul>
          	</div> <!-- nav-collapse -->
		</div> <!-- container -->
	</div> <!-- navbar-inner -->
</div> <!-- navbar -->