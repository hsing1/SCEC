<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.department.model.*"%>

<%  
// EmpService empSvc = new EmpService();
// List<EmpVO> list = empSvc.getAll();
// pageContext.setAttribute("list",list);
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="standSvc" scope="page" class="com.standard.model.StandardViewService" />

<html>
<head>
<title>快癡ヘだ猂</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/fancybox2.1.5/jquery.fancybox.css"/>
<script src="<%=request.getContextPath() %>/util/fancybox2.1.5/jquery.fancybox.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script type="text/javascript">
	$(function () {
    	$("#standTable").tablesorter({widgets: ['zebra']});
    	$(".showDetail").fancybox();
	});
</script>

</head>

<body>

<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<% 
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="span5">
				<ul class="breadcrumb">
					<li><a href="#">癡絤</a> <span class="divider">&gt;</span></li>
					<li class="active">快癡ヘ</li>
				</ul>
			</div>
		</div>
		<table id="standTable" class="tablesorter">
			<thead>
			<tr>
				<th>腹</th>
				<th>絪腹</th>
				<th>ヘ</th>
				<th>惠―计 </th>
				<th>キА</th>
				<th> L0计</th>
				<th> L1计</th>
				<th> L2计</th>
				<th> L3计</th>
				<th> L4计</th>
			</tr>
			</thead>
			
			<c:forEach var="map" items="${mapUnqualified}" varStatus="loop">
				<tr>
					<td>${loop.count}</td>
					<td>${map.key}</td>
					<td>${subjectMap[map.key]}</td>
					<td>
						<a class="showDetail fancybox.ajax" href="<%=request.getContextPath()%>/employee/emp.do?action=listUnqualifiedEmp&subjectId=${map.key}"> ${map.value}</a>
					</td>
					<td>
						${subjectMapAvg[map.key]}
					</td>
					<td>
						${mapL0[map.key]}
					</td>
					<td>
						${mapL1[map.key]}
					</td>
					<td>
						${mapL2[map.key]}
					</td>
					<td>
						${mapL3[map.key]}
					</td>
					<td>
						${mapL4[map.key]}
					</td>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of container -->
</div> <!--  end of wrapper -->

</body>
</html>
