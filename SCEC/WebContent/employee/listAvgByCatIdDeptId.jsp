<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeAbilityViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />
<jsp:useBean id="catSvc" scope="page" class="com.category.model.CategoryService" />
<jsp:useBean id="avgSvc" scope="page" class="com.ability.model.AvgService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />

<html>
<head>
<title>部門評比</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

</head>

<body>
<script type="text/javascript">
$(function () {
    $("#empTable").tablesorter({widgets: ['zebra']});
});

</script>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<%
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>

	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="span12">
				<ul class="breadcrumb">
					<li><a href="#">評比</a> <span class="divider">/</span></li>
					<li class="active">部門評比</li>
				</ul>
			</div>
		</div>
		
		<table id="empTable" class="tablesorter">
			<thead>
				<tr>
					<th>職能類別  </th>
					<c:forEach var="deptVO" items="${deptSvc.all}">
						<th> ${deptVO["name"]} </th>
					</c:forEach>
				</tr>
			</thead>
			
			<c:set var="avgMap" value="${avgSvc.avgByCatIdDeptId}"/>
			<c:forEach var="catVO" items="${catSvc.all}" varStatus="loop">
				<tr>
					<td> ${catVO.name}</td>
					<c:set var="avgList" value="${avgMap[loop.index+1]}" />
					<c:forEach var="avgVO" items="${avgList}">
						<td> ${avgVO["avg"]} </td>
					</c:forEach>
				</tr>
			</c:forEach>
			
			<tr>	
				<td>部門總平均</td>
				<c:set var="deptAvgList" value="${avgSvc.avgByDeptId}"/>
				<c:forEach var="avgVO" items="${deptAvgList}">
					<td> ${avgVO["avg"]} </td>
				</c:forEach>
			</tr>
		</table>
	</div>

	<c:if test="${debugMode != null}">
		<%=request.getRequestURI() %> <br>
		From : "${requestPath}"
	</c:if>
</div> <!-- end of wrapper -->
</body>
</html>
