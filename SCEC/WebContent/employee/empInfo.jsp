<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
Integer priv = (Integer)session.getAttribute("priv");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>


<!DOCTYPE HTML>
<html>
<head>
<title>帳戶管理</title></head>
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap_datepicker/css/datepicker.css" />
<script src="<%=request.getContextPath()%>/util/bootstrap_datepicker/js/bootstrap-datepicker.js"> </script>

<script>
	$(function() {
		$('.datepicker').datepicker( {format : "yyyy-mm-dd"});
	});
</script>


<body>
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp" %>
		<%
			included = true;
			request.setAttribute("included", included);
		}
		%>
	</div>

	<div class="container">
		<div class="row">
			<div class="span6 offset3">
				<form method="POST" action="<%=request.getContextPath()%>/employee/emp.do" class="form-horizontal">
            		<input type="hidden" name="action" value="updateEmpInfo">
					<fieldset>
						<legend> 帳戶管理 </legend>
						<div class="control-group">
							<label class="control-label" for="empId">工號</label>
							<div class="controls">
								<input type="text" name="empId" readOnly="true" value="${empVO.empId}">
								<span class="help-inline">
									<c:if test="${not empty empIdErrMsg}">
										${empIdErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="name">姓名</label>
							<div class="controls">
								<input type="text" name="name" readOnly="true" value="${empVO.empName}">
								<span class="help-inline">
									<c:if test="${not empty empNameErrMsg}">
										${empNameErrMsg}
									</c:if>
								</span>
								
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="dept">部門</label>
							<div class="controls">
								<input type="text" name="dept" readOnly="true" value="${empVO.deptName}">
								<span class="help-inline">
									<c:if test="${not empty empDeptErrMsg}">
										${empDeptErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="section">單位</label>
							<div class="controls">
								<c:choose>
									<c:when test="${not empty section}">
										<input type="text" name="section" readOnly="true" value="${section}">
									</c:when>
									<c:otherwise>
										<input type="text" name="section" readOnly="true" value="---">
									</c:otherwise>
								</c:choose>
								<c:if test="${not empty section}">
									<input type="text" name="section" readOnly="true" value="${section}">
								</c:if>
								<span class="help-inline">
									<c:if test="${not empty empSectionErrMsg}">
										${empSectionErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="title">職稱</label>
							<div class="controls">
								<input type="text" name="title" readOnly="true" value="${empVO.titleName}">
								<span class="help-inline">
									<c:if test="${not empty empTitleErrMsg}">
										${empTitleErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="password">密碼</label>
							<div class="controls">
								<input type="password" name="password" placeholder="存檔請輸入密碼">
								<span class="help-inline">
									<c:if test="${not empty errorPasswdMsgs}">
										<c:forEach var="msg" items="${errorPasswdMsgs}">
											${msg} <br>
										</c:forEach>
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="newPassword1">新密碼</label>
							<div class="controls">
								<input type="password" name="newPassword1" placeholder="更新密碼，請輸入新密碼">
								<span class="help-inline">
									<c:if test="${not empty newPasswd1Err}">
										<c:forEach var="msg" items="${newPasswd1Err}">
											${msg} <br>
										</c:forEach>
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="newPassword2">再次輸入新密碼</label>
							<div class="controls">
								<input type="password" name="newPassword2" placeholder="請輸入新密碼">
								<span class="help-inline">
									<c:if test="${not empty newPasswd2Err}">
										<c:forEach var="msg" items="${newPasswd2Err}">
											${msg} <br>
										</c:forEach>
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="form-actions">  
            				<button type="submit" class="btn btn-primary">儲存</button>  
            				<button class="btn">取消</button>  
          				</div>  
					</fieldset>
				</form>
			</div>
		</div>
	
	</div> <!-- container -->
</body>
</html>
