<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeAbilityViewService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />

<html>
<head>
<title></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

</head>

<body>
<script type="text/javascript">
$(function () {
    $("#empTable").tablesorter({widgets: ['zebra']});
});

</script>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<%
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>

	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="primary">
		<table id="empTable" class="tablesorter">
			<caption><span class="label label-info">查詢結果</span></caption>
			<thead>
			<tr>
				<th> 科目 </th>
				<th> 類別 </th>
				<th> 項目 </th>
				<th> 標準 </th>
				<th> 應備 </th>
				<th> 同職位平均 </th>
				<th> 同職位平均差異 </th>
				<th> 部門平均 </th>
				<th> 部門平均差異 </th>
				<th> 公司平均 </th>
				<th> 公司平均差異 </th>
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${listSubjectByEmpAndGrade}">
				<tr>
					<td> ${empVO.subjectName} </td>
					<td> ${empVO.categoryName} </td>
					<td> ${empVO.className} </td>
					<td> ${empVO.standard} </td>
					
					<c:choose>
						<c:when test="${empVO.requiredGrade < empVO.standard}" >
							<td class="alert"> ${empVO.requiredGrade} &nbsp;</td>
						</c:when>
						<c:otherwise>
							<td> ${empVO.requiredGrade}</td>
						</c:otherwise>
					</c:choose>
					
					<td>
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.titleAvg}"/>
					</td>
					<td>
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.titleAvg}"/>
					</td>
					<td>
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.deptAvg}"/>
					</td>
					<td> 
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.deptAvg}"/>
					</td>
					<td> ${empVO.companyAvg}</td>
					<td> 
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.companyAvg}"/>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of primary -->

	<c:if test="${debugMode != null}">
		<%=request.getRequestURI() %> <br>
		From : "${requestPath}"
	</c:if>
</div> <!-- end of wrapper -->
</body>
<script>
	$('.alert').css({
		'background-color' : 'yellow'
	});
</script>
</html>
