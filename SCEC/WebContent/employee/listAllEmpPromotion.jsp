<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
int empIdx = 0;
%>
<jsp:useBean id="proSvc" scope="page" class="com.promotion.model.PromotionViewService" />

<html>
<head>
<title></title>
<!-- Table sorter -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap_datepicker/css/datepicker.css" />
<script src="<%=request.getContextPath()%>/util/bootstrap_datepicker/js/bootstrap-datepicker.js"> </script>

<script type="text/javascript">
	var pieChartData = [], countEmp = 0, countCat = 0;
	var barChartData = [];
	for (var i = 0; i < 10; i++) {
		pieChartData[i] = [];
		barChartData[i] = [];
	}
    
	$(function () {
    	$("#proTable").tablesorter({widgets: ['zebra']});
	});
	
	// require access internet, place in the last section, due to it will block subsequence function
    google.load("visualization", "1", {packages:["corechart"]});
</script>

<body>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<% 
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>
	
	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="primary">
		<table id="proTable" class="tablesorter">
			<caption><span class="label label-info">異動記錄</span></caption>
			<thead>
			<tr>
				<th> 工號 </th>
				<th> 員工姓名 </th>
				<th> 廠區/事業單位 </th>
				<th> 部門  </th>
				<th> 職稱  </th>
				<th> 異動日期  </th>
			</tr>
			</thead>
			
			<c:forEach var="proVO" items="${listAllEmpPromotion}">
				<tr>
					<td> ${proVO.empId} </td>
					<td> ${proVO.empName} </td>
					<td> ${proVO.devision} </td>
					<td> ${proVO.deptName} </td>
					<td> ${proVO.titleName} </td>
					<td> ${proVO.promotionDate}</td>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of primary -->
	
	<div id="chart_div" style="width: 900px; height: 500px;"></div>
	
	<c:if test="${debugMode != null}">
		employee/listAllEmpPromotion.jsp
	</c:if>
</div> <!-- end of wrapper -->
</body>
</html>
