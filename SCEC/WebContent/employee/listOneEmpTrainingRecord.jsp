<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeTrainingViewService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />

<html>
<head>
<title></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

</head>

<body>
<script type="text/javascript">
$(function () {
    $("#empTable").tablesorter({widgets: ['zebra']});
});

</script>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<%
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>
	
	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="primary">
		<table id="empTable" class="tablesorter">
			<thead>
			<tr>
				<th> 訓練科目 </th>
				<th> 訓練日期 </th>
				<th> 訓練時數 </th>
				<th> 訓練費用 </th>
				<th> 訓練方式 </th>
				<th> 轉訓日期 </th>
				<th> 服務保證 </th>
				<th> 服務期限 </th>
				<th> 賠償條件 </th>
				<th> 備註 </th>
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${listOneEmpTraining}">
				<tr>
					<td> ${empVO.subjectName} </td>
					<td> ${empVO.trainingDate}</td>
					<td> ${empVO.trainingHour}</td>
					<td> ${empVO.fee}</td>
					<c:if test="${empVO.trainingType==1}">
						<td> 外部訓練 </td>
					</c:if>
					<c:if test="${empVO.trainingType==0}">
						<td> 內部訓練 </td>
					</c:if>
					<td> ${empVO.transfer}</td>
					<td> ${empVO.guarantee}</td>
					<td> ${empVO.guarantee}</td>
					<td> ${empVO.compensate}</td>
					<td> ${empVO.note}</td>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of primary -->

	<c:if test="${debugMode != null}">
		employee/listOneEmpTrainingRecord.jsp <br>
	</c:if>
</div> <!-- end of wrapper -->
</body>
</html>
