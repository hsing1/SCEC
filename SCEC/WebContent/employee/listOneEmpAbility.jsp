<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeAbilityViewService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />

<html>
<head>
<title></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

</head>

<body>
<script type="text/javascript">
$(function () {
    $("#empTable").tablesorter({widgets: ['zebra']});
});

</script>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<%
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>

	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="container">
		<div class="row">
			<div class="span5">
				<ul class="breadcrumb">
					<li><a href="#">報表</a> <span class="divider">&gt;</span></li>
					<li><a href="<%=request.getContextPath()%>/employee/emp.do?action=listAllEmp">職能總表</a> <span class="divider">&gt;</span></li>
					<li class="active">職能列表 <span class="divider">&gt;</span></li>
					<li class="active">${empName}</li>
				</ul>
			</div>
		</div>
		<!-- 
	<div class="primary">
	-->
		<table id="empTable" class="tablesorter">
			<caption><span class="label label-info">職能列表</span></caption>
			<thead>
			<tr>
				<th> 序號 &nbsp; </th>
				<th> 編號 &nbsp; </th>
				<th> 科目 &nbsp;</th>
				<th> 類別 &nbsp;</th>
				<th> 項目 &nbsp;</th>
				<th> 標準 &nbsp; </th>
				<th> 應備 &nbsp; </th>
				<th> 具備 &nbsp;</th>
				<th> 同職位平均(應備) </th>
				<th> 同職位平均差異(應備) </th>
				<th> 部門平均(應備) </th>
				<th> 部門平均差異(應備) </th>
				<th> 公司平均(應備) </th>
				<th> 公司平均差異(應備) </th>
				<th> 同職位平均(具備) </th>
				<th> 部門平均(具備) </th>
				<th> 公司平均(具備) </th>
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${listOneEmpAbility}" varStatus="loop">
				<tr>
					<td> ${loop.count} </td>
					<td> ${empVO.subjectId} </td>
					<td> ${empVO.subjectName} </td>
					<td> ${empVO.categoryName} </td>
					<td> ${empVO.className} </td>
					
					<c:choose>
						<c:when test="${empVO.standard <= 0}">
							<td> ----- </td>
						</c:when>
						<c:otherwise>
							<td> ${empVO.standard}</td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when test="${empVO.required == true}">
							<c:choose>
								<c:when test="${empVO.requiredGrade < empVO.deptAvg}" >
									<td class="alert"> ${empVO.requiredGrade}</td>
								</c:when>
								<c:otherwise>
									<td> ${empVO.requiredGrade}</td>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td> ----- </td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when test="${empVO.required == true}">
							<td> ----- </td>
						</c:when>
						<c:otherwise>
							<td> ${empVO.grade}</td>
						</c:otherwise>
					</c:choose>
					
					<td class="titleAvg">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.titleAvg}"/>
					</td>
					
					<td class="titleAvg">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.titleAvg}"/>
					</td>
					
					<td class="deptAvg">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.deptAvg}"/>
					</td>
					
					<!-- 部門應備差異 低於鰾準標黃色-->
					<c:choose>
						<c:when test="${(empVO.requiredGrade - empVO.deptAvg) < 0}">
							<c:choose>
								<c:when test="${empVO.required == true}">
									<td class="deptAvg alertDiff">
										<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.deptAvg}"/>
									</td>
								</c:when>
								<c:otherwise>
									<td class="deptAvg">
										<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.deptAvg}"/>
									</td>
								</c:otherwise>
						    </c:choose>
						</c:when>
						<c:otherwise>
							<td class="deptAvg">
								<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.deptAvg}"/>
							</td>
						</c:otherwise>
					</c:choose>
					
					<td class="companyAvg">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.companyAvg}"/>
					</td>
					
					<td class="companyAvg">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.companyAvg}"/>
					</td>
					
					<td>
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.titleAvgD}"/>
					</td>
					
					<td class="titleAvgD">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.deptAvgD}"/>
					</td>
					
					<td class="">
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.companyAvgD}"/>
					</td>
				</tr>
			</c:forEach>
		</table>
		<!--
	</div>  end of primary 
	-->
	</div> <!-- end of container -->
	<c:if test="${debugMode != null}">
		<%=request.getRequestURI() %> <br>
		From : "${requestPath}"
	</c:if>
</div> <!-- end of wrapper -->
</body>
<script>
	$('.alert').css({
		'background-color' : 'yellow'
	});
	
	$('.alertDiff').css({
		'background-color' : 'lightgreen'
	});
	
	$('.titleAvg').css({
		'background-color' : '#E5E5E5'
	})
	
	$('.companyAvg').css({
		'background-color' : '#E5E5E5'
	})
</script>
</html>
