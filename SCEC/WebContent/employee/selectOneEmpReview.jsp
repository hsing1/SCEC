<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>

<title></title>
</head>

<body>
	<div class="wrapper">
		<div class="header">
			<% if (included == null) { %>
				<%@include file="../menu.jsp"%>
			<% 
				included = true;
				request.setAttribute("included", included);	
			}
			%>
		</div>
		<div class="primary">
			<div class="formWrapper">
				<form method = "post" action = "<%=request.getContextPath()%>/employee/emp.do">
					<fieldset>
			　			<legend>職能評量</legend>
			       		<div class='line'>
							<div class='leftDiv'>員工 : </div>
							<div class="rightDiv">
						    	<label for="">部門</label>
						    	<select id="selectDept1" name="selectDept1" onchange="getTitle1()">
									<option value="-1">請選擇</option>
									<c:forEach var="deptVO" items="${deptSvc.all}">	
							 			<option value="${deptVO.deptId}"> ${deptVO.name} </option>                  
						     		</c:forEach>   
						    	</select>
						    	&nbsp;	
						    	<label for="">職稱</label>
						    	<select id="selectTitle1" name="selectTitle1" onchange="getEmp1()">
									<option value="-1">請選擇</option>
						    	</select>
						    	
						    	&nbsp;	
						    	<label for="">員工</label>
						    	<select id="selectEmp1" name="selectEmp1">
									<option value="-1">請選擇</option>
						    	</select>
						    </div>
						</div> 
						
					</fieldset>
					<input type="submit" value = "確認">
					<input type="hidden" name = "action" value = "listOneEmpReview">
				</form>
			</div> <!-- end of formWrapper -->
		</div> <!-- end of primary -->
		
		<div class="secondary">
			<%if (request.getAttribute("listOneEmpReview") != null) {%>
				<jsp:include page="/employee/listOneEmpReview.jsp"/>
			<%} %>
		</div>
	</div> <!-- end of wrapper -->

</body>
</html>