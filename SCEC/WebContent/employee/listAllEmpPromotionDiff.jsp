<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
int empIdx = 0;
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />
<jsp:useBean id="catSvc" scope="page" class="com.category.model.CategoryService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />
<jsp:useBean id="titleSvc" scope="page" class="com.title.model.TitleService" />

<html>
<head>
<title></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script type="text/javascript">
	var xhr = null;
	var pieChartData = [], countEmp = 0, countCat = 0;
	var barChartData = [];
	for (var i = 0; i < 10; i++) {
		pieChartData[i] = [];
		barChartData[i] = [];
	}
    
	$(function () {
    	$("#empTable").tablesorter({widgets: ['zebra']});
	});
	
	function createXHR() {
		var xhr1 = XMLHttpRequest();
		return xhr1;
	}
	
	function showDiffResult() {
		alert("aaa");
	}
	
	function showAbilityDiff(newDeptId, newTitleId) {
		//var obj = document.getElementById(titleId);
		xhr = createXHR();	
		debugger;
		if (xhr == null) {
			alert("no xhr created");
			return;
		}
		xhr.onreadystatechange = showDiffResult;
		xhr.open('GET', '/SCEC/employee/emp.do?action=listAbilityDiff&newDeptId='+newDeptId+'&newTitleId='+newTitleId, true);
		xhr.send(null);
		
	}
	
    google.load("visualization", "1", {packages:["corechart"]});
</script>

<body>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<% 
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>
	
	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="container">
		<table id="empTable" class="tablesorter">
			<thead>
			<tr>
				<th> 工號 </th>
				<th> 員工姓名 </th>
				<th> 部門  </th>
				<th> 職稱  </th>
				<th> 異動部門 </th>
				<th> 異動職位 </th>
				<th> 查詢職能差距 </th>
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${empSvc.all}">
				<tr>
					<td> ${empVO.empId} </td>
					<td> ${empVO.empName} </td>
					<td> ${empVO.deptName} </td>
					<td> ${empVO.titleName} </td>
					<td id="${empVO.empId}td">
						<select id="${empVO.empId}newDept" name="selectDept" onchange="getTitle()">
							<option value=-1> 請選擇 </option>
							<c:forEach var="deptVO" items="${deptSvc.all}">
								<option value="${deptVO.deptId}"> ${deptVO.name} </option>
							</c:forEach>
						</select>
					</td>
					<td> 
						<select id="${empVO.empId}newTitle" name="selectTitle">
							<option value=-1> 請選擇 </option>
							<c:forEach var="titleVO" items="${titleSvc.all}">
								<option value="${titleVO.titleId}"> ${titleVO.name} </option>
							</c:forEach>
						</select>
					</td>
					<td>
						<button onclick="showAbilityDiff('${emoVO.empId}', '${empVO.empId}newDept', '${empVO.empId}newTitle')">查詢</button>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of primary -->
	
	<div id="chart_div" style="width: 900px; height: 500px;"></div>
	
	<c:if test="${debugMode != null}">
		employee/listAllEmp.jsp
	</c:if>
</div> <!-- end of wrapper -->
</body>
</html>
