<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
Integer priv = (Integer)session.getAttribute("priv");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>


<!DOCTYPE HTML>
<html>
<head>
<title>新增員工</title></head>
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap_datepicker/css/datepicker.css" />
<script src="<%=request.getContextPath()%>/util/bootstrap_datepicker/js/bootstrap-datepicker.js"> </script>

<script>
	var elementObj = {
		deptElemId : "selectDept", 
		sectionElemId : "selectSection"
	};
	
	$(function() {
		$('.datepicker').datepicker( {format : "yyyy-mm-dd"});
	});
</script>

<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />
<jsp:useBean id="devisionSvc" scope="page" class="com.devision.model.DevisionService" />

<body>
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp" %>
		<%
			included = true;
			request.setAttribute("included", included);
		}
		%>
	</div>

	<div class="container">
		<div class="row">
			<div class="span6 offset3">
				<form method="POST" action="<%=request.getContextPath()%>/employee/emp.do" class="form-horizontal">
            		<input type="hidden" name="action" value="addEmp">
					<fieldset>
						<legend> 新增員工 </legend>
						<div class="control-group">
							<label class="control-label" for="empId">工號</label>
							<div class="controls">
								<input type="text" name="empId">
								<span class="help-inline">
									<c:if test="${not empty empIdErrMsg}">
										${empIdErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="name">姓名</label>
							<div class="controls">
								<input type="text" name="name">
								<span class="help-inline">
									<c:if test="${not empty empNameErrMsg}">
										${empNameErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="devision">廠區/事業單位</label>
							<div class="controls">
								<select name="devision" onchange="getDept()">
									<option value="-1"> --- 請選擇 --- </option>
									<c:forEach var="devisionVO" items="${devisonSvc.all}">
										<option value="${devisionVO.devisionId}"> ${devisionVO.name} </option>
									</c:forEach>
								</select>
								<span class="help-inline">
									<c:if test="${not empty empDevisionErrMsg}">
										${empDevisionErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="dept">部門</label>
							<div class="controls">
								<select id="selectDept" name="dept" onchange="getSection('selectDept')">
									<option value="-1"> --- 請選擇 --- </option>
									<c:forEach var="deptVO" items="${deptSvc.all}">
										<option value="${deptVO.deptId}"> ${deptVO.name} </option>
									</c:forEach>
								</select>
								<span class="help-inline">
									<c:if test="${not empty empDeptErrMsg}">
										${empDeptErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="section">單位</label>
							<div class="controls">
								<select id="selectSection" name="section" onchange="getTitle('/SCEC/dept/dept.do', 'getTitleByDeptAndSec', elementObj, updateTitle)">
									<option value="-1"> --- 請選擇 --- </option>
								</select>
								<span class="help-inline">
									<c:if test="${not empty sectionErrMsg}">
										${empSectionErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="title">職稱</label>
							<div class="controls">
								<select id="selectTitle" name="selectedTitle" onchange="updateSalaryLevel()">
									<option value="-1"> --- 請選擇 --- </option>
								</select>
								<span class="help-inline">
									<c:if test="${not empty empTitleErrMsg}">
										${empTitleErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="salaryLevel">薪資等級</label>
							<div class="controls">
								<select name="dept" onchange="updateSection()">
									<option value="-1"> --- 請選擇 --- </option>
								</select>
								<span class="help-inline">
									<c:if test="${not empty empDeptErrMsg}">
										${empDeptErrMsg}
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="identity">身份證字號</label>
							<div class="controls">
								<input type="text" name="identity" placeholder="輸入身份證字號">
								<span class="help-inline">
									<c:if test="${not empty errorIdentityMsgs}">
										<c:forEach var="msg" items="${errorIdentityMsgs}">
											${msg} <br>
										</c:forEach>
									</c:if>
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="onboard">到職日期</label>
							<div class="controls">
								<div class="input-append date" id="dp3" data-date="2012-12-02" data-date-format="yyyy-mm-dd">
							    	<label for=""><span class="label label-inverse">日期</span></label>
									<input class="span2 datepicker" name="onboard" size="20" type="text">
								</div>
							</div>
						</div>	
						
						<div class="control-group">
							<label class="control-label" for="resign">離職日期</label>
							<div class="controls">
								<div class="input-append date" id="dp3" data-date="2012-12-02" data-date-format="yyyy-mm-dd">
							    	<label for=""><span class="label label-inverse">日期</span></label>
									<input class="span2 datepicker" name="resign" size="20" type="text">
								</div>
							</div>
						</div>	
								
						<div class="form-actions">  
            				<button type="submit" class="btn btn-primary">儲存</button>  
            				<button class="btn">取消</button>  
          				</div>  
					</fieldset>
				</form>
			</div>
		</div>
	
	</div> <!-- container -->
</body>
</html>
