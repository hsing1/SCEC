<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ page import="java.util.*"%> 
<%@ page import="com.employee.model.*"%> 
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
Integer priv = (Integer)session.getAttribute("priv");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>

<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />
-->
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/style.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap_datepicker/css/datepicker.css" />
<script src="<%=request.getContextPath()%>/util/bootstrap_datepicker/js/bootstrap-datepicker.js"> </script>

<script>
	$(function() {
		$('.datepicker').datepicker( {format : "yyyy-mm-dd"});
	});
</script>

<title></title>
</head>

<body>
	<div class="wrapper">
		<div class="header">
			<% if (included == null) { %>
				<%@include file="../menu.jsp"%>
			<% 
				included = true;
				request.setAttribute("included", included);	
			}
			%>
		</div>
		<div class="container">
			<div class="push"></div>
			<div class="row">
				<div class="span20">
					<ul class="breadcrumb">
						<li><a href="#">查詢</a> <span class="divider">&gt;</span></li>
						<li class="active">職務異動記錄</li>
					</ul>
				</div>
			</div>
			<div class="formWrapper">
			<c:choose>
				<c:when test="${priv == 1 || priv == 2}">
					<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
						<fieldset>
				　			<legend><span class="label label-info">選擇查詢範圍</span></legend>
							<br>
							<label for=""><span class="label label-inverse">部門</span></label>
							<br>
							<select id="selectDept1" name="selectDept1" multiple="multiple" onchange="getTitle1Multi()">
								<option value="-1">--請選擇--</option>
								<c:forEach var="deptVO" items="${deptSvc.all}">
									<c:choose>
										<c:when test="${deptVO.deptId == currentDept}">
											<option value="${deptVO.deptId}" selected> ${deptVO.name} </option>                  
										</c:when>
										<c:otherwise>
											<option value="${deptVO.deptId}"> ${deptVO.name} </option>                  
										</c:otherwise>
									</c:choose>
								</c:forEach>   
							</select>
							<br><br>
							    	
							<label for=""><span class="label label-inverse">職稱</span></label>
							<br>
							<select id="selectTitle1" name="selectTitle1" multiple="multiple" onchange="getEmp1Multi()">
								<option value="-1">--請選擇--</option>
								<c:forEach var="titleVO" items="${titleList}">
									<c:choose>
										<c:when test="${titleVO.titleId == currentTitle}">
							 				<option value="${titleVO.titleId}" selected> ${titleVO.name} </option>                  
							 			</c:when>
							 			<c:otherwise>
							 				<option value="${titleVO.titleId}"> ${titleVO.name} </option>                  
							 			</c:otherwise>
							 		</c:choose>
								</c:forEach>
							</select>
							    	
						  	<br><br>
							<label for=""><span class="label label-inverse">姓名</span></label>
							<br>
							<select id="selectEmp1" name="selectEmp1" multiple="multiple">
								<option value="-1">--請選擇--</option>
									<c:forEach var="empViewVO" items="${empViewList}">
										<c:choose>
											<c:when test="${empViewVO.empId == currentEmp}">
								 				<option value="${empViewVO.empId}" selected> ${empViewVO.empName} </option>                  
								 			</c:when>
								 			<c:otherwise>
								 				<option value="${empViewVO.empId}"> ${empViewVO.empName} </option>                  
								 			</c:otherwise>
								 		</c:choose>
									</c:forEach>
							</select>
							<br><br>
							    	
							<div class="input-append date" id="dp3" data-date="2012-12-02" data-date-format="yyyy-mm-dd">
							   	<label for=""><span class="label label-inverse">日期</span></label>
								<br>
								<input class="span2 datepicker" name="startDate" value="${startDate}" size="20" type="text">
								<span class="add-on"> 之前 </span>
									<!-- 
									<span class="add-on"><i class="icon-th"></i></span>
									-->
							</div>
							    	
							<br><br>
							    	
							<button type="submit" class="btn btn-success">查詢 <i class="icon-play icon-white"></i></button>
							<input type="hidden" name="action" value="listAllEmpPromotion">
						</fieldset>
					</form>
				</c:when> <%-- end of priv = 1, 2 --%>
				
				<c:when test="${priv == 3}">
					<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
						<fieldset>
				　			<legend><span class="label label-info">選擇查詢範圍</span></legend>
							<br>
							<label for=""><span class="label label-inverse">部門</span></label>
							<br>
							<select id="selectDept1" name="selectDept1">
							   	<%-- 查詢 自己或自己的部門--%>
								<option value="${myEmpVO.deptId}"> ${myEmpVO.deptName} </option>                  
							</select>
							<br><br>
							    	
							<label for=""><span class="label label-inverse">職稱</span></label>
							<br>
							<select id="selectTitle1" name="selectTitle1" multiple="multiple" onchange="getEmp1Multi()">
								<option value="-1">--請選擇--</option>
								<c:forEach var="titleVO" items="${titleList}">
									<c:choose>
										<c:when test="${titleVO.titleId == currentTitle}">
											<option value="${titleVO.titleId}" selected> ${titleVO.name} </option>                  
										</c:when>
										<c:otherwise>
											<option value="${titleVO.titleId}"> ${titleVO.name} </option>                  
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
							    	
							<br><br>
							<label for=""><span class="label label-inverse">姓名</span></label>
							<br>
						   	<select id="selectEmp1" name="selectEmp1" multiple="multiple">
								<option value="-1">--請選擇--</option>
								<c:forEach var="empViewVO" items="${empViewList}">
									<c:choose>
										<c:when test="${empViewVO.empId == currentEmp}">
						 					<option value="${empViewVO.empId}" selected> ${empViewVO.empName} </option>                  
						 				</c:when>
										<c:otherwise>
						 					<option value="${empViewVO.empId}"> ${empViewVO.empName} </option>                  
						 				</c:otherwise>
						 			</c:choose>
								</c:forEach>
						   	</select>
						    <br><br>	
						    
							<div class="input-append date" id="dp3" data-date="2012-12-02" data-date-format="yyyy-mm-dd">
							   	<label for=""><span class="label label-inverse">日期</span></label>
								<br>
								<input class="span2 datepicker" name="startDate" value="${startDate}" size="20" type="text">
								<span class="add-on"> 之前 </span>
									<!-- 
									<span class="add-on"><i class="icon-th"></i></span>
									-->
							</div>
							    	
							<br><br>
						   	<button type="submit" class="btn btn-success">查詢<i class="icon-play icon-white"></i></button>
						    <input type="hidden" name="action" value="listAllEmpPromotion">
						    <div id="selectedInfo"></div>
						</fieldset>
					</form>
				</c:when> <%-- end of priv = 3 --%>
				
				<%-- 查詢 自己--%>
				<c:when test="${priv == 4}">
					<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
						<fieldset>
				　			<legend><span class="label label-info">選擇查詢範圍</span></legend>
							<br>
							<label for=""><span class="label label-inverse">部門</span></label>
							<br>
							<select id="selectDept1" name="selectDept1">
								<option value="${myEmpVO.deptId}"> ${myEmpVO.deptName} </option>                  
							</select>
							    	
							<br><br>    	
							<label for=""><span class="label label-inverse">職稱</span></label>
							<br>
							<select id="selectTitle1" name="selectTitle1" onchange="getEmp1()">
								<option value="${myEmpVO.titleId}"> ${myEmpVO.titleName} </option>                  
							</select>
							    	
							<br><br>
							<label for=""><span class="label label-inverse">員工</span></label>
							<br>
							<select id="selectEmp1" name="selectEmp1">
								<option value="${myEmpVO.empId}"> ${myEmpVO.empName} </option>                  
							</select>
							<br><br>							    	
							
							<div class="input-append date" id="dp3" data-date="2012-12-02" data-date-format="yyyy-mm-dd">
							   	<label for=""><span class="label label-inverse">日期</span></label>
								<br>
								<input class="span2 datepicker" name="startDate" value="${startDate}" size="20" type="text">
								<span class="add-on"> 之前 </span>
									<!-- 
									<span class="add-on"><i class="icon-th"></i></span>
									-->
							</div>
							<br><br>
							<button type="submit" class="btn btn-success">查詢<i class="icon-play icon-white"></i></button>
							<input type="hidden" name="action" value="listAllEmpPromotion">
						</fieldset>
					</form>
				</c:when> <%-- end of priv = 4 --%>
			</c:choose>
			</div> <!-- end of formWrapper -->
			
			<div class="secondary">
				<%if (request.getAttribute("listAllEmpPromotion") != null) {%>
					<jsp:include page="/employee/listAllEmpPromotion.jsp"/>
				<%} %>
			</div>
		</div> <!-- end of container -->
		
	</div> <!-- end of wrapper -->
</body>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>
</html>