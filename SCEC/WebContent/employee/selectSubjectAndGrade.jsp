<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />
<jsp:useBean id="catSvc" scope="page" class="com.category.model.CategoryService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>


<title>Insert title here</title>
</head>

<body>
	<div class="wrapper">
		<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<%
			included = true;
			request.setAttribute("included", included);	
		}
		%>
		</div>
		
		<div class="container">
		<%
			//TODO: issue #60, breadcrumb lost after submit
		%>
			<div class="row">
				<div class="span20">
					<ul class="breadcrumb">
						<li><a href="#">報表</a> <span class="divider">&gt;</span></li>
						<li class="active">科目報表</li>
					</ul>
				</div>
			</div>
			<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
				<fieldset>
		　			<legend><span class="label label-info">科目報表查詢</span></legend>
					<br>
			    	<label for=""><span class="label label-inverse">職能類別</span></label>
			    	<br>
			    	<select id="selectCat" name="selectCat" onchange="getSubject()">
						<option value="-1">--請選擇--</option>
						<c:forEach var="catVO" items="${catSvc.all}">	
				 			<option value="${catVO.categoryId}"> ${catVO.name} </option>                  
			     		</c:forEach>   
			    	</select>
			    	
			    	<br>	
			    	<br>	
			    	<label for=""><span class="label label-inverse">科目</span></label>
			    	<br>
			    	<select id="selectSubject" name="selectSubject">
						<option value="-1">--請選擇--</option>
			    	</select>
			    	
			    	<br>
			    	<br>	
			    	<label for="selectCond"><span class="label label-inverse">比較條件</span></label>
			    	<br>
			    	<select class="input-small" id="selectCond" name="selectCond">
						<option value="-1">--請選擇--</option>
						<option value="gt"> &gt; </option>
						<option value="ge"> &gt;= </option>
						<option value="eq"> = </option>
						<option value="le"> &lt;= </option>
						<option value="lt"> &lt;  </option>
			    	</select>
			    	
			    	<br>
			    	<br>	
			    	<label for="grade"> <span class="label label-inverse">分數</span> </label>
			    	<br>
			    	<input class="input-small" type="text" name="grade" style="height:30px">
			    	
			    	<br>
			    	<br>
			    	<button type="submit" class="btn btn-success">查詢<i class="icon-play icon-white"></i></button>
			    	<input type="hidden" name="action" value="listEmpBySubjectAndGrade">
				</fieldset>
			</form>
			
			<div class="secondary">
				<%if (request.getAttribute("listEmpBySubjectAndGrade") != null) {%>
					<jsp:include page="/employee/listEmpBySubjectAndGrade.jsp"/>
				<%} %>
			</div>
		</div> <!-- end of primary -->
			
	</div> <!-- end of wrapper -->
</body>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>
</html>