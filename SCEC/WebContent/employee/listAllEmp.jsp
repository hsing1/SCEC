<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
int empIdx = 0;
if (debugMode != null) {
	ArrayList<EmployeeViewVO> list = (ArrayList<EmployeeViewVO>) request.getAttribute("listAllEmp");
	float [] arr;
	for (EmployeeViewVO empVO : list) {
		System.out.printf("empId:%d   ", empVO.getEmpId());
		arr = empVO.getAverage();
		System.out.printf("%f:%f:%f:%f\n", arr[0], arr[1], arr[2], arr[3]);
	}
}
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />
<jsp:useBean id="catSvc" scope="page" class="com.category.model.CategoryService" />

<html>
<head>
<title></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<!--
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
-->

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/fancybox2.1.5/jquery.fancybox.css"/>
<script src="<%=request.getContextPath() %>/util/fancybox2.1.5/jquery.fancybox.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script>
	var pieChartData = [], countEmp = 0, countCat = 0;
	var barChartData = [];
	var radarChartData = [];
	var empId = 0, maxValue = 4; //TODO:fix hardcode
	
		var radarChartData1 = [];
		//radarChartData1[0] = {labels : [], datasets : []};
		radarChartData1[0] = {labels : new Array(4), datasets : new Array(1)};
		radarChartData1[0].datasets[0] = {};
		radarChartData1[0].datasets[0].data = new Array(4);
		radarChartData1[0].labels[0] = "核心職能";
		radarChartData1[0].labels[1] = "一般職能";
		radarChartData1[0].labels[2] = "專業職能";
		radarChartData1[0].labels[3] = "共同職能";
		radarChartData1[0].datasets[0].fillColor = "rgba(220,220,220,0.5)";
		radarChartData1[0].datasets[0].strokeColor = "rgba(220,220,220,1)";
		radarChartData1[0].datasets[0].pointColor = "rgba(220,220,220,1)";
		radarChartData1[0].datasets[0].pointStrokeColor = "#fff";
		radarChartData1[0].datasets[0].data[0] = 3.5;
		radarChartData1[0].datasets[0].data[1] = 3.1;
		radarChartData1[0].datasets[0].data[2] = 2.5;
		radarChartData1[0].datasets[0].data[3] = 4.5;
	
	for (var i = 0; i < 2000; i++) {
		pieChartData[i] = [];
		barChartData[i] = [];
	 	radarChartData[i] = {labels : [], datasets : []};
	 	radarChartData[i].datasets[0] = {};
	 	radarChartData[i].datasets[0].data = [];
	}
	
	var tb = $("#empTable");
	$(function () {
    	$("#empTable").tablesorter({widgets: ['zebra']});
	});
	
	jQuery(function($) {
		$(".doPieChart").on("click", function() {
			//var empIdx = $(this).closest('tr').find('td:first').text();
			var empIdx = $(this).closest('tr').find('td.empId').text();
			empIdx = parseInt(empIdx);
			showChartByEmp(pieChartData[empIdx], 'pie', maxValue);
			$.fancybox("#chart_div");	
		}); //on click
		
		$(".doBarChart").on("click", function() {
			//var empIdx = $(this).closest('tr').find('td:first').text();
			var empIdx = $(this).closest('tr').find('td.empId').text();
			empIdx = parseInt(empIdx);
			showChartByEmp(barChartData[empIdx], 'bar', maxValue);
			$.fancybox("#chart_div");	
		}); //on click
		
		$(".doRadarChart").on("click", function() {
			//var empIdx = $(this).closest('tr').find('td:first').text();
			var empIdx = $(this).closest('tr').find('td.empId').text();
			empIdx = parseInt(empIdx);
			showChartByEmp(radarChartData[empIdx], 'radar', maxValue);
			$.fancybox("#chart_canvas");	
		}); //on click	
		
		$(".showDetail").fancybox();
	});
	
</script>

<body>
<div class="wrapper">
	<div class="header">
	<% 
	if (included == null) { %>
		<%@include file="../menu.jsp"%>
	<% 
		included = true;
		request.setAttribute("included", included);	
		}
	%>
	</div>
	
	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="container">
		
		<div class="row">
			<div class="span20">
				<ul class="breadcrumb">
					<li><a href="#">報表</a> <span class="divider">&gt;</span></li>
					<li class="active">職能總表</li>
				</ul>
			</div>
		</div>
		
		<table id="empTable" class="tablesorter">
			<thead>
			<tr>
				<th> 序號 </th>
				<th> 工號 </th>
				<th> 員工姓名 </th>
				<th> 部門  </th>
				<th> 職稱  </th>
				<th> 職系  </th>
				<th> 職等  </th>
				<c:forEach var="catVO" items="${catSvc.all}">
					<th> ${catVO.name} </th>
				</c:forEach>
				<th> 總平均  </th>
				<th> 訓練紀錄 </th>
				<th> 圖表 </th>
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${listAllEmp}" varStatus="loop">
				<tr>
					<td class="idx"> ${loop.count} </td>
					<td class="empId"> ${empVO.empId} </td>
					<td> ${empVO.empName} </td>
					<td> ${empVO.deptName} </td>
					<td> ${empVO.titleName} </td>
					<td> ${empVO.seriesName}</td>
					<td> ${empVO.levelName}</td>
					<c:set var="empId" value="${empVO.empId}"></c:set>
					<script>
						countCat = 1;
						empId = parseInt("${empId}");
						pieChartData[empId][0] = ['職能類別', '平均'];
						barChartData[empId][0] = ['職能類別', '平均'];
						radarChartData[empId].datasets[0].fillColor = "rgba(220,220,220,0.5)";
						radarChartData[empId].datasets[0].strokeColor = "rgba(220,220,220,1)";
						radarChartData[empId].datasets[0].pointColor = "rgba(220,220,220,1)";
						radarChartData[empId].datasets[0].pointStrokeColor = "#fff";
					</script>
					
					<c:forEach var="catVO" items="${catSvc.all}">
						<script>
							//initialize data
							//initPieChartData(pieChartData, ++count, '${empVO.empName}', '${empVO.empId}');
							pieChartData[empId][countCat] = ['${catVO.name}', 0];
							barChartData[empId][countCat] = ['${catVO.name}', 0];
							radarChartData[empId].labels[countCat-1] = "${catVO.name}";
							countCat++;
						</script>
					</c:forEach>
					
					<script>
						//reset countCate
						countCat = 1;
					</script>
					
					<c:forEach var="average" items="${empVO.average}" varStatus="loop">
						<td> 
							<a class="showDetail fancybox.ajax" href="<%=request.getContextPath()%>/employee/emp.do?action=listOneEmpAbility&empId=${empVO.empId}&catId=${loop.count}"> ${average}</a>
						</td>
						<script>
							var avg = parseFloat('${average}');
							//initPieChartData(pieChartData, ++count, '${empVO.empName}', '${empVO.empId}');
							pieChartData[empId][countCat][1] = avg;
							barChartData[empId][countCat][1] = avg;
							radarChartData[empId].datasets[0].data[countCat-1] = avg;
							<%
							if (debugMode != null) {
							%>
							if (empId == 461) {
								alert(countCat + ":" + avg + "pieChartData:" + pieChartData[empId][countCat][1]);
							}
							<%}%>
							countCat++;
						</script>
					</c:forEach>
					
					<td> 
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.totalAvg}"/>
					</td>
					
					<td> <a class="showDetail fancybox.ajax" href="<%=request.getContextPath()%>/employee/emp.do?empId=${empVO.empId}&action=listOneEmpTraining"> 查詢 </a></td>
					<td> 
						<!--
						<FORM>
							<select id="chartType" onchange="drawChart(pieChartData[<%=empIdx%>], 'chart_div')">
								<option> 請選擇 </option>
								<option> Pie Chart </option>
								<option> Bar Chart </option>
							</select>
						</FORM>
						<button onclick="drawPieChart(pieChartData[<%=empIdx%>], 'chart_div')"> 圓餅圖 </button>
						<button onclick="drawBarChart(barChartData[<%=empIdx%>], 'chart_div')"> 長條圖 </button> 
						<button onclick="drawRadarChart(radarChartData[<%=empIdx%>], 'chart_canvas')"> 雷達圖 </button> 
						-->	
						
						<!--
						<div class="btn-group">
							<button class="btn btn-info btn-small doPieChart" onclick="showChartByEmp(pieChartData[<%=empIdx%>], 'pie')"> 圓餅圖 </button>
							<button class="btn btn-success btn-small doBarChart" onclick="showChartByEmp(barChartData[<%=empIdx%>], 'bar')"> 長條圖 </button> 
							<button class="btn btn-warning btn-small doRadarChart" onclick="showChartByEmp(radarChartData[<%=empIdx%>], 'radar')"> 雷達圖 </button> 
						</div>
						-->
						<div class="btn-group">
							<button class="btn btn-info btn-small doPieChart"> 圓餅圖 </button>
							<button class="btn btn-success btn-small doBarChart"> 長條圖 </button> 
							<button class="btn btn-warning btn-small doRadarChart"> 雷達圖 </button> 
						</div>
					</td>
					
					<script>
						countEmp++;
					</script>
					<% empIdx++; %>
				</tr>
			</c:forEach>
		</table>
		
		<div class="secondary"> </div>
	</div> <!-- end of container -->
	
	
	<c:if test="${debugMode != null}">
		employee/listAllEmp.jsp
	</c:if>
</div> <!-- end of wrapper -->
	
</body>
<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>
</html>
