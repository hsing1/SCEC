<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ page import="java.util.*"%> 
<%@ page import="com.employee.model.*"%> 
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
Integer priv = (Integer)session.getAttribute("priv");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
Map<String, String> condMap = new LinkedHashMap<String, String>();
condMap.put("gt", "&gt;");
condMap.put("ge", "&ge;");
condMap.put("eq", "=");
condMap.put("le", "&le;");
condMap.put("lt", "&lt;");
request.setAttribute("condMap", condMap);
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<!-- Google chart -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<!-- Chart.js -->
<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>

<title></title>
</head>

<body>
	<div class="wrapper">
		<div class="container">
			<% if (included == null) { %>
				<%@include file="../menu.jsp"%>
			<% 
				included = true;
				request.setAttribute("included", included);	
			}
			%>
		</div>
		<div class="container">
			<div class="row">
				<div class="span20">
					<ul class="breadcrumb">
						<li><a href="#">報表</a> <span class="divider">&gt;</span></li>
						<li class="active">個人報表</li>
					</ul>
				</div>
			</div>
			
			<div class="formWrapper">
			<c:choose>
				<c:when test="${priv == 1 || priv == 2}">
					<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
						<fieldset>
				　			<legend><span class="label label-info">職能報表查詢</span></legend>
							<br>
							<label for=""><span class="label label-inverse">部門</span></label>
							<br>
						   	<select class="input" id="selectDept1" name="selectDept1" onchange="getTitle1()">
								<option value="-1">--請選擇--</option>
								<c:forEach var="deptVO" items="${deptSvc.all}">	
									<c:choose>
										<c:when test="${deptVO.deptId == currentDept}">
											<option value="${deptVO.deptId}" selected> ${deptVO.name} </option>                  
										</c:when>
										<c:otherwise>
											<option value="${deptVO.deptId}"> ${deptVO.name} </option>                  
										</c:otherwise>
									</c:choose>
							   	</c:forEach>   
						   	</select>
						    	
					    	<br><br>
					    	<label for=""><span class="label label-inverse">職稱</span></label>
					    	<br>
					    	<select id="selectTitle1" name="selectTitle1" onchange="getEmp1Multi()">
								<option value="-1">--請選擇--</option>
								<c:forEach var="titleVO" items="${titleList}">
									<c:choose>
										<c:when test="${titleVO.titleId == currentTitle}">
						 					<option value="${titleVO.titleId}" selected> ${titleVO.name} </option>                  
						 				</c:when>
						 				<c:otherwise>
						 					<option value="${titleVO.titleId}"> ${titleVO.name} </option>                  
						 				</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
					    	
					    	<br><br>	
					    	<label for=""><span class="label label-inverse">員工</span></label>
					    	<br>
					    	<select id="selectEmp1" name="selectEmp1">
								<option value="-1">--請選擇--</option>
								<c:forEach var="empViewVO" items="${empViewList}">
									<c:choose>
										<c:when test="${empViewVO.empId == currentEmp}">
						 					<option value="${empViewVO.empId}" selected> ${empViewVO.empName} </option>                  
						 				</c:when>
						 				<c:otherwise>
						 					<option value="${empViewVO.empId}"> ${empViewVO.empName} </option>                  
						 				</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
					    	
					    	<br><br>
					    	<label for=""><span class="label label-inverse">比較條件</span></label>
					    	<br>
					    	<select class="input-small" id="selectCond" name="selectCond">
								<option value="-1">--請選擇--</option>
								<c:forEach var="cond" items="${condMap}">
									<c:choose>
									<c:when test="${cond.key == currentCond}">
						 				<option value="${cond.key}" selected> ${cond.value} </option>                  
						 			</c:when>
						 			<c:otherwise>
						 				<option value="${cond.key}"> ${cond.value} </option>                  
						 			</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
					    	
					    	<br><br>
					    	<label for="grade"> <span class="label label-inverse">分數</span> </label>
					    	<br>
					    	<input class="input-small" type="text" style="height:30px" name="grade" value="${currentGrade}">
					    	<br><br>
					    	<button class="btn btn-success" type="submit"> 查詢 </button>
					    	<input type="hidden" name="action" value="listSubjectByEmpAndGrade">
						</fieldset>
					</form>
				</c:when> <%-- end of priv = 1, 2 --%>
				
				<c:when test="${priv == 3}">
					<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
						<fieldset>
				　			<legend><span class="label label-info">職能報表查詢</span></legend>
							<br>
					    	<label for=""><span class="label label-inverse">部門</span></label>
					    	<br>
					    	<select class="input-small" id="selectDept1" name="selectDept1">
					    		<%-- 查詢 自己或自己的部門--%>
								<option value="${myEmpVO.deptId}"> ${myEmpVO.deptName} </option>                  
					    	</select>
					    	
					    	<br><br>
					    	<label for=""><span class="label label-inverse">職稱</span></label>
					    	<br>
					    	<select id="selectTitle1" name="selectTitle1" onchange="getEmp1()">
								<option value="-1">--請選擇--</option>
								<c:forEach var="titleVO" items="${titleList}">
									<c:choose>
										<c:when test="${titleVO.titleId == currentTitle}">
						 					<option value="${titleVO.titleId}" selected> ${titleVO.name} </option>                  
						 				</c:when>
						 				<c:otherwise>
						 					<option value="${titleVO.titleId}"> ${titleVO.name} </option>                  
						 				</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
					    	
					    	<br><br>
					    	<label for=""><span class="label label-inverse">員工</span></label>
					    	<br>
					    	<select id="selectEmp1" name="selectEmp1">
								<option value="-1">--請選擇--</option>
								<c:forEach var="empViewVO" items="${empViewList}">
									<c:choose>
										<c:when test="${empViewVO.empId == currentEmp}">
						 					<option value="${empViewVO.empId}" selected> ${empViewVO.empName} </option>                  
						 				</c:when>
						 				<c:otherwise>
						 					<option value="${empViewVO.empId}"> ${empViewVO.empName} </option>                  
						 				</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
					    	
					    	<br><br>
					    	<label for=""><span class="label label-inverse">比較條件</span></label>
					    	<br>
					    	<select class="input-small" id="selectCond" name="selectCond">
								<option value="-1">--請選擇--</option>
								<c:forEach var="cond" items="${condMap}">
									<c:choose>
									<c:when test="${cond.key == currentCond}">
						 				<option value="${cond.key}" selected> ${cond.value} </option>                  
						 			</c:when>
						 			<c:otherwise>
						 				<option value="${cond.key}"> ${cond.value} </option>                  
						 			</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
					    	
					    	<br><br>
					    	<label for="grade"><span class="label label-inverse">分數</span></label>
					    	<br>
					    	<input class="input-small" type="text" name="grade" value="${currentGrade}" style="height:30px">
					    	<br><br>
					    	<button class="btn btn-success" type="submit">查詢</button>
					    	<input type="hidden" name="action" value="listSubjectByEmpAndGrade">
					    	<div id="selectedInfo"></div>
						</fieldset>
					</form>
				</c:when> <%-- end of priv = 3 --%>
				
				<%-- 查詢 自己--%>
				<c:when test="${priv == 4}">
					<form class="form-inline" method="post" action="<%= request.getContextPath() %>/employee/emp.do">
						<fieldset>
				　			<legend><span class="label label-info">職能報表查詢</span></legend>
							<br>
					    	<label for=""><span class="label label-inverse">部門</span></label>
					    	<br>
					    	<select class="input-small" id="selectDept1" name="selectDept1">
								<option value="${myEmpVO.deptId}"> ${myEmpVO.deptName} </option>                  
					    	</select>
					    	
							<br><br>					    	
					    	<label for=""><span class="label label-inverse">職稱</span></label>
					    	<br>
					    	<select id="selectTitle1" name="selectTitle1" onchange="getEmp1()">
								<option value="${myEmpVO.titleId}"> ${myEmpVO.titleName} </option>                  
					    	</select>
					    	
							<br><br>					    	
					    	<label for=""><span class="label label-inverse">員工</span></label>
					    	<br>
					    	<select id="selectEmp1" name="selectEmp1">
								<option value="${myEmpVO.empId}"> ${myEmpVO.empName} </option>                  
					    	</select>
					    	
							<br><br>					    	
					    	<label for=""><span class="label label-inverse">比較條件</span></label>
					    	<br>
					    	<select class="input-small" id="selectCond" name="selectCond">
								<option value="-1">--請選擇--</option>
								<c:forEach var="cond" items="${condMap}">
									<c:choose>
									<c:when test="${cond.key == currentCond}">
						 				<option value="${cond.key}" selected> ${cond.value} </option>                  
						 			</c:when>
						 			<c:otherwise>
						 				<option value="${cond.key}"> ${cond.value} </option>                  
						 			</c:otherwise>
						 			</c:choose>
								</c:forEach>
					    	</select>
							<br><br>					    	
					    	<label for="grade"><span class="label label-inverse">分數</span></label>
					    	<br>
					    	<input class="input-small" type="text" name="grade" value="${currentGrade}" style="height:30px">
							<br><br>					    	
					    	<button class="btn btn-success" type="submit">查詢</button>
					    	<input type="hidden" name="action" value="listSubjectByEmpAndGrade">
						</fieldset>
					</form>
				</c:when> <%-- end of priv = 4 --%>
			</c:choose>
			</div> <!-- end of formWrapper -->
				
			<div class="secondary">
				<%if (request.getAttribute("listSubjectByEmpAndGrade") != null) {%>
				<jsp:include page="/employee/listSubjectByEmpAndGrade.jsp"/>
				<%} %>
			</div>
		</div> <!-- end of container-->
	</div> <!-- end of wrapper -->

</body>
</html>