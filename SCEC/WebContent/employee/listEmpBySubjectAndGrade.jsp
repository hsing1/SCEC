<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.employee.model.*"%>
<%@ page import="com.subject.model.*"%>
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeAbilityViewService" />
<jsp:useBean id="subSvc" scope="page" class="com.subject.model.SubjectService" />

<html>
<head>
<title></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

</head>

<body>
<script type="text/javascript">
$(function () {
    $("#empTable").tablesorter({widgets: ['zebra']});
});

</script>
<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<%
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>

	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>

	<div class="primary">
		<table id="empTable" class="tablesorter">
			<caption><span class="label label-info">查詢結果</span></caption>
			<thead>
			<tr>
				<th> 工號 </th>
				<th> 員工 </th>
				<th> 部門</th>
				<th> 職稱 </th>
				<th> 科目 </th>
				<th> 類別 </th>
				<th> 項目 </th>
				<th> 應備 </th>
				<th> 具備 </th>
				<th> 標準</th>
				<th> 部門平均 </th>
				<th> 部門平均差異 </th>
				<th> 公司平均(應備) </th>
				<th> 公司平均差異 </th>
			</tr>
			</thead>
			
			<c:forEach var="empVO" items="${listEmpBySubjectAndGrade}">
				<tr>
					<td> ${empVO.empId} </td>
					<td> ${empVO.empName} </td>
					<td> ${empVO.deptName} </td>
					<td> ${empVO.titleName} </td>
					<td> ${empVO.subjectName} </td>
					<td> ${empVO.categoryName} </td>
					<td> ${empVO.className} </td>
					
					<!-- 應備 -->
					<c:if test="${empVO.requiredGrade == 0}">
						<td></td>
					</c:if>
					<c:if test="${empVO.requiredGrade > 0}">
						<c:if test="${empVO.requiredGrade < empVO.deptAvg}" >
							<td class="underDeptAvg"> ${empVO.requiredGrade} &nbsp; *</td>
						</c:if>
						<c:if test="${empVO.requiredGrade >= empVO.deptAvg}" >
							<td> ${empVO.requiredGrade}</td>
						</c:if>
					</c:if>
					
					<!-- 具備 -->
					<c:if test="${empVO.grade == 0}">
						<td></td>
					</c:if>
					<c:if test="${empVO.grade > 0}">
						<td> ${empVO.grade}</td>
					</c:if>
					
					<!-- 標準 -->
					<c:if test="${empVO.standard <= 0}">
						<td> ---- </td>
					</c:if>
					<c:if test="${empVO.standard > 0}">
						<td> ${empVO.standard}</td>
					</c:if>
					
					<!-- 部門平均 應備 -->
					<td>
						<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.deptAvg}"/>
					</td>
					
					<!-- 部門平均差異 -->
					<c:if test="${empVO.requiredGrade > 0}">
						<td>
							<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.deptAvg}"/>
						</td>
					</c:if>
					<c:if test="${empVO.requiredGrade == 0}">
						<td></td>
					</c:if>
					
					<!-- 公司平均 -->
					<td> ${empVO.companyAvg}</td>
					
					<!--
					<td> ${cmpAvgMap[empVO.subjectId]} </td>
					-->
					<c:if test="${empVO.requiredGrade > 0}">
						<td> 
							<fmt:formatNumber type="number" maxFractionDigits="2" value="${empVO.requiredGrade - empVO.companyAvg}"/>
						</td>
					</c:if>
					<c:if test="${empVO.requiredGrade == 0}">
						<td></td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of primary -->

	<c:if test="${debugMode != null}">
		<%=request.getRequestURI() %> <br>
		From : "${requestPath}"
	</c:if>
</div> <!-- end of wrapper -->
</body>
</html>
