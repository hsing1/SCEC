<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
			
			.form-inline {
				margin-bottom : 30px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script>
	$(function () {
    	//$("#output").tablesorter({widgets: ['zebra']});
	});
</script>

<title></title>
</head>

<body>
	<div class="wrapper">
		<div class="header">
			<% if (included == null) { %>
				<%@include file="../menu.jsp"%>
			<% 
				included = true;
				request.setAttribute("included", included);	
			}
			%>
		</div>
		<div class="container">
			<div class="row">
				<div class="span20">
					<ul class="breadcrumb">
						<li><a href="#">評比</a> <span class="divider">&gt;</span></li>
						<li class="active">個人評比</li>
					</ul>
				</div>
			</div>
			
			<div class="formWrapper">
				<div class="form-inline">
					<fieldset>
			　			<legend><span class="label label-info">員工1 </span></legend>
					   	<label for=""><span class="label label-inverse">部門</span></label>
					   	<select id="selectDept1" name="selectDept1" onchange="getTitle1Multi()">
							<option value="-1">--請選擇--</option>
							<c:forEach var="deptVO" items="${deptSvc.all}">	
								<c:choose>
									<c:when test="${deptVO.deptId == currentDept}">
					 					<option value="${deptVO.deptId}" selected> ${deptVO.name} </option>                  
					 				</c:when>
					 				<c:otherwise>
					 					<option value="${deptVO.deptId}"> ${deptVO.name} </option>                  
					 				</c:otherwise>
					 			</c:choose>
					   		</c:forEach>   
					   	</select>
					   	
					   	<label for=""><span class="label label-inverse">職稱</span></label>
					   	<select id="selectTitle1" name="selectTitle1" onchange="getEmp1Multi()">
							<option value="-1">--請選擇--</option>
							<c:forEach var="titleVO" items="${titleList}">
								<c:choose>
									<c:when test="${titleVO.titleId == currentTitle}">
						 				<option value="${titleVO.titleId}" selected> ${titleVO.name} </option>                  
						 			</c:when>
						 			<c:otherwise>
						 				<option value="${titleVO.titleId}"> ${titleVO.name} </option>                  
						 			</c:otherwise>
						 		</c:choose>
							</c:forEach>
					   	</select>
				    	
					   	<label for=""><span class="label label-inverse">姓名</span></label>
					   	<select id="selectEmp1" name="selectEmp1">
							<option value="-1">--請選擇--</option>
							<c:forEach var="empViewVO" items="${empViewList}">
								<c:choose>
									<c:when test="${empViewVO.empId == currentEmp}">
						 				<option value="${empViewVO.empId}" selected> ${empViewVO.empName} </option>                  
						 			</c:when>
						 			<c:otherwise>
						 				<option value="${empViewVO.empId}"> ${empViewVO.empName} </option>                  
						 			</c:otherwise>
						 		</c:choose>
							</c:forEach>
					   	</select>
					</fieldset>
				</div> <!-- end of form-inline -->
					    
				<div class="form-inline">
					<fieldset>
			　			<legend><span class="label label-info">員工2</span></legend>
				   		<label for=""><span class="label label-inverse">部門</span></label>
				   		<select id="selectDept2" name="selectDept2" onchange="getTitle2()">
							<option value="-1">--請選擇--</option>
							<c:forEach var="deptVO" items="${deptSvc.all}">	
				 				<option value="${deptVO.deptId}"> ${deptVO.name} </option>                  
				   			</c:forEach>   
				   		</select>
				   		
				   		<label for=""><span class="label label-inverse">職稱</span></label>
				   		<select id="selectTitle2" name="selectTitle2" onchange="getEmp2()">
							<option value="-1">--請選擇--</option>
				   		</select>
					    	
				   		<label for=""><span class="label label-inverse">姓名</span></label>
				   		<select id="selectEmp2" name="selectEmp2">
							<option value="-1">--請選擇--</option>
				   		</select>
					</fieldset>
				</div>
						
				<div class="form-inline" id="selectChart">    	
					<fieldset>
			　			<legend><span class="label label-info">圖表</span></legend>
						<input type="radio" name="chartType" value="radar" onclick="showChartByEmpId()"> 雷達圖
						<input type="radio" name="chartType" value="bar" onclick="showChartByEmpId()"> 長條圖 
						<input type="radio" name="chartType" value="combo" onclick="showChartByEmpId()"> 複合圖 
					</fieldset>
				</div>
			</div> <!-- end of container-->
			
			<div id="table">
				<!-- show output table here -->
			</div>
			
			<div class="secondary">
				<!--
				display chart dynamically here
				<canvas id="chart_canvas" height="450" width="450"></canvas>
				<div id="chart_div" style="width: 900px; height: 500px;"></div>
				-->
			</div>
		</div> <!-- end of primary -->
		
		
	</div> <!-- end of wrapper -->

</body>
<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>
</html>