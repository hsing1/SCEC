<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />

<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>

<title></title>
</head>

<body>
	<div class="wrapper">
		<div class="header">
			<% if (included == null) { %>
				<%@include file="../menu.jsp"%>
			<% 
				included = true;
				request.setAttribute("included", included);	
			}
			%>
		</div>
		<div class="container">
			<div class="row">
				<div class="span20">
					<ul class="breadcrumb">
						<li><a href="#">查詢</a> <span class="divider">&gt;</span></li>
						<li class="active">個人職務異動分析</li>
					</ul>
				</div>
			</div>
			<div class="formWrapper">
				<form class="form-inline" method="post" action="<%= request.getContextPath() %>/standard/standard.do">
					<fieldset>
			　			<legend><span class="label label-info">選擇職位</span></legend>
				    	<label for=""><span class="label label-inverse">部門</span></label>
				    	<select id="selectDept1" name="selectDept" onchange="getTitle1()">
							<option value="-1">--請選擇--</option>
							<c:forEach var="deptVO" items="${deptSvc.all}">	
								<c:if test="${deptVO.deptId == selectedDept}">
						 			<option value="${deptVO.deptId}" selected> ${deptVO.name} </option>                  
								</c:if>
								<c:if test="${deptVO.deptId != selectedDept}">
						 			<option value="${deptVO.deptId}"> ${deptVO.name} </option>                  
								</c:if>
								
				     		</c:forEach>   
				    	</select>
				    	&nbsp;	
				    	<label for=""><span class="label label-inverse">職稱</span></label>
				    	<select id="selectTitle1" name="selectTitle">
							<option value="-1">--請選擇--</option>
				    	</select>
				    	
				    	<button type="submit" class="btn btn-success">查詢<i class="icon-play icon-white"></i></button>
				    	<input type="hidden" name="action" value="listStandardByTitle">
				    	<div id="selectedInfo"></div>
					</fieldset>
				</form>
			</div> <!-- end of formWrapper -->
			
			<div class="secondary">
				<%if (request.getAttribute("listStandard") != null) {%>
					<jsp:include page="/standard/listStandard.jsp"/>
				<%} %>
				<%if (request.getAttribute("listStandardDiff") != null) {%>
					<jsp:include page="/standard/listStandardDiff.jsp"/>
				<%} %>
			</div>
		</div> <!-- end of container-->
		
	</div> <!-- end of wrapper -->

</body>
</html>