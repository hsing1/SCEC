<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%
Boolean debugMode = (Boolean)application.getAttribute("debugMode");
boolean login = (session.getAttribute("account") != null) ? true : false;
String account = (String) session.getAttribute("account");
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="empSvc" scope="page" class="com.employee.model.EmployeeViewService" />
<jsp:useBean id="deptSvc" scope="page" class="com.department.model.DepartmentService" />
<jsp:useBean id="catSvc" scope="page" class="com.category.model.CategoryService" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>

<script src="<%=request.getContextPath()%>/util/chart/Chart.js"> </script>
<script src="<%=request.getContextPath()%>/util/chart/chart_js.js"> </script>

<script src="<%=request.getContextPath()%>/util/ajax/selectEmps.js"> </script>



<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-select.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap-select.js"> </script>
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/select2-3.4.2/select2.css" />
<script src="<%=request.getContextPath()%>/util/jquery/select2-3.4.2/select2.js"> </script>

<script>
$(function () {
	//$('.selectpicker').selectpicker();
	var sel = $('.selectpicker');
	var sel2 = $('#selectCat');
	$('#selectCat').select2();
});
</script>

<title></title>
</head>

<body>
	<div class="wrapper">
		<div class="header">
			<% if (included == null) { %>
				<%@include file="../menu.jsp"%>
			<% 
				included = true;
				request.setAttribute("included", included);	
			}
			%>
		</div>
		<div class="container">
			<div class="row">
				<div class="span20">
					<ul class="breadcrumb">
						<li><a href="#">查詢</a> <span class="divider">&gt;</span></li>
						<li class="active">職能標準</li>
					</ul>
				</div>
			</div>
			
			<div class="formWrapper">
				<form class="form-inline" method="post" action="<%= request.getContextPath() %>/standard/standard.do">
					<fieldset>
			　			<legend><span class="label label-info">職能標準查尋</span></legend>
				    	<label for=""><span class="label label-inverse">職能類別</span></label>
				    	<select style="width:180px" class="selectpicker" id="selectCat" name="selectCat" onchange="getSubject()">
							<option value="0">全部</option>
							<c:forEach var="catVO" items="${catSvc.all}">	
					 			<c:if test="${selectedCat == catVO.categoryId}">
						 			<option value="${catVO.categoryId}" selected> ${catVO.name} </option>                  
						 		</c:if>
						 		<c:if test="${selectedCat != catVO.categoryId}">
						 			<option value="${catVO.categoryId}"> ${catVO.name} </option>                  
						 		</c:if>
				     		</c:forEach>   
				    	</select>
					<button type="submit" class="btn btn-success">確認<i class="icon-play icon-white"></i></button>
						<input type="hidden" name="action" value="listStandardByCat">
					</fieldset>
				</form>
			</div> <!-- end of formWrapper -->
			
			<div class="secondary">
				<%if (request.getAttribute("listStandard") != null) {%>
					<jsp:include page="/standard/listStandard.jsp"/>
				<%} %>
			</div>
			</div> <!-- end of container -->
		
	</div> <!-- end of wrapper -->
</body>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
</script>
</html>