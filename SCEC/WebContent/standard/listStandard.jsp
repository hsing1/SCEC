<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.department.model.*"%>

<%  
// EmpService empSvc = new EmpService();
// List<EmpVO> list = empSvc.getAll();
// pageContext.setAttribute("list",list);
Boolean included = (Boolean)request.getAttribute("included");
%>
<jsp:useBean id="standSvc" scope="page" class="com.standard.model.StandardViewService" />

<html>
<head>
<title></title>

<!-- Bootstrap -->
<% 
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/jquery/tablesorter/style.css" />
		<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>
		<script src="<%=request.getContextPath()%>/util/jquery/tablesorter/jquery.tablesorter.js"> </script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script src="<%=request.getContextPath()%>/util/chart/google_chart.js"> </script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-select.css" />
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap-select.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>


<script type="text/javascript">
	$(function () {
    	$("#standTable").tablesorter({widgets: ['zebra']});
	});
</script>

</head>

<body>

<div class="wrapper">
	<div class="header">
		<% if (included == null) { %>
			<%@include file="../menu.jsp"%>
		<% 
			included = true;
			request.setAttribute("included", included);	
		}
		%>
	</div>
	
	<div class="errorMsg">
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
		  <font color='red'>請修正以下錯誤:
		  <ul>
		  <c:forEach var="message" items="${errorMsgs}">
		    <li>${message}</li>
		  </c:forEach>
		  </ul>
		  </font>
		</c:if>
	</div>
	
	<div class="primary">
		<table id="standTable" class="tablesorter">
			<caption><span class="label label-info">職能標準</span></caption>
			<thead>
			<tr>
				<th>部門名稱</th>
				<th>職稱</th>
				<th>科目</th>
				<th>應備標準</th>
				<th>認證</th>
			</tr>
			</thead>
			
			<c:forEach var="standVO" items="${listStandard}">
				<tr>
					<td>${standVO.deptName}</td>
					<td>${standVO.titleName}</td>
					<td>${standVO.subjectName}</td>
					<td>${standVO.standard}</td>
					<td>
						<c:if test="${standVO.certification == true}">
							o
						</c:if>
						<c:if test="${standVO.certification == false}">
							x
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div> <!-- end of primary -->
</div> <!--  end of wrapper -->

</body>
</html>
