<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>

<%
	Boolean debugMode = null;
	application.setAttribute("debugMode", debugMode);

	String a = request.getContextPath();
	System.out.println(a);
	session.removeAttribute("requestURL");
	int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<!-- 
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/tabular-table.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/orange-brown.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/turkish-delight.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/grayed-out.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/orange-in-the-sky.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/coffee-with-milk.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/iTunes.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/spread-firefox.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/muted.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/blue-on-blue.css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/desert.css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/orange-grey.css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/red-black.css"/>

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/table/blue-gradient.css"/>

<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/front_index_html5.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/tablesorter/style.css"/>
-->


<!--
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
-->

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

<!--
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="<%=request.getContextPath()%>/util/jquery/jquery-ui-1.10.3.custom.js"> </script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/common.css" />
-->
<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/style.css" />

<!-- jQuery -->
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>

<style>
	.span3 {
		width:240px;
	}
	
	.span4 {
		width:290px;
	}
</style>
<script type="text/javascript">
</script>

</head>

<body>
	<%@include file="menu.jsp" %>

	<div class="container" style="background-color:#f4f4f4; padding-bottom:20px">
	<!-- 
		<div id="myCarousel" class="carousel slide" style="background-color:#76AEC9; padding-bottom:3px">
		  	<ol class="carousel-indicators">
		    	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    	<li data-target="#myCarousel" data-slide-to="1"></li>
		    	<li data-target="#myCarousel" data-slide-to="2"></li>
		  	</ol>
		  	<div class="carousel-inner">
		    	<div class="active item">
					<img src="images/carousel/learn001.jpg"/> 
		    	</div>
		    	<div class="item">
					<img src="images/carousel/hr1.png" /> 
		    	</div>
		    	<div class="item">
					<img src="images/carousel/career4.jpg" /> 
		    	</div>
		  	</div>
		  	<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		  	<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		</div>
		-->
		<div class="row">
			<div class="span3 offset1" style="text-align:center">
				<img src="images/homepage/chart.png">
				<h3>報表分析</h3>
				<p>透過職能行為向度, 定期產生員工職能報表分析, 讓員工更快了解自己的能力與工作目標</p>
			</div>
			<div class="span3" style="text-align:center; margin-left:72px">
				<img src="images/homepage/management.png">
				<h3>培訓與學習管理</h3>
				<p>訓練記錄,訓練需求統計(包含員工、科目能力查詢、培訓保證查詢等各項查詢功能) </p>
			</div>
			<div class="span3" style="text-align:center; margin-left:95px">
				<img src="images/homepage/training.png">
				<h3>訓練體系建置</h3>
				<p>訓練記錄,訓練需求統計(包含員工、科目能力查詢、培訓保證查詢等各項查詢功能) </p>
			</div>
		</div>
		<hr>
		<div class="row" style="background-color:#dfe6ec; padding:10px; margin-left:10px; margin-right:10px">
			<div class="span4">
				<img src="images/homepage/370-01.jpg">
				<h3>人力資源管理</h3>
				<p>提供員工新增與刪除/權限管理/</p>
				<p>組織異動/職等職系異動 各種功能</p>
				<p>讓對的人在對的地方,發揮最大的人力功效</p>
			</div>
			<div class="span4">
				<img src="images/homepage/370-2.jpg">
				<h3>職涯規劃</h3>
				<p>包括職務異動查詢</p>
				<p>升遷管理</p>
				<p>了解各人在職專長</p>
			</div>
			<div class="span4">
				<img src="images/homepage/370-4.jpg">
				<h3>能力盤點</h3>
				<p>利用檢查表或是特定的軟體,</p>
				<p>追蹤員工現在的能力,</p>
				<p>以及應具備的能力,</p>
				<p>將所蒐集的資料告訴員工,幫助他們發展</p>
			</div>
		</div>

	</div> <!-- container -->

	<!-- footer -->
	<%@include file="footer.jsp" %>
		
</body>
</html>