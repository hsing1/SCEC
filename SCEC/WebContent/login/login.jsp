<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>

<%
	Boolean debugMode = null;
	application.setAttribute("debugMode", debugMode);

	String a = request.getContextPath();
	System.out.println(a);
	session.removeAttribute("requestURL");
	int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<!-- General Metas -->
	<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	<!-- Force Latest IE rendering engine -->
	<title>Login Form</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- [if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js.notused"></script>
	<! [endif] -->
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
	
	<!-- Stylesheets
	<link rel="stylesheet" href="css/base.css">
	<link rel="stylesheet" href="css/skeleton.css">
	<link rel="stylesheet" href="css/layout.css">
	-->
	
<!-- jQuery -->
<script src="<%=request.getContextPath()%>/util/jquery/jquery-1.9.1.js"> </script>

<!-- Bootstrap -->
<% 
//Cannot include bootstrap.css twice
Boolean hasBootstrap = (Boolean)request.getAttribute("hasBootstrap");
	if (hasBootstrap == null) {
%>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap.css" />
		<style type="text/css">
			body {
			/*
				padding-top: 60px;
				padding-bottom: 40px;
				*/
			}
		</style>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/util/bootstrap2.3.2/css/logo-nav.css" />
		<script src="<%=request.getContextPath()%>/util/bootstrap2.3.2/js/bootstrap.js"> </script>
<%
		hasBootstrap = true;
		request.setAttribute("hasBootstrap", hasBootstrap);
	} //end of if
%>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/util/css/style.css" />

</head>

<body style="margin-top:0px">

<!--
	<div class="notice">
		<a href="" class="close">close</a>
		<p class="warn">Whoops! We didn't recognise your username or password. Please try again.</p>
	</div>
-->



	<!-- Primary Page Layout -->

	<div class="container" style="position:relative; margin-top:0px; margin-right:auto; margin-left:auto" >
	
		<div class="row-fluid" style="background-color:#bcd3e1">
			<div class="span10 offset1" style="background-color:#bcd3e1">
				<img src="../images/login_bar.png"/> 
			</div>
		</div>
		<br>
		<div class="row-fluid">
			<div class="span6 offset1">
				<h1 style="font-size:38px">提供能力與職涯發展管理平台</h1>
				<div id="ad">
				</div>
				<p>
本管理系統由: 盛全企業管理顧問有限公司設計開發,
旨在提供使用者進行職能發展與教育訓練之管理平台
提供管理者進行從業人員之職能(KSAO)量化管理及協助個人
進行職務與能力落差之分析與應用,以利進行個人之職涯發展與績效管理
</p>
<p>
功能包含: 組織各項職能訂定,訓練體系/地圖,工作說明書,訓練紀錄,訓後服務保證管理
員工能力比較,職務異動能力落差分析,部門能力比較分析,訓練紀錄,行為職能定義,
職能標準訂定,認證科目與基準設定,職能落差GAP篩選,內部講師遴選,接班人遴選....等多種功能,
並可使用多種圖表顯示與列印,本系統足以讓組織[人財]管理掌握於彈指之間,且本系統無空間與時間之使用限制!
				</p>
		        <p><a href="http://www.scec.com.tw/" class="btn btn-info btn-large">了解更多...</a></p>
	        </div>
	        
	        <div class="span4">
	        	<div class="hero-unit">
	    		<h2>登入帳戶</h2>
				<form method="post" action="<%=request.getContextPath()%>/login/login.do">
				    <label>帳號</label>
				    <input type="text" name="account" class="span12">
				    <label>密碼</label>
				    <input type="password" name="password" class="span12">
				    <input type="submit" value="登入" class="btn-large btn-primary pull-left">
					<input type="hidden" name="action" value="Login">
				    <div class="clearfix"></div>
			    </form>
			    </div>
			</div><!-- .span4 -->
		</div><!-- end .row-fluit -->
		
		<div class="row-fluid" style="margin-top:10px; background-color:#f4f2f3; text-align:center">
			<div class="span10 offset1">
			<p>盛全企業管理顧問有限公司 版權所有   &nbsp;&nbsp; TEL:03-4229668  &nbsp;&nbsp; Contact:service@scec.com.tw</p>
			</div>
		</div>
	</div><!-- container -->
	
	<%--
	<%@ include file="../footer.jsp" %>
	--%>

	<!-- JS  -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js"></script>
	<script>window.jQuery || document.write("<script src='js/jquery-1.5.1.min.js'>\x3C/script>")</script>
	<script src="js/app.js"></script>
	
<!-- End Document -->
</body>
</html>