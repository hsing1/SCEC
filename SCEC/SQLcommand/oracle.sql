/*
 * 刪除
 */

DROP TABLE  ABILITY;
DROP TABLE  PROMOTION;
DROP TABLE  RECORD;
DROP TABLE  STANDARD;
DROP TABLE  GRADE;
DROP TABLE  DEPT_TITLE_SUBJECT;
DROP TABLE  DEPT_TITLE;
DROP TABLE  EMPLOYEE;
DROP TABLE  DEPARTMENT;
DROP TABLE  TITLE;
DROP TABLE  LEVELS;
DROP TABLE  SERIES;
DROP TABLE  SUBJECT;
DROP TABLE  CLASSES;
DROP TABLE  CATEGORY;
DROP TABLE  ACCOUNT;





DROP SEQUENCE EMPLOYEE_seq;
DROP SEQUENCE LEVELSN_seq;
DROP SEQUENCE SERIES_seq;
DROP SEQUENCE DEPARTMENT_seq;
DROP SEQUENCE DEPT_TITLE_seq;
DROP SEQUENCE CATEGORY_seq;
DROP SEQUENCE CLASSES_seq;
DROP SEQUENCE SUBJECT_seq;
DROP SEQUENCE DEPT_TITLE_SUBJECT_seq;
DROP SEQUENCE GRADE_seq;
DROP SEQUENCE STANDARD_seq;
DROP SEQUENCE ACCOUNT_seq;
DROP SEQUENCE EMPLOYEE_seq;
DROP SEQUENCE RECORD_seq;
DROP PROMOTION_seq;



/*
 * 新增_組織部分
 */


DROP TABLE LEVELS;
DROP SEQUENCE LEVELSN_seq;
CREATE TABLE LEVELS(
	level_id 	 NUMBER(2),
        name 	  	 NUMBER(2) NOT NULL UNIQUE,	
	PRIMARY KEY (level_id)
);

CREATE  SEQUENCE LEVELSN_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;




DROP TABLE SERIES;
DROP SEQUENCE SERIES_seq;
CREATE TABLE SERIES(
	series_id 	 NUMBER(2),
        name 	  	 VARCHAR(30) NOT NULL UNIQUE,	
	PRIMARY KEY (series_id)
);
CREATE  SEQUENCE SERIES_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;




DROP TABLE TITLE;
DROP SEQUENCE TITLE_seq;
CREATE TABLE TITLE(
	title_id 	 NUMBER(2),
        name 	  	 VARCHAR(30) NOT NULL UNIQUE,
        series_id 	 NUMBER(2),
     	level_id 	 NUMBER(2),
	PRIMARY KEY (title_id)
);
CREATE  SEQUENCE TITLE_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;




DROP TABLE DEPARTMENT;
DROP SEQUENCE DEPARTMENT_seq;
CREATE TABLE DEPARTMENT(
	dept_id 	 NUMBER(3),
    name 	  	 VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY (dept_id )
);

CREATE  SEQUENCE DEPARTMENT_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;


INSERT INTO DEPARTMENT VALUES (DEPARTMENT_seq.NEXTVAL, '財務部');
INSERT INTO DEPARTMENT VALUES (DEPARTMENT_seq.NEXTVAL, '研發部');
INSERT INTO DEPARTMENT VALUES (DEPARTMENT_seq.NEXTVAL, '業務部');
INSERT INTO DEPARTMENT VALUES (DEPARTMENT_seq.NEXTVAL, '生管部');


DROP TABLE DEPT_TITLE;
DROP SEQUENCE DEPT_TITLE_seq;
CREATE TABLE DEPT_TITLE(
	dept_title_id 	 NUMBER(4),
	title_id 	 NUMBER(2),
	dept_id 	 NUMBER(3),
        FOREIGN KEY(title_id) REFERENCES TITLE(title_id),
	FOREIGN KEY(dept_id ) REFERENCES DEPARTMENT(dept_id ),
	PRIMARY KEY (dept_title_id)
);
CREATE  SEQUENCE DEPT_TITLE_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;


/*
 * 新增_職能部分
 */


DROP TABLE CATEGORY;
DROP SEQUENCE CATEGORY_seq;
CREATE TABLE CATEGORY(
	category_id 	 NUMBER(3),
        name 	  	 VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY (category_id)
);
CREATE  SEQUENCE CATEGORY_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;




DROP TABLE CLASSES;
DROP SEQUENCE CLASSES_seq;
CREATE TABLE CLASSES(
	class_id 	 NUMBER(4),
        name 	  	 VARCHAR(30) NOT NULL UNIQUE,
	category_id 	 NUMBER(3),
	FOREIGN KEY(category_id) REFERENCES CATEGORY(category_id),
	PRIMARY KEY (class_id) 
);
CREATE  SEQUENCE CLASSES_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;



DROP TABLE SUBJECT;
DROP SEQUENCE SUBJECT_seq;
CREATE TABLE SUBJECT(
	subject_id 	 NUMBER(4),
        license		 VARCHAR(50),	
        name 	  	 VARCHAR(100) NOT NULL,
	class_id	 NUMBER(4),
        FOREIGN KEY(class_id) REFERENCES CLASSES(class_id),
	PRIMARY KEY (subject_id) 
);


CREATE  SEQUENCE SUBJECT_seq
	INCREMENT BY 1
	START WITH 1000
	NOMAXVALUE
	NOCYCLE
	NOCACHE;





DROP TABLE DEPT_TITLE_SUBJECT;
DROP SEQUENCE DEPT_TITLE_SUBJECT_seq;
CREATE TABLE DEPT_TITLE_SUBJECT(
	dept_title_subject_id  NUMBER(5),
	subject_id 	 NUMBER(4),
	dept_title_id 	 NUMBER(4),
        FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id),
	FOREIGN KEY(dept_title_id ) REFERENCES DEPT_TITLE(dept_title_id ),
	PRIMARY KEY (dept_title_subject_id)
	
);

CREATE  SEQUENCE DEPT_TITLE_SUBJECT_seq
	INCREMENT BY 1
	START WITH 100
	NOMAXVALUE
	NOCYCLE
	NOCACHE;



/*
 * 新增_評量部分
 */



DROP TABLE GRADE;
DROP SEQUENCE GRADE_seq;
CREATE TABLE GRADE(
	grade_id 	 NUMBER(3),
        grade 	  	 NUMBER(3) NOT NULL UNIQUE,
	PRIMARY KEY (grade_id)
);

CREATE  SEQUENCE GRADE_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;




DROP TABLE STANDARD;
DROP SEQUENCE STANDARD_seq;
CREATE TABLE STANDARD(
	standard_id  		 NUMBER(5),
	grade_id 	 	 NUMBER(3),
	dept_title_subject_id 	 NUMBER(5),
        FOREIGN KEY(grade_id) REFERENCES GRADE(grade_id),
	FOREIGN KEY(dept_title_subject_id) REFERENCES DEPT_TITLE_SUBJECT(dept_title_subject_id),
	PRIMARY KEY (standard_id)
	
);

CREATE  SEQUENCE STANDARD_seq
	INCREMENT BY 1
	START WITH 1000
	NOMAXVALUE
	NOCYCLE
	NOCACHE;


DROP TABLE ACCOUNT;
DROP SEQUENCE ACCOUNT_seq;
CREATE TABLE ACCOUNT(
	account_id 	 NUMBER(3),
	account 	 VARCHAR(30),
	password 	 VARCHAR(30),
        type  		 NUMBER(1),
	PRIMARY KEY (account_id)
);

CREATE  SEQUENCE ACCOUNT_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;


DROP TABLE EMPLOYEE;
DROP SEQUENCE EMPLOYEE_seq;

CREATE TABLE EMPLOYEE(
	emp_id  NUMBER(5),
        first_name  VARCHAR(30) NOT NULL,
        last_name   VARCHAR(30) NOT NULL,
	dept_id 	 NUMBER(3),
	title_id 	 NUMBER(2),
	account_id  NUMBER(3),
        FOREIGN KEY(dept_id)  REFERENCES DEPARTMENT(dept_id), 
	FOREIGN KEY(title_id) REFERENCES TITLE(title_id),
    	FOREIGN KEY(account_id) REFERENCES ACCOUNT(account_id),
	PRIMARY KEY (emp_id)
);

CREATE  SEQUENCE EMPLOYEE_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;



DROP TABLE ABILITY;
CREATE TABLE ABILITY(
	subject_id 	 NUMBER(5),
	standard_id  NUMBER(5),
	grade_id 	 NUMBER(3),
	emp_id  	 NUMBER(5),
	
    FOREIGN KEY(grade_id) REFERENCES GRADE(grade_id),
	FOREIGN KEY(emp_id) REFERENCES EMPLOYEE(emp_id),
	FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id),
	FOREIGN KEY(standard_id) REFERENCES STANDARD(standard_id)
);


DROP TABLE RECORD;
DROP SEQUENCE RECORD_seq;
CREATE TABLE RECORD(
	training_id     NUMBER(5),
	score	     	NUMBER(2),
	training_date   DATE,
        fee  	     	NUMBER(5),
        training_hours  NUMBER(5),
        certificate  	VARCHAR(30),
	guarantee    	DATE,
        trun	    	DATE,
	remark		VARCHAR(255),
 	feedback	CLOB,/*CLOB*/
	emp_id  	NUMBER(5),
	subject_id 	NUMBER(4),
        FOREIGN KEY(emp_id)  REFERENCES EMPLOYEE(emp_id), 
	FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id),
	PRIMARY KEY (training_id)
	
);


CREATE  SEQUENCE RECORD_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;




DROP TABLE PROMOTION;
DROP SEQUENCE PROMOTION_seq;
CREATE TABLE PROMOTION(
	promotion_id     NUMBER(5),	
	promotion_date   DATE,
	emp_id           NUMBER(5),
	dept_title_id 	 NUMBER(3),
        FOREIGN KEY(emp_id)  REFERENCES EMPLOYEE(emp_id), 
	FOREIGN KEY(dept_title_id) REFERENCES DEPT_TITLE(dept_title_id),
	PRIMARY KEY (promotion_id)
);

CREATE  SEQUENCE PROMOTION_seq
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE
	NOCYCLE
	NOCACHE;

