
/*
mysql -uscec -h127.0.0.1 -p
use test;
create user 'scec'@'127.0.0.1' identified by '12345';
GRANT ALL PRIVILEGES ON *.* TO scec@127.0.0.1
DROP DATABASE SCEC;
CREATE SCHEMA `SCEC` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
*/
USE SCEC2;
SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE DEPT_TITLE;
TRUNCATE TABLE TRAINING_RECORD;
TRUNCATE TABLE ABILITY;

TRUNCATE TABLE PROMOTION;
TRUNCATE TABLE TRAINING_SYSTEM;
TRUNCATE TABLE SALARY;

TRUNCATE TABLE EMPLOYEE;
TRUNCATE TABLE TITLE;

TRUNCATE TABLE DEPARTMENT;
TRUNCATE TABLE DEVISION;
TRUNCATE TABLE SECTION;


TRUNCATE TABLE SUBJECT;
TRUNCATE TABLE CLASS;


TRUNCATE TABLE CATEGORY;
TRUNCATE TABLE LEVEL;
TRUNCATE TABLE SERIES;
TRUNCATE TABLE ACCOUNT;
SET FOREIGN_KEY_CHECKS=1;

DROP VIEW EMPLOYEE_VIEW;
DROP VIEW EMPLOYEE_ABILITY_VIEW;
DROP VIEW EMPLOYEE_ABILITY_VIEW_REQUIRED;
DROP VIEW EMPLOYEE_TRAINING_VIEW;
DROP VIEW SUBJECT_STANDARD;
DROP VIEW PROMOTION_VIEW;

DROP TABLE DEPT_TITLE;
DROP TABLE TRAINING_RECORD;
DROP TABLE ABILITY;

DROP TABLE PROMOTION;
DROP TABLE EMPLOYEE;
DROP TABLE TRAINING_SYSTEM;
DROP TABLE SUBJECT;
DROP TABLE CLASS;
DROP TABLE SALARY;
DROP TABLE TITLE;
DROP TABLE DEPARTMENT;
DROP TABLE DEVISION;
DROP TABLE SECTION;
DROP TABLE CATEGORY;
DROP TABLE LEVEL;
DROP TABLE SERIES;
DROP TABLE ACCOUNT;

/*
 * 職等
 */
CREATE TABLE LEVEL(
	level_id 	 INT(2) AUTO_INCREMENT,
    name 	  	 VARCHAR(3) NOT NULL UNIQUE,	
	PRIMARY KEY (level_id)
);

/*
 * 職系 
 */
CREATE TABLE SERIES(
	series_id 	 INT(2) AUTO_INCREMENT NOT NULL,
    name 	  	 VARCHAR(30) NOT NULL UNIQUE,	
	PRIMARY KEY (series_id)
);

CREATE TABLE DEVISION(
	devision_id 	INT(2) AUTO_INCREMENT NOT NULL,
	name 			VARCHAR(20) UNIQUE,
	PRIMARY KEY(devision_id)
);

/*
 * 單位
 */
CREATE TABLE SECTION(
	section_id 		INT(2) AUTO_INCREMENT NOT NULL,
	name			VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY (section_id)
);

/*
 * 部門
 */
CREATE TABLE DEPARTMENT(
	dept_id 	 INT(3) AUTO_INCREMENT NOT NULL,
    name 	  	 VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY (dept_id )
);



CREATE TABLE TITLE(
	title_id 	 INT(2) AUTO_INCREMENT,
    name 	  	 VARCHAR(30) NOT NULL,
    series_id 	 INT(2),
    level_id 	 INT(2),
    salary_level INT(2),
	priv		 INT(2),
    
	PRIMARY KEY (title_id),
	FOREIGN KEY(series_id ) REFERENCES SERIES(series_id ),
	FOREIGN KEY(level_id ) REFERENCES LEVEL(level_id )
);

/*
 * 薪資結構
 */
CREATE TABLE SALARY(
	salary_id 	 INT(2) AUTO_INCREMENT,
    title_id 	 INT(2),
    salary_level INT(2),
    salary		 INT(9),
    
	PRIMARY KEY (salary_id),
	FOREIGN KEY(title_id ) REFERENCES TITLE(title_id )
);

/*
 * 部門設定 
 */
CREATE TABLE DEPT_TITLE(
	title_id 	 	INT(2),
	section_id		INT(2) DEFAULT 1, 
	dept_id 	 	INT(3),
	
    FOREIGN KEY(title_id) REFERENCES TITLE(title_id),
	FOREIGN KEY(dept_id ) REFERENCES DEPARTMENT(dept_id ),
	FOREIGN KEY(section_id) REFERENCES SECTION(section_id),
	PRIMARY KEY (dept_id, section_id, title_id)
);



CREATE TABLE CATEGORY(
	category_id 	INT(3) AUTO_INCREMENT,
    name 	  	 	VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY(category_id)
);

CREATE TABLE CLASS(
	class_id 	 INT(4) AUTO_INCREMENT,
    name 	  	 VARCHAR(30) NOT NULL UNIQUE,
	category_id  INT(3),
	
	FOREIGN KEY(category_id) REFERENCES CATEGORY(category_id),
	PRIMARY KEY(class_id) 
);

CREATE TABLE SUBJECT(
	subject_id 	 INT(4) AUTO_INCREMENT,
    license		 VARCHAR(50),
    name 	  	 VARCHAR(100) NOT NULL,
    classify 	 INT(1),
	class_id	 INT(4),
	category_id  INT(3),
	
    FOREIGN KEY(class_id) REFERENCES CLASS(class_id),
    FOREIGN KEY(category_id) REFERENCES CATEGORY(category_id),
	PRIMARY KEY (subject_id) 
);
/*
CREATE TABLE DEPT_TITLE_SUBJECT(
	dept_title_subject_id  	INT(5),
	subject_id 	 			INT(4),
	dept_title_id 	 		INT(4),
	
    FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id),
	FOREIGN KEY(dept_title_id ) REFERENCES DEPT_TITLE(dept_title_id ),
	PRIMARY KEY (dept_title_subject_id)
);
*/


CREATE TABLE TRAINING_SYSTEM(
	dept_id  	INT(5),
	section_id	INT(5),
	title_id  	INT(5),
	subject_id 	INT(4),
	standard	INT(1),
	certification BOOLEAN,
	
	PRIMARY KEY(dept_id, section_id, title_id, subject_id),
    FOREIGN KEY(dept_id) REFERENCES DEPARTMENT(dept_id),
	FOREIGN KEY(section_id) REFERENCES SECTION(section_id),
	FOREIGN KEY(title_id ) REFERENCES TITLE(title_id ),
    FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id)
);


/*
CREATE TABLE GRADE(
	grade_id 	 INT(3),
    grade 	  	 INT(3) NOT NULL UNIQUE,
	PRIMARY KEY (grade_id)
);


CREATE TABLE STANDARD(
	standard_id  	INT(5),
	grade_id 	 	INT(3),
	dept_title_subject_id 	 INT(5),
	
    FOREIGN KEY(grade_id) REFERENCES GRADE(grade_id),
	FOREIGN KEY(dept_title_subject_id) REFERENCES DEPT_TITLE_SUBJECT(dept_title_subject_id),
	PRIMARY KEY(standard_id)
);
*/

CREATE TABLE ACCOUNT(
	account_id 	INT(3) AUTO_INCREMENT,
	account 	VARCHAR(30),
	password 	VARCHAR(30),
    priv  		INT(1),
    
	PRIMARY KEY(account_id)
);

CREATE TABLE EMPLOYEE(
	emp_id  	INT(5),
    first_name  VARCHAR(30),
    last_name   VARCHAR(30),
    name  	    VARCHAR(30) NOT NULL,
    devision_id 	INT(2),
	dept_id 	INT(3),
	section_id	INT(2),
	title_id 	INT(2),
	salary_level INT(2),
	account_id  INT(3) UNIQUE,
	onboard_date	DATE,
	leave_date		DATE,
	
    FOREIGN KEY(dept_id)  REFERENCES DEPARTMENT(dept_id), 
	FOREIGN KEY(title_id) REFERENCES TITLE(title_id),
    FOREIGN KEY(account_id) REFERENCES ACCOUNT(account_id),
    FOREIGN KEY(devision_id) REFERENCES DEVISION(devision_id),
    FOREIGN KEY(section_id) REFERENCES SECTION(section_id),
	PRIMARY KEY(emp_id)
);

CREATE TABLE ABILITY(
	emp_id  	INT(5),
	subject_id 	INT(5),
	grade 	 	float(3),     #具備
	required_grade  float(3), #應備
	required	BOOLEAN,
	
	FOREIGN KEY(emp_id) REFERENCES EMPLOYEE(emp_id),
	FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id)
	/*
	FOREIGN KEY(standard_id) REFERENCES STANDARD(standard_id)
    FOREIGN KEY(grade_id) REFERENCES GRADE(grade_id),
    */
);


CREATE TABLE TRAINING_RECORD(
	emp_id  		INT(5),
	subject_id 		INT(4),
	grade	     	FLOAT(2),
	training_date   DATE,
    fee  	     	INT(5),
    training_hour  	INT(5),
    certificate  	VARCHAR(30),
	guarantee    	BOOLEAN,   /* 服務保證  */
    transfer    	DATE,   /* 轉訓日期 */
    training_type    	INT(1), /* 訓練方式 */
    compensate    	FLOAT(3), /* 賠償條件 */
	note			VARCHAR(255),
 	report			BLOB,
	
    FOREIGN KEY(emp_id)  REFERENCES EMPLOYEE(emp_id), 
	FOREIGN KEY(subject_id) REFERENCES SUBJECT(subject_id),
	PRIMARY KEY(emp_id, subject_id)
);

CREATE TABLE PROMOTION(
	emp_id      INT(5),
	dept_id		INT(3),
	title_id	INT(2),
	devision	VARCHAR(30),	
	promotion_date   DATE,
	note		VARCHAR(255),

    FOREIGN KEY(emp_id)  REFERENCES EMPLOYEE(emp_id), 
	FOREIGN KEY(dept_id) REFERENCES DEPARTMENT(dept_id),
	FOREIGN KEY(title_id) REFERENCES TITLE(title_id),
	PRIMARY KEY(emp_id, dept_id, title_id)
);


/*
 * View
 */

CREATE VIEW EMPLOYEE_VIEW AS
SELECT e.emp_id, e.name emp_name, e.onboard_date onboard_date, d.name dept_name, d.dept_id dept_id, t.name title_name, t.title_id title_id, s.name series_name, l.name level_name
, acc.account_id account_id, acc.account, acc.priv, sec.name section_name, sec.section_id
from EMPLOYEE e, DEPARTMENT d, TITLE t, SERIES s, LEVEL l, ACCOUNT acc, SECTION sec
where e.dept_id = d.dept_id and e.title_id = t.title_id and t.series_id=s.series_id 
and t.level_id=l.level_id and acc.account_id = e.account_id and e.section_id = sec.section_id;

#DROP VIEW SUBJECT_STANDARD;
CREATE VIEW SUBJECT_STANDARD AS
SELECT d.name dept_name, d.dept_id dept_id, sec.name section_name, sec.section_id,  t.name title_name, t.title_id title_id, s.name subject_name, 
s.subject_id subject_id, trn.standard, trn.certification
, cat.name cat_name, cat.category_id category_id, cls.name class_name, cls.class_id class_id
FROM DEPARTMENT d, SECTION sec, TITLE t, SUBJECT s, TRAINING_SYSTEM trn, CATEGORY cat, CLASS cls
where trn.dept_id=d.dept_id and trn.title_id=t.title_id and trn.subject_id=s.subject_id 
AND s.category_id = cat.category_id AND s.class_id = cls.class_id AND trn.section_id = sec.section_id;


CREATE VIEW EMPLOYEE_TRAINING_VIEW AS
SELECT e.emp_id, e.name emp_name, d.name dept_name, sec.name section_name, t.name title_name, s.name series_name, 
l.name level_name, sub.name subject_name, 
trn.grade, trn.training_hour, trn.training_date, trn.fee, trn.guarantee, trn.transfer, trn.certificate, trn.note, trn.report,
trn.compensate compensate, trn.training_type training_type
from EMPLOYEE e, DEPARTMENT d, TITLE t, SERIES s, LEVEL l, TRAINING_RECORD trn, SUBJECT sub, SECTION sec
where e.dept_id = d.dept_id and e.title_id = t.title_id and t.series_id=s.series_id 
and t.level_id=l.level_id and e.emp_id=trn.emp_id and trn.subject_id=sub.subject_id and e.section_id = sec.section_id;


CREATE OR REPLACE VIEW EMPLOYEE_ABILITY_VIEW_REQUIRED AS
SELECT e.emp_id emp_id, e.name emp_name, d.name dept_name, d.dept_id dept_id, t.name title_name, 
t.title_id title_id, s.name subject_name, s.subject_id subject_id, ab.required_grade, ab.grade, 
cat.name cat_name, cat.category_id category_id, c.name class_name, c.class_id class_id, trn.standard
FROM EMPLOYEE e, DEPARTMENT d, TITLE t, ABILITY ab, SUBJECT s, CATEGORY cat, CLASS c, TRAINING_SYSTEM trn
WHERE e.dept_id=d.dept_id and e.title_id=t.title_id and e.emp_id=ab.emp_id and ab.subject_id=s.subject_id 
and s.category_id=cat.category_id and s.class_id=c.class_id 
AND e.dept_id = trn.dept_id AND e.title_id = trn.title_id AND ab.subject_id = trn.subject_id;

CREATE VIEW EMPLOYEE_ABILITY_VIEW AS
SELECT e.emp_id emp_id, e.name emp_name, d.name dept_name, d.dept_id dept_id, t.name title_name, 
t.title_id title_id, s.name subject_name, s.subject_id subject_id, ab.required_grade, ab.grade, 
cat.name cat_name, cat.category_id category_id, c.name class_name, c.class_id class_id, ab.required
FROM EMPLOYEE e, DEPARTMENT d, TITLE t, ABILITY ab, SUBJECT s, CATEGORY cat, CLASS c
WHERE e.dept_id=d.dept_id and e.title_id=t.title_id and e.emp_id=ab.emp_id and ab.subject_id=s.subject_id 
and s.category_id=cat.category_id and s.class_id=c.class_id;

CREATE VIEW PROMOTION_VIEW AS
SELECT e.emp_id emp_id, e.name emp_name, d.name dept_name, d.dept_id dept_id, t.name title_name, t.title_id title_id, p.promotion_date promotion_date, p.devision devision
FROM EMPLOYEE e, DEPARTMENT d, TITLE t, PROMOTION p
WHERE e.emp_id = p.emp_id and d.dept_id = p.dept_id and t.title_id = p.title_id;

#INSERT INTO SECTION (name) VALUES("none");

/*
INSERT INTO DEPARTMENT (name) VALUES("研發部");
INSERT INTO DEPARTMENT (name) VALUES("行政部");
INSERT INTO DEPARTMENT (name) VALUES("行銷部");
INSERT INTO DEPARTMENT (name) VALUES("財務部");

INSERT INTO SERIES (name) VALUES("管理職系");
INSERT INTO SERIES (name) VALUES("行政職系");
INSERT INTO SERIES (name) VALUES("工程職系");

INSERT INTO LEVEL (name) VALUES("一職等");
INSERT INTO LEVEL (name) VALUES("二職等");
INSERT INTO LEVEL (name) VALUES("三職等");
INSERT INTO LEVEL (name) VALUES("四職等");
INSERT INTO LEVEL (name) VALUES("五職等");
INSERT INTO LEVEL (name) VALUES("六職等");
INSERT INTO LEVEL (name) VALUES("七職等");

INSERT INTO TITLE (name) VALUES("總經理");
INSERT INTO TITLE (name) VALUES("副總經理");
INSERT INTO TITLE (name) VALUES("經理");
INSERT INTO TITLE (name) VALUES("副經理");
INSERT INTO TITLE (name) VALUES("課長");
INSERT INTO TITLE (name) VALUES("副課長");
INSERT INTO TITLE (name) VALUES("組長");
INSERT INTO TITLE (name) VALUES("領班");
INSERT INTO TITLE (name) VALUES("資深高級專員");
INSERT INTO TITLE (name) VALUES("高級專員");
INSERT INTO TITLE (name) VALUES("專員");
INSERT INTO TITLE (name) VALUES("行政助理");
INSERT INTO TITLE (name) VALUES("資深高級工程師");
INSERT INTO TITLE (name) VALUES("高級工程師");
INSERT INTO TITLE (name) VALUES("工程師");

INSERT INTO CATEGORY (name) VALUES("核心職能");
INSERT INTO CATEGORY (name) VALUES("管理職能");
INSERT INTO CATEGORY (name) VALUES("專業職能");
INSERT INTO CATEGORY (name) VALUES("一般職能");

INSERT INTO CLASS (name) VALUES("軟體");
INSERT INTO CLASS (name) VALUES("財務");
INSERT INTO CLASS (name) VALUES("行政");
INSERT INTO CLASS (name) VALUES("技術");

INSERT INTO SUBJECT (name) VALUES("SEM/EDS儀器操作及原理");
INSERT INTO SUBJECT (name) VALUES("化合物半導體知識");
INSERT INTO SUBJECT (name) VALUES("市場調查");
INSERT INTO SUBJECT (name) VALUES("植物培養知識");
INSERT INTO SUBJECT (name) VALUES("SPAM");
INSERT INTO SUBJECT (name) VALUES("IQC檢測");
INSERT INTO SUBJECT (name) VALUES("FQC檢測");
INSERT INTO SUBJECT (name) VALUES("AIX");
INSERT INTO SUBJECT (name) VALUES("OracleDBA");
INSERT INTO SUBJECT (name) VALUES("AutoCAD");
INSERT INTO SUBJECT (name) VALUES("銀行實務");
INSERT INTO SUBJECT (name) VALUES("PPT");
INSERT INTO SUBJECT (name) VALUES("excel");
INSERT INTO SUBJECT (name) VALUES("ISO系統");
INSERT INTO SUBJECT (name) VALUES("採購與議價技巧");
INSERT INTO SUBJECT (name) VALUES("公司規章制度");
INSERT INTO SUBJECT (name) VALUES("問題分析與解決技巧");
INSERT INTO SUBJECT (name) VALUES("溝通協調技巧");

INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (1, 3, 6, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (1, 3, 7, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (1, 3, 8, 4);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (1, 3, 9, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (1, 3, 10, 4);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (1, 3, 1, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (2, 11, 11, 5);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (2, 11, 12, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (2, 11, 13, 5);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (2, 11, 14, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (3, 3, 15, 4);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (3, 3, 3, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (4, 4, 11, 4);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (4, 4, 18, 3);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (4, 4, 16, 4);
INSERT INTO TRAINING_SYSTEM (dept_id, title_id, subject_id, standard) VALUES (4, 4, 17, 3);

INSERT INTO EMPLOYEE (emp_id, dept_id, name, title_id) VALUES (102, 1, '林志強', 3);
INSERT INTO EMPLOYEE (emp_id, dept_id, name, title_id) VALUES (156, 4, '劉美蕙', 3);
INSERT INTO EMPLOYEE (emp_id, dept_id, name, title_id) VALUES (103, 2, '林慧秋', 7);
INSERT INTO EMPLOYEE (emp_id, dept_id, name, title_id) VALUES (105, 4, '羅紹純', 7);
INSERT INTO EMPLOYEE (emp_id, dept_id, name, title_id) VALUES (78, 2, '周亭君', 11);
INSERT INTO EMPLOYEE (emp_id, dept_id, name, title_id) VALUES (56, 1, '陳家宏', 13);

INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 6, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 7, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 8, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 9, 4.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 10, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 1, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 11, 2.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 18, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 16, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 17, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 4, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 13, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 12, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (102, 14, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 11, 1);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 18, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 16, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 17, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 13, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 12, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 14, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 15, 1.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (156, 11, 1);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 18, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 16, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 17, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 3, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 4, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 13, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 12, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 14, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 15, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (103, 11, 5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 11, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 18, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 16, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 17, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 3, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 13, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 12, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (105, 14, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (78, 11, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 6, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 7, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 8, 4.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 9, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 10, 1);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 1, 1);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 18, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 16, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 17, 4);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 4, 3);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 13, 2);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 12, 3.5);
INSERT INTO ABILITY (emp_id, subject_id, grade) VALUES (56, 14, 4);

UPDATE TITLE set series_id = 1 , level_id = 7  where title_id = 1;
UPDATE TITLE set series_id = 1 , level_id = 7  where title_id = 2;
UPDATE TITLE set series_id = 1 , level_id = 6  where title_id = 3;
UPDATE TITLE set series_id = 1 , level_id = 6  where title_id = 4;
UPDATE TITLE set series_id = 1 , level_id = 5  where title_id = 5;
UPDATE TITLE set series_id = 1 , level_id = 5  where title_id = 6;
UPDATE TITLE set series_id = 1 , level_id = 4  where title_id = 7;
UPDATE TITLE set series_id = 1 , level_id = 3  where title_id = 8;
UPDATE TITLE set series_id = 2 , level_id = 5  where title_id = 9;
UPDATE TITLE set series_id = 2 , level_id = 4  where title_id = 10;
UPDATE TITLE set series_id = 2 , level_id = 3  where title_id = 11;
UPDATE TITLE set series_id = 2 , level_id = 2  where title_id = 12;
UPDATE TITLE set series_id = 3 , level_id = 5  where title_id = 13;
UPDATE TITLE set series_id = 3 , level_id = 4  where title_id = 14;
UPDATE TITLE set series_id = 3 , level_id = 3  where title_id = 15;

UPDATE SUBJECT set class_id = 4, category_id = 1, classify = 4 where subject_id=1;
UPDATE SUBJECT set class_id = 4, category_id = 1, classify = 3 where subject_id=2;
UPDATE SUBJECT set class_id = 2, category_id = 3, classify = 5 where subject_id=3;
UPDATE SUBJECT set class_id = 1, category_id = 3, classify = 4 where subject_id=4;
UPDATE SUBJECT set class_id = 1, category_id = 3, classify = 4 where subject_id=5;
UPDATE SUBJECT set class_id = 1, category_id = 4, classify = 4 where subject_id=6;
UPDATE SUBJECT set class_id = 4, category_id = 4, classify = 4 where subject_id=7;
UPDATE SUBJECT set class_id = 4, category_id = 4, classify = 3 where subject_id=8;
UPDATE SUBJECT set class_id = 4, category_id = 1, classify = 4 where subject_id=9;
UPDATE SUBJECT set class_id = 3, category_id = 1, classify = 4 where subject_id=10;
UPDATE SUBJECT set class_id = 1, category_id = 4, classify = 4 where subject_id=11;
UPDATE SUBJECT set class_id = 1, category_id = 4, classify = 3 where subject_id=12;
UPDATE SUBJECT set class_id = 1, category_id = 3, classify = 4 where subject_id=13;
UPDATE SUBJECT set class_id = 4, category_id = 3, classify = 4 where subject_id=14;
UPDATE SUBJECT set class_id = 4, category_id = 3, classify = 5 where subject_id=15;
UPDATE SUBJECT set class_id = 4, category_id = 3, classify = 4 where subject_id=16;
UPDATE SUBJECT set class_id = 2, category_id = 2, classify = 5 where subject_id=17;
UPDATE SUBJECT set class_id = 3, category_id = 2, classify = 5 where subject_id=18;
UPDATE SUBJECT set class_id = 4, category_id = 2, classify = 4 where subject_id=19;
*/

