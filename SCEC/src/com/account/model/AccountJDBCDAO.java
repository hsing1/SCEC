package com.account.model;

import java.util.*;
import java.sql.*;

public class AccountJDBCDAO implements AccountDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO ACCOUNT (account_id ,account ,type) VALUES ( ACCOUNT_seq.NEXTVAL, ?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT account_id ,account ,type  FROM ACCOUNT order by account_id";
	private static final String GET_ONE_STMT = 
		"SELECT account_id ,account ,type   FROM ACCOUNT  where account_id = ?";
	private static final String DELETE = 
		"DELETE FROM ACCOUNT  where account_id = ?";
	private static final String UPDATE = 
		"UPDATE ACCOUNT  set account_id = ?, account=? where account_id=?"; 
	@Override
	public void insert(AccountVO accountVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, accountVO.getAccount());
			pstmt.setInt(2, accountVO.getType());
	
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(AccountVO accountVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
	
			
			pstmt.setString(1, accountVO.getAccount());
			pstmt.setInt(2, accountVO.getType());
			pstmt.setInt(3,accountVO.getAccountId());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer accountId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, accountId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AccountVO findByPrimaryKey(Integer accountId) {

		AccountVO accountVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, accountId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				accountVO = new AccountVO();
				accountVO.setAccountId(rs.getInt("account_id"));
				accountVO.setAccount(rs.getString("account"));
				accountVO.setType(rs.getInt("type"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accountVO;
	}

	@Override
	public List<AccountVO> getAll() {
		List<AccountVO> list = new ArrayList<AccountVO>();
		AccountVO accountVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				accountVO = new AccountVO();
				accountVO.setAccountId(rs.getInt("account_id"));
				accountVO.setAccount(rs.getString("account"));
				accountVO.setType(rs.getInt("type"));
				list.add(accountVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<AccountVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		AccountJDBCDAO dao = new AccountJDBCDAO();

		// 新增
//		AccountVO accountVO = new AccountVO();
//		accountVO.setAccount("人資人員");
//		accountVO.setType(1);
//		dao.insert(accountVO);	
//		accountVO.setAccount("部門主管");
//		accountVO.setType(2);
//		dao.insert(accountVO);	
//		System.out.println("insert sucessful");
	
		// 修改
//		AccountVO accountVO2 = new AccountVO();
//		accountVO2.setAccountId(1);
//		accountVO2.setAccount("使用者");
//		accountVO2.setType(1);
//		dao.update(accountVO2);
//		System.out.println(accountVO2.getAccountId());
//		System.out.println(accountVO2.getAccount());
//		System.out.println(accountVO2.getType());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// 查詢
//		AccountVO accountVO4 = dao.findByPrimaryKey(2);
//		System.out.println(accountVO4.getAccountId()+ ",");
//		System.out.println(accountVO4.getAccount()+ ",");
//		System.out.println(accountVO4.getType()+ ",");
//		System.out.println("select sucessful");

		// 查詢
//		List<AccountVO> list = dao.getAll();
//		for (AccountVO accountVO5 : list) {
//			System.out.println(accountVO5.getAccountId()+ ",");
//			System.out.println(accountVO5.getAccount()+ ",");
//			System.out.println(accountVO5.getType()+ ",");
//			System.out.println();
//		}
	}


}