package com.account.model;

import java.util.List;
import java.util.Map;

public class AccountService {

	private AccountDAO_interface dao;

	public AccountService() {
		dao = new  AccountDAO();
	}
	
	public AccountVO addAccount(String account, int priv) {

		AccountVO accountVO = new AccountVO();
		accountVO.setAccount(account);
		accountVO.setPriv(priv);
		accountVO.setPassword(account);

		dao.insert(accountVO);

		return accountVO;
	}

	public AccountVO addAccount(String account , Integer type, String password) {

		AccountVO accountVO = new AccountVO();
		accountVO.setAccount(account);
		accountVO.setPriv(type);
		accountVO.setPassword(password);

		dao.insert(accountVO);

		return accountVO;
	}

	public AccountVO updatePassword(String account, String newPasswd) {

		AccountVO accountVO = new AccountVO();
		accountVO.setAccount(account);
		accountVO.setPassword(newPasswd);

		dao.update(accountVO);

		return accountVO;
	}

	public void deleteTitle(Integer titleId) {
		dao.delete(titleId);
	}

	public AccountVO getOneTitle(Integer accountId) {
		return dao.findByPrimaryKey(accountId);
	}

	public List<AccountVO> getAll() {
		return dao.getAll();
	}
	
	public List<AccountVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public AccountVO getAccount(String account) {
		return dao.getAccount(account);
	}
}
