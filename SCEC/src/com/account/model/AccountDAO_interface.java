package com.account.model;

import java.util.*;

public interface AccountDAO_interface {
	public int insert(AccountVO accountVO);
	public void update(AccountVO accountVO);
	public void delete(Integer accountId);
	public AccountVO findByPrimaryKey(Integer accountId);
	public List<AccountVO> getAll();
	public List<AccountVO> getAll(Map<String, String[]> map); 
	public AccountVO getAccount(String account);
}
