package com.account.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class AccountDAO implements AccountDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO ACCOUNT (account, password, priv) VALUES (?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT account_id, account, priv FROM ACCOUNT order by account_id";
	private static final String GET_ONE_STMT = 
		"SELECT account_id, account, priv FROM ACCOUNT  where account_id = ?";
	private static final String DELETE = 
		"DELETE FROM ACCOUNT  where account_id = ?";
	private static final String UPDATE = 
		"UPDATE ACCOUNT  set password = ? where account=?"; 
	private static final String GET_ACCOUNT_STMT = 
		"SELECT * FROM ACCOUNT where account = ?";
	
	@Override
	public int insert(AccountVO accountVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		int accountId = 0;

		try {
			con = ds.getConnection();
			String cols[] = {"account_id"};
			//pstmt = con.prepareStatement(INSERT_STMT);
			pstmt = con.prepareStatement(INSERT_STMT, cols);		
			
			pstmt.setString(1, accountVO.getAccount());
			pstmt.setString(2, accountVO.getPassword());
			pstmt.setInt(3, accountVO.getPriv());
	
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				accountId = rs.getInt(1);
				accountVO.setAccountId(accountId);
			}
			rs.close();
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		return accountId;
	}

	@Override
	public void update(AccountVO accountVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
	
			pstmt.setString(1, accountVO.getPassword());
			pstmt.setString(2, accountVO.getAccount());

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, null, pstmt, true);
		}

	}

	@Override
	public void delete(Integer accountId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, accountId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AccountVO findByPrimaryKey(Integer accountId) {

		AccountVO accountVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, accountId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				accountVO = new AccountVO();
				accountVO.setAccountId(rs.getInt("account_id"));
				accountVO.setAccount(rs.getString("account"));
				accountVO.setPriv(rs.getInt("priv"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accountVO;
	}

	@Override
	public List<AccountVO> getAll() {
		List<AccountVO> list = new ArrayList<AccountVO>();
		AccountVO accountVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				accountVO = new AccountVO();
				accountVO.setAccountId(rs.getInt("account_id"));
				accountVO.setAccount(rs.getString("account"));
				accountVO.setPriv(rs.getInt("priv"));
				list.add(accountVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<AccountVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public AccountVO getAccount(String account) {
		AccountVO accountVO = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(GET_ACCOUNT_STMT);
			pstmt.setString(1, account);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				accountVO = new AccountVO();
				accountVO.setAccountId(rs.getInt("account_id"));
				accountVO.setAccount(rs.getString("account"));
				accountVO.setPassword(rs.getString("password"));
				accountVO.setPriv(rs.getInt("priv"));
				// Store the row in the vector
			}
			// Handle any SQL errors
		} catch (SQLException e) {
			throw new RuntimeException("A database error occured. "
					+ e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accountVO;
	}
	
}