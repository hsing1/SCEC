package com.level.model;

import java.util.List;
import java.util.Map;

public class LevelService {

	private LevelDAO_interface dao;

	public LevelService() {
		dao = new LevelDAO();
	}

	public LevelVO addLevel(String name) {

		LevelVO levelVO = new LevelVO();

		levelVO.setName(name);
	
		dao.insert(levelVO);

		return levelVO;
	}

	public LevelVO updateLevel(Integer levelId, String name) {

		LevelVO levelVO = new LevelVO();

		levelVO.setLevelId(levelId);
		levelVO.setName(name);
	
		dao.update(levelVO);

		return levelVO;
	}

	public void deleteLevels(Integer levelId) {
		dao.delete(levelId);
	}

	public LevelVO getOneLevel(Integer levelId) {
		return dao.findByPrimaryKey(levelId);
	}
	
	public LevelVO getOneLevel(String level) {
		return dao.findByLevelName(level);
	}

	public List<LevelVO> getAll() {
		return dao.getAll();
	}
	
	public List<LevelVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
