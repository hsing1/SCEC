package com.level.model;

import java.util.*;

public interface LevelDAO_interface {
          public void insert(LevelVO levelVO);
          public void update(LevelVO levelVO);
          public void delete(Integer levelId);
          public LevelVO findByPrimaryKey(Integer levelId);
          public LevelVO findByLevelName(String level);
          public List<LevelVO> getAll();
          //萬用複合查詢(傳入參數型態Map)(回傳 List)
         public List<LevelVO> getAll(Map<String, String[]> map); 
}
