package com.level.model;
import java.sql.Date;

public class LevelVO implements java.io.Serializable{
	private Integer levelId;
	private String name;

	public Integer getLevelId() {
		return levelId;
	}
	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
