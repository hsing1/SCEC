package com.login.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.department.model.DepartmentService;
import com.devision.model.DevisionService;
import com.section.model.SectionService;
import com.subject.model.SubjectService;

public class LoginFilter implements Filter {
	
	private FilterConfig config;
	
	public void init(FilterConfig config){
		this.config = config;
	}
	
	public void destroy(){
		config = null;
	}
    
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws ServletException, IOException{
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		
//		res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
//		res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
//		res.setDateHeader("Expires", 0); // Proxies.
		
		HttpSession session = req.getSession();
		String ip = req.getRemoteAddr();
		String host = req.getRemoteHost();
		String user = req.getRemoteUser();
		String authType = req.getAuthType();
		String scheme = req.getScheme();
		String server = req.getServerName();
		String contextPath = req.getContextPath();
		String servletPath = req.getServletPath();
		String servletName = req.getServerName();
		String pathInfo = req.getPathInfo();
		String pathTranslated = req.getPathTranslated();
		String requestURI = req.getRequestURI();
		StringBuffer requestURL = req.getRequestURL();
		int port = req.getServerPort();
		
		Object account = session.getAttribute("account"); 
		
		if (account == null) {
			session.setAttribute("location_back", req.getRequestURI());
			res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
			return;
		}else{
			if (!account.equals("hr")) {
				if (DevisionService.devisionMap == null)
					DevisionService.setDevisionMap();
				if (DepartmentService.deptMap == null)
					DepartmentService.setDeptMap();
				if (SectionService.sectionMap == null)
					SectionService.setSectionMap();
				if (SubjectService.subjectMap == null)
					SubjectService.setSubjectMap();
			}
			chain.doFilter(request, response);
		}
	}
}
