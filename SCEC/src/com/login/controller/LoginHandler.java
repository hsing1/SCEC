package com.login.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
//import com.member.model.*;
import com.account.model.*;
import com.employee.model.*;

public class LoginHandler extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("Big5");
		res.setContentType("text/html; charset=Big5");
		String action = req.getParameter("action");
		PrintWriter out = res.getWriter();
		EmployeeViewService empSvc = null;
		EmployeeViewVO myEmpVO = null;

		HttpSession session = req.getSession();

		System.out.println(132 + action);
		// System.out.println(session.getAttribute("location").toString());

		if ("Logout".equals(action)) {
			session.invalidate();
			RequestDispatcher successView = req.getRequestDispatcher("/login/login.jsp");
			successView.forward(req, res);
			return;
		}

		if ("Login".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			String account = req.getParameter("account");
			if (account == null || (account.trim()).length() == 0) {
				errorMsgs.add("請輸入帳號");
			}
			String password = req.getParameter("password");
			if (password == null || (password.trim()).length() == 0) {
				errorMsgs.add("請輸入密碼");
			}
			if (!errorMsgs.isEmpty()) {
				RequestDispatcher failureView = req
						.getRequestDispatcher("/login/login.jsp");
				failureView.forward(req, res);
				return;
			}
			AccountService accSvc = new AccountService();
			AccountVO accountVO = accSvc.getAccount(account);
			try {
				if (account.equals("hr")) {
					if (password.equals("1234")) {
						session.setAttribute("account", account);
						res.sendRedirect(req.getContextPath() + "/index.jsp"); 
						return;
					}
				}
				
				if (!accountVO.getAccount().equals(account)) {
					out.println("<HTML><HEAD><TITLE>Access Denied</TITLE></HEAD>");
					out.println("<BODY>�A���b�� , �K�X�L��!<BR>");
					out.println("�Ы������s�n�J <A HREF=" + req.getContextPath()
							+ "/index.jsp>���s�n�J</A>");
					out.println("</BODY></HTML>");
				} else if (!accountVO.getPassword().equals(password)) {
					errorMsgs.add("密碼錯誤");
					if (!errorMsgs.isEmpty()) {
						RequestDispatcher failureView = req
								.getRequestDispatcher("/login/login.jsp");
						failureView.forward(req, res);
						return;
					}
				} else {
					empSvc = new EmployeeViewService();
					session.setAttribute("account", account);
					myEmpVO = empSvc.getOneEmpByAccount(account, 0);
					session.setAttribute("myEmpVO", myEmpVO);
					//String empId = empSvc.getId(account);
					//session.setAttribute("memberId", memberId);
					try {
						String location = (String) session.getAttribute("location");
						if (location != null) {
							session.removeAttribute("location");
							res.sendRedirect(location);
							return;
						}
					} catch (Exception ignored) {
					}
					// res.sendRedirect("/index.html"); //*�u�@3: (-�p�L:
					// �L�ӷ�����,�h���ɦ�index.html����)
					res.sendRedirect(req.getContextPath() + "/index.jsp"); 
				}
			} catch (Exception e) {
				errorMsgs.add("�b����~�A�п�J���T�b��");
				errorMsgs.add("�K�X��~�A�п�J���T�K�X");
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/login/login.jsp");
					failureView.forward(req, res);
					return;
				}

			}
		}
	}
}
