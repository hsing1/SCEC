package com.login.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.employee.model.*;

public class PrivilegeFilter implements Filter {
	
	private FilterConfig config;
	
	public void init(FilterConfig config){
		this.config = config;
	}
	
	public void destroy(){
		config = null;
	}
    
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws ServletException, IOException{
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		
//		res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
//		res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
//		res.setDateHeader("Expires", 0); // Proxies.
		
		HttpSession session = req.getSession();
		String account = (String)session.getAttribute("account"); 
		
		if (account == null) {
			session.setAttribute("prevPage", req.getRequestURI());
			res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
			return;
		} else if (account.equals("hr")) {
			int priv = 1;
			EmployeeViewVO empVO = new EmployeeViewVO();
			
			empVO.setAccount("hr");
			empVO.setEmpId(1);
			empVO.setEmpName("hr");
			
			session.setAttribute("priv", priv);
			request.setAttribute("empVO", empVO);
			
			chain.doFilter(request, response);
		} else {
			int empId = 0, priv = 0, deptId = 0;
			EmployeeViewService empSvc = null;
			EmployeeViewVO empVO = null;
			
			empSvc = new EmployeeViewService();
			empVO = empSvc.getOneEmpByAccount(account, 0);
			empId = empVO.getEmpId();
			deptId = empVO.getDeptId();
			
			priv = empVO.getPriv();
			session.setAttribute("priv", priv);
			request.setAttribute("empVO", empVO);
			
			chain.doFilter(request, response);
		}
	}
}
