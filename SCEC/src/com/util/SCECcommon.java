package com.util;

import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.sql.DataSource;
import com.section.model.*;
import com.department.model.*;
import com.subject.model.*;
import com.title.model.*;
import com.employee.model.*;
import com.devision.model.*;


public class SCECcommon {
	public static String ctxStr = "java:comp/env/jdbc/SC_PROJECT_TEST";
	private static DataSource ds = null;
	private static String [] tabArray = {
		  "DEPT_TITLE"
		, "TRAINING_RECORD"
		, "ABILITY"
		, "PROMOTION"
		, "EMPLOYEE"
		, "TRAINING_SYSTEM"
		, "SUBJECT"
		, "CLASS"
		, "SALARY"
		, "TITLE"
		, "DEPARTMENT"
		, "DEVISION"
		, "SECTION"
		, "CATEGORY"
		, "LEVEL"
		, "SERIES"
		, "ACCOUNT"
	};
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/SC_PROJECT");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String DELETE_ALL = "DELETE FROM ";
	
	public static void checkDevision(String devision) {
	    if (!DevisionService.devisionMapInv.containsKey(devision))
	    	throw new RuntimeException("Cannot find devision " + devision);
	}
	
	public static void checkDept(String dept) {
	    if (!DepartmentService.deptMapInv.containsKey(dept))
	    	throw new RuntimeException("Cannot find department " + dept);
	}
	
	public static void checkSection(String section) {
	    if (!SectionService.sectionMapInv.containsKey(section))
	    	throw new RuntimeException("Cannot find section " + section);
	}
	
	public static void checkTitle(String title) {
	    if (!TitleService.titleMapInv.containsKey(title))
	    	throw new RuntimeException("Cannot find title " + title);
	}
	
	public static void checkSubject(String subject) {
	    if (!SubjectService.subjectMapInv.containsKey(subject))
	    	throw new RuntimeException("Cannot find subject " + subject);
	}
	
	public static void checkEmp(int empId, String name) {
	    if (!EmployeeService.empMapInv.containsKey(name))
	    	throw new RuntimeException("Cannot find employee " + name);
	    
	    if (!EmployeeService.empMap.containsKey(empId))
	    	throw new RuntimeException("Cannot find employee ID " + empId);
	    
	    if (empId != EmployeeService.empMapInv.get(name))
	    	throw new RuntimeException("Employee ID and name mismatch " + empId + "," + name);
	}

	public static void truncateTable() {
		String stmt = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		SectionService secSvc = null;
		
		try {
			con = ds.getConnection();
			stmt = "SET FOREIGN_KEY_CHECKS = 0";
			pstmt = con.prepareStatement(stmt);
			pstmt.executeUpdate();
			for(String table : tabArray) {
				stmt = "truncate table " + table;
				pstmt = con.prepareStatement(stmt);
				pstmt.executeUpdate();
			}
			pstmt = con.prepareStatement(stmt);
			pstmt.executeUpdate();
			stmt = "SET FOREIGN_KEY_CHECKS = 1";
			
			secSvc = new SectionService();
			secSvc.insert("none");
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con, null, pstmt, true);
		}
	}
	
	public static void deleteTable() {
		String stmt = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		SectionService secSvc = null;
		
		try {
			con = ds.getConnection();
			for(String table : tabArray) {
				stmt = DELETE_ALL + table;
				pstmt = con.prepareStatement(stmt);
				pstmt.executeUpdate();
				stmt = "ALTER TABLE " + table + " AUTO_INCREMENT = 1";
				pstmt = con.prepareStatement(stmt);
				pstmt.executeUpdate();
			}
			
			secSvc = new SectionService();
			secSvc.insert("none");
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con, null, pstmt, true);
		}
	}
	
	public static EmployeeViewVO getMyEmp(HttpServletRequest req, HttpServletResponse res) 
		throws ServletException {
		
		HttpSession session = req.getSession();
		String account = (String)session.getAttribute("account");
		EmployeeViewService empSvc = null;
		EmployeeViewVO empVO = null;
		
		try {
			if (account == null) {
				session.setAttribute("prevPage", req.getRequestURI());
				res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
			}
			
			empSvc = new EmployeeViewService();
			empVO = empSvc.getOneEmpByAccount(account, 0);
			
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		return empVO;
	}
	
	public static void close(Connection con, ResultSet rs, PreparedStatement pstmt, boolean closeCon) {
		
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
		
		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
		
		if (closeCon) {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
}
