package com.util;

import java.io.IOException;
import java.io.File;
import java.io.PrintWriter;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;


/**
 * Servlet implementation class SCECinitial
 */
public class SCECinitial extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SCECinitial() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		Boolean MACOS = false;
		//String action = request.getParameter("action");
		//String requestURL = request.getParameter("requestURL");
		String os = System.getProperty("os.name").toLowerCase();
		MultipartRequest multi = null;
		String excelPath = request.getServletContext().getRealPath("excel_upload");
		/*
		if (os.contains("mac")) 
			multi = new MultipartRequest(request, "/Users/MBP/Documents", 5 * 1024 * 1024, "Big5"); 
		else
			multi = new MultipartRequest(request, "C:\\images", 5 * 1024 * 1024, "Big5"); 
		*/
		multi = new MultipartRequest(request, excelPath, 5 * 1024 * 1024, "Big5"); 
		String action = multi.getParameter("action");
		String requestURL = multi.getParameter("requestURL");
		PrintWriter out = response.getWriter();
		String saveDir = null;
		boolean res = false;
		String errMsg = null;
		List<String> errorMsgs = new LinkedList<String>();
		
		request.setAttribute("errorMsgs", errorMsgs);
		
		if ("readExcel".equals(action)) {
			//saveDir = request.getServletContext().getRealPath(".");
			//saveDir = request.getServletContext().getRealPath("/SCEC"); 
			//C:\Project\Jcarpool\workspace\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\SCEC\SCEC
			Enumeration files = multi.getFileNames();
		    while (files.hasMoreElements()) {
		    	String name = (String)files.nextElement();
		    	String filename = multi.getFilesystemName(name);
		    	String type = multi.getContentType(name);
		    	File f = multi.getFile(name);
		    	//out.println("name: " + name);
		    	//out.println("filename: " + filename);
		    	//out.println("type: " + type);
		    	if (f != null) {
		    		//out.println("length: " + f.length());
		    		
		    		try {
		    			//SCECcommon.deleteTable();
		    			SCECcommon.truncateTable();
		    			SCECworkbookUtil.loadExcel(f);
		    			
		    			request.setAttribute("uploadStatus", res);
		    			//String url = "/util/uploadStatus.jsp";
		    			String url = "/util/readExcel.jsp";

		    			RequestDispatcher successView = request.getRequestDispatcher(url);
		    			successView.forward(request, response);
		    		} catch (ServletException e) {
		    			errMsg = e.getMessage();
		    			errorMsgs.add(errMsg);
		    			String url = "/util/readExcel.jsp";

		    			RequestDispatcher successView = request.getRequestDispatcher(url);
		    			successView.forward(request, response);
		    		} catch (RuntimeException r) {
		    			errMsg = r.getMessage();
		    			errorMsgs.add(errMsg);
		    			String url = "/util/readExcel.jsp";

		    			RequestDispatcher successView = request.getRequestDispatcher(url);
		    			successView.forward(request, response);
		    		}
		    	}
		    }
		} //end of processing action
	}

}
