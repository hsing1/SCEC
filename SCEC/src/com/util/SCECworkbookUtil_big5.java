package com.util;

import java.io.*;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.poi.poifs.filesystem.*;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;

import com.ability.model.AbilityService;
import com.category.model.CategoryService;
import com.category.model.CategoryVO;
import com.classes.model.ClassService;
import com.department.model.*;
import com.employee.model.EmployeeService;
import com.level.model.LevelService;
import com.title.model.*;
import com.training.model.TrainingRecordService;
import com.training.model.TrainingService;
import com.series.model.*;
import com.subject.model.SubjectService;

public class SCECworkbookUtil_big5 {
	private static String [] itemArr = SCECLang.TZ;
	private static String [] sheetArr = SCECLang.TZ;
	private static String deptStr = itemArr[SCECenum.ITEM_DEPT.ordinal()];
	private static String seriesStr = itemArr[SCECenum.ITEM_SERIES.ordinal()];
	private static String levelStr = itemArr[SCECenum.ITEM_LEVEL.ordinal()];
	private static String titleStr = itemArr[SCECenum.ITEM_TITLE.ordinal()];
	private static String categoryStr = itemArr[SCECenum.ITEM_CATEGORY.ordinal()];
	private static String classStr = itemArr[SCECenum.ITEM_CLASS.ordinal()];
	private static String subjectStr = itemArr[SCECenum.ITEM_SUBJECT.ordinal()];
	private static String standardStr = itemArr[SCECenum.ITEM_STANDARD.ordinal()];
	private static String certificationStr= itemArr[SCECenum.ITEM_CERTIFICATION.ordinal()];
	private static String empIdStr = itemArr[SCECenum.ITEM_EMP_ID.ordinal()];
	private static String empOnboardStr = itemArr[SCECenum.ITEM_EMP_ONBOARD.ordinal()];
	private static String empLeavedateStr = itemArr[SCECenum.ITEM_EMP_LEAVEDATE.ordinal()];
	private static String nameStr = itemArr[SCECenum.ITEM_NAME.ordinal()];
	private static String trainingSubjectStr = itemArr[SCECenum.ITEM_TRAINING_SUBJECT.ordinal()];
	private static String trainingDateStr = itemArr[SCECenum.ITEM_TRAINING_DATE.ordinal()];
	private static String trainingHourStr = itemArr[SCECenum.ITEM_TRAINING_HOUR.ordinal()];
	private static String trainingFeeStr = itemArr[SCECenum.ITEM_TRAINING_FEE.ordinal()];
	private static String trainingTransferStr = itemArr[SCECenum.ITEM_TRAINING_TRANSFER.ordinal()];
	private static String trainingGuaranteeStr = itemArr[SCECenum.ITEM_TRAINING_GUARANTEE.ordinal()];
	
	public static void LoadExcel(File xlsf) {
		Workbook wb = null;
		Sheet sheet = null;
		String sName = null;
		int numOfSheet = 0;
		SCECstatus.status = SCECstatusEnum.SCEC_LOAD_EXCEL;
		
		try {
			wb = WorkbookFactory.create(xlsf);
			
			//wb = WorkbookFactory.create(new File("/Users/MBP/Project/SCEC/職能分析表.xlsx"));
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		numOfSheet = wb.getNumberOfSheets();
		for (int ns = 0; ns < numOfSheet; ns++) {
			sheet = wb.getSheetAt(ns);
			sName = sheet.getSheetName();
			if (sName.equals(sheetArr[SCECenum.SHEET_ORG.ordinal()])) { //組織架構
				setupOrganization(sheet);
			} else if (sName.equals(sheetArr[SCECenum.SHEET_TITLE.ordinal()])) { //職稱設定
				setupTitle(sheet);
			} else if (sName.equals(sheetArr[SCECenum.SHEET_SUBJECT.ordinal()])) { //科目項目
				setupSubject(sheet);
			} else if (sName.equals(sheetArr[SCECenum.SHEET_TRAINING.ordinal()])) { //訓練體系設定 
				setupTrainingSystem(sheet);
			} else if (sName.equals(sheetArr[SCECenum.SHEET_EMP.ordinal()])) { //員工資料
				createEmployee(sheet);
			} else if (sName.equals(sheetArr[SCECenum.SHEET_ABILITY.ordinal()])) { //職能評量
				createEmpAbility(sheet);
			} else if (sName.equals(sheetArr[SCECenum.SHEET_TRAINING_RECORD.ordinal()])) { //訓練紀錄
				createTrainingRecord(sheet);
			}
		}
	}
	
	public static void setupOrganization(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		
		String sName = null;
		sName = sheet.getSheetName();
		if (!sName.equals("組織架構")) {
			return;
		}
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		System.out.println("Row start=" + rowStart);
		System.out.println("Row End=" + rowEnd);
		value = null;
	    // Decide which rows to process
	    //rowStart = Math.min(15, sheet.getFirstRowNum());
	    //rowEnd = Math.max(1400, sheet.getLastRowNum());

		//Check header
	    Row r1 = sheet.getRow(rowStart);
	    int firstCol = r1.getFirstCellNum();
	    int lastCol = r1.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	Cell c1 = r1.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	        System.out.printf("\n(%d,%d)", rowStart, colIdx);
	        if (c1 == null) {
	        	continue;
	        }
	        
	        value = getCellValueStr(c1);
	        printCellValue(c1);
	        System.out.println();
	        currentCol = c1.getColumnIndex();
	        System.out.printf("Current column=(%d,%d)", colIdx, currentCol);
	        if (value.equals(deptStr)) { //部門
	        	//createDepartment(sheet, colIdx);
	        	createOrganization(sheet, colIdx, 1);
	        } else if (value.equalsIgnoreCase(seriesStr)) { //職系
	        	createOrganization(sheet, colIdx, 2);
	        } else if (value.equalsIgnoreCase(levelStr)) { //職等
	        	createOrganization(sheet, colIdx, 3);
	        } else if (value.equalsIgnoreCase(titleStr)) { //職稱
	        	createOrganization(sheet, colIdx, 7);
	        } else if (value.equals(categoryStr)) { //職能類別
	        	createOrganization(sheet, colIdx, 4);
	        } else if (value.equalsIgnoreCase(classStr)) { //職能項目
	        	createOrganization(sheet, colIdx, 5);
	        } else if (value.equalsIgnoreCase(subjectStr)) { //科目
	        	createOrganization(sheet, colIdx, 6);
	        }
	        //TODO: need check if all columns are found, if false then break and issue error
	        
	    }
	    
	    for (int rowNum1 = rowStart; rowNum1 <= rowEnd + 1; rowNum1++) {
	       r1 = sheet.getRow(rowNum1);

	       if (r1 == null)
	    	   continue;
	       //int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
	       firstCol = r1.getFirstCellNum();
	       lastCol = r1.getLastCellNum();
	       System.out.printf("\nFirst column = %d, Last column = %d\n", firstCol, lastCol);

	       for (int cn = firstCol; cn < lastCol; cn++) {
	          Cell c1 = r1.getCell(cn, Row.RETURN_BLANK_AS_NULL);
	          System.out.printf("(%d,  %d) = ", rowNum1, cn);
	          if (c1 == null) {
	        	  value = "null";
	          } else {
	        	  value = getCellValueStr(c1);
	        	  printCellValue(c1);
	          }
	          //System.out.println(value);
	       }
	    }
	}
	
	private static void createDepartment(Sheet sheet, int colIdx) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		DepartmentVO deptVO = null;
		String dName = null;
		DepartmentService deptSvc = new DepartmentService();
		
		for (int r = firstRow + 1; r < lastRow; r++) {
			row = sheet.getRow(r);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null)
				continue;
			
			dName = getCellValueStr(cell);
			deptSvc.addDepartment(dName);
			
			//printCellValue(cell);
		}
	}
	
	private static void createEmployee(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		Map<String, Integer> header = null;
		
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    int empId;
	    String dept = null, title = null, empName = null;
	    Date onboardDate = null, leaveDate = null;
	    EmployeeService empSvc = new EmployeeService();
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
			cell = row.getCell(header.get(empIdStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empId = getCellValueInt(cell);
	    	
			cell = row.getCell(header.get(deptStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			dept= getCellValueStr(cell);
			
			cell = row.getCell(header.get(titleStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			title = getCellValueStr(cell);
			
			cell = row.getCell(header.get(nameStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empName = getCellValueStr(cell);
			
			cell = row.getCell(header.get(empOnboardStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			onboardDate = cell.getDateCellValue();
			
			cell = row.getCell(header.get(empLeavedateStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
				leaveDate = null;
			} else {
				leaveDate = cell.getDateCellValue();
			}
			
			empSvc.addEmployee(empId, dept, title, empName, onboardDate, leaveDate);
	    }
	}
	
	private static void createEmpAbility(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		Map<String, Integer> header = null;
		Map<String, Integer> empMap = new HashMap<String, Integer>();
		Map<String, Integer> subjectMap = new HashMap<String, Integer>();
		String [] empKey = {"工號", "部門", "職稱", "姓名"};
		AbilityService abSvc = new AbilityService();
		double grade = 0;
		
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    
	    header = readHeader(sheet);
	    for (String key : empKey) {
	    	empMap.put(key , header.get(key));
	    	header.remove(key);
	    }
	    
	    for (String key : header.keySet()) {
	    	subjectMap.put(key, header.get(key));
	    }
	    
	    String dept = null, title = null, empName = null;
	    int empId = 0;
	    boolean certification = false;
	    TrainingService trnSvc = new TrainingService();
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
			cell = row.getCell(empMap.get(empKey[0]), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empId = getCellValueInt(cell);
	    	
			cell = row.getCell(empMap.get(empKey[1]), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			dept= getCellValueStr(cell);
			
			cell = row.getCell(empMap.get(empKey[2]), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			title = getCellValueStr(cell);
			
			cell = row.getCell(empMap.get(empKey[3]), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empName = getCellValueStr(cell);
			
			/*
			 * Reading subject grade and store in DB
			 */
			for (String subject : subjectMap.keySet()) {
				cell = row.getCell(header.get(subject), Row.RETURN_NULL_AND_BLANK);
				if (cell == null) {
					System.out.println("Error: null cell");
					continue;
				}
				System.out.println(subject);
				grade = getCellValueDouble(cell);
				abSvc.addAbility(empId, subject, grade);
			}
			
	    }
		
	}
	
	private static void createTrainingRecord(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		Map<String, Integer> header = null;
		String subject = null, note = null;
	    String dept = null, title = null, empName = null;
		Date trainingDate = null, transferDate = null, guarantee = null;
		int empId, trainingHour, trainingFee;
	    TrainingRecordService trnSvc = new TrainingRecordService();
		
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
			cell = row.getCell(header.get(empIdStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empId = getCellValueInt(cell);
	    	
			cell = row.getCell(header.get(trainingSubjectStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			subject = getCellValueStr(cell);
			
			cell = row.getCell(header.get(nameStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empName = getCellValueStr(cell);
			
			cell = row.getCell(header.get(trainingHourStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			trainingHour = getCellValueInt(cell);
			
			cell = row.getCell(header.get(trainingFeeStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			trainingFee = getCellValueInt(cell);
			
			cell = row.getCell(header.get(trainingDateStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				trainingDate = cell.getDateCellValue();
			
			cell = row.getCell(header.get(trainingTransferStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				transferDate = cell.getDateCellValue();
			
			cell = row.getCell(header.get(trainingGuaranteeStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				guarantee = cell.getDateCellValue();
			
			trnSvc.addTrainingRecord(empId, subject, trainingDate, transferDate, guarantee, 
					trainingHour, trainingFee, note);
	    }
	}
	
	private static void createOrganization(Sheet sheet, int colIdx, int orgType) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		DepartmentVO deptVO = null;
		String name = null;
		int count = 0;
		
		for (int r = firstRow + 1; r <= lastRow; r++) {
			row = sheet.getRow(r);
			if (isRowEmpty(row))
				continue;
			
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				continue;
			
			count++;
			name = getCellValueStr(cell);
			switch(orgType) {
			case 1: //build department
				DepartmentService deptSvc = new DepartmentService();
				deptSvc.addDepartment(name);
				break;
			case 2: //build series
				SeriesService seriesSvc = new SeriesService();
				seriesSvc.addSeries(name);
				break;
			case 3: //build level 
				LevelService levelSvc = new LevelService();
				levelSvc.addLevel(name);
				break;
			case 4: //build category 
				CategoryService categorySvc = new CategoryService();
				categorySvc.addCategory(name);
				break;
			case 5: //build class 
				ClassService classSvc = new ClassService();
				classSvc.addClass(name);
				break;
			case 6: //build subject 
				SubjectService subjectSvc = new SubjectService();
				subjectSvc.addSubject(null, name, null);
				break;
			case 7: //build title 
				TitleService titleSvc = new TitleService();
				titleSvc.addTitle(null, name, null, null);
				break;
			}
			
			//printCellValue(cell);
		}
		switch(orgType) {
		case 4:
			CategoryService.numOfCat = count;
			break;
		case 6:
			SubjectService.setNumOfSubject();
			break;
		}
	}

	private static void setupTitle(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		sName = sheet.getSheetName();
		HashMap<String, Integer> header = new HashMap<String, Integer>();
		Row row = null;
		Cell cell = null;
		
		if (!sName.equals("職稱設定")) {
			return;
		}
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		System.out.println("Row start=" + rowStart);
		System.out.println("Row End=" + rowEnd);
		value = null;
	    // Decide which rows to process
	    //rowStart = Math.min(15, sheet.getFirstRowNum());
	    //rowEnd = Math.max(1400, sheet.getLastRowNum());

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	        System.out.printf("\n(%d,%d)", rowStart, colIdx);
	        if (cell == null) {
	        	continue;
	        }
	        
	        value = getCellValueStr(cell);
	        printCellValue(cell);
	        System.out.println();
	        currentCol = cell.getColumnIndex();
	        System.out.printf("Current column=(%d,%d)", colIdx, currentCol);
	        header.put(value, currentCol);
	    }
	    
	    //Pass2:Read data
	    String title = null, level = null, series = null;
	    TitleService titleSvc = new TitleService();
	    for (int rowIdx = rowStart + 1; rowIdx < rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
			cell = row.getCell(header.get(titleStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			title = getCellValueStr(cell);
			cell = row.getCell(header.get(levelStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			level = getCellValueStr(cell);
			cell = row.getCell(header.get(seriesStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			series = getCellValueStr(cell);
			
			titleSvc.setupTitle(title, series, level);
	    }
	}
	
	private static void setupSubject(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		HashMap<String, Integer> header = null;
		Row row = null;
		Cell cell = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    String subject = null, category = null, classes = null;
	    int classify = 0;
	    SubjectService subSvc = new SubjectService();
	    for (int rowIdx = rowStart + 1; rowIdx < rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
			cell = row.getCell(header.get(subjectStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			subject = getCellValueStr(cell);
			
			cell = row.getCell(header.get(categoryStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			category = getCellValueStr(cell);
			
			cell = row.getCell(header.get(classStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			classes = getCellValueStr(cell);
			
			cell = row.getCell(header.get("分級"), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			classify = getCellValueInt(cell);
			
			subSvc.setupSubject(subject, category, classes, classify);
	    }
	}
	
	private static void setupTrainingSystem(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		HashMap<String, Integer> header = null;
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    String dept = null, title = null, subject = null;
	    int standard = 0;
	    boolean certification = false;
	    TrainingService trnSvc = new TrainingService();
	    for (int rowIdx = rowStart + 1; rowIdx < rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	//部門
			cell = row.getCell(header.get(deptStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			dept= getCellValueStr(cell);
			
			//職稱
			cell = row.getCell(header.get(titleStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			title = getCellValueStr(cell);
			
			//科目
			cell = row.getCell(header.get(subjectStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			subject = getCellValueStr(cell);
			
			//標準
			cell = row.getCell(header.get(standardStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			standard = getCellValueInt(cell);
			
			//認證
			cell = row.getCell(header.get(certificationStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			ans = getCellValueStr(cell);
			
			if (ans.equals("y"))
				certification = true;
			else
				certification = false;
			
			trnSvc.addTraining(dept, title, subject, standard, certification);
	    }
	}
	
	private static HashMap<String, Integer> readHeader(Sheet sheet) {
		HashMap<String, Integer> header = new HashMap<String, Integer>();
		int rowStart = sheet.getFirstRowNum();
		String value = null;
		Row row = null;
		Cell cell = null;

	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	    	
	        if (cell == null)
	        	continue;
	        
	        value = getCellValueStr(cell);
	        currentCol = cell.getColumnIndex();
	        header.put(value, currentCol);
	    }
		
		return header;
	}

	private static void readColumn(Sheet sheet, int colIdx) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		
		for (int r = firstRow + 1; r < lastRow; r++) {
			row = sheet.getRow(r);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null)
				continue;
			
			printCellValue(cell);
		}
	}
	
    /**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param halign the horizontal alignment for the cell.
     */
    private static void createCell(Workbook wb, Row row, short column, short halign, short valign) {
        Cell cell = row.createCell(column);
        cell.setCellValue("Align It");
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(halign);
        cellStyle.setVerticalAlignment(valign);
        cell.setCellStyle(cellStyle);
    }
    
    private static short getAllignmentType(Cell cell) {
    	CellStyle style = cell.getCellStyle();
    	short type = style.getAlignment();
    	switch(type) {
		case CellStyle.ALIGN_GENERAL:
			System.out.println("ALIGN_GENERAL");
			break;
		case CellStyle.ALIGN_LEFT:
			System.out.println("ALIGN_LEFT");
			break;
		case CellStyle.ALIGN_CENTER:
			System.out.println("ALIGN_CENTER");
			break;
		case CellStyle.ALIGN_RIGHT:
			System.out.println("ALIGN_RIGHT");
			break;
		case CellStyle.ALIGN_FILL:
			System.out.println("ALIGN_FILL");
			break;
		case CellStyle.ALIGN_JUSTIFY:
			System.out.println("ALIGN_JUSTIFY");
			break;
		case CellStyle.ALIGN_CENTER_SELECTION:
			System.out.println("ALIGN_CENTER_SELECTION");
			break;
		default:
			System.out.println("Error:Unknow Align Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static int getCellType(Cell cell) {
    	int type = cell.getCellType();
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			System.out.println("ALIGN_GENERAL");
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.println("ALIGN_LEFT");
			break;
		case Cell.CELL_TYPE_ERROR:
			System.out.println("ALIGN_CENTER");
			break;
		case Cell.CELL_TYPE_FORMULA:
			System.out.println("ALIGN_RIGHT");
			break;
		case Cell.CELL_TYPE_NUMERIC:
			System.out.println("ALIGN_FILL");
			break;
		case Cell.CELL_TYPE_STRING:
			System.out.println("ALIGN_JUSTIFY");
			break;
		default:
			System.out.println("Error:Unknow Cell Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static String getCellValueStr(Cell cell) {
    	int type = cell.getCellType();
    	String value = null;
    	
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			value = "Blank";
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			Boolean boVal = cell.getBooleanCellValue();
			value = boVal.toString();
			break;
		case Cell.CELL_TYPE_ERROR:
			Byte btVal = cell.getErrorCellValue();
			value = btVal.toString();
			break;
		case Cell.CELL_TYPE_FORMULA:
			value = cell.getCellFormula();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			Double dVal = cell.getNumericCellValue();
			value = dVal.toString();
			break;
		case Cell.CELL_TYPE_STRING:
			value = cell.getStringCellValue();
			break;
		default:
			type = -1;
			break;
		}
		//System.out.printf("Value = %s\n", value);
    	
    	return value;
    }
    
    private static double getCellValueDouble(Cell cell) {
    	int type = cell.getCellType();
    	double value = 0;
    	
    	switch(type) {
		case Cell.CELL_TYPE_NUMERIC:
			value = cell.getNumericCellValue();
			break;
		default:
			type = -1;
			break;
		}
    	
    	return value;
    }
    
    private static int getCellValueInt(Cell cell) {
    	int type = cell.getCellType();
    	int value = 0;
    	
    	switch(type) {
		case Cell.CELL_TYPE_NUMERIC:
			value = (int)cell.getNumericCellValue();
			break;
		default:
			type = -1;
			break;
		}
    	
    	return value;
    }
    
    private static void printCellValue(Cell cell) {
    	int type = cell.getCellType();
    	
        CellReference cellRef = new CellReference(cell.getRowIndex(), cell.getColumnIndex());
        System.out.print(cellRef.formatAsString());
        System.out.print("=");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                System.out.printf("%s", cell.getRichStringCellValue().getString());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    System.out.printf("%s", cell.getDateCellValue());
                } else {
                    System.out.printf("%s", cell.getNumericCellValue());
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                System.out.printf("%s", cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                System.out.printf("%s", cell.getCellFormula());
                break;
            default:
                System.out.printf("Error:unknow cell value type");
        }
    }
    
    private static boolean isRowEmpty(Row row) {
    	if (row == null)
    		return true;
    	
        for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
                return false;
        }
        return true;
    }
}
