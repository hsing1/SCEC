package com.util;

import java.io.*;
import java.util.*;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.poi.poifs.filesystem.*;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;

import com.ability.model.AbilityService;
import com.category.model.CategoryService;
import com.category.model.CategoryVO;
import com.classes.model.ClassService;
import com.department.model.*;
import com.employee.model.EmployeeService;
import com.level.model.LevelService;
import com.title.model.*;
import com.training.model.TrainingRecordService;
import com.training.model.TrainingService;
import com.section.model.SectionService;
import com.series.model.*;
import com.subject.model.SubjectService;
import com.promotion.model.*;
import com.deptTitle.model.*;
import com.devision.model.*;
import com.standard.model.*;
import org.apache.log4j.*;

public class SCECworkbookUtil {
	private static String [] sheetArr = SCECLang.TZ;
	private static String trainingSystem = sheetArr[SCECenum.SHEET_TRAINING.ordinal()];
	
	private static String [] itemArr = SCECLang.TZ;
	private static String deptStr = itemArr[SCECenum.ITEM_DEPT.ordinal()];
	private static String seriesStr = itemArr[SCECenum.ITEM_SERIES.ordinal()];
	private static String levelStr = itemArr[SCECenum.ITEM_LEVEL.ordinal()];
	private static String salaryLevelStr = itemArr[SCECenum.ITEM_SALARY_LEVEL.ordinal()];
	private static String titleStr = itemArr[SCECenum.ITEM_TITLE.ordinal()];
	private static String categoryStr = itemArr[SCECenum.ITEM_CATEGORY.ordinal()];
	private static String classStr = itemArr[SCECenum.ITEM_CLASS.ordinal()];
	private static String subjectStr = itemArr[SCECenum.ITEM_SUBJECT.ordinal()];
	private static String numOfGradeStr = itemArr[SCECenum.ITEM_NUM_OF_GRADE.ordinal()];
	private static String standardStr = itemArr[SCECenum.ITEM_STANDARD.ordinal()];
	private static String certificationStr= itemArr[SCECenum.ITEM_CERTIFICATION.ordinal()];
	private static String empIdStr = itemArr[SCECenum.ITEM_EMP_ID.ordinal()];
	private static String empOnboardStr = itemArr[SCECenum.ITEM_EMP_ONBOARD.ordinal()];
	private static String empLeavedateStr = itemArr[SCECenum.ITEM_EMP_LEAVEDATE.ordinal()];
	private static String nameStr = itemArr[SCECenum.ITEM_NAME.ordinal()];
	private static String trainingSubjectStr = itemArr[SCECenum.ITEM_TRAINING_SUBJECT.ordinal()];
	private static String trainingDateStr = itemArr[SCECenum.ITEM_TRAINING_DATE.ordinal()];
	private static String trainingHourStr = itemArr[SCECenum.ITEM_TRAINING_HOUR.ordinal()];
	private static String trainingFeeStr = itemArr[SCECenum.ITEM_TRAINING_FEE.ordinal()];
	private static String trainingTransferStr = itemArr[SCECenum.ITEM_TRAINING_TRANSFER.ordinal()];
	private static String trainingGuaranteeStr = itemArr[SCECenum.ITEM_TRAINING_GUARANTEE.ordinal()];
	private static String trainingTypeStr = itemArr[SCECenum.ITEM_TRAINING_TYPE.ordinal()];
	private static String trainingCompensateStr = itemArr[SCECenum.ITEM_TRAINING_COMPENSATE.ordinal()];
	private static String promotionDateStr = itemArr[SCECenum.ITEM_PROMOTION_DATE.ordinal()];
	private static String devisionStr = itemArr[SCECenum.ITEM_DEVISION.ordinal()];
	private static String empAccountStr = itemArr[SCECenum.ITEM_ACCOUNT.ordinal()];
	private static String privStr = itemArr[SCECenum.ITEM_PRIVILEGE.ordinal()];
	private static String defPrivStr = itemArr[SCECenum.ITEM_DEFAULT_PRIV.ordinal()];
	private static String identityStr = itemArr[SCECenum.ITEM_IDENTITY.ordinal()];
	private static String sectionStr = itemArr[SCECenum.ITEM_SECTION.ordinal()];
	private static Logger logger = Logger.getLogger(SCECworkbookUtil.class);
	
	public static String loadExcel(File xlsf) throws RuntimeException {
		Workbook wb = null;
		Sheet sheet = null;
		String sName = null;
		int numOfSheet = 0;
		File logf = new File("/Users/MBP/Documents/out.log");
		FileOutputStream fs = null;
		DataOutputStream df = null;
		SCECstatus.status = SCECstatusEnum.SCEC_LOAD_EXCEL;
		String encoding = System.getProperty("file.encoding");
		boolean res = true;
		String errMsg = null;
		
		try {
			wb = WorkbookFactory.create(xlsf);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			numOfSheet = wb.getNumberOfSheets();
			
			for (int ns = 0; ns < numOfSheet; ns++) {
				sheet = wb.getSheetAt(ns);
				sName = sheet.getSheetName();
				String s = sheetArr[SCECenum.SHEET_ORG.ordinal()];
				if (sName.equalsIgnoreCase(s))
					s = null;
				
				if (sName.equals(sheetArr[SCECenum.SHEET_ORG.ordinal()])) { //組織架構
					setupOrganization(sheet);
					DevisionService.setDevisionMap();
					DepartmentService.setDeptMap();
					SectionService.setSectionMap();
					SubjectService.setSubjectMap();
				} else if (sName.equals(sheetArr[SCECenum.SHEET_TITLE.ordinal()])) { //職稱設定
					setupTitle(sheet);
					TitleService.setTitleMap();
				} else if (sName.equals(sheetArr[SCECenum.SHEET_SUBJECT.ordinal()])) { //科目項目
					setupSubject(sheet);  //TODO issue #32
				} else if (sName.equals(sheetArr[SCECenum.SHEET_TRAINING.ordinal()])) { //訓練體係設定
					setupTrainingSystem(sheet);
					StandardViewService.setDeptSectionTitleMap();
				} else if (sName.equals(sheetArr[SCECenum.SHEET_EMP.ordinal()])) { //員工資料
					createEmployee(sheet);
					EmployeeService.setEmpMap();
				} else if (sName.equals(sheetArr[SCECenum.SHEET_ABILITY.ordinal()])) { //職能評量
					createEmpAbility(sheet);
				} else if (sName.equals(sheetArr[SCECenum.SHEET_TRAINING_RECORD.ordinal()])) { //訓練記錄
					createTrainingRecord(sheet);
				} else if (sName.equals(sheetArr[SCECenum.SHEET_PROMOTION_RECORD.ordinal()])) { //升遷記錄
					createPromotionRecord(sheet);
				} else if (sName.equals(sheetArr[SCECenum.SHEET_DEPT_TITLE.ordinal()])) { //部門設定
					createDeptTitle(sheet);
				}
			}
			
			//setupMatrix();
		} catch (RuntimeException r) {
			//TODO:should log this information
			r.printStackTrace();
			throw new RuntimeException(r.getMessage()
										+ "<br/> Sheet : " + SCECmessage.sheet
										+ "<br/> Cell : " + SCECmessage.location);
		} finally {
			try {
				fs = new FileOutputStream(logf);
				df = new DataOutputStream(fs);
				for (String msg : SCECmessage.log) {
					df.writeUTF(msg);
				}
				SCECmessage.log.clear();
				df.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return errMsg;
	}
	
	public static void setupOrganization(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		
		System.out.println("Reading Organization ...");
		String sName = null;
		sName = sheet.getSheetName();
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		System.out.println("Row start=" + rowStart);
		System.out.println("Row End=" + rowEnd);
		value = null;
	    // Decide which rows to process
	    //rowStart = Math.min(15, sheet.getFirstRowNum());
	    //rowEnd = Math.max(1400, sheet.getLastRowNum());

		//Check header
	    Row r1 = sheet.getRow(rowStart);
	    int firstCol = r1.getFirstCellNum();
	    int lastCol = r1.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	Cell c1 = r1.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	        System.out.printf("\n(%d,%d)", rowStart, colIdx);
	        if (c1 == null) {
	        	continue;
	        }
	        
	        value = getCellValueStr(c1);
	        printCellValue(c1);
	        System.out.println();
	        currentCol = c1.getColumnIndex();
	        System.out.printf("Current column=(%d,%d)", colIdx, currentCol);
	        if (value.equals(deptStr)) { //部門
	        	//createDepartment(sheet, colIdx);
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_DEPT);
	        } else if (value.equals(seriesStr)) { //職系
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_SERIES);
	        } else if (value.equals(levelStr)) { //職等
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_LEVEL);
	        } else if (value.equals(titleStr)) { //職稱
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_TITLE);
	        } else if (value.equals(categoryStr)) { //職能類別
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_CATEGORY);
	        } else if (value.startsWith(classStr)) { //職能項目
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_CLASS);
	        } else if (value.equals(subjectStr)) { //科目
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_SUBJECT);
	        } else if (value.equals(sectionStr)) { //單位
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_SECTION);
	        } else if (value.equals(devisionStr)) { //廠區
	        	createOrganization(sheet, colIdx, SCECenum.ITEM_DEVISION);
	        }
	        //TODO: need check if all columns are found, if false then break and issue error
	        
	    }
	    
	    for (int rowNum1 = rowStart; rowNum1 <= rowEnd + 1; rowNum1++) {
	       r1 = sheet.getRow(rowNum1);

	       if (r1 == null)
	    	   continue;
	       //int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
	       firstCol = r1.getFirstCellNum();
	       lastCol = r1.getLastCellNum();
	       System.out.printf("\nFirst column = %d, Last column = %d\n", firstCol, lastCol);

	       for (int cn = firstCol; cn < lastCol; cn++) {
	          Cell c1 = r1.getCell(cn, Row.RETURN_BLANK_AS_NULL);
	          System.out.printf("(%d,  %d) = ", rowNum1, cn);
	          if (c1 == null) {
	        	  value = "null";
	          } else {
	        	  value = getCellValueStr(c1);
	        	  printCellValue(c1);
	          }
	          //System.out.println(value);
	       }
	    }
	}
	
	private static void createDepartment(Sheet sheet, int colIdx) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		DepartmentVO deptVO = null;
		String dName = null;
		DepartmentService deptSvc = new DepartmentService();
		
		logger.debug("BEGIN:createDepartment()");
		for (int r = firstRow + 1; r <= lastRow; r++) {
			row = sheet.getRow(r);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null)
				continue;
			
			dName = getCellValueStr(cell);
			System.out.print("Department :" + dName);
			try {
				System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.debug("insert dept name = " + dName);
			deptSvc.addDepartment(dName);
			
			//printCellValue(cell);
		}
		logger.debug("END:createDepartment()");
	}
	
	private static void createEmployee(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null, section = null, devision = null;
		String sName = sheet.getSheetName(), account = null, tmpStr = null;
		Map<String, Integer> header = null;
		
		Row row = null;
		Cell cell = null;
		String ans = null, identity = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    int priv = 0;
	    StringBuilder location = new StringBuilder();
	    
	    header = readHeader(sheet);
	    
	    int empId;
		SCECmessage.location = location;
		SCECmessage.sheet = sName;
	    String dept = null, title = null, empName = null;
	    Date onboardDate = null, leaveDate = null;
	    EmployeeService empSvc = new EmployeeService();
	    
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
			
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	colIdx = header.get(empIdStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			empId = getCellValueInt(cell);
			
			//廠區/事業單位
	    	colIdx = header.get(devisionStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null)
				throw new RuntimeException("No devision specified");
			devision = getCellValueStr(cell);
	    	SCECcommon.checkDevision(devision);
	    	
			//部門
	    	colIdx = header.get(deptStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null)
				throw new RuntimeException("No department specified");
			dept = getCellValueStr(cell);
	    	SCECcommon.checkDept(dept);
	    	
	    	//單位
	    	colIdx = header.get(sectionStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null)
				section = "none";
			else
				section = getCellValueStr(cell);
	    	SCECcommon.checkSection(section);
	    	
	    	//職稱
	    	colIdx = header.get(titleStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null)
				throw new RuntimeException("No title specified");
			title = getCellValueStr(cell);
	    	SCECcommon.checkTitle(title);
			
	    	//姓名
	    	colIdx = header.get(nameStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			empName = getCellValueStr(cell);
			
			//到職日期
	    	colIdx = header.get(empOnboardStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null) {
				System.out.println("Error: null cell");
				onboardDate = null;
			} else {
				onboardDate = cell.getDateCellValue();
			}
			
			//離職日期
	    	colIdx = header.get(empLeavedateStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null) {
				System.out.println("Error: null cell");
				leaveDate = null;
			} else {
				leaveDate = cell.getDateCellValue();
			}
			
	    	colIdx = header.get(identityStr);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null) {
				System.out.println("Error: null cell");
				leaveDate = null;
			} else {
				identity = getCellValueStr(cell);
				account = identity;
			}
			
	    	colIdx = header.get(privStr);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			SCECmessage.setLocation(rowIdx, colIdx);
			if (cell == null) {
				System.out.println("Error: null cell");
				priv = 0;
			} else
				priv = getCellValueInt(cell);
			
			empSvc.addEmployee(empId, devision, dept, section, title, empName, onboardDate, leaveDate, identity, account, priv);
	    }
	}
	
	//TODO:should revise algorithm
	private static void createEmpAbility(Sheet sheet) {
		int rowStart, rowEnd;
		double grade = 0;
		boolean isRequired = false;
		Integer colIdx = 0;
		String value = null, section = null, tmpStr = null;
		String gradeStr = null;
		String [] empKey = {"工號", "部門", "單位", "職稱", "姓名"};
		Map<String, Integer> header = null;
		Map<String, Integer> empMap = new LinkedHashMap<String, Integer>();
		Map<String, Integer> subjectMap = new LinkedHashMap<String, Integer>();
		AbilityService abSvc = new AbilityService();
		StandardViewService standSvc = new StandardViewService();
		
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int currentCol = 0, idx = -1;
	    
	    SCECmessage.sheet = sheet.getSheetName();
	    header = readHeader(sheet);
	    for (String key : empKey) {
	    	empMap.put(key , header.get(key));
	    	header.remove(key);
	    }
	    
	    for (String key : header.keySet()) {
	    	SCECcommon.checkSubject(key);
	    	subjectMap.put(key, header.get(key));
	    }
	    
	    //check if all of the subject has listed in the employee ability sheet
	    for (String subject : SubjectService.subjectMapInv.keySet()) {
	    	if (subjectMap.containsKey(subject))
	    		continue;
	    	throw new RuntimeException("No such subject in sheet " + subject);
	    }
	    
	    String dept = null, title = null, empName = null;
	    int empId = 0;
	    boolean certification = false;
	    TrainingService trnSvc = new TrainingService();
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	//工號
	    	colIdx = empMap.get(empKey[0]);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				if (SCECstatus.debugMode)
					throw new RuntimeException("No employee id specified");
				else
					continue;
			}
			empId = getCellValueInt(cell);
			
	    	
			//部門
	    	colIdx = empMap.get(empKey[1]);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell != null)
				dept = getCellValueStr(cell);
			SCECcommon.checkDept(dept);
	    	tmpStr = EmployeeService.empToDeptMap.get(empId);
	    	if (!dept.equals(tmpStr)) {
	    		if (SCECstatus.debugMode)
	    			throw new RuntimeException("Department should be " + tmpStr);
	    		dept = tmpStr;
	    	}
			
			//單位
	    	colIdx = empMap.get(empKey[2]);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell != null)
				section = getCellValueStr(cell);
			else
				section = "none";
			SCECcommon.checkSection(section);
	    	tmpStr = EmployeeService.empToSectionMap.get(empId);
	    	if (!section.equals(tmpStr)) {
	    		if (SCECstatus.debugMode)
	    			throw new RuntimeException("Section should be " + tmpStr);
	    		section = tmpStr;
	    	}
			
			//職稱
	    	colIdx = empMap.get(empKey[3]);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell != null) {
				title = getCellValueStr(cell);
			} 
			SCECcommon.checkTitle(title);
	    	tmpStr = EmployeeService.empToTitleMap.get(empId);
	    	if (!title.equals(tmpStr)) {
	    		if (SCECstatus.debugMode)
	    			throw new RuntimeException("Title should be " + tmpStr);
	    		title = tmpStr;
	    	}
			
			//姓名
	    	colIdx = empMap.get(empKey[4]);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				if (SCECstatus.debugMode)
					throw new RuntimeException("No employee name specified");
				else
					continue;
			}
			empName = getCellValueStr(cell);
			try {
				SCECcommon.checkEmp(empId, empName);
			} catch (RuntimeException r) {
				if (SCECstatus.debugMode)
					throw r;
				else
					System.out.println(r.getMessage());
			}
	    	tmpStr = EmployeeService.empMap.get(empId);
	    	if (!empName.equals(tmpStr)) {
				if (SCECstatus.debugMode)
					throw new RuntimeException("Title should be " + tmpStr);
				empName = tmpStr;
	    	}
			
			
			//List<StandardViewVO> statdardList = standSvc.getStandardByTitle(dept, section, title);
			/*
			 * Reading subject grade and store in DB
			 */
			for (String subject : subjectMap.keySet()) {
				//colIdx = header.get(subject);
				colIdx = subjectMap.get(subject);
				if (colIdx == null)
					throw new RuntimeException("No such subject " + subject);
				
				SCECmessage.setLocation(rowIdx, colIdx);
				cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
				
				//check if this subject is required ability for the title
				isRequired = TrainingService.isRequired(subject, dept, section, title);
				if (cell == null) {
					if (isRequired) {
						SCECmessage.log.add("Subject " + subject + " is required, should have value" + SCECmessage.cellRef.formatAsString() + ")\n");
						gradeStr = "0";
					} else
						continue;
				} else {
					gradeStr = getCellValueStr(cell);
					if (gradeStr.equals(""))
						continue;
				}
				
				idx = -1;
				if ((idx = gradeStr.indexOf('D')) >= 0) {
					if (isRequired) {
						SCECmessage.log.add("Subject " + subject + " is required, should not have D(" + SCECmessage.cellRef.formatAsString() + ")\n");
					}
					
					gradeStr = gradeStr.substring(idx + 1);
					grade = Double.valueOf(gradeStr);
				} else {
					if (!isRequired) {
						SCECmessage.log.add("Subject " + subject + " is not required, should have D(" + SCECmessage.cellRef.formatAsString() + ")\n");
					}
					grade = Double.valueOf(gradeStr);
				}
				if (isRequired)
					abSvc.addAbility(empId, subject, grade, 0, isRequired);
				else
					abSvc.addAbility(empId, subject, 0, grade, isRequired);
			} //end for
	    }
	}
	
	private static void createTrainingRecord(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		Map<String, Integer> header = null;
		String subject = null, note = null;
	    String dept = null, title = null, empName = null, str = null;
		Date trainingDate = null, transferDate = null, guarantee = null;
		int empId, trainingHour, trainingFee, trainingType;
	    TrainingRecordService trnSvc = new TrainingRecordService();
	    double compensate = 0;
		
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		SCECmessage.sheet = sheet.getSheetName();
		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	colIdx = header.get(empIdStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empId = getCellValueInt(cell);
	    	
	    	colIdx = header.get(trainingSubjectStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			subject = getCellValueStr(cell);
			
	    	colIdx = header.get(nameStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empName = getCellValueStr(cell);
			
	    	colIdx = header.get(trainingHourStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			trainingHour = getCellValueInt(cell);
			
	    	colIdx = header.get(trainingFeeStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			trainingFee = getCellValueInt(cell);
			
	    	colIdx = header.get(trainingDateStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				trainingDate = cell.getDateCellValue();
			
	    	colIdx = header.get(trainingTransferStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				transferDate = cell.getDateCellValue();
			
	    	colIdx = header.get(trainingGuaranteeStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				guarantee = cell.getDateCellValue();
			
	    	colIdx = header.get(trainingCompensateStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			compensate = getCellValueDouble(cell);
			
	    	colIdx = header.get(trainingTypeStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			str = getCellValueStr(cell);
			if (str.equals("內部訓練"))
				trainingType = 0; 
			else
				trainingType = 1;
			
			trnSvc.addTrainingRecord(empId, subject, trainingDate, transferDate, guarantee, 
					trainingHour, trainingFee, trainingType, compensate, note);
	    }
	}
	
	private static void createPromotionRecord(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		Map<String, Integer> header = null;
		String subject = null, note = null;
	    String dept = null, title = null, empName = null, devision;
		Date promotionDate = null;
		Integer empId;
	    PromotionService proSvc = new PromotionService();
		
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		SCECmessage.sheet = sheet.getSheetName();
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	colIdx = header.get(empIdStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empId = getCellValueInt(cell);
	    	
	    	colIdx = header.get(deptStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			dept = getCellValueStr(cell);
			
	    	colIdx = header.get(nameStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			empName = getCellValueStr(cell);
			
	    	colIdx = header.get(titleStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			title = getCellValueStr(cell);
			
	    	colIdx = header.get(promotionDateStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				System.out.println("Error: null cell");
			else
				promotionDate = cell.getDateCellValue();
			
	    	colIdx = header.get(devisionStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			devision = getCellValueStr(cell);
			
			proSvc.addPromotionRecord(empId, dept, title, devision, promotionDate, note);
	    }
	}
	
	private static void createOrganization(Sheet sheet, int colIdx, SCECenum orgType) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		DepartmentVO deptVO = null;
		String name = null;
		int count = 0;
		Set<String> set = new HashSet<String>();
		
		logger.debug("BEGIN:createOrganization");
		
	    SCECmessage.sheet = sheet.getSheetName();
		for (int r = firstRow + 1; r <= lastRow; r++) {
			row = sheet.getRow(r);
			if (isRowEmpty(row))
				continue;
			
	    	SCECmessage.setLocation(r, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null)
				continue;
			
			//TODO:could have performance issue here, run time creating Service object
			count++;
			name = getCellValueStr(cell);
			switch(orgType) {
			case ITEM_DEVISION: //build department
				DevisionService devSvc = new DevisionService();
				devSvc.insert(name);
				break;
			case ITEM_DEPT: //build department
				DepartmentService deptSvc = new DepartmentService();
				deptSvc.addDepartment(name);
				break;
			case ITEM_SECTION:
				SectionService secSvc = new SectionService();
				secSvc.insert(name);
				break;
			case ITEM_SERIES: //build series
				SeriesService seriesSvc = new SeriesService();
				seriesSvc.addSeries(name);
				break;
			case ITEM_LEVEL: //build level 
				LevelService levelSvc = new LevelService();
				levelSvc.addLevel(name);
				break;
			case ITEM_CATEGORY: //build category 
				CategoryService categorySvc = new CategoryService();
				categorySvc.addCategory(name);
				break;
			case ITEM_CLASS: //build class 
				ClassService classSvc = new ClassService();
				classSvc.addClass(name);
				break;
			case ITEM_SUBJECT: //build subject 
				SubjectService subjectSvc = new SubjectService();
				if (set.contains(name))
					continue;
				subjectSvc.addSubject(null, name, null);
				set.add(name);
				break;
			case ITEM_TITLE: //build title 
				Integer i = 0; //initial value of salary level
				TitleService titleSvc = new TitleService();
				titleSvc.addTitleName(name, i);
				break;
			default:
				break;
			}
			
			//printCellValue(cell);
		}
		switch(orgType) {
		case ITEM_CATEGORY:
			CategoryService.numOfCat = count;
			break;
		default:
			break;
		}
		
		logger.debug("END:createOrganization");
	}

	private static void setupTitle(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		sName = sheet.getSheetName();
		HashMap<String, Integer> header = new HashMap<String, Integer>();
		Row row = null;
		Cell cell = null;
		Integer numOfSalLevel = null;
		Integer priv = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		System.out.println("Row start=" + rowStart);
		System.out.println("Row End=" + rowEnd);
		value = null;
	    // Decide which rows to process
	    //rowStart = Math.min(15, sheet.getFirstRowNum());
	    //rowEnd = Math.max(1400, sheet.getLastRowNum());

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    SCECmessage.sheet = sheet.getSheetName();
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	        System.out.printf("\n(%d,%d)", rowStart, colIdx);
	        if (cell == null) {
	        	continue;
	        }
	        
	        value = getCellValueStr(cell);
	        printCellValue(cell);
	        System.out.println();
	        currentCol = cell.getColumnIndex();
	        System.out.printf("Current column=(%d,%d)", colIdx, currentCol);
	        header.put(value, currentCol);
	    }
	    
	    //Pass2:Read data
	    String title = null, level = null, series = null;
	    TitleService titleSvc = new TitleService();
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	colIdx = header.get(titleStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			title = getCellValueStr(cell);
			
	    	colIdx = header.get(levelStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			level = getCellValueStr(cell);
			if (!level.matches(".*職等$")) {
				throw new RuntimeException("Unknown level name!");
			}
			
	    	colIdx = header.get(seriesStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			series = getCellValueStr(cell);
			
	    	colIdx = header.get(salaryLevelStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			numOfSalLevel = getCellValueInt(cell);
			
	    	colIdx = header.get(defPrivStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			priv = getCellValueInt(cell);
			
			titleSvc.setupTitle(title, series, level, numOfSalLevel, priv);
	    }
	}
	
	private static void setupSubject(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		HashMap<String, Integer> header = null;
		Row row = null;
		Cell cell = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    SCECmessage.sheet = sheet.getSheetName();
	    String subject = null, category = null, classes = null;
	    int classify = 0;
	    SubjectService subSvc = new SubjectService();
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	colIdx = header.get(subjectStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
				continue;
			}
			subject = getCellValueStr(cell);
			
	    	colIdx = header.get(categoryStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				throw new RuntimeException("No category specified");
			}
			category = getCellValueStr(cell);
			
	    	colIdx = header.get(classStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				throw new RuntimeException("No class specified");
			}
			classes = getCellValueStr(cell);
			
	    	colIdx = header.get(numOfGradeStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				throw new RuntimeException("No classify number specified");
			}
			classify = getCellValueInt(cell);
			
			subSvc.setupSubject(subject, category, classes, classify);
	    }
		SubjectService.setNumOfSubject();
	}
	
	private static void setupTrainingSystem(Sheet sheet) {
		int rowStart, rowEnd;
	    int firstCol, lastCol, colIdx = 0, currentCol = 0;
	    int colDept = 0, colTitle = 0, colSection = 0;
	    int standard = 0;
	    boolean certification = false, noDept = true, noTitle = true;
		String ans = null, tmpStr [] = null;
	    TrainingService trnSvc = new TrainingService();
	    String dept = null, title = null, subject = null, section = null;
		String value = null;
		String sName = null;
		HashMap<String, Integer> header = null;
		Row row = null;
		Cell cell = null;
		
		SCECmessage.sheet = sheet.getSheetName();
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    firstCol = row.getFirstCellNum();
	    lastCol = row.getLastCellNum();
	    
	    header = readHeader(sheet);
	    colDept = header.get(deptStr);
	    colTitle = header.get(titleStr);
	    colSection = header.get(sectionStr);
	    
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
			//科目
	    	colIdx = header.get(subjectStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			subject = getCellValueStr(cell);
			SCECcommon.checkSubject(subject);
			
			//標準
	    	colIdx = header.get(standardStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			standard = getCellValueInt(cell);
			
			//認證
	    	colIdx = header.get(certificationStr);
	    	SCECmessage.setLocation(rowIdx, colIdx);
			cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			ans = getCellValueStr(cell);
			
			if (ans.equals("y"))
				certification = true;
			else
				certification = false;
			
	    	for (int c = colDept; c < lastCol; c+=3) {
				
	    		//部門, required
	    		SCECmessage.setLocation(rowIdx, c);
	    		cell = row.getCell(c, Row.RETURN_BLANK_AS_NULL);
	    		if (cell == null) {
	    			noDept = true; 
	    		} else {
	    			dept= getCellValueStr(cell);
	    			if (!dept.equals("全部")) {
	    				if (!dept.matches(".+部$"))
	    					dept = dept + "部";
	    				try {
	    					SCECcommon.checkDept(dept);
	    				} catch (RuntimeException r) {
	    					throw r;
	    				}
	    			}
	    			noDept = false;
	    		}
	    		
	    		//單位, optional
	    		SCECmessage.setLocation(rowIdx, c+1);
	    		cell = row.getCell(c+1, Row.RETURN_BLANK_AS_NULL);
	    		if (cell == null) {
	    			section = "none";
	    		} else {
	    			section = getCellValueStr(cell);
	    			if (section.equals(""))
	    				section = "none";
	    		}
			
				//職稱, required
	    		SCECmessage.setLocation(rowIdx, c+3);
				cell = row.getCell(c+2, Row.RETURN_BLANK_AS_NULL);
				if (cell == null)
					noTitle = true;
				else {
					title = getCellValueStr(cell);
					noTitle = false;
					if (!title.equals("全部")) {
						tmpStr = title.split("以上");
						if (!TitleService.titleMapInv.containsKey(tmpStr[0]))
	    					throw new RuntimeException("Cannot find title " + tmpStr[0]);
					}
				}
			
				if (noDept && noTitle)
					continue;
				
				// The department and title are required, if not issue error
				
				if (noDept)
	    			throw new RuntimeException("An empty dept found");
				if (noTitle)
	    			throw new RuntimeException("An empty title foun");
				if (subject.equals(""))
	    			throw new RuntimeException("An empty subject found");
				
	    		trnSvc.addTraining(dept, section, title, subject, standard, certification);
	    	}
	    }
	}
	
	private static void setupTrainingSystem_old(Sheet sheet) {
		int rowStart, rowEnd;
		String value = null;
		String sName = null;
		HashMap<String, Integer> header = null;
		Row row = null;
		Cell cell = null;
		String ans = null;
		
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    
	    header = readHeader(sheet);
	    
	    String dept = null, title = null, subject = null;
	    int standard = 0;
	    boolean certification = false;
	    TrainingService trnSvc = new TrainingService();
	    for (int rowIdx = rowStart + 1; rowIdx < rowEnd; rowIdx++) {
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	//部門
			cell = row.getCell(header.get(deptStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			dept= getCellValueStr(cell);
			
			//職稱
			cell = row.getCell(header.get(titleStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			title = getCellValueStr(cell);
		
			//科目
			cell = row.getCell(header.get(subjectStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			subject = getCellValueStr(cell);
			
			//標準
			cell = row.getCell(header.get(standardStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			standard = getCellValueInt(cell);
			
			//認證
			cell = row.getCell(header.get(certificationStr), Row.RETURN_NULL_AND_BLANK);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			ans = getCellValueStr(cell);
			
			if (ans.equals("y"))
				certification = true;
			else
				certification = false;
			
			//trnSvc.addTraining(dept, title, subject, standard, certification);
	    }
	}
	
	private static void createDeptTitle(Sheet sheet) {
		String value = null;
		String sName = null;
		String ans = null, dept = null, title = null, section = null;
		HashMap<String, Integer> header = null;
		Row row = null;
		Cell cell = null;
	    boolean certification = false;
	    DeptTitleService dtSvc = new DeptTitleService();
	    int firstCol, lastCol;
	    int colIdx = 0, currentCol = 0, standard = 0;
	    int colTitle = 0, rowStart, rowEnd;
		
		SCECmessage.sheet = sheet.getSheetName();
		rowStart = sheet.getFirstRowNum();
		rowEnd = sheet.getLastRowNum();
		value = null;

		//Pass1:Check header
	    row = sheet.getRow(rowStart);
	    firstCol = row.getFirstCellNum();
	    
	    header = readHeader(sheet);
	    //locate the first column number of title
	    colTitle = header.get(titleStr);
	    
	    for (int rowIdx = rowStart + 1; rowIdx <= rowEnd; rowIdx++) {
	    	SCECmessage.setLocation(rowIdx, colIdx);
	    	row = sheet.getRow(rowIdx);
	    	
	    	if (isRowEmpty(row))
	    		continue;
	    	
	    	lastCol = row.getLastCellNum();
	    	//部門
			cell = row.getCell(header.get(deptStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			dept = getCellValueStr(cell);
			
			//單位
			cell = row.getCell(header.get(sectionStr), Row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				System.out.println("Error: null cell");
			}
			section = getCellValueStr(cell);
			if (section.equals(""))
				section = "none";
			
	    	for (int c = colTitle; c < lastCol; c++) {
				//職稱
				cell = row.getCell(c, Row.RETURN_BLANK_AS_NULL);
				if (cell == null) {
					System.out.println("Error: null cell");
	    			continue;
				}
				title = getCellValueStr(cell);
			
	    		dtSvc.addDeptTitle(dept, section, title);
	    	}
	    }
	}
	
	private static HashMap<String, Integer> readHeader(Sheet sheet) {
		HashMap<String, Integer> header = new LinkedHashMap<String, Integer>();
		int rowStart = sheet.getFirstRowNum();
		String value = null;
		Row row = null;
		Cell cell = null;

	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	    	
	        if (cell == null)
	        	continue;
	        
	        value = getCellValueStr(cell);
	        //To get first column index of dept
	        if (value.equals(deptStr)) {
	        	if (header.containsKey(deptStr))
	        		continue;
	        }
	        if (value.equals(titleStr)) {
	        	if (header.containsKey(titleStr))
	        		continue;
	        }
	        currentCol = cell.getColumnIndex();
	        header.put(value, currentCol);
	    }
		
		return header;
	}

	private static void readColumn(Sheet sheet, int colIdx) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		
		for (int r = firstRow + 1; r < lastRow; r++) {
			row = sheet.getRow(r);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null)
				continue;
			
			printCellValue(cell);
		}
	}
	
    /**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param halign the horizontal alignment for the cell.
     */
    private static void createCell(Workbook wb, Row row, short column, short halign, short valign) {
        Cell cell = row.createCell(column);
        cell.setCellValue("Align It");
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(halign);
        cellStyle.setVerticalAlignment(valign);
        cell.setCellStyle(cellStyle);
    }
    
    private static short getAllignmentType(Cell cell) {
    	CellStyle style = cell.getCellStyle();
    	short type = style.getAlignment();
    	switch(type) {
		case CellStyle.ALIGN_GENERAL:
			System.out.println("ALIGN_GENERAL");
			break;
		case CellStyle.ALIGN_LEFT:
			System.out.println("ALIGN_LEFT");
			break;
		case CellStyle.ALIGN_CENTER:
			System.out.println("ALIGN_CENTER");
			break;
		case CellStyle.ALIGN_RIGHT:
			System.out.println("ALIGN_RIGHT");
			break;
		case CellStyle.ALIGN_FILL:
			System.out.println("ALIGN_FILL");
			break;
		case CellStyle.ALIGN_JUSTIFY:
			System.out.println("ALIGN_JUSTIFY");
			break;
		case CellStyle.ALIGN_CENTER_SELECTION:
			System.out.println("ALIGN_CENTER_SELECTION");
			break;
		default:
			System.out.println("Error:Unknow Align Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static int getCellType(Cell cell) {
    	int type = cell.getCellType();
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			System.out.println("ALIGN_GENERAL");
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.println("ALIGN_LEFT");
			break;
		case Cell.CELL_TYPE_ERROR:
			System.out.println("ALIGN_CENTER");
			break;
		case Cell.CELL_TYPE_FORMULA:
			System.out.println("ALIGN_RIGHT");
			break;
		case Cell.CELL_TYPE_NUMERIC:
			System.out.println("ALIGN_FILL");
			break;
		case Cell.CELL_TYPE_STRING:
			System.out.println("ALIGN_JUSTIFY");
			break;
		default:
			System.out.println("Error:Unknow Cell Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static String getCellValueStr(Cell cell) {
    	int type = -1;
    	String value = null;
    	
    	if (cell == null) 
    		value = "";
    	else
    		type = cell.getCellType();
    	
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			value = "Blank";
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			Boolean boVal = cell.getBooleanCellValue();
			value = boVal.toString();
			break;
		case Cell.CELL_TYPE_ERROR:
			Byte btVal = cell.getErrorCellValue();
			value = btVal.toString();
			break;
		case Cell.CELL_TYPE_FORMULA:
			value = cell.getCellFormula();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			Double dVal = cell.getNumericCellValue();
			value = dVal.toString();
			break;
		case Cell.CELL_TYPE_STRING:
			value = cell.getStringCellValue();
			value = value.trim();
			break;
		default:
			type = -1;
			break;
		}
		//System.out.printf("Value = %s\n", value);
    	
    	return value;
    }
    
    private static double getCellValueDouble(Cell cell) {
    	int type = -1;
    	double value = 0;
    	
    	if (cell != null)
    		type = cell.getCellType();
    	
    	switch(type) {
		case Cell.CELL_TYPE_NUMERIC:
			value = cell.getNumericCellValue();
			break;
		default:
			break;
		}
    	
    	return value;
    }
    
    private static int getCellValueInt(Cell cell) {
    	int type = -1;
    	int value = 0;
    	
    	if (cell != null)
    		type = cell.getCellType();
    	
    	switch(type) {
		case Cell.CELL_TYPE_NUMERIC:
			value = (int)cell.getNumericCellValue();
			break;
		default:
			type = -1;
			break;
		}
    	
    	return value;
    }
    
    private static void printCellValue(Cell cell) {
    	int type = -1;
    	
    	if (cell != null)
    		type = cell.getCellType();
    	
        CellReference cellRef = new CellReference(cell.getRowIndex(), cell.getColumnIndex());
        System.out.print(cellRef.formatAsString());
        System.out.print("=");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                System.out.printf("%s", cell.getRichStringCellValue().getString());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    System.out.printf("%s", cell.getDateCellValue());
                } else {
                    System.out.printf("%s", cell.getNumericCellValue());
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                System.out.printf("%s", cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                System.out.printf("%s", cell.getCellFormula());
                break;
            default:
                System.out.printf("Error:unknow cell value type");
        }
    }
    
    private static boolean isRowEmpty(Row row) {
    	int first = -1, last = -1;
    	
    	if (row == null)
    		return true;
    	
    	first = row.getFirstCellNum();
    	last = row.getLastCellNum();
    	
    	if (first == -1 || last == -1)
    		return true;
    	
        for (int c = first; c <= last; c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
                return false;
        }
        return true;
    }
}
