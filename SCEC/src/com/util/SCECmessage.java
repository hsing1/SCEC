package com.util;

import org.apache.poi.hssf.util.CellReference;
import java.util.*;

public class SCECmessage {
	public static String sheet;
	public static StringBuilder location = new StringBuilder();
	public static CellReference cellRef;
	public static List<String> log;
	
	static {
		if (log == null)
			log = new ArrayList<String>();
	}
	
	public static void clear() {
		location.delete(0, location.length());
	}
	
	public static void setLocation(int row, int col) {
		clear();
		location.append("(").append(row + 1).append(":").append(col + 1).append(")");
		cellRef = new CellReference(row, col);
		location.append(" | ").append(cellRef.formatAsString());
	}
}
