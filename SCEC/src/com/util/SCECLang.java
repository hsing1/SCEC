package com.util;

import java.util.*;

public class SCECLang {
	//static List<String> TZ = new ArrayList<String>();
	static String [] TZ = new String[SCECenum.SCEC_END.ordinal()];
	
	static {
		TZ[SCECenum.SHEET_ORG.ordinal()] 			= "組織架構";
		TZ[SCECenum.SHEET_TITLE.ordinal()] 			= "職稱設定";
		TZ[SCECenum.SHEET_SUBJECT.ordinal()] 		= "科目項目";
		TZ[SCECenum.SHEET_TRAINING.ordinal()] 		= "訓練體系設定";
		TZ[SCECenum.SHEET_EMP.ordinal()] 			= "員工資料";
		TZ[SCECenum.SHEET_ABILITY.ordinal()] 		= "職能評量";
		TZ[SCECenum.SHEET_TRAINING_RECORD.ordinal()]= "訓練記錄";
		TZ[SCECenum.SHEET_PROMOTION_RECORD.ordinal()]= "異動記錄";
		TZ[SCECenum.SHEET_DEPT_TITLE.ordinal()]		= "部門設定";
		
		TZ[SCECenum.ITEM_DEVISION.ordinal()]		= "廠區/事業單位";
		TZ[SCECenum.ITEM_DEPT.ordinal()] 			= "部門";
		TZ[SCECenum.ITEM_SECTION.ordinal()] 		= "單位";
		TZ[SCECenum.ITEM_TITLE.ordinal()] 			= "職稱";
		TZ[SCECenum.ITEM_SERIES.ordinal()] 			= "職系";
		TZ[SCECenum.ITEM_LEVEL.ordinal()] 			= "職等";
		TZ[SCECenum.ITEM_SALARY_LEVEL.ordinal()] 	= "級數";
		TZ[SCECenum.ITEM_SUBJECT.ordinal()] 		= "科目";
		TZ[SCECenum.ITEM_CATEGORY.ordinal()] 		= "職能類別";
		TZ[SCECenum.ITEM_CLASS.ordinal()] 			= "職能項目(訓練體系)";
		TZ[SCECenum.ITEM_NAME.ordinal()] 			= "姓名";
		TZ[SCECenum.ITEM_STANDARD.ordinal()] 		= "標準";
		TZ[SCECenum.ITEM_CERTIFICATION.ordinal()] 	= "認證";
		TZ[SCECenum.ITEM_EMP_ID.ordinal()] 			= "工號";
		TZ[SCECenum.ITEM_EMP_ONBOARD.ordinal()] 	= "到職日期";
		TZ[SCECenum.ITEM_EMP_LEAVEDATE.ordinal()]	= "離職日期";
		TZ[SCECenum.ITEM_TRAINING_SUBJECT.ordinal()]= "訓練科目";
		TZ[SCECenum.ITEM_TRAINING_DATE.ordinal()]	= "訓練日期";
		TZ[SCECenum.ITEM_TRAINING_FEE.ordinal()]	= "訓練費用";
		TZ[SCECenum.ITEM_TRAINING_HOUR.ordinal()]	= "訓練時數";
		TZ[SCECenum.ITEM_TRAINING_TRANSFER.ordinal()]	= "轉訓日期";
		TZ[SCECenum.ITEM_TRAINING_GUARANTEE.ordinal()]	= "服務保證";
		TZ[SCECenum.ITEM_TRAINING_NOTE.ordinal()]	= "備註";
		TZ[SCECenum.ITEM_TRAINING_REPORT.ordinal()]	= "報告";
		TZ[SCECenum.ITEM_TRAINING_TYPE.ordinal()]	= "訓練方式";
		TZ[SCECenum.ITEM_TRAINING_COMPENSATE.ordinal()]	= "賠償條件";
		TZ[SCECenum.ITEM_NUM_OF_GRADE.ordinal()]	= "分級";
		TZ[SCECenum.ITEM_PROMOTION_DATE.ordinal()]	= "異動日期";
		TZ[SCECenum.ITEM_PROMOTION_DEVISION.ordinal()]	= "廠區/事業單位";
		TZ[SCECenum.ITEM_ACCOUNT.ordinal()]	= "帳號";
		TZ[SCECenum.ITEM_PRIVILEGE.ordinal()] = "權限";
		TZ[SCECenum.ITEM_DEFAULT_PRIV.ordinal()] = "預設權限";
		TZ[SCECenum.ITEM_IDENTITY.ordinal()] = "身份證";
	}
	
	public static void main(String[] args) {
		String str = "SHEET_ORG";
		if (str.equals(SCECenum.SHEET_ORG))
			System.out.println("found org");
		
		System.out.println(SCECenum.SCEC_END);
		System.out.println(SCECenum.SCEC_END.ordinal());
		System.out.println(TZ[SCECenum.ITEM_DEPT.ordinal()]);
	}
}

