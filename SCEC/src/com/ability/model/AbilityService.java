package com.ability.model;

import java.util.List;
import java.util.Map;

import com.subject.model.SubjectService;

public class AbilityService {

	private AbilityDAO_interface dao;

	public AbilityService() {
		dao = new AbilityDAO();
	}

	public AbilityVO addAbility(int empId, String subject, double requiredGrade, double grade, boolean isRequired) {
		SubjectService subSvc = new SubjectService();
		int subjectId;
		
		AbilityVO abilityVO = new AbilityVO();
		
		subjectId = subSvc.getOneSubject(subject).getSubjectId();
		abilityVO.setSubjectId(subjectId);
		abilityVO.setGrade(grade);
		abilityVO.setRequiredGrade(requiredGrade);
		abilityVO.setEmpId(empId);
		abilityVO.setRequired(isRequired);
		dao.insert(abilityVO);
		

		return abilityVO;
	}

//	public AbilityVO updateTitle(Integer titleId,String  name) {
//	}

	public void deleteTitle(Integer subjectId,Integer  standardId,Integer  gradeId,Integer empId) {
		dao.delete( subjectId,  standardId,  gradeId, empId);
	}


	public List<AbilityVO> getAll() {
		return dao.getAll();
	}
	
	public List<AbilityVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
