package com.ability.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class AbilityDAO implements AbilityDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO ABILITY (emp_id, subject_id, required_grade, grade, required) VALUES (?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT subject_id,standard_id,grade_id,emp_id  FROM ABILITY order by subject_id";
	private static final String DELETE = 
		"DELETE FROM ABILITY where subject_id = ? and standard_id = ? and grade_id = ? and emp_id = ?";
/*
 	private static final String UPDATE = 
"UPDATE ABILITY set license=?,name = ?,class_id=? where subject_id=?";
*/	
	

    @Override
	public void insert(AbilityVO abilityVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, abilityVO.getEmpId());
			pstmt.setInt(2, abilityVO.getSubjectId());
			pstmt.setDouble(3, abilityVO.getRequiredGrade());
			pstmt.setDouble(4, abilityVO.getGrade());
			pstmt.setBoolean(5,  abilityVO.isRequired());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, null, pstmt, true);
		}

	}

/*	
	@Override
	public void update(AbilityVO abilityVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, subjectVO.getLicense());
			pstmt.setString(2, subjectVO.getName());
			pstmt.setInt(3, subjectVO.getClassId());
			pstmt.setInt(4, subjectVO.getSubjectId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
*/
	@Override
	public void delete(Integer subjectId,Integer  standardId,Integer  gradeId,Integer empId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, subjectId);
			pstmt.setInt(2, standardId);
			pstmt.setInt(3, gradeId);
			pstmt.setInt(4, empId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}




	@Override
	public List<AbilityVO> getAll() {
		List<AbilityVO> list = new ArrayList<AbilityVO>();
		AbilityVO abilityVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				abilityVO = new AbilityVO();
				abilityVO.setSubjectId(rs.getInt("subject_id"));
				abilityVO.setGrade(rs.getInt("grade_id"));
				abilityVO.setEmpId(rs.getInt("emp_id"));
				list.add(abilityVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<AbilityVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

}