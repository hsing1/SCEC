package com.ability.model;

import java.util.*;
import java.sql.*;

public class AbilityJDBCDAO implements AbilityDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO ABILITY (subject_id,standard_id,grade_id,emp_id ) VALUES ( ?, ?,?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT subject_id,standard_id,grade_id,emp_id  FROM ABILITY order by subject_id";
	private static final String DELETE = 
		"DELETE FROM ABILITY where subject_id = ? and standard_id = ? and grade_id = ? and emp_id = ?";
/*
 	private static final String UPDATE = 
"UPDATE ABILITY set license=?,name = ?,class_id=? where subject_id=?";
*/	
	

    @Override
	public void insert(AbilityVO abilityVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, abilityVO.getSubjectId());
			pstmt.setInt(2, abilityVO.getStandardId());
			pstmt.setInt(3, abilityVO.getGrandId());
			pstmt.setInt(4, abilityVO.getEmpId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

/*	
	@Override
	public void update(AbilityVO abilityVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, subjectVO.getLicense());
			pstmt.setString(2, subjectVO.getName());
			pstmt.setInt(3, subjectVO.getClassId());
			pstmt.setInt(4, subjectVO.getSubjectId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
*/
	@Override
	public void delete(Integer subjectId,Integer  standardId,Integer  gradeId,Integer empId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, subjectId);
			pstmt.setInt(2, standardId);
			pstmt.setInt(3, gradeId);
			pstmt.setInt(4, empId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}




	@Override
	public List<AbilityVO> getAll() {
		List<AbilityVO> list = new ArrayList<AbilityVO>();
		AbilityVO abilityVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				abilityVO = new AbilityVO();
				abilityVO.setSubjectId(rs.getInt("subject_id"));
				abilityVO.setStandardId(rs.getInt("standard_id"));
				abilityVO.setGrandId(rs.getInt("grade_id"));
				abilityVO.setEmpId(rs.getInt("emp_id"));
				list.add(abilityVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<AbilityVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		AbilityJDBCDAO dao = new AbilityJDBCDAO();

		// 新增
//		AbilityVO abilityVO = new AbilityVO();
//		abilityVO.setSubjectId(1002);
//		abilityVO.setStandardId(1002);
//		abilityVO.setGrandId(2);
//		abilityVO.setEmpId(3);
//		dao.insert(abilityVO);
//		System.out.println("insert successdsful");
	
		// 修改
//		SubjectVO subjectVO2 = new SubjectVO();
//		subjectVO2.setSubjectId(1002);
//		subjectVO2.setLicense("word_TQC國際認證");
//		subjectVO2.setName("word");	
//		subjectVO2.setClassId(1);
//		dao.update(subjectVO2);
//		System.out.println(subjectVO2.getLicense());
//		System.out.println(subjectVO2.getName());
//		System.out.println(subjectVO2.getClassId());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(1002,1002,2,3);
//		System.out.println("delete sucessful");


		// 查詢
//		List<AbilityVO> list = dao.getAll();
//		for (AbilityVO abilityVO5 : list) {
//			System.out.println(abilityVO5.getSubjectId()+",");
//			System.out.println(abilityVO5.getStandardId()+",");
//			System.out.println(abilityVO5.getGrandId()+",");
//			System.out.println(abilityVO5.getEmpId()+",");
//			System.out.println("SUCCEEDS");
//		}
	}

	


}