package com.ability.model;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.category.model.*;
import com.department.model.*;
import com.employee.model.*;
//import com.sun.xml.internal.xsom.impl.scd.Iterators.Map;
import com.util.SCECcommon;

public class AvgDAO implements AvgDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * get all average for all category and department
	 * Why using Long as map key ?
	 * In EL, numbers are treated as Long. It's looking for a Long key. It'll work if you use Long instead of Integer as map key.
	 */
	@Override
	public HashMap<Long, List<AvgVO>> getAvgByCatIdDeptId() {
		HashMap<Long, List<AvgVO>> map = new HashMap<Long, List<AvgVO>>();
		List<AvgVO> list = null;
		AvgVO avgVO = null;
		Connection con = null;
		CategoryService catSvc = new CategoryService();
		DepartmentService deptSvc = new DepartmentService();
		EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
		List<CategoryVO> catList = catSvc.getAll();
		List<DepartmentVO> deptList = deptSvc.getAll();
		int catId = 0, deptId = 0;
		long key = 0;
		float avg = 0;
		
		try {
			con = ds.getConnection();
			for (CategoryVO catVO : catList) {
				catId = catVO.getCategoryId();
				for (DepartmentVO deptVO : deptList) {
					avgVO = new AvgVO();
					deptId = deptVO.getDeptId();
					avg = empSvc.getAvgByDeptIdCatId(con, deptId, catId);
					
					avgVO.setCategoryId(catId);
					avgVO.setDeptId(deptId);
					avgVO.setAvg(avg);
					
					list = map.get((long)(catId));
					if (list == null) {
						list = new ArrayList<AvgVO>();
						map.put((long)catId, list);
					}
					list.add(avgVO);
				}
			}
		} catch (SQLException se) { 
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con, null, null, true);
		}
		
		return map;
	}
	
	
	/*
	 * get department average 
	 */
	@Override
	public List<AvgVO> getAvgByDeptId() {
		List<AvgVO> list = new ArrayList<AvgVO>();
		AvgVO avgVO = null;
		Connection con = null;
		DepartmentService deptSvc = new DepartmentService();
		EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
		List<DepartmentVO> deptList = deptSvc.getAll();
		int catId = 0, deptId = 0;
		long key = 0;
		float avg = 0;
		
		try {
			con = ds.getConnection();
			for (DepartmentVO deptVO : deptList) {
				avgVO = new AvgVO();
				deptId = deptVO.getDeptId();
				avg = empSvc.getAvgByDeptId(con, deptId);
					
				avgVO.setDeptId(deptId);
				avgVO.setAvg(avg);
				list.add(avgVO);
			}
		} catch (SQLException se) { 
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con, null, null, true);
		}
		
		return list;
	}

}
