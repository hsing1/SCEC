package com.ability.model;

public class AbilityVO implements java.io.Serializable{
	private Integer empId;
	private Integer subjectId;
	private double requiredGrade;
	private double grade;
	private boolean isRequired;
	
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public double getGrade() {
		return grade;
	}
	public void setGrade(double grade) {
		this.grade = grade;
	}
	public double getRequiredGrade() {
		return requiredGrade;
	}
	public void setRequiredGrade(double requiredGrade) {
		this.requiredGrade = requiredGrade;
	}
	public boolean isRequired() {
		return isRequired;
	}
	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}
}
