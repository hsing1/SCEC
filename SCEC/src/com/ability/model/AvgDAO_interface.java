package com.ability.model;

import java.util.*;



public interface AvgDAO_interface {

	HashMap<Long, List<AvgVO>> getAvgByCatIdDeptId();
	List<AvgVO> getAvgByDeptId();
}
