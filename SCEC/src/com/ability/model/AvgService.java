package com.ability.model;

import java.util.List;
import java.util.Map;

import com.subject.model.SubjectService;

public class AvgService {

	private AvgDAO_interface dao;

	public AvgService() {
		dao = new AvgDAO();
	}

	public Map<Long, List<AvgVO>> getAvgByCatIdDeptId() {
		return dao.getAvgByCatIdDeptId();
	}
	
	public List<AvgVO> getAvgByDeptId() {
		return dao.getAvgByDeptId();
	}
}
