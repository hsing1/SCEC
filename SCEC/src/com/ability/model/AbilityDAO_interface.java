package com.ability.model;

import java.util.*;

public interface AbilityDAO_interface {
          public void insert(AbilityVO abilityVO);
 //         public void update(AbilityVO abilityVO);
          public void delete(Integer subjectId,Integer  standardId,Integer  grandId,Integer empId);
          public List<AbilityVO> getAll();
          //萬用複合查詢(傳入參數型態Map)(回傳 List)
         public List<AbilityVO> getAll(Map<String, String[]> map); 
}
