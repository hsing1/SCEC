package com.subject.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONArray;

import com.department.model.*;
import com.employee.model.*;
import com.title.model.TitleVO;
import com.category.model.*;
import com.subject.model.*;

public class SubjectServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=Big5");
		PrintWriter out = res.getWriter();
		String action = req.getParameter("action");
		
		if ("listSubjectByEmpAndGrade".equals(action)) {
			String empIdStr = req.getParameter("selectEmp1");
			String gradeStr = req.getParameter("grade");
			String condStr = req.getParameter("selectCond");
			Integer empId = Integer.valueOf(empIdStr);
			Float grade = Float.valueOf(gradeStr);
			List<EmployeeAbilityViewVO> list = null;
			EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
			
			list = empSvc.getSubjectByEmpAndGrade(empId, grade, condStr);
			req.setAttribute("listSubjectByEmpAndGrade", list);

			String url = "/employee/selectEmpAndGrade.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
		}
		
		if ("listByCategoryIdJSON".equals(action)) {
			String catIdStr = req.getParameter("catId");
			Integer catId = Integer.valueOf(catIdStr);
			List<SubjectVO> list = null;
			SubjectService subSvc = new SubjectService();
			JSONArray subjectList = new JSONArray();
			
			list = subSvc.getByCategoryId(catId);
			for(SubjectVO subVO : list) {
				subjectList.put(subVO.getSubjectId());
				subjectList.put(subVO.getName());
			}
			
			System.out.println(subjectList);
			out.println(subjectList);
		}
		
		//request from listAllEmp.jsp 
		if ("listOneEmpAbility".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String requestPath = req.getRequestURI();
			String empIdStr = req.getParameter("empId");
			String catIdStr = req.getParameter("catId");
			Integer empId = Integer.valueOf(empIdStr);
			Integer catId = Integer.valueOf(catIdStr);

			try {
				EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
				List<EmployeeAbilityViewVO> list = empSvc.getOneEmployee(empId, catId);

				req.setAttribute("listOneEmpAbility", list);
				req.setAttribute("requestPath", requestPath);

				String url = "/employee/listOneEmpAbility.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		//request from listAllEmp.jsp 
		if ("listOneEmpTraining".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String empIdStr = req.getParameter("empId");
			Integer empId = Integer.valueOf(empIdStr);

			try {
				EmployeeTrainingViewService empSvc = new EmployeeTrainingViewService();
				List<EmployeeTrainingViewVO> list = empSvc.getOneEmployee(empId);

				req.setAttribute("listOneEmpTraining", list);

				String url = "/employee/listOneEmpTrainingRecord.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}

		//request from listAllEmp.jsp 
		if ("listAllEmp".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			int deptId = 0;

			try {
				/*************************** 1.�����ШD�Ѽ� ****************************************/

				/*************************** 2.�}�l�d�߸�� ****************************************/
				DepartmentService deptSvc = new DepartmentService();
				Set<EmployeeVO> set = deptSvc.getEmpsByDeptId(deptId);

				/*************************** 3.�d�ߧ���,�ǳ����(Send the Success view) ************/
				req.setAttribute("listEmpsByDeptId", set);    // ��Ʈw��X��set����,�s�Jrequest

				String url = null;
				if ("listEmps_ByDeptno_A".equals(action))
					url = "/dept/listEmps_ByDeptno.jsp";        // ���\��� dept/listEmps_ByDeptno.jsp
				else if ("listEmpsByDeptId_B".equals(action))
					url = "/dept/listAllDept.jsp";              // ���\��� dept/listAllDept.jsp

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

				/*************************** ��L�i�઺��~�B�z ***********************************/
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listAllEmpByDeptId".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				Integer deptId = new Integer(req.getParameter("deptId"));

				DepartmentService deptSvc = new DepartmentService();
				Set<EmployeeVO> set = deptSvc.getEmpsByDeptId(deptId);

				req.setAttribute("listEmpsByDeptId", set);

				String url = null;
				if ("listEmps_ByDeptno_A".equals(action))
					url = "/dept/listEmps_ByDeptno.jsp";
				else if ("listEmpsByDeptId_B".equals(action))
					url = "/dept/listAllDept.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listEmpByDeptAndTitle".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				Integer deptId = new Integer(req.getParameter("deptId"));
				Integer titleId = new Integer(req.getParameter("titleId"));

				EmployeeService empSvc = new EmployeeService();
				List<EmployeeVO> list = empSvc.getEmpsByDeptIdAndTitleId(deptId, titleId);
				JSONArray empList = new JSONArray();
				
				for (EmployeeVO empVO : list) {
					empList.put(empVO.getEmpId());
					empList.put(empVO.getName());
				}
				System.out.println(empList);
				out.println(empList);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		
		if ("delete_Dept".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.�����ШD�Ѽ�***************************************/
				Integer deptno = new Integer(req.getParameter("deptno"));
				
				/***************************2.�}�l�R�����***************************************/
				DepartmentService deptSvc = new DepartmentService();
				//deptSvc.deleteDept(deptno);
				
				/***************************3.�R������,�ǳ����(Send the Success view)***********/
				String url = "/dept/listAllDept.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// �R�����\��, ���\��� �^�� /dept/listAllDept.jsp
				successView.forward(req, res);
				
				/***************************��L�i�઺��~�B�z***********************************/
			} catch (Exception e) {
				errorMsgs.add("�R����ƥ���:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/dept/listAllDept.jsp");
				failureView.forward(req, res);
			}
		}

	}
}
