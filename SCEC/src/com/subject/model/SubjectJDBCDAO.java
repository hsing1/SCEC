package com.subject.model;

import java.util.*;
import java.sql.*;

public class SubjectJDBCDAO implements SubjectDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO SUBJECT (subject_id,license,name,class_id ) VALUES ( SUBJECT_seq.NEXTVAL, ?,?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT subject_id,license,name,class_id  FROM SUBJECT order by subject_id";
	private static final String GET_ONE_STMT = 
		"SELECT subject_id,license,name,class_id  FROM SUBJECT where subject_id = ?";
	private static final String DELETE = 
		"DELETE FROM SUBJECT where subject_id = ?";
	private static final String UPDATE = 
		"UPDATE SUBJECT set license=?,name = ?,class_id=? where subject_id=?";

	@Override
	public void insert(SubjectVO subjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, subjectVO.getLicense());
			pstmt.setString(2, subjectVO.getName());
			pstmt.setInt(3, subjectVO.getClassId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(SubjectVO subjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, subjectVO.getLicense());
			pstmt.setString(2, subjectVO.getName());
			pstmt.setInt(3, subjectVO.getClassId());
			pstmt.setInt(4, subjectVO.getSubjectId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer subjectId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, subjectId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public SubjectVO findByPrimaryKey(Integer subjectId) {

		SubjectVO subjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, subjectId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				subjectVO = new SubjectVO();
				subjectVO.setSubjectId(rs.getInt("subject_id"));
				subjectVO.setLicense(rs.getString("license"));
				subjectVO.setName(rs.getString("name"));
				subjectVO.setClassId(rs.getInt("class_id"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return subjectVO;
	}

	@Override
	public List<SubjectVO> getAll() {
		List<SubjectVO> list = new ArrayList<SubjectVO>();
		SubjectVO subjectVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				subjectVO = new SubjectVO();
				subjectVO.setSubjectId(rs.getInt("subject_id"));
				subjectVO.setLicense(rs.getString("license"));
				subjectVO.setName(rs.getString("name"));
				subjectVO.setClassId(rs.getInt("class_id"));
				list.add(subjectVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<SubjectVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		SubjectJDBCDAO dao = new SubjectJDBCDAO();

		// 新增
//		SubjectVO subjectVO = new SubjectVO();
//		subjectVO.setLicense("EXCEL國際認證");
//		subjectVO.setName("EXCEL");	
//		subjectVO.setClassId(1);
//		subjectVO.setLicense("word國際認證");
//		subjectVO.setName("word");	
//		subjectVO.setClassId(1);
//		dao.insert(subjectVO);	
//		System.out.println("insert successdsful");
	
		// 修改
//		SubjectVO subjectVO2 = new SubjectVO();
//		subjectVO2.setSubjectId(1002);
//		subjectVO2.setLicense("word_TQC國際認證");
//		subjectVO2.setName("word");	
//		subjectVO2.setClassId(1);
//		dao.update(subjectVO2);
//		System.out.println(subjectVO2.getLicense());
//		System.out.println(subjectVO2.getName());
//		System.out.println(subjectVO2.getClassId());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(1001);
//		System.out.println("delete sucessful");

		// 查詢
//		SubjectVO subjectVO3 = dao.findByPrimaryKey(1002);
//		System.out.println(subjectVO3.getSubjectId()+",");
//		System.out.println(subjectVO3.getLicense()+",");
//		System.out.println(subjectVO3.getName()+",");
//		System.out.println(subjectVO3.getClassId()+",");
//		System.out.print("select sucessful");

		// 查詢
//		List<SubjectVO> list = dao.getAll();
//		for (SubjectVO subjectVO5 : list) {
//			System.out.println(subjectVO5.getSubjectId()+",");
//			System.out.println(subjectVO5.getLicense()+",");
//			System.out.println(subjectVO5.getName()+",");
//			System.out.println(subjectVO5.getClassId()+",");
//			System.out.println("SUCCEEDS");
//		}
	}


}