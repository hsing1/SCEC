package com.subject.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.category.model.CategoryService;
import com.classes.model.ClassService;
import com.util.SCECstatus;
import com.util.SCECstatusEnum;

public class SubjectService {

	private SubjectDAO_interface dao;
	private static SubjectDAO daoS;
	public static int[] numOfSubArr; //number of subjects of each category
	public static int totalSub; //total number of subject
	public static Map<Integer, String> subjectMap;
	public static Map<String, Integer> subjectMapInv;
	public static Map<Integer, Float> subjectCmpAvgMap;
	
	static {
		if (daoS == null)
			daoS = new SubjectDAO();
		
		if ((SCECstatus.status == SCECstatusEnum.SCEC_DB_MODE) && (numOfSubArr == null)) {
			setNumOfSubject();
		}
	}

	public SubjectService() {
		dao = new SubjectDAO();
	}

	public SubjectVO addSubject(String license, String name, Integer classId) {

		SubjectVO subjectVO = new SubjectVO();	
		subjectVO.setLicense(license);
		subjectVO.setName(name);
		subjectVO.setClassId(classId);

		dao.insert(subjectVO);

		return subjectVO;
	}
	
	public SubjectVO setupSubject(String subject, String category, String classes, int classify) {
		CategoryService catSvc = new CategoryService();
		ClassService classSvc = new ClassService();
		SubjectVO subjectVO = new SubjectVO();	
		int subjectId, categoryId, classId = 0;
		
		subjectId = dao.findBySubjectName(subject).getSubjectId();
		categoryId = catSvc.getOneCategory(category).getCategoryId();
		classId = classSvc.getOneClass(classes).getClassId();
		
		subjectVO.setSubjectId(subjectId);
		subjectVO.setCategoryId(categoryId);
		subjectVO.setClassId(classId);
		subjectVO.setClassify(classify);
	
		dao.setup(subjectVO);

		return subjectVO;
	}

	public SubjectVO updateSubject(String license, String name, Integer classId) {

		SubjectVO subjectVO = new SubjectVO();	
		subjectVO.setLicense( license);
		subjectVO.setName(name);
		subjectVO.setClassId( classId);
	
		dao.update(subjectVO);

		return subjectVO;
	}

	public void deleteSubject(Integer subjectId) {
		dao.delete(subjectId);
	}

	public SubjectVO getOneSubject(Integer subjectId) {
		return dao.findByPrimaryKey(subjectId);
	}
	
	public SubjectVO getOneSubject(String subject) {
		return dao.findBySubjectName(subject);
	}
	
	public List<SubjectVO> getByCategoryId(Integer catId) {
		return dao.findByCategoryId(catId);
	}

	public List<SubjectVO> getAll() {
		return dao.getAll();
	}
	
	public List<SubjectVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public static void setNumOfSubject() {
		//TODO:the numOfSubArr[0] is total number of subject, it's better to process here
		numOfSubArr = SubjectDAO.getNumOfSubject();
		totalSub += numOfSubArr[0];
	}
	
	public static void setSubjectMap() {
		int subjectId = 0;
		String name = null;
		List<SubjectVO> list = daoS.getAll();
		subjectMap = new LinkedHashMap<Integer, String>();
		subjectMapInv = new LinkedHashMap<String, Integer>();
		subjectCmpAvgMap = new LinkedHashMap<Integer, Float>();
		
		for (SubjectVO subjectVO: list) {
			subjectId = subjectVO.getSubjectId();
			name = subjectVO.getName();
			subjectMap.put(subjectId, name);
			subjectMapInv.put(name, subjectId);
			subjectCmpAvgMap.put(subjectId, subjectVO.getCmpAvg());
		}
	}
}