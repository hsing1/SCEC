package com.subject.model;

public class SubjectVO implements java.io.Serializable{
	private Integer subjectId  ;
	private String  license;
	private String  name;
	private Integer classId;
	private Integer categoryId;
	private int classify;
	private float cmpAvg;
	
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getClassId() {
		return classId;
	}
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public int getClassify() {
		return classify;
	}
	public void setClassify(int classify) {
		this.classify = classify;
	}
	public float getCmpAvg() {
		return cmpAvg;
	}
	public void setCmpAvg(float cmpAvg) {
		this.cmpAvg = cmpAvg;
	}
}
