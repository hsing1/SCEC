package com.subject.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;
import com.employee.model.*;

public class SubjectDAO implements SubjectDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO SUBJECT (subject_id,license,name,class_id ) VALUES ( SUBJECT_seq.NEXTVAL, ?,?,?)";
	private static final String INSERT_STMT_MYSQL = 
			"INSERT INTO SUBJECT (name) VALUES (?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM SUBJECT order by subject_id";
	private static final String GET_ONE_STMT = 
			"SELECT subject_id,license,name,class_id  FROM SUBJECT where subject_id = ?";
	private static final String DELETE = 
			"DELETE FROM SUBJECT where subject_id = ?";
	private static final String UPDATE = 
			"UPDATE SUBJECT set license = ?, name = ?, class_id = ? where subject_id = ?";
	private static final String SETUP = 
			"UPDATE SUBJECT set class_id = ?, category_id = ?, classify = ? where subject_id=?";
	private static final String FIND_BY_SUBJECT = 
			"SELECT subject_id, license, name, category_id, class_id, classify  FROM SUBJECT where name = ?";
	private static final String COUNT_STMT = "SELECT count(subject_id) count FROM SUBJECT";
	private static final String GET_BY_CATEGORY_ID = 
			"SELECT subject_id, license, name, classify, class_id, category_id FROM SUBJECT where category_id = ?";

	public static void setVO(SubjectVO subjectVO, ResultSet rs, Connection con) throws SQLException {
		float cmpAvg = 0;
		int subjectId = 0;
		EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
		
		subjectId = rs.getInt("subject_id");
		subjectVO.setSubjectId(subjectId);
		subjectVO.setName(rs.getString("name"));
		subjectVO.setCategoryId(rs.getInt("category_id"));
		subjectVO.setClassId(rs.getInt("class_id"));
		subjectVO.setLicense(rs.getString("license"));
		subjectVO.setClassify(rs.getInt("classify"));
		cmpAvg = empAbSvc.getCompanyAvgBySubjetId(con, subjectId);
		subjectVO.setCmpAvg(cmpAvg);
	}
	
	public static int[] getNumOfSubject() {
		List<SubjectVO> list = new ArrayList<SubjectVO>();
		SubjectVO subjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int [] numOfSubArr = null;
		int classId = 0, catId = 0, count = 0, total = 0;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(COUNT_STMT);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				total = rs.getInt("count");
			}
			
			numOfSubArr = new int[total]; //TODO:the total number of subject is store in Subject.totalSub, it is no need calculate here
			numOfSubArr[0] = total;
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			//TODO issue 32 : it got catId = 0, cause incorrect numOfSubArr[0]
			while (rs.next()) {
				classId = rs.getInt("class_id");
				catId = rs.getInt("category_id");
				numOfSubArr[catId]++;
			}
			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return numOfSubArr;
	}
	
	
	@Override
	public void insert(SubjectVO subjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT_MYSQL);

			pstmt.setString(1, subjectVO.getName());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(SubjectVO subjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, subjectVO.getLicense());
			pstmt.setString(2, subjectVO.getName());
			pstmt.setInt(3, subjectVO.getClassId());
			pstmt.setInt(4, subjectVO.getSubjectId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public void setup(SubjectVO subjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(SETUP);
		
			pstmt.setInt(1, subjectVO.getClassId());
			pstmt.setInt(2, subjectVO.getCategoryId());
			pstmt.setInt(3, subjectVO.getClassify());
			pstmt.setInt(4, subjectVO.getSubjectId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(Integer subjectId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, subjectId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public SubjectVO findBySubjectName(String subject) {

		SubjectVO subjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean recordNotFound = true;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_SUBJECT);

			pstmt.setString(1, subject);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				recordNotFound = false;
				subjectVO = new SubjectVO();
				subjectVO.setSubjectId(rs.getInt("subject_id"));
				subjectVO.setLicense(rs.getString("license"));
				subjectVO.setName(subject);
				subjectVO.setClassId(rs.getInt("class_id"));
				subjectVO.setCategoryId(rs.getInt("category_id"));
				subjectVO.setClassify(rs.getInt("classify"));
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
			if (recordNotFound)
				throw new RuntimeException("Cannot find " + subject + " in database.");
		}
		return subjectVO;
	}
	
	@Override
	public List<SubjectVO> findByCategoryId(Integer catId) {

		SubjectVO subjectVO = null;
		List<SubjectVO> list = new ArrayList<SubjectVO>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_BY_CATEGORY_ID);

			pstmt.setInt(1, catId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				subjectVO = new SubjectVO();
				subjectVO.setSubjectId(rs.getInt("subject_id"));
				subjectVO.setLicense(rs.getString("license"));
				subjectVO.setName(rs.getString("name"));
				subjectVO.setClassId(rs.getInt("class_id"));
				subjectVO.setClassify(rs.getInt("classify"));
				subjectVO.setCategoryId(rs.getInt("category_id"));
				
				list.add(subjectVO);
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public SubjectVO findByPrimaryKey(Integer subjectId) {

		SubjectVO subjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, subjectId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				subjectVO = new SubjectVO();
				subjectVO.setSubjectId(rs.getInt("subject_id"));
				subjectVO.setLicense(rs.getString("license"));
				subjectVO.setName(rs.getString("name"));
				subjectVO.setClassId(rs.getInt("class_id"));
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return subjectVO;
	}

	@Override
	public List<SubjectVO> getAll() {
		List<SubjectVO> list = new ArrayList<SubjectVO>();
		SubjectVO subjectVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				subjectVO = new SubjectVO();
				setVO(subjectVO, rs, con);
				list.add(subjectVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<SubjectVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public static void main(String[] args) {

		SubjectDAO dao = new SubjectDAO();

	}


}