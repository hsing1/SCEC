package com.subject.model;

import java.util.*;

public interface SubjectDAO_interface {
	public void insert(SubjectVO subjectVO);
	public void update(SubjectVO subjectVO);
	public void setup(SubjectVO subjectVO);
	public void delete(Integer subjectId);
	public SubjectVO findByPrimaryKey(Integer subjectId);
	public List<SubjectVO> findByCategoryId(Integer catId);
	public SubjectVO findBySubjectName(String subject);
	public List<SubjectVO> getAll();
	public List<SubjectVO> getAll(Map<String, String[]> map); 
}
