package com.training.model;

import java.sql.Connection;
import java.util.*;

public interface TrainingDAO_interface {
    public void insert(TrainingVO trainingVO);
    public void update(TrainingVO trainingVO);
    public void delete();
    public TrainingVO findByPrimaryKey(int deptId, int sectionId, int titleId, int subjectId);
	public float getStandard(Connection con, int deptId, int titleId, int subjectId);
    public List<TrainingVO> getAll();
    public List<TrainingVO> getAll(Map<String, String[]> map); 
    
	//calculate number of employee for specified department 
	public int getNumOfEmp(Connection con, int deptId, int subjectId);
	public int getNumOfEmpD(Connection con, int deptId, int subjectId);
	//calculate number of employee for whole company
	public int getNumOfEmp(Connection con, int subjectId); 
	public int getNumOfEmpByTitle(Connection con, int titleId, int subjectId);
}
