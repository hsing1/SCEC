package com.training.model;
import java.sql.Date;

public class TrainingRecordVO implements java.io.Serializable{
	private int empId ;
	private int subjectId;
	private double grade;
	private Date  trainingDate;
	private Integer fee ;
	private Integer trainingHour ;
	private String certificate ;
	private Date  guarantee;	
	private Date  transfer;
	private String note;
	private byte [] report;
	private double compensate;
	private int type;
	
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public int getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	public double getGrade() {
		return grade;
	}
	public void setGrade(double grade) {
		this.grade = grade;
	}
	public Date getTrainingDate() {
		return trainingDate;
	}
	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}
	public Integer getFee() {
		return fee;
	}
	public void setFee(Integer fee) {
		this.fee = fee;
	}
	public Integer getTrainingHour() {
		return trainingHour;
	}
	public void setTrainingHour(Integer trainingHour) {
		this.trainingHour = trainingHour;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public Date getGuarantee() {
		return guarantee;
	}
	public void setGuarantee(Date guarantee) {
		this.guarantee = guarantee;
	}
	public Date getTransfer() {
		return transfer;
	}
	public void setTransfer(Date transfer) {
		this.transfer = transfer;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public byte[] getReport() {
		return report;
	}
	public void setReport(byte[] report) {
		this.report = report;
	}
	public double getCompensate() {
		return compensate;
	}
	public void setCompensate(double compensate) {
		this.compensate = compensate;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
