package com.training.model;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.subject.model.SubjectService;

public class TrainingRecordService {

	private TrainingRecordDAO_interface dao;

	public TrainingRecordService() {
		dao = new TrainingRecordDAO();
	}

	public TrainingRecordVO addTrainingRecord(int empId, String subject, 
			java.util.Date uDate, java.util.Date uTransfer, java.util.Date uGuarantee,
			int hour, int fee, int trainingType, double compensate, String note) {

		SubjectService subSvc = new SubjectService();
		int subjectId = subSvc.getOneSubject(subject).getSubjectId();
		Date trainingDate = new Date(uDate.getTime());
		Date transfer, guarantee = null;
		
		transfer = (uTransfer != null) ? new Date(uTransfer.getTime()) : null; 
		guarantee = (uGuarantee != null) ? new Date(uGuarantee.getTime()) : null; 
		
		TrainingRecordVO trainingRecordVO = new TrainingRecordVO();	
		trainingRecordVO.setEmpId(empId);
		trainingRecordVO.setSubjectId(subjectId);
		trainingRecordVO.setTrainingDate(trainingDate);
		trainingRecordVO.setFee(fee);
		trainingRecordVO.setTrainingHour(hour);
		trainingRecordVO.setGuarantee(guarantee);
		trainingRecordVO.setTransfer(transfer);
		trainingRecordVO.setTransfer(transfer);
		trainingRecordVO.setType(trainingType);
		trainingRecordVO.setCompensate(compensate);
		trainingRecordVO.setNote(note);
		dao.insert(trainingRecordVO);

		return trainingRecordVO;
	}

	public TrainingRecordVO updateTitle(float grade, Date trainingDate , Integer fee, Integer trainingHour, 
			String certificate , Date guarantee, Date transfer, String remark, String note, byte [] report, 
			Integer empId, Integer subjectId, Integer trainingId) {

		TrainingRecordVO rainingRecordVO = new TrainingRecordVO();	
		rainingRecordVO.setGrade(grade);
		rainingRecordVO.setTrainingDate(trainingDate);
		rainingRecordVO.setFee(fee);
		rainingRecordVO.setTrainingHour(trainingHour);
		rainingRecordVO.setCertificate(certificate);
		rainingRecordVO.setGuarantee(guarantee);
		rainingRecordVO.setTransfer(transfer);
		rainingRecordVO.setNote(note);
		rainingRecordVO.setEmpId(empId);
		rainingRecordVO.setSubjectId(subjectId);
	
		dao.update(rainingRecordVO);

		return rainingRecordVO;
	}

	public void deleteTitle(Integer empId) {
		dao.delete(empId);
	}

	public TrainingRecordVO getOneTitle(Integer trainingId) {
		return dao.findByPrimaryKey(trainingId);
	}

	public List<TrainingRecordVO> getAll() {
		return dao.getAll();
	}
	
	public List<TrainingRecordVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
