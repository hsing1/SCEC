package com.training.model;

import java.sql.Connection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.department.model.*;
import com.section.model.SectionService;
import com.subject.model.SubjectService;
import com.subject.model.SubjectVO;
import com.title.model.*;
import com.category.model.CategoryService;
import com.classes.model.ClassService;
import com.deptTitle.model.*;

public class TrainingService {
	public static Map<Integer, List<String>> subjectMap; //mapping subjectId -> list of deptId-sectionId-titleId
	public static Map<String, List<Integer>> subjectMapInv; //mapping deptId-sectionId-titleId -> list of subjectId
	private TrainingDAO_interface dao;
	
	static {
		if (subjectMap == null)
			subjectMap = new LinkedHashMap<Integer, List<String>>();
		
		if (subjectMapInv == null)
			subjectMapInv = new LinkedHashMap<String, List<Integer>>();
	}

	public TrainingService() {
		dao = new TrainingDAO();
	}

	public void addTraining(String dept, String section, String title, String subject, int standard, boolean certification) {
		int deptId, titleId, subjectId, sectionId, lowLevel = 0, level;
		String lowBound = null, res[] = null, positionCode = null;
		StringBuilder str = new StringBuilder();
		DepartmentService deptSvc = new DepartmentService();
		TitleService titleSvc = new TitleService();
		SubjectService subSvc = new SubjectService();
		SectionService secSvc = new SectionService();
		DeptTitleService deptTitleSvc = new DeptTitleService();
		Map<Integer, String> deptMap = null;
		List<String> list = null;
		List<Integer> listi = null;
		Set<Integer> sectionSet = new HashSet<Integer>();
		Set<Integer> allTitleSet = new HashSet<Integer>(), titleSet = new HashSet<Integer>();
		Pattern p = Pattern.compile(".+以上$");
		Matcher m = null;
		
		subjectId = subSvc.getOneSubject(subject).getSubjectId();
		
		if (dept.equals("全部")) {
			deptMap = DepartmentService.deptMap;
		} else {
			if (!DepartmentService.deptMapInv.containsKey(dept))
				throw new RuntimeException("Cannot find department " + dept);
			
			deptId = DepartmentService.deptMapInv.get(dept);
			deptMap = new LinkedHashMap<Integer, String>();
			deptMap.put(deptId, dept);
		}
		
		for (int dIdx : deptMap.keySet()) {
			sectionSet.clear();
			titleSet.clear();
			
			if (section.equals("全部")) {
				sectionSet = deptTitleSvc.getSectionSet(dIdx); 
			} else {
				if (!SectionService.sectionMapInv.containsKey(section))
					throw new RuntimeException("Cannot find section " + section);
				
				sectionId = SectionService.sectionMapInv.get(section);
				sectionSet.add(sectionId);
			}
			for (int sIdx : sectionSet) {
				if (title.equals("全部")) {
					titleSet = deptTitleSvc.getTitleSet(dIdx, sIdx);
				} else if (title.matches("(.+)以上$")) {
					/*
					m = p.matcher(title);
					if (m.find())
						lowBound = m.group(0);
						*/
					allTitleSet = deptTitleSvc.getTitleSet(dIdx, sIdx);
					res = title.split("以上");
					lowBound = res[0];
					//get title level
					if (!TitleService.titleLevelMap.containsKey(lowBound))
						throw new RuntimeException("Cannot find title " + lowBound);
					
					lowLevel = TitleService.titleLevelMap.get(lowBound);
					for (int t : allTitleSet) {
						level = TitleService.titleLevelMapInt.get(t);
						if (level >= lowLevel)
							titleSet.add(t);
					}
					
				} else {
					titleId = TitleService.titleMapInv.get(title);
					titleSet.add(titleId);
				}
				for (int tIdx : titleSet) {
					TrainingVO trainingVO = new TrainingVO();
					trainingVO.setDeptId(dIdx);
					trainingVO.setSectionId(sIdx);
					trainingVO.setTitleId(tIdx);
					trainingVO.setSubjectId(subjectId);
					trainingVO.setStandard(standard);
					trainingVO.setCertification(certification);
			
					dao.insert(trainingVO);
					
					str.append(dIdx).append("-").append(sIdx).append("-").append(tIdx);
					positionCode = str.toString();
					list = subjectMap.get(subjectId);
					if (list == null) {
						list = new ArrayList<String>();
						list.add(positionCode);
						subjectMap.put(subjectId, list);
					} else
						list.add(positionCode);
					
					listi = subjectMapInv.get(positionCode);
					if (listi == null) {
						listi = new ArrayList<Integer>();
						listi.add(subjectId);
						subjectMapInv.put(positionCode, listi);
					} else
						listi.add(subjectId);
					
					str.delete(0,  str.length());
				}
			}
		}

	}
	

	public TrainingVO updateTraining() {
		return null;
	}

	public void deleteTraining() {
	}

	public TrainingVO getOneTraining(String dept, String title, String subject) {
		return null;
	}
	
	public float getStandard(Connection con, int deptId, int titleId, int subjectId) {
		return dao.getStandard(con, deptId, titleId, subjectId);
	}
	
	//某個部門、某項職能的應備人數
	public int getNumOfEmp(Connection con, int deptId, int subjectId) {
		return dao.getNumOfEmp(con, deptId, subjectId);
	}
	
	public int getNumOfEmpD(Connection con, int deptId, int subjectId) {
		return dao.getNumOfEmpD(con, deptId, subjectId);
	}
	
	//某個職位（職稱）、某項職能的應備人數
	public int getNumOfEmpByTitle(Connection con, int titleId, int subjectId) {
		return dao.getNumOfEmpByTitle(con, titleId, subjectId);
	}
	
	//全公司某項職能的應備人數
	public int getNumOfEmp(Connection con, int subjectId) {
		return dao.getNumOfEmp(con, subjectId);
	}

	public List<TrainingVO> getAll() {
		return dao.getAll();
	}
	
	public List<TrainingVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public static boolean isRequired(String subject, String dept, String section, String title) {
		Integer subjectId, deptId, sectionId, titleId;
		StringBuilder valStr = new StringBuilder();
		List<String> list = null;
		
		subjectId = SubjectService.subjectMapInv.get(subject);
		deptId = DepartmentService.deptMapInv.get(dept);
		sectionId = SectionService.sectionMapInv.get(section);
		titleId = TitleService.titleMapInv.get("aaa");
		titleId = TitleService.titleMapInv.get(title);
		
		list = TrainingService.subjectMap.get(subjectId);
		if (list == null)
			return false;
			
		valStr.append(deptId).append("-").append(sectionId).append("-").append(titleId);
		if (list.contains(valStr.toString()))
			return true;
		else
			return false;
		
	}
	
}