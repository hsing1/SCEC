package com.training.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.department.model.DepartmentService;
import com.employee.model.EmployeeService;
import com.section.model.SectionService;
import com.title.model.TitleService;
import com.util.SCECcommon;
import com.subject.model.*;

public class TrainingDAO implements TrainingDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO TRAINING_SYSTEM (dept_id, section_id, title_id, subject_id, standard, certification) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT subject_id,license,name,class_id  FROM SUBJECT order by subject_id";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM TRAINING_SYSTEM where dept_id = ? and section_id = ? and title_id = ? and subject_id = ?";
	private static final String DELETE = 
			"DELETE FROM SUBJECT where subject_id = ?";
	private static final String UPDATE = 
			"UPDATE SUBJECT set license = ?, name = ?, class_id = ? where subject_id = ?";
	private static final String SETUP = 
			"UPDATE SUBJECT set class_id = ?, category_id = ?, classify = ? where subject_id=?";
	private static final String FIND_BY_SUBJECT = 
			"SELECT subject_id, license, name, category_id, class_id, classify  FROM SUBJECT where name = ?";
	private static final String GET_STANDARD_STMT = "SELECT standard FROM TRAINING_SYSTEM where dept_id = ? and title_id = ? and subject_id = ?";
	private static final String GET_BY_DEPT_SUBJECT_STMT = "SELECT * FROM TRAINING_SYSTEM where dept_id = ? and subject_id = ?";
	private static final String GET_BY_TITLE_SUBJECT_STMT = "SELECT * FROM TRAINING_SYSTEM where title_id = ? and subject_id = ?";
	private static final String GET_BY_SUBJECT_STMT = "SELECT * FROM TRAINING_SYSTEM where subject_id = ?";

	@Override
	public void insert(TrainingVO trainingVO) {
		int deptId, sectionId, titleId, subjectId;
		String dept, section, title, subject;
		Connection con = null;
		PreparedStatement pstmt = null;
		TrainingVO trnVO = null;
		
		deptId = trainingVO.getDeptId();
		sectionId = trainingVO.getSectionId();
		titleId = trainingVO.getTitleId();
		subjectId = trainingVO.getSubjectId();
		
		dept = DepartmentService.deptMap.get(deptId);
		section = SectionService.sectionMap.get(sectionId);
		title = TitleService.titleMap.get(titleId);
		subject = SubjectService.subjectMap.get(subjectId);
		
		trnVO = findByPrimaryKey(deptId, sectionId, titleId, subjectId);
		if (trnVO != null)
			return;
			//TODO:should add error msg to SCECerrorMsg here
			//throw new RuntimeException("Duplicate record : " + subject + " " + dept + " " + section + " " + title + " in training system");

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, trainingVO.getDeptId());
			pstmt.setInt(2, trainingVO.getSectionId());
			pstmt.setInt(3, trainingVO.getTitleId());
			pstmt.setInt(4, trainingVO.getSubjectId());
			pstmt.setInt(5, trainingVO.getStandard());
			pstmt.setBoolean(6, trainingVO.getCertification());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(TrainingVO subjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	

	@Override
	public void delete() {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	@Override
	public float getStandard(Connection con, int deptId, int titleId, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float standard = 0;
		
		try {
			pstmt = con.prepareStatement(GET_STANDARD_STMT);

			pstmt.setInt(1, deptId);
			pstmt.setInt(2, titleId);
			pstmt.setInt(3, subjectId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				standard = rs.getFloat("standard");
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			/*
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
			*/
		}
		return standard;
	}
	
	/*
	 * get number of employee in company that required the ability of the specified subject 
	 */
	@Override
	public int getNumOfEmp(Connection con, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float standard = 0;
		TrainingVO trainingVO = null;
		List<TrainingVO> list = new ArrayList<TrainingVO>();
		int titleId = 0, deptId = 0;
		EmployeeService empSvc = new EmployeeService();
		int numOfEmp = 0;
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			pstmt = con.prepareStatement(GET_BY_SUBJECT_STMT);
			pstmt.setInt(1, subjectId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				deptId = rs.getInt("dept_id");
				titleId = rs.getInt("title_id");
				numOfEmp += empSvc.getNumOfEmpByDeptIdAndTitleId(con, deptId, titleId);
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			close(rs, con, pstmt, closeCon);
		}
		return numOfEmp;
	}
	
	/*
	 * get number of employee in department that required the ability of the specified subject 
	 */
	@Override
	public int getNumOfEmp(Connection con, int deptId, int subjectId) {
		int titleId = 0, sectionId = 0;
		int numOfEmp = 0;
		float standard = 0;
		boolean closeCon = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		TrainingVO trainingVO = null;
		List<TrainingVO> list = new ArrayList<TrainingVO>();
		EmployeeService empSvc = new EmployeeService();
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			pstmt = con.prepareStatement(GET_BY_DEPT_SUBJECT_STMT);

			pstmt.setInt(1, deptId);
			pstmt.setInt(2, subjectId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				sectionId = rs.getInt("section_id");
				titleId = rs.getInt("title_id");
				//TODO:Issue #91, should add sectionId
				//numOfEmp += empSvc.getNumOfEmpByDeptIdAndTitleId(con, deptId, titleId);
				numOfEmp += empSvc.getNumOfEmpByDeptIdAndSectionIdAndTitleId(con, deptId, sectionId, titleId);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			close(rs, con, pstmt, closeCon);
		}
		return numOfEmp;
	}
	
	@Override
	public int getNumOfEmpD(Connection con, int deptId, int subjectId) {
		int titleId = 0, sectionId = 0;
		int numOfEmp = 0;
		float standard = 0;
		boolean closeCon = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		TrainingVO trainingVO = null;
		List<TrainingVO> list = new ArrayList<TrainingVO>();
		EmployeeService empSvc = new EmployeeService();
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			pstmt = con.prepareStatement(GET_BY_DEPT_SUBJECT_STMT);

			pstmt.setInt(1, deptId);
			pstmt.setInt(2, subjectId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				sectionId = rs.getInt("section_id");
				titleId = rs.getInt("title_id");
				//TODO:Issue #91, should add sectionId
				//numOfEmp += empSvc.getNumOfEmpByDeptIdAndTitleId(con, deptId, titleId);
				numOfEmp += empSvc.getNumOfEmpByDeptIdAndSectionIdAndTitleId(con, deptId, sectionId, titleId);
			}
			numOfEmp = DepartmentService.numOfEmpDept.get(deptId);

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			close(rs, con, pstmt, closeCon);
		}
		return numOfEmp;
	}
	
	/*
	 * get number of employee in department that required the ability of the specified subject 
	 */
	@Override
	public int getNumOfEmpByTitle(Connection con, int titleId, int subjectId) {
		int deptId = 0, sectionId = 0;
		int numOfEmp = 0;
		float standard = 0;
		boolean closeCon = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		TrainingVO trainingVO = null;
		List<TrainingVO> list = new ArrayList<TrainingVO>();
		EmployeeService empSvc = new EmployeeService();
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			pstmt = con.prepareStatement(GET_BY_TITLE_SUBJECT_STMT);

			pstmt.setInt(1, titleId);
			pstmt.setInt(2, subjectId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				deptId = rs.getInt("dept_id");
				sectionId = rs.getInt("section_id");
				//numOfEmp += empSvc.getNumOfEmpByDeptIdAndTitleId(con, deptId, titleId);
				numOfEmp += empSvc.getNumOfEmpByDeptIdAndSectionIdAndTitleId(con, deptId, sectionId, titleId);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			close(rs, con, pstmt, closeCon);
		}
		return numOfEmp;
	}

	@Override
	public TrainingVO findByPrimaryKey(int deptId, int sectionId, int titleId, int subjectId) {

		TrainingVO trainingVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptId);
			pstmt.setInt(2, sectionId);
			pstmt.setInt(3, titleId);
			pstmt.setInt(4, subjectId);
	
			rs = pstmt.executeQuery();
			while (rs.next()) {
				trainingVO = new TrainingVO();
				trainingVO.setDeptId(rs.getInt("dept_id"));
				trainingVO.setSectionId(rs.getInt("section_id"));
				trainingVO.setTitleId(rs.getInt("title_id"));
				trainingVO.setSubjectId(rs.getInt("subject_id"));
				trainingVO.setStandard(rs.getInt("standard"));
				trainingVO.setCertification(rs.getBoolean("certification"));
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return trainingVO;
	}

	@Override
	public List<TrainingVO> getAll() {
		List<TrainingVO> list = new ArrayList<TrainingVO>();
		TrainingVO subjectVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			close(rs, con, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<TrainingVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void close(ResultSet rs, Connection con, PreparedStatement pstmt, boolean closeCon) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
		
		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
		
		if (closeCon) {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

}