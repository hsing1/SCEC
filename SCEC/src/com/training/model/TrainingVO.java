package com.training.model;

public class TrainingVO implements java.io.Serializable{
	private int deptId;
	private int sectionId;
	private int titleId;
	private int subjectId;
	private int standard;
	private boolean certification;
	
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public int getTitleId() {
		return titleId;
	}
	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}
	public int getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	public int getStandard() {
		return standard;
	}
	public void setStandard(int standard) {
		this.standard = standard;
	}
	public boolean getCertification() {
		return certification;
	}
	public void setCertification(boolean certification) {
		this.certification = certification;
	}
	public int getSectionId() {
		return sectionId;
	}
	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}
}
