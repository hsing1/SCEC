package com.training.model;

import java.util.*;

public interface TrainingRecordDAO_interface {
	public void insert(TrainingRecordVO recordVO);
	public void update(TrainingRecordVO recordVO);
	public void delete(Integer trainingId);
	public TrainingRecordVO findByPrimaryKey(Integer trainingId);
	public List<TrainingRecordVO> getAll();
	//萬用複合查詢(傳入參數型態Map)(回傳 List)
	public List<TrainingRecordVO> getAll(Map<String, String[]> map); 
}
