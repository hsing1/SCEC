package com.training.model;
import java.sql.*;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class TrainingRecordDAO implements TrainingRecordDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO TRAINING_RECORD (emp_id, subject_id, training_date, fee, training_hour, transfer, guarantee, note, training_type, compensate) " +
		"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT =  
		"SELECT emp_id, subject_id, training_date, fee, training_hours, guarantee, transfer, note, FROM TRAINING_RECORD order by emp_id";
	private static final String GET_ONE_STMT = 
		"SELECT training_id,score,training_date,fee,training_hours,certificate, guarantee, trun,remark ,feedback,emp_id,subject_id  FROM RECORD where training_id = ?";
	private static final String DELETE = 
		"DELETE FROM RECORD where training_id = ?";
	private static final String UPDATE = 
		"UPDATE RECORD  SET  score=?,training_date=?,fee=?,training_hours=?,certificate=?, guarantee=?, trun=?,remark=? ,feedback=?,emp_id=?,subject_id=?  where training_id=?";

	@Override
	public void insert(TrainingRecordVO recordVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
		
			pstmt.setInt(1, recordVO.getEmpId());	
			pstmt.setInt(2, recordVO.getSubjectId());	
			pstmt.setDate(3, recordVO.getTrainingDate());
			pstmt.setInt(4, recordVO.getFee());
			pstmt.setInt(5, recordVO.getTrainingHour());
			pstmt.setDate(6, recordVO.getTransfer());
			pstmt.setDate(7, recordVO.getGuarantee());
			pstmt.setString(8, recordVO.getNote());
			pstmt.setInt(9, recordVO.getType());
			pstmt.setDouble(10, recordVO.getCompensate());
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(TrainingRecordVO recordVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setInt(10, recordVO.getEmpId());	
			pstmt.setInt(11, recordVO.getSubjectId());	
			pstmt.setDouble(1, recordVO.getGrade());
			pstmt.setDate(2, recordVO.getTrainingDate());
			pstmt.setInt(3, recordVO.getFee());
			pstmt.setInt(4, recordVO.getTrainingHour());
			pstmt.setString(5, recordVO.getCertificate());
			pstmt.setDate(6, recordVO.getGuarantee());
			pstmt.setDate(7, recordVO.getTransfer());
			pstmt.setString(8, recordVO.getNote());
			pstmt.executeUpdate();
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer trainingId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, trainingId);

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public TrainingRecordVO findByPrimaryKey(Integer trainingId) {

		TrainingRecordVO recordVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, trainingId);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				recordVO = new TrainingRecordVO();
				recordVO.setEmpId(rs.getInt("emp_id"));
				recordVO.setSubjectId(rs.getInt("subject_id"));
				recordVO.setGrade(rs.getDouble("grade"));
				recordVO.setTrainingDate(rs.getDate("training_date"));
				recordVO.setFee(rs.getInt("fee"));
				recordVO.setTrainingHour(rs.getInt("training_hours"));
				recordVO.setCertificate(rs.getString("certificate"));
				recordVO.setGuarantee(rs.getDate("guarantee"));
				recordVO.setTransfer(rs.getDate("transfer"));
				recordVO.setNote(rs.getString("notw"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return recordVO;
	}

	@Override
	public List<TrainingRecordVO> getAll() {
		List<TrainingRecordVO> list = new ArrayList<TrainingRecordVO>();
		TrainingRecordVO recordVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				recordVO = new TrainingRecordVO();
				recordVO.setEmpId(rs.getInt("emp_id"));
				recordVO.setSubjectId(rs.getInt("subject_id"));
				recordVO.setTrainingDate(rs.getDate("training_date"));
				recordVO.setFee(rs.getInt("fee"));
				recordVO.setTrainingHour(rs.getInt("training_hours"));
				recordVO.setGuarantee(rs.getDate("guarantee"));
				recordVO.setTransfer(rs.getDate("transfer"));
				recordVO.setNote(rs.getString("note"));
				list.add(recordVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<TrainingRecordVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

}