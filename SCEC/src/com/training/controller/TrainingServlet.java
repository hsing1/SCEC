package com.training.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONArray;

import com.department.model.*;
import com.employee.model.*;
import com.title.model.TitleVO;
import com.category.model.*;
import com.subject.model.*;
import com.standard.model.*;
import com.util.*;

public class TrainingServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		PrintWriter out = res.getWriter();
		String action = req.getParameter("action");
		
		//辦訓科目
		if ("showUnqualified".equals(action)) {
			Map<Integer, Integer> map = null, mapL0 =null, mapL1 = null, mapL2 = null, mapL3 = null, mapL4 = null;
			EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
			EmployeeViewVO myEmpVO = SCECcommon.getMyEmp(req, res);
			RequestDispatcher successView = null;
			
			map = empAbSvc.getUnqualified(myEmpVO, -1);
			mapL0 = empAbSvc.getUnqualified(myEmpVO, 0);
			mapL1 = empAbSvc.getUnqualified(myEmpVO, 1);
			mapL2 = empAbSvc.getUnqualified(myEmpVO, 2);
			mapL3 = empAbSvc.getUnqualified(myEmpVO, 3);
			mapL4 = empAbSvc.getUnqualified(myEmpVO, 4);
			req.setAttribute("mapUnqualified", map);
			req.setAttribute("mapL0", mapL0);
			req.setAttribute("mapL1", mapL1);
			req.setAttribute("mapL2", mapL2);
			req.setAttribute("mapL3", mapL3);
			req.setAttribute("mapL4", mapL4);
			req.setAttribute("subjectMap", SubjectService.subjectMap);
			req.setAttribute("subjectMapAvg", SubjectService.subjectCmpAvgMap);
			String url = "/training/listUnqualified.jsp";
			successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
		}
		
	}
}
