package com.classes.model;

import java.util.*;

public interface ClassDAO_interface {
          public void insert(ClassVO classesVO);
          public void update(ClassVO classesVO);
          public void delete(Integer classesId);
          public ClassVO findByPrimaryKey(Integer classesId);
          public ClassVO findByClassName(String className);
          public List<ClassVO> getAll();
          //萬用複合查詢(傳入參數型態Map)(回傳 List)
         public List<ClassVO> getAll(Map<String, String[]> map); 
}
