package com.classes.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class ClassDAO implements ClassDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO CLASS (class_id ,name,category_id ) VALUES ( CLASSES_seq.NEXTVAL, ?,?)";
	private static final String INSERT_STMT_MYSQL = "INSERT INTO CLASS (name) VALUES (?)";
	private static final String GET_ALL_STMT = 
		"SELECT class_id ,name,category_id  FROM CLASSES order by class_id";
	private static final String GET_ONE_STMT = 
		"SELECT class_id ,name,category_id  FROM CLASSES where class_id = ?";
	private static final String DELETE = 
		"DELETE FROM CLASSES where class_id = ?";
	private static final String UPDATE = 
		"UPDATE CLASSES set name = ?, category_id =?  where class_id=?";
	private static final String FIND_BY_CLASS = "SELECT class_id, name, category_id FROM CLASS where name = ?";

	@Override
	public void insert(ClassVO classesVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT_MYSQL);

			pstmt.setString(1, classesVO.getName());
			//pstmt.setInt(2, classesVO.getCategoryId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(ClassVO classesVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, classesVO.getName());
			pstmt.setInt(2, classesVO.getCategoryId());
			pstmt.setInt(3, classesVO.getClassId());
			
			
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer classId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, classId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public ClassVO findByClassName(String className) {

		ClassVO classesVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean recordNotFound = true;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_CLASS);

			pstmt.setString(1, className);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				recordNotFound = false;
				classesVO = new ClassVO();
				classesVO.setClassId(rs.getInt("class_id"));
				classesVO.setName(className);
				classesVO.setCategoryId(rs.getInt("category_id"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
			if (recordNotFound)
				throw new RuntimeException("Cannot find " + className + " in database 職能項目.");
		}
		return classesVO;
	}

	@Override
	public ClassVO findByPrimaryKey(Integer classId) {

		ClassVO classesVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, classId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				classesVO = new ClassVO();
				classesVO.setClassId(rs.getInt("class_id"));
				classesVO.setName(rs.getString("name"));
				classesVO.setCategoryId(rs.getInt("category_id"));

			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return classesVO;
	}

	@Override
	public List<ClassVO> getAll() {
		List<ClassVO> list = new ArrayList<ClassVO>();
		ClassVO classesVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				classesVO = new ClassVO();
				classesVO = new ClassVO();
				classesVO.setClassId(rs.getInt("class_id"));
				classesVO.setName(rs.getString("name"));
				classesVO.setCategoryId(rs.getInt("category_id"));
				list.add(classesVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<ClassVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;  
	}

	public static void main(String[] args) {

		ClassDAO dao = new ClassDAO();

		// �s�W
//		ClassVO classesVO = new ClassesVO();
//		classesVO.setClassId(1);
//		classesVO.setName("�ޯඵ");
//		classesVO.setCategoryId(1);
//		dao.insert(classesVO);
//		
//		System.out.println("insert sucessful");
	
		// �ק�
//		ClassVO classesVO2 = new ClassesVO();
//		classesVO2.setClassId(1);
//		classesVO2.setName("�޳N��");
//		classesVO2.setCategoryId(1);
//		dao.update(classesVO2);
//		System.out.println(classesVO2.getClassId());
//		System.out.println(classesVO2.getName());
//		System.out.println(classesVO2.getCategoryId());
//		System.out.println("update sucessful");
	
		// �R��
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// �d��
//		ClassVO classesVO4 = dao.findByPrimaryKey(1);
//		System.out.print(classesVO4.getClassId() + ",");
//		System.out.print(classesVO4.getName() + ",");
//		System.out.print(classesVO4.getClassId() + ",");
//		System.out.println("select sucessful");

		// �d��
//		List<ClassVO> list = dao.getAll();
//		for (ClassVO classesVO5 : list) {
//		System.out.print(classesVO5.getClassId() + ",");
//		System.out.print(classesVO5.getName() + ",");
//		System.out.print(classesVO5.getClassId() + ",");
//		System.out.println();
//		}
	}


}