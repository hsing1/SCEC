package com.classes.model;

import java.util.*;
import java.sql.*;

public class ClassJDBCDAO implements ClassDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl1";
	String userid = "scec";
	String passwd = "1234";

	private static final String INSERT_STMT = 
		"INSERT INTO CLASSES (class_id ,name,category_id ) VALUES ( CLASSES_seq.NEXTVAL, ?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT class_id ,name,category_id  FROM CLASSES order by class_id";
	private static final String GET_ONE_STMT = 
		"SELECT class_id ,name,category_id  FROM CLASSES where class_id = ?";
	private static final String DELETE = 
		"DELETE FROM CLASSES where class_id = ?";
	private static final String UPDATE = 
		"UPDATE CLASSES set name = ?, category_id =?  where class_id=?";

	@Override
	public void insert(ClassVO classesVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, classesVO.getName());
			pstmt.setInt(2, classesVO.getCategoryId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(ClassVO classesVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, classesVO.getName());
			pstmt.setInt(2, classesVO.getCategoryId());
			pstmt.setInt(3, classesVO.getClassId());
			
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer classId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, classId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public ClassVO findByPrimaryKey(Integer classId) {

		ClassVO classesVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, classId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				classesVO = new ClassVO();
				classesVO.setClassId(rs.getInt("class_id"));
				classesVO.setName(rs.getString("name"));
				classesVO.setCategoryId(rs.getInt("category_id"));

			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return classesVO;
	}

	@Override
	public List<ClassVO> getAll() {
		List<ClassVO> list = new ArrayList<ClassVO>();
		ClassVO classesVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				classesVO = new ClassVO();
				classesVO = new ClassVO();
				classesVO.setClassId(rs.getInt("class_id"));
				classesVO.setName(rs.getString("name"));
				classesVO.setCategoryId(rs.getInt("category_id"));
				list.add(classesVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<ClassVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;  
	}

	public static void main(String[] args) {

		ClassJDBCDAO dao = new ClassJDBCDAO();
		int type = 0;

		switch (type) {
		case 0:
			// 新增
			ClassVO classesVO = new ClassVO();
			classesVO.setName("技能項");
			classesVO.setCategoryId(2);
			dao.insert(classesVO);
			
			System.out.println("insert sucessful");
			break;
		case 1:
			// 修改
			ClassVO classesVO2 = new ClassVO();
			classesVO2.setClassId(1);
			classesVO2.setName("技術項");
			classesVO2.setCategoryId(1);
			dao.update(classesVO2);
			System.out.println(classesVO2.getClassId());
			System.out.println(classesVO2.getName());
			System.out.println(classesVO2.getCategoryId());
			System.out.println("update sucessful");
			break;
		case 2:
			// 刪除
			dao.delete(1);
			System.out.println("delete sucessful");
			break;
		case 3:
			// 查詢
			ClassVO classesVO4 = dao.findByPrimaryKey(1);
			System.out.print(classesVO4.getClassId() + ",");
			System.out.print(classesVO4.getName() + ",");
			System.out.print(classesVO4.getClassId() + ",");
			System.out.println("select sucessful");
			break;
		default:
			// 查詢
			List<ClassVO> list = dao.getAll();
			for (ClassVO classesVO5 : list) {
				System.out.print(classesVO5.getClassId() + ",");
				System.out.print(classesVO5.getName() + ",");
				System.out.print(classesVO5.getClassId() + ",");
				System.out.println();
			}
			break;
		}
	}
}