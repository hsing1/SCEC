package com.classes.model;

import java.util.List;
import java.util.Map;

public class ClassService {

	private ClassDAO_interface dao;

	public ClassService() {
		dao = new ClassDAO();
	}

	public ClassVO addClass(String name) {

		ClassVO classesVO = new ClassVO();
	
		classesVO.setName(name);

		dao.insert(classesVO);

		return classesVO;
	}

	public ClassVO updateClass(Integer classId, String name, Integer categoryId) {

		ClassVO classesVO = new ClassVO();

		classesVO.setClassId( classId);
		classesVO.setName(name);
		classesVO.setCategoryId(categoryId);
	
		dao.update(classesVO);

		return classesVO;
	}

	public void deleteClass(Integer classId) {
		dao.delete(classId);
	}

	public ClassVO getOneClass(Integer classId) {
		return dao.findByPrimaryKey(classId);
	}
	
	public ClassVO getOneClass(String className) {
		return dao.findByClassName(className);
	}

	public List<ClassVO> getAll() {
		return dao.getAll();
	}
	
	public List<ClassVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
