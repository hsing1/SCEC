package com.title.model;

import java.util.*;

public interface TitleDAO_interface {
          public void insertName(TitleVO titleVO);
          public void insert(TitleVO titleVO);
          public void update(TitleVO titleVO);
          public void delete(Integer titleId);
          public void setup(TitleVO titleVO);
          public TitleVO findByPrimaryKey(Integer titleId);
          public TitleVO findByTitleName(String title);
          public List<TitleVO> getAll();
         public List<TitleVO> getAll(Map<String, String[]> map); 
}
