package com.title.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.salary.model.*;
import com.util.SCECcommon;

public class TitleDAO implements TitleDAO_interface {

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}


	private static final String INSERT_STMT = "INSERT INTO TITLE (title_id,name,series_id,level_id ) VALUES ( TITLE_seq.NEXTVAL, ?,?,?)";
	private static final String INSERT_STMT_MYSQL = "INSERT INTO TITLE (name, series_id, level_id, salary_level, priv) VALUES (?, ?, ?, ?, ?)";
	private static final String INSERT_NAME= "INSERT INTO TITLE (name, salary_level) VALUES (?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT title_id, name, series_id, level_id, priv FROM TITLE order by title_id";
	private static final String GET_ONE_STMT = 
		"SELECT title_id, name, series_id, level_id, priv FROM TITLE where title_id = ?";
	private static final String DELETE = 
		"DELETE FROM TITLE where title_id = ?";
	private static final String UPDATE = 
		"UPDATE TITLE set series_id = ?, level_id = ? where title_id = ? and salary_level = ?";
	private static final String SETUP = 
		"UPDATE TITLE set series_id = ? , level_id = ?, salary_level = ?, priv = ? where title_id = ?";
	private static final String FIND_BY_TITLE = 
		"SELECT title_id, name, series_id, level_id, priv FROM TITLE where name = ?";

	@Override
	public void insertName(TitleVO titleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_NAME);

			pstmt.setString(1, titleVO.getName());
			pstmt.setInt(2, titleVO.getSalaryLevel());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	@Override
	public void insert(TitleVO titleVO) {
		Integer titleId = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer salLevel = titleVO.getSalaryLevel();
		SalaryService salSvc = new SalaryService();

		try {

			con = ds.getConnection();
			String cols[] = {"title_id"};
			pstmt = con.prepareStatement(INSERT_STMT_MYSQL, cols);

			pstmt.setString(1, titleVO.getName());
			pstmt.setInt(2, titleVO.getSeriesId());
			pstmt.setInt(3, titleVO.getLevelId());
			pstmt.setInt(4, titleVO.getPriv());
			pstmt.setInt(4, salLevel);
		
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				titleId = rs.getInt(1);
			}
			
			for (int i = 1; i <= salLevel; i++) {
				salSvc.addSalary(titleId, i, 0);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public void setup(TitleVO titleVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer titleId = titleVO.getTitleId();
		Integer salaryLevel = titleVO.getSalaryLevel();
		SalaryService salSvc = new SalaryService();

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(SETUP);
			
			pstmt.setInt(1, titleVO.getSeriesId());
			pstmt.setInt(2, titleVO.getLevelId());
			pstmt.setInt(3, salaryLevel);
			pstmt.setInt(4, titleVO.getPriv());
			pstmt.setInt(5, titleVO.getTitleId());
			
			pstmt.executeUpdate();
			
			for (int i = 1; i <= salaryLevel; i++) {
				salSvc.addSalary(titleId, i, 0);
			}
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(TitleVO titleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, titleVO.getName());
			pstmt.setInt(2, titleVO.getTitleId());
			pstmt.setInt(3, titleVO.getLevelId());
			pstmt.setInt(4, titleVO.getTitleId());
			
			
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer titleId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, titleId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public TitleVO findByTitleName(String title) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		TitleVO titleVO = null;
		int titleId = 0;
		boolean recordNotFound = true;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_TITLE);

			pstmt.setString(1, title);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				recordNotFound = false;
				titleVO = new TitleVO();
				titleVO.setTitleId(rs.getInt("title_id"));
				titleVO.setName(title);
				titleVO.setSeriesId(rs.getInt("series_id"));
				titleVO.setLevelId(rs.getInt("level_id"));
				titleVO.setPriv(rs.getInt("priv"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
			if (recordNotFound)
				throw new RuntimeException("Cannot find " + title + " in database.");
		}
		
		return titleVO;
	}

	@Override
	public TitleVO findByPrimaryKey(Integer titleId) {

		TitleVO titleVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, titleId);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				titleVO = new TitleVO();
				titleVO.setTitleId(rs.getInt("title_id"));
				titleVO.setName(rs.getString("name"));
				titleVO.setSeriesId(rs.getInt("series_id"));
				titleVO.setLevelId(rs.getInt("level_id"));
				titleVO.setPriv(rs.getInt("priv"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return titleVO;
	}

	@Override
	public List<TitleVO> getAll() {
		List<TitleVO> list = new ArrayList<TitleVO>();
		TitleVO titleVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				titleVO = new TitleVO();
				titleVO.setTitleId(rs.getInt("title_id"));
				titleVO.setName(rs.getString("name"));
				titleVO.setSeriesId(rs.getInt("series_id"));
				titleVO.setLevelId(rs.getInt("level_id"));
				titleVO.setPriv(rs.getInt("priv"));
				list.add(titleVO); // Store the row in the list
			}

			// Handle any driver errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<TitleVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		TitleDAO dao = new TitleDAO();

		// �s�W
//		TitleVO titleVO = new TitleVO();
//		titleVO.setTitleId(1);
//		titleVO.setName("���űM��");
//		titleVO.setSeriesId(1);
//		titleVO.setLevelId(4);
//		dao.insert(titleVO);
//		
//		System.out.println("insert sucessful");
	
		// �ק�
//		TitleVO titleVO2 = new TitleVO();
//		titleVO2.setTitleId(1);
//		titleVO2.setName("��¦�M��");
//		titleVO2.setSeriesId(2);
//		titleVO2.setLevelId(2);
//		dao.update(titleVO2);
//		System.out.println(titleVO2.getTitleId());
//		System.out.println(titleVO2.getName());
//		System.out.println(titleVO2.getSeriesId());
//		System.out.println(titleVO2.getLevelId());
//		System.out.println("update sucessful");
	
		// �R��
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// �d��
//		TitleVO titleVO4 = dao.findByPrimaryKey(1);
//		System.out.print(titleVO4.getTitleId() + ",");
//		System.out.print(titleVO4.getName() + ",");
//		System.out.print(titleVO4.getSeriesId() + ",");
//		System.out.println(titleVO4.getLevelId() + ",");
//		System.out.print("select sucessful");

		// �d��
//		List<TitleVO> list = dao.getAll();
//		for (TitleVO titleVO5 : list) {
//			System.out.print(titleVO5.getTitleId() + ",");
//			System.out.print(titleVO5.getName() + ",");
//			System.out.print(titleVO5.getSeriesId()+ ",");
//			System.out.print(titleVO5.getLevelId()+",");
//			System.out.println();
//		}
	}


}