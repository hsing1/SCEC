package com.title.model;

import java.util.*;
import java.sql.*;

public class TitleJDBCDAO implements TitleDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO TITLE (title_id,name,series_id,level_id ) VALUES ( TITLE_seq.NEXTVAL, ?,?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT title_id,name,series_id,level_id  FROM TITLE order by title_id";
	private static final String GET_ONE_STMT = 
		"SELECT title_id,name,series_id,level_id  FROM TITLE where title_id = ?";
	private static final String DELETE = 
		"DELETE FROM TITLE where title_id = ?";
	private static final String UPDATE = 
		"UPDATE TITLE set name = ? ,series_id = ? ,level_id = ? where title_id=?";

	@Override
	public void insert(TitleVO titleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, titleVO.getName());
			pstmt.setInt(2, titleVO.getSeriesId());
			pstmt.setInt(3, titleVO.getLevelId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(TitleVO titleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, titleVO.getName());
			pstmt.setInt(2, titleVO.getTitleId());
			pstmt.setInt(3, titleVO.getLevelId());
			pstmt.setInt(4, titleVO.getTitleId());
			
			
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer titleId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, titleId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public TitleVO findByPrimaryKey(Integer titleId) {

		TitleVO titleVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, titleId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				titleVO = new TitleVO();
				titleVO.setTitleId(rs.getInt("title_id"));
				titleVO.setName(rs.getString("name"));
				titleVO.setSeriesId(rs.getInt("series_id"));
				titleVO.setLevelId(rs.getInt("level_id"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return titleVO;
	}

	@Override
	public List<TitleVO> getAll() {
		List<TitleVO> list = new ArrayList<TitleVO>();
		TitleVO titleVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				titleVO = new TitleVO();
				titleVO.setTitleId(rs.getInt("title_id"));
				titleVO.setName(rs.getString("name"));
				titleVO.setSeriesId(rs.getInt("series_id"));
				titleVO.setLevelId(rs.getInt("level_id"));
				list.add(titleVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<TitleVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		TitleJDBCDAO dao = new TitleJDBCDAO();

		// 新增
//		TitleVO titleVO = new TitleVO();
//		titleVO.setName("高級專員");
//		titleVO.setSeriesId(1);
//		titleVO.setLevelId(4);
//		dao.insert(titleVO);
//		
//		System.out.println("insert sucessful");
	
		// 修改
//		TitleVO titleVO2 = new TitleVO();
//		titleVO2.setTitleId(1);
//		titleVO2.setName("基礎專員");
//		titleVO2.setSeriesId(2);
//		titleVO2.setLevelId(2);
//		dao.update(titleVO2);
//		System.out.println(titleVO2.getTitleId());
//		System.out.println(titleVO2.getName());
//		System.out.println(titleVO2.getSeriesId());
//		System.out.println(titleVO2.getLevelId());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// 查詢
//		TitleVO titleVO4 = dao.findByPrimaryKey(1);
//		System.out.print(titleVO4.getTitleId() + ",");
//		System.out.print(titleVO4.getName() + ",");
//		System.out.print(titleVO4.getSeriesId() + ",");
//		System.out.println(titleVO4.getLevelId() + ",");
//		System.out.print("select sucessful");

		// 查詢
//		List<TitleVO> list = dao.getAll();
//		for (TitleVO titleVO5 : list) {
//			System.out.print(titleVO5.getTitleId() + ",");
//			System.out.print(titleVO5.getName() + ",");
//			System.out.print(titleVO5.getSeriesId()+ ",");
//			System.out.print(titleVO5.getLevelId()+",");
//			System.out.println();
//		}
	}


}