package com.title.model;

public class TitleVO implements java.io.Serializable{
	private Integer titleId  ;
	private String  name;
	private Integer seriesId   ;
	private Integer levelId   ;
	private Integer salaryLevel;	//薪資分級
	private Integer priv;			//privilege
	
	public Integer getTitleId() {
		return titleId;
	}
	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSeriesId() {
		return seriesId;
	}
	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}
	public Integer getLevelId() {
		return levelId;
	}
	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}
	public Integer getSalaryLevel() {
		return salaryLevel;
	}
	public void setSalaryLevel(Integer salaryLevel) {
		this.salaryLevel = salaryLevel;
	}
	public Integer getPriv() {
		return priv;
	}
	public void setPriv(Integer priv) {
		this.priv = priv;
	}
}
