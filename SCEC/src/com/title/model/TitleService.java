package com.title.model;

import java.util.*;

import com.level.model.*;
import com.series.model.*;

public class TitleService {
	public static Map<Integer, String> titleMap;
	public static Map<String, Integer> titleMapInv;
	public static Map<String, Integer> titleLevelMap;
	public static Map<Integer, Integer> titleLevelMapInt;
	
	private TitleDAO_interface dao;
	private static TitleDAO daoS;

	static {
		if (daoS == null)
			daoS = new TitleDAO();
	}
	
	public TitleService() {
		dao = new TitleDAO();
	}

	/*
	public TitleVO addTitle(Integer titleId, String name, Integer seriesId, Integer levelId) {

		TitleVO titleVO = new TitleVO();
		
		titleVO.setName(name);
		titleVO.setSeriesId(seriesId);
		titleVO.setLevelId(levelId);
	
		dao.insert(titleVO);

		return titleVO;
	}
	*/
	
	public TitleVO addTitleName(String title, Integer salLevel) {
		TitleVO titleVO = new TitleVO();
		
		titleVO.setName(title);
		titleVO.setSalaryLevel(salLevel);
	
		dao.insertName(titleVO);

		return titleVO;
	}
	
	public TitleVO addTitle(String title, String series, String level, Integer salLevel) {
		TitleVO titleVO = new TitleVO();
		SeriesService seriesSvc = new SeriesService();
		LevelService levelSvc = new LevelService();
		SeriesVO seriesVO = seriesSvc.getOneSeries(series);
		LevelVO levelVO = levelSvc.getOneLevel(level);
		int seriesId, levelId;
		
		seriesId = (seriesVO == null) ? 0 : seriesVO.getSeriesId();
		levelId = (levelVO == null) ? 0 : levelVO.getLevelId();
		
		titleVO.setName(title);
		titleVO.setSeriesId(seriesId);
		titleVO.setLevelId(levelId);
		titleVO.setSalaryLevel(salLevel);
	
		dao.insert(titleVO);

		return titleVO;
	}
	
	public TitleVO setupTitle(String title, String series, String level, Integer salLevel, Integer priv) {
		TitleVO titleVO = new TitleVO();
		SeriesService seriesSvc = new SeriesService();
		LevelService levelSvc = new LevelService();
		int titleId, seriesId, levelId;
		
		titleId = dao.findByTitleName(title).getTitleId();
		seriesId = seriesSvc.getOneSeries(series).getSeriesId();
		levelId = levelSvc.getOneLevel(level).getLevelId();
		
		titleVO.setTitleId(titleId);
		titleVO.setLevelId(levelId);
		titleVO.setSeriesId(seriesId);
		titleVO.setSalaryLevel(salLevel);
		titleVO.setPriv(priv);
		
		dao.setup(titleVO);
		
		return titleVO;
	}

	public TitleVO updateTitle(Integer titleId, String  name, Integer seriesId, Integer levelId) {

		TitleVO titleVO = new TitleVO();
		titleVO.setTitleId(titleId);
		titleVO.setName(name);
		titleVO.setSeriesId(seriesId);
		titleVO.setLevelId(levelId);
	
		dao.update(titleVO);

		return titleVO;
	}

	public void deleteTitle(Integer titleId) {
		dao.delete(titleId);
	}

	public TitleVO getOneTitle(Integer titleId) {
		return dao.findByPrimaryKey(titleId);
	}
	
	public TitleVO getOneTitle(String title) {
		return dao.findByTitleName(title);
	}

	public List<TitleVO> getAll() {
		return dao.getAll();
	}
	
	public List<TitleVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public static void setTitleMap() {
		int titleId = 0, levelId = 0;
		String name = null;
		List<TitleVO> list = daoS.getAll();
		titleMap = new LinkedHashMap<Integer, String>();
		titleMapInv = new LinkedHashMap<String, Integer>();
		titleLevelMap = new LinkedHashMap<String, Integer>();
		titleLevelMapInt = new LinkedHashMap<Integer, Integer>();
		
		for (TitleVO titleVO : list) {
			titleId = titleVO.getTitleId();
			levelId = titleVO.getLevelId();
			name = titleVO.getName();
			titleMap.put(titleId, name);
			titleMapInv.put(name, titleId);
			titleLevelMap.put(name, levelId);
			titleLevelMapInt.put(titleId, levelId);
		}
	}
}
