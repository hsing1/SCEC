package com.deptTitleSubject.model;

import java.util.*;

public interface DeptTitleSubjectDAO_interface {
          public void insert(DeptTitleSubjectVO deptTitleSubjectVO);
          public void update(DeptTitleSubjectVO deptTitleSubjectVO);
          public void delete(Integer DeptTitleSubjectId);
          public DeptTitleSubjectVO findByPrimaryKey(Integer DeptTitleSubjectId);
          public List<DeptTitleSubjectVO> getAll();
          //萬用複合查詢(傳入參數型態Map)(回傳 List)
         public List<DeptTitleSubjectVO> getAll(Map<String, String[]> map); 
}
