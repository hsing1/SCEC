package com.deptTitleSubject.model;

public class DeptTitleSubjectVO implements java.io.Serializable{
	private Integer DeptTitleSubjectId  ;
	private Integer subjectId   ;
	private Integer deptTitleId   ;
	public Integer getDeptTitleSubjectId() {
		return DeptTitleSubjectId;
	}
	public void setDeptTitleSubjectId(Integer deptTitleSubjectId) {
		DeptTitleSubjectId = deptTitleSubjectId;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public Integer getDeptTitleId() {
		return deptTitleId;
	}
	public void setDeptTitleId(Integer deptTitleId) {
		this.deptTitleId = deptTitleId;
	}
	
}
