package com.deptTitleSubject.model;

import java.util.*;
import java.sql.*;

public class DeptTitleSubjectJDBCDAO implements DeptTitleSubjectDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO DEPT_TITLE_SUBJECT (dept_title_subject_id,subject_id,dept_title_id ) VALUES ( DEPT_TITLE_SUBJECT_seq.NEXTVAL, ?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT dept_title_subject_id,subject_id,dept_title_id  FROM DEPT_TITLE_SUBJECT order by dept_title_subject_id";
	private static final String GET_ONE_STMT = 
		"SELECT dept_title_subject_id,subject_id,dept_title_id  FROM DEPT_TITLE_SUBJECT where dept_title_subject_id = ?";
	private static final String DELETE = 
		"DELETE FROM DEPT_TITLE_SUBJECT where dept_title_subject_id = ?";
	private static final String UPDATE = 
		"UPDATE DEPT_TITLE_SUBJECT set subject_id = ?,dept_title_id = ? where dept_title_subject_id=?";

	@Override
	public void insert(DeptTitleSubjectVO deptTitleSubjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, deptTitleSubjectVO.getSubjectId());
			pstmt.setInt(2, deptTitleSubjectVO.getDeptTitleId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(DeptTitleSubjectVO deptTitleSubjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setInt(1, deptTitleSubjectVO.getSubjectId());
			pstmt.setInt(2, deptTitleSubjectVO.getDeptTitleId());
			pstmt.setInt(3, deptTitleSubjectVO.getDeptTitleSubjectId());
	
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer DeptTitleSubjectId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, DeptTitleSubjectId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public DeptTitleSubjectVO findByPrimaryKey(Integer deptTitleSubjectId) {

		DeptTitleSubjectVO deptTitleSubjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptTitleSubjectId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				deptTitleSubjectVO = new DeptTitleSubjectVO();
				deptTitleSubjectVO.setDeptTitleSubjectId(rs.getInt("dept_title_subject_id"));
				deptTitleSubjectVO.setSubjectId(rs.getInt("subject_id"));
				deptTitleSubjectVO.setDeptTitleId(rs.getInt("dept_title_id"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return deptTitleSubjectVO;
	}

	@Override
	public List<DeptTitleSubjectVO> getAll() {
		List<DeptTitleSubjectVO> list = new ArrayList<DeptTitleSubjectVO>();
		DeptTitleSubjectVO deptTitleSubjectVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				deptTitleSubjectVO = new DeptTitleSubjectVO();
				deptTitleSubjectVO.setDeptTitleSubjectId(rs.getInt("dept_title_subject_id"));
				deptTitleSubjectVO.setSubjectId(rs.getInt("subject_id"));
				deptTitleSubjectVO.setDeptTitleId(rs.getInt("dept_title_id"));
				list.add(deptTitleSubjectVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<DeptTitleSubjectVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		DeptTitleSubjectJDBCDAO dao = new DeptTitleSubjectJDBCDAO();

		// 新增
		DeptTitleSubjectVO deptTitleSubjectVO = new DeptTitleSubjectVO();
		deptTitleSubjectVO.setSubjectId(1002);
		deptTitleSubjectVO.setDeptTitleId(1);
		dao.insert(deptTitleSubjectVO);
		System.out.println("insert sucessful");
	
		// 修改
//		DeptTitleSubjectVO deptTitleSubjectVO2 = new DeptTitleSubjectVO();
//		deptTitleSubjectVO2.setDeptTitleSubjectId(10001);
//		deptTitleSubjectVO2.setSubjectId(1003);
//		deptTitleSubjectVO2.setDeptTitleId(1);
//		dao.update(deptTitleSubjectVO2);
//		System.out.println(deptTitleSubjectVO2.getDeptTitleSubjectId());
//		System.out.println(deptTitleSubjectVO2.getSubjectId());
//		System.out.println(deptTitleSubjectVO2.getDeptTitleId());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(10001);
//		System.out.println("delete sucessful");

		// 查詢
//		DeptTitleSubjectVO deptTitleSubjectVO4 = dao.findByPrimaryKey(10003);
//		System.out.print(deptTitleSubjectVO4.getDeptTitleSubjectId() + ",");
//		System.out.print(deptTitleSubjectVO4.getSubjectId() + ",");
//		System.out.print(deptTitleSubjectVO4.getDeptTitleId() + ",");
//		System.out.print("select sucessful");

		// 查詢
//		List<DeptTitleSubjectVO> list = dao.getAll();
//		for (DeptTitleSubjectVO deptTitleSubjectVO5 : list) {
//		System.out.print(deptTitleSubjectVO5.getDeptTitleSubjectId() + ",");
//		System.out.print(deptTitleSubjectVO5.getSubjectId() + ",");
//		System.out.print(deptTitleSubjectVO5.getDeptTitleId() + ",");
//		System.out.println();
//		}
	}


}