package com.deptTitleSubject.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class DeptTitleSubjectDAO implements DeptTitleSubjectDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO DEPT_TITLE_SUBJECT (dept_title_subject_id,subject_id,dept_title_id ) VALUES ( DEPT_TITLE_SUBJECT_seq.NEXTVAL, ?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT dept_title_subject_id,subject_id,dept_title_id  FROM DEPT_TITLE_SUBJECT order by dept_title_subject_id";
	private static final String GET_ONE_STMT = 
		"SELECT dept_title_subject_id,subject_id,dept_title_id  FROM DEPT_TITLE_SUBJECT where dept_title_subject_id = ?";
	private static final String DELETE = 
		"DELETE FROM DEPT_TITLE_SUBJECT where dept_title_subject_id = ?";
	private static final String UPDATE = 
		"UPDATE DEPT_TITLE_SUBJECT set subject_id = ?,dept_title_id = ? where dept_title_subject_id=?";

	@Override
	public void insert(DeptTitleSubjectVO deptTitleSubjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, deptTitleSubjectVO.getSubjectId());
			pstmt.setInt(2, deptTitleSubjectVO.getDeptTitleId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(DeptTitleSubjectVO deptTitleSubjectVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setInt(1, deptTitleSubjectVO.getSubjectId());
			pstmt.setInt(2, deptTitleSubjectVO.getDeptTitleId());
			pstmt.setInt(3, deptTitleSubjectVO.getDeptTitleSubjectId());
	
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer DeptTitleSubjectId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, DeptTitleSubjectId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public DeptTitleSubjectVO findByPrimaryKey(Integer deptTitleSubjectId) {

		DeptTitleSubjectVO deptTitleSubjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptTitleSubjectId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				deptTitleSubjectVO = new DeptTitleSubjectVO();
				deptTitleSubjectVO.setDeptTitleSubjectId(rs.getInt("dept_title_subject_id"));
				deptTitleSubjectVO.setSubjectId(rs.getInt("subject_id"));
				deptTitleSubjectVO.setDeptTitleId(rs.getInt("dept_title_id"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return deptTitleSubjectVO;
	}

	@Override
	public List<DeptTitleSubjectVO> getAll() {
		List<DeptTitleSubjectVO> list = new ArrayList<DeptTitleSubjectVO>();
		DeptTitleSubjectVO deptTitleSubjectVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				deptTitleSubjectVO = new DeptTitleSubjectVO();
				deptTitleSubjectVO.setDeptTitleSubjectId(rs.getInt("dept_title_subject_id"));
				deptTitleSubjectVO.setSubjectId(rs.getInt("subject_id"));
				deptTitleSubjectVO.setDeptTitleId(rs.getInt("dept_title_id"));
				list.add(deptTitleSubjectVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<DeptTitleSubjectVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}



}