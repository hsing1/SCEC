package com.deptTitleSubject.model;

import java.util.List;
import java.util.Map;

public class DeptTitleSubjectService {

	private DeptTitleSubjectDAO_interface dao;

	public DeptTitleSubjectService() {
		dao = new DeptTitleSubjectDAO();
	}

	public DeptTitleSubjectVO addDeptTitleSubject(Integer subjectId,Integer deptTitleId) {

		DeptTitleSubjectVO deptTitleSubjectVO = new DeptTitleSubjectVO();
		
		deptTitleSubjectVO.setSubjectId(subjectId);
		deptTitleSubjectVO.setDeptTitleId(deptTitleId);
		dao.insert(deptTitleSubjectVO);

		return deptTitleSubjectVO;
	}

	public DeptTitleSubjectVO updateTitle(Integer subjectId,Integer deptTitleId) {

		DeptTitleSubjectVO deptTitleSubjectVO = new DeptTitleSubjectVO();

		deptTitleSubjectVO.setSubjectId(subjectId);
		deptTitleSubjectVO.setDeptTitleId(deptTitleId);

		return deptTitleSubjectVO;
	}

	public void deleteTitle(Integer titleId) {
		dao.delete(titleId);
	}

	public DeptTitleSubjectVO getOneTitle(Integer DeptTitleSubjectId) {
		return dao.findByPrimaryKey(DeptTitleSubjectId);
	}

	public List<DeptTitleSubjectVO> getAll() {
		return dao.getAll();
	}
	
	public List<DeptTitleSubjectVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
