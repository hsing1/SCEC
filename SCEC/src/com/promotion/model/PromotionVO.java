package com.promotion.model;

import java.sql.Date;

public class PromotionVO implements java.io.Serializable{
	private Integer promotionId;
	private Date  promotionDate;
	private Integer empId;
	private Integer deptId;
	private Integer titleId;
	private String devision;
	private String note;
	
	public Integer getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}
	public Date getPromotionDate() {
		return promotionDate;
	}
	public void setPromotionDate(Date promotionDate) {
		this.promotionDate = promotionDate;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public Integer getTitleId() {
		return titleId;
	}
	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getDevision() {
		return devision;
	}
	public void setDevision(String devision) {
		this.devision = devision;
	}
}
