package com.promotion.model;

import java.util.*;
import java.sql.*;

public class PromotionJDBCDAO implements PromotionDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO PROMOTION (promotion_id,promotion_date,emp_id,dept_title_id ) VALUES ( PROMOTION_seq.NEXTVAL, ?,?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT promotion_id,promotion_date,emp_id,dept_title_id  FROM PROMOTION order by promotion_id";
	private static final String GET_ONE_STMT = 
		"SELECT promotion_id,promotion_date,emp_id,dept_title_id  FROM PROMOTION where promotion_id = ?";
	private static final String DELETE = 
		"DELETE FROM PROMOTION where promotion_id = ?";
	private static final String UPDATE = 
		"UPDATE PROMOTION set promotion_date = ?, emp_id = ?,dept_title_id = ? where promotion_id=?";

	@Override
	public void insert(PromotionVO promotionVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setDate(1, promotionVO.getPromotionDate());
			pstmt.setInt(2, promotionVO.getEmpId());
			pstmt.setInt(3, promotionVO.getDeptTitleId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(PromotionVO promotionVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setDate(1, promotionVO.getPromotionDate());
			pstmt.setInt(2, promotionVO.getEmpId());
			pstmt.setInt(3, promotionVO.getDeptTitleId());
			pstmt.setInt(4, promotionVO.getPromotionId());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer promotionId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, promotionId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public PromotionVO findByPrimaryKey(Integer promotionId) {

		PromotionVO promotionVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, promotionId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				promotionVO = new PromotionVO();
				promotionVO.setPromotionId(rs.getInt("promotion_id"));
				promotionVO.setPromotionDate(rs.getDate("promotion_date"));
				promotionVO.setEmpId(rs.getInt("emp_id"));
				promotionVO.setDeptTitleId(rs.getInt("dept_title_id"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return promotionVO;
	}

	@Override
	public List<PromotionVO> getAll() {
		List<PromotionVO> list = new ArrayList<PromotionVO>();
		PromotionVO promotionVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				promotionVO = new PromotionVO();
				promotionVO.setPromotionId(rs.getInt("promotion_id"));
				promotionVO.setPromotionDate(rs.getDate("promotion_date"));
				promotionVO.setEmpId(rs.getInt("emp_id"));
				promotionVO.setDeptTitleId(rs.getInt("dept_title_id"));
				list.add(promotionVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<PromotionVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		PromotionJDBCDAO dao = new PromotionJDBCDAO();

		// 新增
//		PromotionVO promotionVO = new PromotionVO();
//		promotionVO.setPromotionDate(java.sql.Date.valueOf("2012-08-12"));
//		promotionVO.setEmpId(3);
//		promotionVO.setDeptTitleId(1);
//		dao.insert(promotionVO);
//		System.out.println("insert sucessful");
	
		// 修改
//		PromotionVO promotionVO2 = new PromotionVO();
//		promotionVO2.setPromotionDate(java.sql.Date.valueOf("2012-09-12"));
//		promotionVO2.setEmpId(3);
//		promotionVO2.setDeptTitleId(1);
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(2);
//		System.out.println("delete sucessful");

		// 查詢
//		PromotionVO promotionVO4 = dao.findByPrimaryKey(2);
//		System.out.print(promotionVO4.getPromotionId() + ",");
//		System.out.print(promotionVO4.getPromotionDate() + ",");
//		System.out.print(promotionVO4.getEmpId() + ",");
//		System.out.println(promotionVO4.getDeptTitleId() + ",");
//		System.out.print("select sucessful");

		// 查詢
//		List<PromotionVO> list = dao.getAll();
//		for (PromotionVO promotionVO5 : list) {
//			System.out.print(promotionVO5.getPromotionId() + ",");
//			System.out.print(promotionVO5.getPromotionDate() + ",");
//			System.out.print(promotionVO5.getEmpId() + ",");
//			System.out.println(promotionVO5.getDeptTitleId() + ",");
//			System.out.println();
//		}
	}


}