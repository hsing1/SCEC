package com.promotion.model;

import java.sql.Timestamp;
import java.util.*;

public interface PromotionViewDAO_interface {
      public List<PromotionViewVO> getAll(Integer empId);
      public List<PromotionViewVO> getAll(Integer [] empArr, Timestamp startDate);
      public List<PromotionViewVO> getAll();
}
