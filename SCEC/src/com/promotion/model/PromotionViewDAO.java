package com.promotion.model;

import java.util.*;
import java.sql.*;
import java.sql.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class PromotionViewDAO implements PromotionViewDAO_interface {
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String GET_ALL_STMT = "SELECT * FROM PROMOTION_VIEW order by emp_id";
	private static final String GET_ALL_BY_EMP = "SELECT * FROM PROMOTION_VIEW where emp_id = ? AND promotion_date < ?";


	@Override
	public List<PromotionViewVO> getAll() {
		List<PromotionViewVO> list = new ArrayList<PromotionViewVO>();
		PromotionViewVO promotionVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				promotionVO = new PromotionViewVO();
				promotionVO.setEmpId(rs.getInt("emp_id"));
				promotionVO.setEmpName(rs.getString("emp_name"));
				promotionVO.setDeptId(rs.getInt("dept_id"));
				promotionVO.setDeptName(rs.getString("dept_name"));
				promotionVO.setTitleId(rs.getInt("title_id"));
				promotionVO.setTitleName(rs.getString("title_name"));
				promotionVO.setPromotionDate(rs.getDate("promotion_date"));
				promotionVO.setDevision(rs.getString("devision"));
				list.add(promotionVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
		
	
	@Override
	public List<PromotionViewVO> getAll(Integer empId) {
		List<PromotionViewVO> list = new ArrayList<PromotionViewVO>();
		PromotionViewVO promotionVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_EMP);
			pstmt.setInt(1, empId);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				promotionVO = new PromotionViewVO();
				promotionVO.setEmpId(rs.getInt("emp_id"));
				promotionVO.setEmpName(rs.getString("emp_name"));
				promotionVO.setDeptId(rs.getInt("dept_id"));
				promotionVO.setDeptName(rs.getString("dept_name"));
				promotionVO.setTitleId(rs.getInt("title_id"));
				promotionVO.setTitleName(rs.getString("title_name"));
				promotionVO.setPromotionDate(rs.getDate("promotion_date"));
				list.add(promotionVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<PromotionViewVO> getAll(Integer [] empArr, Timestamp startDate) {
		List<PromotionViewVO> list = new ArrayList<PromotionViewVO>();
		PromotionViewVO promotionVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_EMP);
			
			for (int i = 0; i < empArr.length; i++) {
				pstmt.setInt(1, empArr[i]);
				pstmt.setDate(2, new Date(startDate.getTime()));
				rs = pstmt.executeQuery();
	
				while (rs.next()) {
					promotionVO = new PromotionViewVO();
					promotionVO.setEmpId(rs.getInt("emp_id"));
					promotionVO.setEmpName(rs.getString("emp_name"));
					promotionVO.setDeptId(rs.getInt("dept_id"));
					promotionVO.setDeptName(rs.getString("dept_name"));
					promotionVO.setTitleId(rs.getInt("title_id"));
					promotionVO.setTitleName(rs.getString("title_name"));
					promotionVO.setPromotionDate(rs.getDate("promotion_date"));
					list.add(promotionVO); // Store the row in the list
				}
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
}