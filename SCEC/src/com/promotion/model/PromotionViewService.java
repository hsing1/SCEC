package com.promotion.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.department.model.DepartmentService;
import com.title.model.TitleService;

public class PromotionViewService {

	private PromotionViewDAO_interface dao;

	public PromotionViewService() {
		dao = new PromotionViewDAO();
	}

	public List<PromotionViewVO> getAll(Integer empId) {
		return dao.getAll(empId);
	}
	
	public List<PromotionViewVO> getAll(Integer [] empArr, Timestamp startDate) {
		return dao.getAll(empArr, startDate);
	}
	
	public List<PromotionViewVO> getAll() {
		return dao.getAll();
	}
}
