package com.promotion.model;

import java.util.*;

public interface PromotionDAO_interface {
      public void insert(PromotionVO promotionVO);
      public void update(PromotionVO promotionVO);
      public void delete(Integer promotionId);
      public PromotionVO findByPrimaryKey(Integer promotionId);
      public List<PromotionVO> getAll();
      public List<PromotionVO> getAll(Map<String, String[]> map);
}
