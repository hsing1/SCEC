package com.promotion.model;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.department.model.DepartmentService;
import com.title.model.TitleService;

public class PromotionService {

	private PromotionDAO_interface dao;

	public PromotionService() {
		dao = new PromotionDAO();
	}

	public PromotionVO addPromotionRecord(Integer empId, String dept, String title, String devision, java.util.Date uDate, String note) {
		DepartmentService deptSvc = new DepartmentService();
		TitleService titleSvc = new TitleService();
		PromotionVO promotionVO = new PromotionVO();
		Integer deptId = deptSvc.getOneDept(dept).getDeptId();
		Integer titleId = titleSvc.getOneTitle(title).getTitleId();
		Date promotionDate = new Date(uDate.getTime());
		
		promotionVO.setPromotionDate(promotionDate);
		promotionVO.setEmpId(empId);
		promotionVO.setDeptId(deptId);
		promotionVO.setTitleId(titleId);
		promotionVO.setDevision(devision);
		promotionVO.setNote(note);
	
		dao.insert(promotionVO);

		return promotionVO;
	}

	public PromotionVO updatePromotion( Date  promotionDate,Integer empId,Integer deptTitleId) {

		PromotionVO promotionVO = new PromotionVO();
		
		promotionVO.setPromotionDate( promotionDate);
		promotionVO.setEmpId( empId);
		promotionVO.setDeptId( deptTitleId);

		dao.update(promotionVO);

		return promotionVO;
	}

	public void deleteTitle(Integer titleId) {
		dao.delete(titleId);
	}

	public PromotionVO getOneTitle(Integer promotionId) {
		return dao.findByPrimaryKey(promotionId);
	}

	public List<PromotionVO> getAll() {
		return dao.getAll();
	}
	
	public List<PromotionVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
