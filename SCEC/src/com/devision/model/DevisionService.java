package com.devision.model;

import java.util.*;

import com.department.model.DepartmentDAO;

public class DevisionService {
	public static LinkedHashMap<Integer, String> devisionMap;
	public static LinkedHashMap<String, Integer> devisionMapInv;
	
	private DevisionDAO_interface dao;
	private static DevisionDAO daoS;
	
	static {
		if (daoS == null)
			daoS = new DevisionDAO();
	}

	public DevisionService() {
		dao = new DevisionDAO();
	}

	public DevisionVO insert(String name) {

		DevisionVO devisionVO = new DevisionVO();
		devisionVO.setName(name);
		dao.insert(devisionVO);

		return devisionVO;
	}
	
	public DevisionVO getOneDevision(String devision) {
		return dao.findByDevisionName(devision);
	}
	
	public List<DevisionVO> getAll() {
		return dao.getAll();
	}
	
	public static void setDevisionMap() {
		int devId = 0;
		String name = null;
		List<DevisionVO> list = daoS.getAll();
		devisionMap = new LinkedHashMap<Integer, String>();
		devisionMapInv = new LinkedHashMap<String, Integer>();
		
		for (DevisionVO devVO : list) {
			devId = devVO.getDevisionId();
			name = devVO.getName();
			devisionMap.put(devId, name);
			devisionMapInv.put(name, devId);
		}
	}
}
