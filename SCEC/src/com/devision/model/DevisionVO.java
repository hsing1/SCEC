package com.devision.model;

public class DevisionVO implements java.io.Serializable{
	private Integer devisionId ;
	private String  name;
	
	public Integer getDevisionId() {
		return devisionId;
	}
	public void setDevisionId(Integer devisionId) {
		this.devisionId = devisionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
