package com.devision.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.util.SCECcommon;

public class DevisionDAO implements DevisionDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO DEVISION(name) VALUES (?)";
	private static final String FIND_BY_DEVISION = 
		"SELECT devision_id, name FROM DEVISION where name = ?";
	private static final String GET_ALL_STMT = 
		"SELECT devision_id, name FROM DEVISION order by devision_id";

	
	@Override
	public void insert(DevisionVO devisionVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, devisionVO.getName());

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, null, pstmt, true);
		}
	}
	
	@Override
	public DevisionVO findByDevisionName(String devision) {
		DevisionVO devisionVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean recordNotFound = true;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_DEVISION);

			pstmt.setString(1, devision);
	
			rs = pstmt.executeQuery();
			while (rs.next()) {
				recordNotFound = false;
				devisionVO = new DevisionVO();
				devisionVO.setDevisionId(rs.getInt("devision_id"));
				devisionVO.setName(rs.getString("name"));
			}
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
			if (recordNotFound)
				throw new RuntimeException("Cannot find " + devision + " in database.");
		}
		
		return devisionVO;
	}
	
	@Override
	public List<DevisionVO> getAll() {
		List<DevisionVO> list = new ArrayList<DevisionVO>();
		DevisionVO devisionVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				devisionVO = new DevisionVO();
				devisionVO.setDevisionId(rs.getInt("devision_id"));
				devisionVO.setName(rs.getString("name"));
				list.add(devisionVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
}