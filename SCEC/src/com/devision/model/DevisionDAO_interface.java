package com.devision.model;

import java.util.*;

public interface DevisionDAO_interface {
	public void insert(DevisionVO devisionVO);
	public DevisionVO findByDevisionName(String devision);
	public List<DevisionVO> getAll();
}
