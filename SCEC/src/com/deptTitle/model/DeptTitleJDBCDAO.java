package com.deptTitle.model;

import java.util.*;
import java.sql.*;

public class DeptTitleJDBCDAO implements DeptTitleDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO DEPT_TITLE (dept_title_id ,title_id ,dept_id ) VALUES ( DEPT_TITLE_seq.NEXTVAL, ?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT dept_title_id ,title_id ,dept_id   FROM DEPT_TITLE order by dept_title_id";
	private static final String GET_ONE_STMT = 
		"SELECT dept_title_id ,title_id ,dept_id  FROM DEPT_TITLE  where dept_title_id = ?";
	private static final String DELETE = 
		"DELETE FROM DEPT_TITLE  where dept_title_id = ?";
	private static final String UPDATE = 
		"UPDATE DEPT_TITLE  set title_id = ?, dept_id=? where dept_title_id=?"; 
	@Override
	public void insert(DeptTitleVO deptTitleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setInt(1, deptTitleVO.getTitleId());
			pstmt.setInt(2, deptTitleVO.getDeptId());
	
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(DeptTitleVO deptTitleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
	
			pstmt.setInt(1, deptTitleVO.getTitleId());
			pstmt.setInt(2, deptTitleVO.getDeptId());
			pstmt.setInt(3, deptTitleVO.getDeptTitleId());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer deptTitleId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, deptTitleId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public DeptTitleVO findByPrimaryKey(Integer deptTitleId) {

		DeptTitleVO deptTitleVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptTitleId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				deptTitleVO = new DeptTitleVO();
				deptTitleVO.setDeptTitleId(rs.getInt("dept_title_id"));
				deptTitleVO.setTitleId(rs.getInt("title_id"));
				deptTitleVO.setDeptId(rs.getInt("dept_id"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return deptTitleVO;
	}

	@Override
	public List<DeptTitleVO> getAll() {
		List<DeptTitleVO> list = new ArrayList<DeptTitleVO>();
		DeptTitleVO deptTitleVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				deptTitleVO = new DeptTitleVO();
				deptTitleVO.setDeptTitleId(rs.getInt("dept_title_id"));
				deptTitleVO.setTitleId(rs.getInt("title_id"));
				deptTitleVO.setDeptId(rs.getInt("dept_id"));
				list.add(deptTitleVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<DeptTitleVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		DeptTitleJDBCDAO dao = new DeptTitleJDBCDAO();

		// 新增
//		DeptTitleVO deptTitleVO = new DeptTitleVO();
//		deptTitleVO.setTitleId(2);
//		deptTitleVO.setDeptId(2);
//		dao.insert(deptTitleVO);		
//		System.out.println("insert sucessful");
	
		// 修改
//		DeptTitleVO deptTitleVO = new DeptTitleVO();
//		deptTitleVO.setDeptTitleId(1);
//		deptTitleVO.setTitleId(1);
//		deptTitleVO.setDeptId(1);
//		dao.update(deptTitleVO);
//		System.out.println(deptTitleVO.getDeptTitleId());
//		System.out.println(deptTitleVO.getTitleId());
//		System.out.println(deptTitleVO.getDeptId());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// 查詢
//		DeptTitleVO deptTitleVO4 = dao.findByPrimaryKey(1);
//		System.out.print(deptTitleVO4.getDeptTitleId() + ",");
//		System.out.print(deptTitleVO4.getTitleId() + ",");
//		System.out.print(deptTitleVO4.getDeptId() + ",");
//		System.out.println("select sucessful");

		// 查詢
//		List<DeptTitleVO> list = dao.getAll();
//		for (DeptTitleVO deptTitleVO5 : list) {
//			System.out.print(deptTitleVO5.getDeptTitleId() + ",");
//			System.out.print(deptTitleVO5.getTitleId() + ",");
//			System.out.print(deptTitleVO5.getDeptId()+ ",");
//			System.out.println();
//		}
	}


}