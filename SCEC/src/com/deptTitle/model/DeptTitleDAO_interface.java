package com.deptTitle.model;

import java.util.*;
import com.title.model.*;
import com.section.model.*;

public interface DeptTitleDAO_interface {
          public void insert(DeptTitleVO deptTitleVO);
          public void update(DeptTitleVO deptTitleVO);
          public void delete(Integer deptTitleId);
          public DeptTitleVO findByPrimaryKey(Integer deptTitleId);
          public List<DeptTitleVO> getAllByDeptId(int deptId);
          public List<DeptTitleVO> getAll();
          public List<SectionVO> getAllSection(int deptId);
          public List<TitleVO> getAll(Integer deptId);
          public List<TitleVO> getAll(Integer [] deptIdArr);
          public List<TitleVO> getAll(Integer [] deptIdArr, Integer [] sectionIdArr);
}
