package com.deptTitle.model;

import java.util.*;
import com.department.model.*;
import com.title.model.*;
import com.section.model.*;

public class DeptTitleService {

	private DeptTitleDAO_interface dao;

	public DeptTitleService() {
		dao = new DeptTitleDAO();
	}

	public DeptTitleVO addDeptTitle(String dept, String section, String title) {
		DeptTitleVO deptTitleVO = new DeptTitleVO();
		DepartmentService deptSvc = new DepartmentService();
		TitleService titleSvc = new TitleService();
		SectionService secSvc = new SectionService();
		DepartmentVO deptVO = deptSvc.getOneDept(dept);
		TitleVO titleVO = titleSvc.getOneTitle(title);
		SectionVO sectionVO = secSvc.getOneSection(section);
		int deptId, sectionId, titleId;
		
		titleId = titleVO.getTitleId();
		deptId = deptVO.getDeptId();
		sectionId = sectionVO.getSectionId();
		deptTitleVO.setTitleId(titleId);
		deptTitleVO.setDeptId(deptId);
		deptTitleVO.setSectionId(sectionId);

		dao.insert(deptTitleVO);

		return deptTitleVO;
	}

	public DeptTitleVO updateTitle(Integer deptTitleId, Integer titleId,
			Integer deptId) {

		DeptTitleVO deptTitleVO = new DeptTitleVO();

		deptTitleVO.setTitleId(titleId);
		deptTitleVO.setDeptId(deptId);

		dao.update(deptTitleVO);

		return deptTitleVO;
	}

	public void deleteTitle(Integer titleId) {
		dao.delete(titleId);
	}

	public DeptTitleVO getOneTitle(Integer deptTitleId) {
		return dao.findByPrimaryKey(deptTitleId);
	}
	
	public List<DeptTitleVO> getAllByDeptId(int deptId) {
		return dao.getAllByDeptId(deptId);
	}

	public List<DeptTitleVO> getAll() {
		return dao.getAll();
	}
	
	public List<TitleVO> getAll(Integer deptId) {
		return dao.getAll(deptId);
	}
	
	public List<SectionVO> getAllSection(int deptId) {
		return dao.getAllSection(deptId);
	}
	
	public List<TitleVO> getAll(Integer [] deptIdArr) {
		return dao.getAll(deptIdArr);
	}
	
	public List<TitleVO> getAll(Integer [] deptIdArr, Integer [] sectionIdArr) {
		return dao.getAll(deptIdArr, sectionIdArr);
	}
	
	public Set<Integer> getTitleSet(int deptId, int sectionId) {
		int titleId = 0;
		DeptTitleService deptSvc = new DeptTitleService();
		List<DeptTitleVO> titleList = null; 
		Set<Integer> titleSet = new HashSet<Integer>();
			
		titleList = deptSvc.getAllByDeptId(deptId);
		
		for (DeptTitleVO titleVO : titleList) {
			if (sectionId != titleVO.getSectionId())
				continue;
			
			titleId = titleVO.getTitleId();
			titleSet.add(titleId);
		}
		
		return titleSet;
	}
	
	public Set<Integer> getSectionSet(int deptId) {
		int sectionId = 0;
		DeptTitleService deptSvc = new DeptTitleService();
		List<DeptTitleVO> titleList = null; 
		Set<Integer> sectionSet = new HashSet<Integer>();
			
		titleList = deptSvc.getAllByDeptId(deptId);
		
		for (DeptTitleVO titleVO : titleList) {
			sectionId = titleVO.getSectionId();
			sectionSet.add(sectionId);
		}
		
		return sectionSet;
	}
	
	public Map<Integer, String> getDeptSectionMap(int deptId) {
		int sectionId = 0;
		String name = null;
		DeptTitleService deptSvc = new DeptTitleService();
		Map<Integer, String> sectionMap = new LinkedHashMap<Integer, String>();
		List<DeptTitleVO> titleList = null;
		Set<Integer> sectionSet = new HashSet<Integer>();
		
		titleList = deptSvc.getAllByDeptId(deptId);
		for (DeptTitleVO titleVO : titleList) {
			sectionId = titleVO.getSectionId();
			if (sectionSet.contains(sectionId))
				continue;
			
			sectionSet.add(sectionId);
			name = SectionService.sectionMap.get(sectionId);
			sectionMap.put(sectionId, name);
		}
		
		return sectionMap;
	}
	
	public Map<Integer, String> getTitleMap(Integer deptId) {
		int titleId = 0;
		String name = null;
		DeptTitleService deptSvc = new DeptTitleService();
		Map<Integer, String> titleMap = new LinkedHashMap<Integer, String>();
		List<TitleVO> titleList = deptSvc.getAll(deptId);
		Set<Integer> titleSet = new HashSet<Integer>();
		
		for (TitleVO titleVO : titleList) {
			titleId = titleVO.getTitleId();
			if (titleSet.contains(titleId))
				continue;
			
			titleSet.add(titleId);
			name = titleVO.getName();
			titleMap.put(titleId, name);
		}
		
		return titleMap;
	}
}
