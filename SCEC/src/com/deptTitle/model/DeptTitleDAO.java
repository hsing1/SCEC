package com.deptTitle.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.title.model.*;
import com.section.model.*;
import com.util.SCECcommon;

public class DeptTitleDAO implements DeptTitleDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO DEPT_TITLE (title_id, dept_id, section_id) VALUES (?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT dept_title_id ,title_id ,dept_id   FROM DEPT_TITLE order by dept_title_id";
	private static final String GET_ONE_STMT = 
		"SELECT dept_title_id ,title_id ,dept_id  FROM DEPT_TITLE  where dept_title_id = ?";
	private static final String DELETE = 
		"DELETE FROM DEPT_TITLE  where dept_title_id = ?";
	private static final String UPDATE = 
		"UPDATE DEPT_TITLE  set title_id = ?, dept_id=? where dept_title_id=?"; 
	private static final String GET_ALL_BY_DEPTID_STMT = 
		"SELECT * FROM DEPT_TITLE where dept_id = ?";
	private static final String GET_ALL_BY_DEPTID_SECID_STMT = 
		"SELECT * FROM DEPT_TITLE where dept_id = ? AND section_id = ?";

	@Override
	public void insert(DeptTitleVO deptTitleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setInt(1, deptTitleVO.getTitleId());
			pstmt.setInt(2, deptTitleVO.getDeptId());
			pstmt.setInt(3, deptTitleVO.getSectionId());
	
			pstmt.executeUpdate();
			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(DeptTitleVO deptTitleVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setInt(1, deptTitleVO.getTitleId());
			pstmt.setInt(2, deptTitleVO.getDeptId());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer deptTitleId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, deptTitleId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public DeptTitleVO findByPrimaryKey(Integer deptTitleId) {

		DeptTitleVO deptTitleVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptTitleId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				deptTitleVO = new DeptTitleVO();
				deptTitleVO.setTitleId(rs.getInt("title_id"));
				deptTitleVO.setDeptId(rs.getInt("dept_id"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return deptTitleVO;
	}

	@Override
	public List<DeptTitleVO> getAllByDeptId(int deptId) {
		List<DeptTitleVO> list = new ArrayList<DeptTitleVO>();
		DeptTitleVO deptTitleVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_DEPTID_STMT);
			pstmt.setInt(1, deptId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				deptTitleVO = new DeptTitleVO();
				deptTitleVO.setTitleId(rs.getInt("title_id"));
				deptTitleVO.setDeptId(rs.getInt("dept_id"));
				deptTitleVO.setSectionId(rs.getInt("section_id"));
				list.add(deptTitleVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<DeptTitleVO> getAll() {
		List<DeptTitleVO> list = new ArrayList<DeptTitleVO>();
		DeptTitleVO deptTitleVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				deptTitleVO = new DeptTitleVO();
				deptTitleVO.setTitleId(rs.getInt("title_id"));
				deptTitleVO.setDeptId(rs.getInt("dept_id"));
				deptTitleVO.setSectionId(rs.getInt("section_id"));
				list.add(deptTitleVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<SectionVO> getAllSection(int deptId) {
		int sectionId = 0;
		List<SectionVO> list = new ArrayList<SectionVO>();
		Set<Integer> set = new HashSet<Integer>();
		SectionVO sectionVO = null;
		DeptTitleVO deptTitleVO = null;
		SectionService sectionSvc = new SectionService();

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_DEPTID_STMT);
			pstmt.setInt(1, deptId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				deptTitleVO = new DeptTitleVO();
				sectionId = rs.getInt("section_id");
				if (set.contains(sectionId))
					continue;
				
				sectionVO = sectionSvc.getOneSection(con, sectionId);
				set.add(sectionId);
				list.add(sectionVO);
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<TitleVO> getAll(Integer deptId) {
		List<TitleVO> list = new ArrayList<TitleVO>();
		TitleVO titleVO = null;
		DeptTitleVO deptTitleVO = null;
		TitleService titleSvc = new TitleService();
		int titleId = 0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_DEPTID_STMT);
			pstmt.setInt(1, deptId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				deptTitleVO = new DeptTitleVO();
				titleId = rs.getInt("title_id");
				titleVO = titleSvc.getOneTitle(titleId);
				list.add(titleVO); // Store the row in the list
			}
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	/*
	 * multiple selection
	 */
	@Override
	public List<TitleVO> getAll(Integer [] deptIdArr, Integer [] sectionIdArr) {
		Set<Integer> set = new HashSet<Integer>();
		List<TitleVO> list = new ArrayList<TitleVO>();
		TitleVO titleVO = null;
		TitleService titleSvc = new TitleService();
		int titleId = 0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_DEPTID_SECID_STMT);
			
			for (int i = 0; i < deptIdArr.length; i++) {
				for (int j = 0; j < sectionIdArr.length; j++) {
					pstmt.setInt(1, deptIdArr[i]);
					pstmt.setInt(2, sectionIdArr[j]);
					rs = pstmt.executeQuery();
					while (rs.next()) {
						titleId = rs.getInt("title_id");
						if (set.contains(titleId)) 
							continue;
						
						titleVO = titleSvc.getOneTitle(titleId);
						set.add(titleId);
						list.add(titleVO); // Store the row in the list
					}
				}
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con , rs, pstmt, true);
		}
		return list;
	}
	
	/*
	 * multiple selection
	 */
	@Override
	public List<TitleVO> getAll(Integer [] deptIdArr) {
		Set<Integer> set = new HashSet<Integer>();
		List<TitleVO> list = new ArrayList<TitleVO>();
		TitleVO titleVO = null;
		DeptTitleVO deptTitleVO = null;
		TitleService titleSvc = new TitleService();
		int titleId = 0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_DEPTID_STMT);
			
			for (int i = 0; i < deptIdArr.length; i++) {
				pstmt.setInt(1, deptIdArr[i]);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					titleId = rs.getInt("title_id");
					if (set.contains(titleId)) 
						continue;
					
					deptTitleVO = new DeptTitleVO();
					titleVO = titleSvc.getOneTitle(titleId);
					set.add(titleId);
					list.add(titleVO); // Store the row in the list
				}
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con , rs, pstmt, true);
		}
		return list;
	}
	
	public static void main(String[] args) {

		DeptTitleDAO dao = new DeptTitleDAO();

		// �s�W
//		DeptTitleVO deptTitleVO = new DeptTitleVO();
//		deptTitleVO.setDeptTitleId(1);
//		deptTitleVO.setTitleId(1);
//		deptTitleVO.setDeptId(1);
//		dao.insert(deptTitleVO);		
//		System.out.println("insert sucessful");
	
		// �ק�
//		TitleVO titleVO2 = new TitleVO();
//		titleVO2.setTitleId(1);
//		titleVO2.setName("��¦�M��");
//		titleVO2.setSeriesId(100);
//		titleVO2.setLevelId(4);
//		dao.update(titleVO2);
//		System.out.println(titleVO2.getTitleId());
//		System.out.println(titleVO2.getName());
//		System.out.println(titleVO2.getSeriesId());
//		System.out.println(titleVO2.getLevelId());
//		System.out.println("update sucessful");
	
		// �R��
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// �d��
//		DeptTitleVO deptTitleVO4 = dao.findByPrimaryKey(1);
//		System.out.print(deptTitleVO4.getDeptTitleId() + ",");
//		System.out.print(deptTitleVO4.getTitleId() + ",");
//		System.out.print(deptTitleVO4.getDeptId() + ",");
//		System.out.println("select sucessful");

		// �d��
//		List<DeptTitleVO> list = dao.getAll();
//		for (DeptTitleVO deptTitleVO5 : list) {
//			System.out.print(deptTitleVO5.getDeptTitleId() + ",");
//			System.out.print(deptTitleVO5.getTitleId() + ",");
//			System.out.print(deptTitleVO5.getDeptId()+ ",");
//			System.out.println();
//		}
	}


}