package com.section.model;

public class SectionVO implements java.io.Serializable{
	private Integer sectionId ;
	private String  name;
	
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
