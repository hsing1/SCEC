package com.section.model;

import java.util.*;
import java.sql.*;

public interface SectionDAO_interface {
	public void insert(SectionVO sectionVO);
	public SectionVO findBySectionName(String section);
	public SectionVO getOneSection(Connection con, int sectionId);
	public List<SectionVO> getAll();
}
