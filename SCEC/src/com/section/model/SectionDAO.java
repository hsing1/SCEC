package com.section.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.util.SCECcommon;

public class SectionDAO implements SectionDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO SECTION(name) VALUES (?)";
	private static final String FIND_BY_SECTION = 
		"SELECT section_id, name FROM SECTION where name = ?";
	private static final String FIND_BY_SECTION_ID = 
		"SELECT section_id, name FROM SECTION where section_id = ?";
	private static final String GET_ALL_STMT = 
		"SELECT section_id, name FROM SECTION order by section_id";

	
	@Override
	public void insert(SectionVO sectionVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, sectionVO.getName());

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, null, pstmt, true);
		}
	}
	
	@Override
	public SectionVO findBySectionName(String section) {
		SectionVO sectionVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean recordNotFound = true;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_SECTION);

			pstmt.setString(1, section);
	
			rs = pstmt.executeQuery();
			while (rs.next()) {
				recordNotFound = false;
				sectionVO = new SectionVO();
				sectionVO.setSectionId(rs.getInt("section_id"));
				sectionVO.setName(rs.getString("name"));
			}
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
			if (recordNotFound)
				throw new RuntimeException("Cannot find " + section + " in database.");
		}
		
		return sectionVO;
	}
	
	@Override
	public SectionVO getOneSection(Connection con, int sectionId) {
		boolean recordNotFound = true;
		String sectionName = null;
		SectionVO sectionVO = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = con.prepareStatement(FIND_BY_SECTION_ID);
			pstmt.setInt(1, sectionId);
	
			rs = pstmt.executeQuery();
			while (rs.next()) {
				recordNotFound = false;
				sectionVO = new SectionVO();
				sectionVO.setSectionId(rs.getInt("section_id"));
				sectionName = rs.getString("name");
				sectionVO.setName(sectionName);
			}
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, false);
			if (recordNotFound)
				throw new RuntimeException("Cannot find (" + sectionId + ", " + sectionName + " in database.");
		}
		
		return sectionVO;
	}
	
	@Override
	public List<SectionVO> getAll() {
		List<SectionVO> list = new ArrayList<SectionVO>();
		SectionVO sectionVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				sectionVO = new SectionVO();
				sectionVO.setSectionId(rs.getInt("section_id"));
				sectionVO.setName(rs.getString("name"));
				list.add(sectionVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
}