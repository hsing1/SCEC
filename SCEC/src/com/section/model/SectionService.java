package com.section.model;

import java.util.*;
import java.sql.*;

import com.department.model.DepartmentDAO;

public class SectionService {
	public static LinkedHashMap<Integer, String> sectionMap;
	public static LinkedHashMap<String, Integer> sectionMapInv;
	
	private SectionDAO_interface dao;
	private static SectionDAO daoS;
	
	static {
		if (daoS == null)
			daoS = new SectionDAO();
	}

	public SectionService() {
		dao = new SectionDAO();
	}

	public SectionVO insert(String name) {

		SectionVO sectionVO = new SectionVO();
		sectionVO.setName(name);
		dao.insert(sectionVO);

		return sectionVO;
	}
	
	public SectionVO getOneSection(String section) {
		return dao.findBySectionName(section);
	}
	
	public SectionVO getOneSection(Connection con, int sectionId) {
		return dao.getOneSection(con, sectionId);
	}
	
	public List<SectionVO> getAll() {
		return dao.getAll();
	}
	
	public static void setSectionMap() {
		int secId = 0;
		String name = null;
		List<SectionVO> list = daoS.getAll();
		sectionMap = new LinkedHashMap<Integer, String>();
		sectionMapInv = new LinkedHashMap<String, Integer>();
		
		for (SectionVO secVO : list) {
			secId = secVO.getSectionId();
			name = secVO.getName();
			sectionMap.put(secId, name);
			sectionMapInv.put(name, secId);
		}
	}
}
