package com.standard.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class StandardDAO implements StandardDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO STANDARD (standard_id ,grade_id ,dept_title_subject_id ) VALUES ( STANDARD_seq.NEXTVAL, ?,?)";
	private static final String GET_ALL_STMT = "SELECT standard_id ,grade_id ,dept_title_subject_id  FROM STANDARD order by standard_id";
	private static final String GET_ONE_STMT = "SELECT standard_id ,grade_id ,dept_title_subject_id   FROM STANDARD  where standard_id = ?";
	private static final String DELETE = "DELETE FROM STANDARD  where standard_id = ?";
	private static final String UPDATE = "UPDATE STANDARD  set grade_id = ?, dept_title_subject_id=? where standard_id=?";

	@Override
	public void insert(StandardVO standardVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, standardVO.getGradeId());
			pstmt.setInt(2, standardVO.getDeptTitleSubjectId());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	

	@Override
	public void update(StandardVO standardVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setInt(1, standardVO.getGradeId());
			pstmt.setInt(2, standardVO.getDeptTitleSubjectId());
			pstmt.setInt(3, standardVO.getStandardId());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer standardId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, standardId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public StandardVO findByPrimaryKey(Integer standardId) {

		StandardVO standardVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, standardId);

			rs = pstmt.executeQuery();

			while (rs.next()) {

				standardVO = new StandardVO();
				standardVO.setStandardId(rs.getInt("standard_id"));
				standardVO.setGradeId(rs.getInt("grade_id"));
				standardVO.setDeptTitleSubjectId(rs
						.getInt("dept_title_subject_id"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return standardVO;
	}

	@Override
	public List<StandardVO> getAll() {
		List<StandardVO> list = new ArrayList<StandardVO>();
		StandardVO standardVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			rs = pstmt.executeQuery();

			while (rs.next()) {

				standardVO = new StandardVO();
				standardVO.setStandardId(rs.getInt("standard_id"));
				standardVO.setGradeId(rs.getInt("grade_id"));
				standardVO.setDeptTitleSubjectId(rs
						.getInt("dept_title_subject_id"));
				list.add(standardVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<StandardVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		StandardDAO dao = new StandardDAO();

		// �s�W
		// StandardVO standardVO = new StandardVO();
		// standardVO.setGradeId(2);
		// standardVO.setDeptTitleSubjectId(10006);
		// dao.insert(standardVO);
		// System.out.println("insert sucessful");

		// �ק�
		// StandardVO standardVO2 = new StandardVO();
		// standardVO2.setStandardId(1);
		// standardVO2.setGradeId(2);
		// standardVO2.setDeptTitleSubjectId(10006);
		// dao.update(standardVO2);
		// System.out.println(standardVO2.getStandardId());
		// System.out.println(standardVO2.getGradeId());
		// System.out.println(standardVO2.getDeptTitleSubjectId());
		// System.out.println("update sucessful");

		// �R��
		// dao.delete(1001);
		// System.out.println("delete sucessful");

		// �d��
		// StandardVO standardVO4 = dao.findByPrimaryKey(1001);
		// System.out.print(standardVO4.getStandardId() + ",");
		// System.out.print(standardVO4.getGradeId() + ",");
		// System.out.print(standardVO4.getDeptTitleSubjectId() + ",");
		// System.out.println("select sucessful");

		// �d��
		// List<StandardVO> list = dao.getAll();
		// for (StandardVO StandardVO5 : list) {
		// System.out.print(StandardVO5.getStandardId() + ",");
		// System.out.print(StandardVO5.getGradeId() + ",");
		// System.out.print(StandardVO5.getDeptTitleSubjectId() + ",");
		// System.out.println();
		// }
	}

}