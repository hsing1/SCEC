package com.standard.model;

import java.util.*;
import com.department.model.DepartmentService;
import com.section.model.SectionService;
import com.title.model.TitleService;

public class StandardViewService {
	public static Map<String, Integer> deptSectionTitleMap;
	public static Map<String, Integer> deptSectionTitleCatMap;
	private static StandardViewService daoS;

	private StandardViewDAO_interface dao;
	
	static {
		if (daoS == null)
			daoS = new StandardViewService();
	}

	public StandardViewService() {
		dao = new StandardViewDAO();
	}

	public List<StandardViewVO> getAll() {
		return dao.getAll();
	}
	
	public List<StandardViewVO> getStandardByCatId(Integer catId) {
		return dao.getStandardByCatId(catId);
	}
	
	public List<StandardViewVO> getStandardByTitle(String dept, String section, String title) {
		int deptId, sectionId, titleId;
		
		deptId = DepartmentService.deptMapInv.get(dept);
		sectionId = SectionService.sectionMapInv.get(section);
		titleId = TitleService.titleMapInv.get(title);
		
		return dao.getStandardByTitle(deptId, sectionId, titleId);
	}
	
	public List<StandardViewVO> getStandardByTitle(Integer deptId, Integer sectionId, Integer titleId) {
		return dao.getStandardByTitle(deptId, sectionId, titleId);
	}
	
	public static void setDeptSectionTitleMap() {
		int deptId = 0, sectionId = 0, titleId = 0, count = 0;
		String key = null;
		StringBuilder str = new StringBuilder();
		String name = null;
		List<StandardViewVO> list = daoS.getAll();
		deptSectionTitleMap = new LinkedHashMap<String, Integer>();
		deptSectionTitleCatMap = new LinkedHashMap<String, Integer>();
		
		for (StandardViewVO standVO : list) {
			deptId = standVO.getDeptId();
			sectionId = standVO.getSectionId();
			titleId = standVO.getTitleId();
			str.append(deptId).append("-").append(sectionId).append("-").append(titleId); 
			key = str.toString();
			if (deptSectionTitleMap.containsKey(key))
				count = deptSectionTitleMap.get(key) + 1;
			else
				count = 1;
			deptSectionTitleMap.put(key, count);
			str.delete(0, str.length());
		}
	}
}
