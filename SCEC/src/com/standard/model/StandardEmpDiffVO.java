package com.standard.model;

import com.employee.model.EmployeeAbilityViewVO;

public class StandardEmpDiffVO extends StandardViewVO implements java.io.Serializable {
	private Integer empId;
	private String empName;
	private Float grade;  //具備
	private Float requiredGrade; //應備
	private Float diff;
	private float companyAvg;
	private float deptAvg;
	
	public StandardEmpDiffVO(StandardViewVO s, EmployeeAbilityViewVO e) {
		super(s);
		
		empId = e.getEmpId();
		empName = e.getEmpName();
		grade = e.getGrade();
		requiredGrade = e.getRequiredGrade();
		companyAvg = e.getCompanyAvg();
		deptAvg = e.getDeptAvg();
		diff = requiredGrade + grade - s.getStandard();
 		
		// override data on StandardViewVO
		setDeptId(e.getDeptId());
		setDeptName(e.getDeptName());
		setTitleId(e.getTitleId());
		setTitleName(e.getTitleName());
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Float getGrade() {
		return grade;
	}

	public void setGrade(Float grade) {
		this.grade = grade;
	}

	public Float getRequiredGrade() {
		return requiredGrade;
	}

	public void setRequiredGrade(Float requiredGrade) {
		this.requiredGrade = requiredGrade;
	}

	public float getCompanyAvg() {
		return companyAvg;
	}

	public void setCompanyAvg(float companyAvg) {
		this.companyAvg = companyAvg;
	}

	public float getDeptAvg() {
		return deptAvg;
	}

	public void setDeptAvg(float deptAvg) {
		this.deptAvg = deptAvg;
	}

	public Float getDiff() {
		return diff;
	}

	public void setDiff(Float diff) {
		this.diff = diff;
	}
}
