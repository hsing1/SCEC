package com.standard.model;

public class StandardVO implements java.io.Serializable{
	private Integer standardId  ;
	private Integer gradeId   ;
	private Integer deptTitleSubjectId   ;

	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getGradeId() {
		return gradeId;
	}
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}
	public Integer getDeptTitleSubjectId() {
		return deptTitleSubjectId;
	}
	public void setDeptTitleSubjectId(Integer deptTitleSubjectId) {
		this.deptTitleSubjectId = deptTitleSubjectId;
	}
	
}
