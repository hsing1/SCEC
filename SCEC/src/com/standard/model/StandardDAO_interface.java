package com.standard.model;

import java.sql.Connection;
import java.util.*;

public interface StandardDAO_interface {
	public void insert(StandardVO standardVO);
	public void update(StandardVO standardVO);
	public void delete(Integer standardId);
	public StandardVO findByPrimaryKey(Integer standardId);
	public List<StandardVO> getAll();
	public List<StandardVO> getAll(Map<String, String[]> map); 
}
