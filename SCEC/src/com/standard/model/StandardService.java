package com.standard.model;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class StandardService {

	private StandardDAO_interface dao;

	public StandardService() {
		dao = new  StandardDAO();
	}

	public StandardVO addStandard(Integer gradeId , Integer deptTitleSubjectId) {

		StandardVO standardVO = new StandardVO();
		standardVO.setGradeId(gradeId);
		standardVO.setDeptTitleSubjectId(deptTitleSubjectId);

		dao.insert(standardVO);

		return standardVO;
	}

	public StandardVO updateStandard(Integer standardId ,Integer gradeId , Integer deptTitleSubjectId) {

		StandardVO standardVO = new StandardVO();
		standardVO.setStandardId(standardId);
		standardVO.setGradeId(gradeId);
		standardVO.setDeptTitleSubjectId(deptTitleSubjectId);

		dao.update(standardVO);

		return standardVO;
	}

	public void deleteTitle(Integer titleId) {
		dao.delete(titleId);
	}

	public StandardVO getOneTitle(Integer standardId) {
		return dao.findByPrimaryKey(standardId);
	}

	public List<StandardVO> getAll() {
		return dao.getAll();
	}
	
	public List<StandardVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
