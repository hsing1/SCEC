package com.standard.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class StandardViewDAO implements StandardViewDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String GET_ALL_STMT = "SELECT * FROM SUBJECT_STANDARD order by title_id";
	private static final String GET_STANDARD_BY_CAT_STMT = "SELECT * FROM SUBJECT_STANDARD where category_id = ? order by title_id";
	private static final String GET_STANDARD_BY_TITLE_STMT = "SELECT * FROM SUBJECT_STANDARD where dept_id = ? and section_id = ? and title_id = ? order by title_id";

	@Override
	public List<StandardViewVO> getStandardByCatId(Integer catId) {
		List<StandardViewVO> list = new ArrayList<StandardViewVO>();
		StandardViewVO standardVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_STANDARD_BY_CAT_STMT);
			pstmt.setInt(1, catId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				standardVO = new StandardViewVO();
				setVO(standardVO, rs);
				list.add(standardVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<StandardViewVO> getStandardByTitle(Integer deptId, Integer sectionId, Integer titleId) {
		List<StandardViewVO> list = new ArrayList<StandardViewVO>();
		StandardViewVO standardVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_STANDARD_BY_TITLE_STMT);
			pstmt.setInt(1, deptId);
			pstmt.setInt(2, sectionId);
			pstmt.setInt(3, titleId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				standardVO = new StandardViewVO();
				setVO(standardVO, rs);
				list.add(standardVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}

	@Override
	public List<StandardViewVO> getAll() {
		List<StandardViewVO> list = new ArrayList<StandardViewVO>();
		StandardViewVO standardVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				standardVO = new StandardViewVO();
				setVO(standardVO, rs);
				list.add(standardVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	private void setVO(StandardViewVO standardVO, ResultSet rs) throws SQLException {
		standardVO.setDeptId(rs.getInt("dept_id"));
		standardVO.setDeptName(rs.getString("dept_name"));
		standardVO.setSectionId(rs.getInt("section_id"));
		standardVO.setSectionName(rs.getString("section_name"));
		standardVO.setTitleId(rs.getInt("title_id"));
		standardVO.setTitleName(rs.getString("title_name"));
		standardVO.setSubjectId(rs.getInt("subject_id"));
		standardVO.setSubjectName(rs.getString("subject_name"));
		standardVO.setCategoryId(rs.getInt("category_id"));
		standardVO.setCatName(rs.getString("cat_name"));
		standardVO.setClassId(rs.getInt("class_id"));
		standardVO.setClassName(rs.getString("class_name"));
		standardVO.setStandard(rs.getFloat("standard"));
		standardVO.setCertification(rs.getBoolean("certification"));
	}
	
}