package com.standard.model;

import java.util.*;

public interface StandardViewDAO_interface {
	public List<StandardViewVO> getAll();
	public List<StandardViewVO> getStandardByCatId(Integer catId);
	public List<StandardViewVO> getStandardByTitle(Integer deptId, Integer sectionId, Integer titleId);
}
