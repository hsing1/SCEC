package com.standard.model;

public class StandardViewVO implements java.io.Serializable{
	private Integer deptId;
	private String deptName;
	private Integer sectionId;
	private String sectionName;
	private Integer titleId;
	private String titleName;
	private Integer subjectId;
	private String subjectName;
	private Integer categoryId;
	private String catName;
	private Integer classId;
	private String className;
	private Float standard;
	private Boolean certification;
	
	public StandardViewVO() {
		
	}
	
	public StandardViewVO(StandardViewVO s) {
		//deptId = s.getDeptId();
		//deptName = s.getDeptName();
		//titleId = s.getDeptId();
		//titleName = s.getTitleName();
		subjectId = s.getSubjectId();
		subjectName = s.getSubjectName();
		categoryId = s.getCategoryId();
		catName = s.getCatName();
		classId = s.getClassId();
		className = s.getClassName();
		standard = s.getStandard();
		certification = s.getCertification();
	}
	
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public Integer getTitleId() {
		return titleId;
	}
	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Float getStandard() {
		return standard;
	}
	public void setStandard(Float standard) {
		this.standard = standard;
	}
	public Boolean getCertification() {
		return certification;
	}
	public void setCertification(Boolean certification) {
		this.certification = certification;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
}
