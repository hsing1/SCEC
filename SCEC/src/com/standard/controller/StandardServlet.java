package com.standard.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONArray;

import com.department.model.*;
import com.employee.model.*;
import com.title.model.TitleVO;
import com.category.model.*;
import com.subject.model.*;
import com.standard.model.*;
import com.util.*;

public class StandardServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		PrintWriter out = res.getWriter();
		String action = req.getParameter("action");
		
		if ("selectCategory".equals(action)) {
			try {
				String url = "/standard/selectCategory.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listStandardByCat".equals(action)) {
			String catIdStr = req.getParameter("selectCat");
			Integer catId = Integer.valueOf(catIdStr);
			List<StandardViewVO> list = null;
			StandardViewService standardSvc = new StandardViewService();
			
			try {
				if (catId == 0)
					list = standardSvc.getAll();
				else
					list = standardSvc.getStandardByCatId(catId);
				req.setAttribute("listStandard", list);
				req.setAttribute("selectedCat", catId);

				String url = "/standard/selectCategory.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		// request from selectTitleToShowStandard.jsp
		if ("listStandardByTitle".equals(action)) {
			String deptIdStr = req.getParameter("selectDept");
			String sectionIdStr = req.getParameter("selectSection");
			String titleIdStr = req.getParameter("selectTitle");
			String empIdStr = req.getParameter("empId");
			Integer deptId = null, titleId = null, sectionId = null, empId = null;
			
			deptId = Integer.valueOf(deptIdStr);
			titleId = Integer.valueOf(titleIdStr);
			sectionId = 1;
			//sectionId = Integer.valueOf(sectionIdStr);
			//Integer empId = Integer.valueOf(empIdStr);
			List<StandardViewVO> standardList = null;
			List<EmployeeAbilityViewVO> abilityList = null;
			EmployeeAbilityViewVO ability = null;
			List<StandardEmpDiffVO> diffList = null;
			StandardViewService standardSvc = new StandardViewService();
			EmployeeAbilityViewService abSvc = new EmployeeAbilityViewService();
			EmployeeService empSvc = new EmployeeService();
			EmployeeViewVO myEmpVO = null;
			
			
			try {
				myEmpVO = SCECcommon.getMyEmp(req, res);
				empId = myEmpVO.getEmpId();
				standardList = standardSvc.getStandardByTitle(deptId, sectionId, titleId);
				abilityList = abSvc.getAll(empId);
				if (abilityList.size() == 0) {
					ability = new EmployeeAbilityViewVO();
					ability.setEmpId(empId);
					ability.setEmpName(myEmpVO.getEmpName());
					ability.setTitleId(myEmpVO.getTitleId());
					ability.setTitleName(myEmpVO.getTitleName());
					ability.setDeptId(myEmpVO.getDeptId());
					ability.setDeptName(myEmpVO.getDeptName());
					abilityList.add(ability);
				}
				diffList = checkAbilityDiff(standardList, abilityList);
				
				req.setAttribute("listStandard", standardList);
				req.setAttribute("listStandardDiff", diffList);
				req.setAttribute("selectedDept", deptId);
				req.setAttribute("selectedTitle", titleId);

				String url = "/standard/selectTitleToShowStandard.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
	}
	
	private List<StandardEmpDiffVO> checkAbilityDiff(List<StandardViewVO> standardList, List<EmployeeAbilityViewVO> abList) {
		List<StandardEmpDiffVO> list = new ArrayList<StandardEmpDiffVO>();
		StandardEmpDiffVO diffVO = null;
		EmployeeAbilityViewVO e = null, tmp = null;
		
		for (StandardViewVO s : standardList) {
			e  = findEmpAbility(s, abList);
			if (e == null) {
				e = abList.get(0);
				e.setCompanyAvg(0);
				e.setDeptAvg(0);
				e.setGrade(0F);
				e.setRequiredGrade(0F);
			}
			diffVO = new StandardEmpDiffVO(s, e);
			list.add(diffVO);
		}
		
		return list;
	}
	
	private EmployeeAbilityViewVO findEmpAbility(StandardViewVO s, List<EmployeeAbilityViewVO> list) {
		Integer subjectId = s.getSubjectId();
		EmployeeAbilityViewVO ab = null;
		
		for (EmployeeAbilityViewVO e : list) {
			if (subjectId == e.getSubjectId()) {
				ab = e;
				break;
			}
		}
		
		return ab;
	}
	
	/* replace by SCECcommon.getMyEmp()
	private EmployeeViewVO getMyEmpVO(HttpServletRequest req) {
		String account = null;
		HttpSession session = null;
		EmployeeViewService empSvc = null;
		EmployeeViewVO empVO = null;
		
		session = req.getSession();
		account = (String)session.getAttribute("account");
		empSvc = new EmployeeViewService();
		empVO = empSvc.getOneEmpByAccount(account, 0);
		
	    return empVO;	
	}
	*/
}
