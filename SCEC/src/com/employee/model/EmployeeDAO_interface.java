package com.employee.model;

import java.sql.Connection;
import java.util.*;

public interface EmployeeDAO_interface {
	public void insert(EmployeeVO employeeVO);
	public void update(EmployeeVO employeeVO);
	public void delete(Integer empId);
	public EmployeeVO findByPrimaryKey(Integer empId);
	public List<EmployeeVO> getAll();
	public List<EmployeeVO> getEmpsByDeptIdAndTitleId(Integer deptId, Integer titleId);
	public List<EmployeeVO> getEmpsByDeptIdAndTitleId(Integer [] deptArr, Integer [] titleArr);
	public List<EmployeeVO> getAll(Map<String, String[]> map);
	public int getNumOfEmpByDeptIdAndTitleId(Connection con, int deptId, int titleId);
	public int getNumOfEmpByDeptIdAndSectionIdAndTitleId(Connection con, int deptId, int sectionId, int titleId);
}
