package com.employee.model;

import java.sql.Date;

public class EmployeeVO implements java.io.Serializable{
	private Integer empId  ;
	private String firstName;
	private String lastName;
	private String name;
	private Integer devisionId;
	private Integer deptId;
	private Integer sectionId;
	private Integer titleId;
	private Integer accountId;
	private Date onboard;
	private Date leaveDate;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public Integer getTitleId() {
		return titleId;
	}
	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getOnboard() {
		return onboard;
	}
	public void setOnboard(Date onboard) {
		this.onboard = onboard;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public Integer getDevisionId() {
		return devisionId;
	}
	public void setDevisionId(Integer devisionId) {
		this.devisionId = devisionId;
	}
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
}
