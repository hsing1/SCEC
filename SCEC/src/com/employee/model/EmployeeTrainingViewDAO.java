package com.employee.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.util.SCECcommon;

public class EmployeeTrainingViewDAO implements EmployeeTrainingViewDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/*
	Error: Unknown column 'emp_id' in 'field list'
	private static final String GET_ALL_STMT =
		"SELECT emp_id, emp_name, dept_name, title_name, series_name, level_name, subject_name, grade, training_hour, training_date" +
		"FROM EMPLOYEE_VIEW order by emp_id";
		
	private static final String GET_ONE_STMT = 
		"SELECT emp_id, emp_name, dept_name, title_name, series_name, level_name, subject_name, grade, training_hour, training_date" +
		"FROM EMPLOYEE_TRAINING_VIEW where emp_id = ?";
	*/
	private static final String GET_ALL_STMT = "SELECT * FROM EMPLOYEE_TRAINING_VIEW";
	private static final String GET_ONE_STMT = 
		"SELECT * FROM EMPLOYEE_TRAINING_VIEW where emp_id = ?";




	@Override
	public List<EmployeeTrainingViewVO> findByPrimaryKey(Integer empId) {
		List<EmployeeTrainingViewVO> list = new ArrayList<EmployeeTrainingViewVO>();
		EmployeeTrainingViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeTrainingViewVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setEmpName(rs.getString("emp_name"));
				employeeVO.setDeptName(rs.getString("dept_name"));
				employeeVO.setTitleName(rs.getString("title_name"));
				employeeVO.setSeriesName(rs.getString("series_name"));
				employeeVO.setLevelName(rs.getString("level_name"));
				employeeVO.setSubjectName(rs.getString("subject_name"));
				employeeVO.setTrainingDate(rs.getDate("training_date"));
				employeeVO.setTransfer(rs.getDate("transfer"));
				employeeVO.setGuarantee(rs.getDate("guarantee"));
				employeeVO.setFee(rs.getInt("fee"));
				employeeVO.setTrainingType(rs.getInt("training_type"));
				employeeVO.setCompensate(rs.getDouble("compensate"));
				employeeVO.setTrainingHour(rs.getInt("training_hour"));
				employeeVO.setNote(rs.getString("note"));
				list.add(employeeVO);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<EmployeeTrainingViewVO> getAll() {
		List<EmployeeTrainingViewVO> list = new ArrayList<EmployeeTrainingViewVO>();
		EmployeeTrainingViewVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeTrainingViewVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setEmpName(rs.getString("emp_name"));
				employeeVO.setDeptName(rs.getString("dept_name"));
				employeeVO.setTitleName(rs.getString("title_name"));
				employeeVO.setSeriesName(rs.getString("series_name"));
				employeeVO.setLevelName(rs.getString("level_name"));
				employeeVO.setSubjectName(rs.getString("subject_name"));
				employeeVO.setTrainingDate(rs.getDate("training_date"));
				employeeVO.setTransfer(rs.getDate("transfer"));
				employeeVO.setGuarantee(rs.getDate("guarantee"));
				employeeVO.setFee(rs.getInt("fee"));
				employeeVO.setTrainingHour(rs.getInt("training_hour"));
				employeeVO.setNote(rs.getString("note"));
				list.add(employeeVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<EmployeeTrainingViewVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

}