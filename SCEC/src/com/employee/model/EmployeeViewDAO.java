package com.employee.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.subject.model.*;
import com.util.SCECcommon;

import com.category.model.CategoryService;

public class EmployeeViewDAO implements EmployeeViewDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String GET_ALL_STMT = "SELECT * FROM EMPLOYEE_VIEW";
	private static final String GET_ALL_BY_DEPT_ID = "SELECT * FROM EMPLOYEE_VIEW where dept_id = ?";
	/*
	Error: Unknown column 'emp_id' in 'field list'
	private static final String GET_ALL_STMT =
		"SELECT emp_id, emp_name, dept_name, title_name, series_name, level_name, subject_name, grade, training_hour, training_date" +
		"FROM EMPLOYEE_VIEW order by emp_id";
	*/
	//private static final String GET_ONE_STMT = 
		//"SELECT emp_id, emp_name, dept_name, dept_id, title_name, title_id, series_name, level_name, account" +
		//"FROM EMPLOYEE_VIEW where emp_id = ?";
	
	private static final String GET_ONE_STMT = "SELECT * FROM EMPLOYEE_VIEW where emp_id = ?";
	private static final String GET_ONE_EMP_BY_ACCOUNT = "SELECT * FROM EMPLOYEE_VIEW where account = ?";
	private static final String GET_ONE_EMP_BY_ACCOUNTID = "SELECT * FROM EMPLOYEE_VIEW where account_id = ?";
	private static final String GET_EMPS_BY_DEPTID_TITLEID = "SELECT * FROM EMPLOYEE_VIEW where dept_id = ? and title_id = ?";

	@Override
	public List<EmployeeViewVO> getEmpsByDeptIdAndTitleId(int deptId, int titleId) {
		List<EmployeeViewVO> list = new ArrayList<EmployeeViewVO>();
		EmployeeViewVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_EMPS_BY_DEPTID_TITLEID);
			pstmt.setInt(1, deptId);
			pstmt.setInt(2, titleId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeViewVO();
				setVO(rs, con, employeeVO);
				list.add(employeeVO); // Store the row in the list
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		
		return list;
	}
	
	@Override
	public EmployeeViewVO getOneEmpByAccount(String account, int accountId) {
		EmployeeViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
		float [] average = null; 

		try {
			con = ds.getConnection();
			if (account != null) { 
				pstmt = con.prepareStatement(GET_ONE_EMP_BY_ACCOUNT);
				pstmt.setString(1, account);
			} else {
				pstmt = con.prepareStatement(GET_ONE_EMP_BY_ACCOUNTID);
				pstmt.setInt(1, accountId);
			}
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeViewVO();
				setVO(rs, con, employeeVO);
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return employeeVO;
	}


	@Override
	public EmployeeViewVO findByPrimaryKey(Integer empId) {
		EmployeeViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
		float [] average = null; 

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeViewVO();
				setVO(rs, con, employeeVO);
				empId = employeeVO.getEmpId();
				average = empAbSvc.getAverage(empId);
				employeeVO.setAverage(average);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return employeeVO;
	}

	@Override
	public List<EmployeeViewVO> getAll() {
		List<EmployeeViewVO> list = new ArrayList<EmployeeViewVO>();
		EmployeeViewVO employeeVO = null;
		EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
		CategoryService catSvc = new CategoryService();
		int numOfCat = catSvc.numOfCat;
		float [] average = null; 
		Integer empId = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float total = 0, totalAvg = 0;
		int i = 0;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				employeeVO = new EmployeeViewVO();
				setVO(rs, con, employeeVO);
				empAbSvc.getEmployeeAverage(employeeVO);
				list.add(employeeVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<EmployeeViewVO> getAll(Integer deptId) {
		List<EmployeeViewVO> list = new ArrayList<EmployeeViewVO>();
		EmployeeViewVO employeeVO = null;
		EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
		CategoryService catSvc = new CategoryService();
		int numOfCat = catSvc.numOfCat;
		float [] average = null; 
		int empId = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_DEPT_ID);
			pstmt.setInt(1, deptId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				employeeVO = new EmployeeViewVO();
				setVO(rs, con, employeeVO);
				empId = employeeVO.getEmpId();
				average = empAbSvc.getAverage(empId);
				employeeVO.setAverage(average);
				list.add(employeeVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<EmployeeViewVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void setVO(ResultSet rs, Connection con, EmployeeViewVO employeeVO) throws SQLException {
		EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
		int empId = rs.getInt("emp_id");
		float [] average = null;
		
		employeeVO.setEmpId(empId);
		employeeVO.setEmpName(rs.getString("emp_name"));
		employeeVO.setDeptName(rs.getString("dept_name"));
		employeeVO.setDeptId(rs.getInt("dept_id"));
		employeeVO.setSectionName(rs.getString("section_name"));
		employeeVO.setSectionId(rs.getInt("section_id"));
		employeeVO.setTitleName(rs.getString("title_name"));
		employeeVO.setTitleId(rs.getInt("title_id"));
		employeeVO.setSeriesName(rs.getString("series_name"));
		employeeVO.setLevelName(rs.getString("level_name"));
		employeeVO.setOnboard(rs.getDate("onboard_date"));
		employeeVO.setAccount(rs.getString("account"));
		employeeVO.setPriv(rs.getInt("priv"));
		average = empAbSvc.getAverage(empId);
		employeeVO.setAverage(average);
		
		//TODO:should get average for development skills
		/*
		employeeVO.setAverageD(averageD);
		employeeVO.setTotalAvgD(totalAvgD);
		*/
	}

}