package com.employee.model;

import java.sql.Date;
import java.util.*;


public class EmployeeViewVO implements java.io.Serializable{
	private Integer empId  ;
	private String empName; 
	private Date onboard;
	private Date leaveDate;
	private String deptName; 
	private int	deptId;
	private String sectionName;
	private int sectionId;
	private String titleName; 
	private int titleId;
	private String seriesName; 
	private String levelName;
	private float [] average; //required ability average of each category
	private float [] averageD; //optional ability average of each category
	private float [] companyAvg;
	private float totalAvg;
	private float totalAvgD;
	private String account;
	private int priv;
	
	public float[] getAverage() {
		return average;
	}
	public void setAverage(float[] average) {
		this.average = average;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Date getOnboard() {
		return onboard;
	}
	public void setOnboard(Date onboard) {
		this.onboard = onboard;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public float getTotalAvg() {
		return totalAvg;
	}
	public void setTotalAvg(float totalAvg) {
		this.totalAvg = totalAvg;
	}
	public float[] getAverageD() {
		return averageD;
	}
	public void setAverageD(float[] averageD) {
		this.averageD = averageD;
	}
	public float getTotalAvgD() {
		return totalAvgD;
	}
	public void setTotalAvgD(float totalAvgD) {
		this.totalAvgD = totalAvgD;
	}
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public int getTitleId() {
		return titleId;
	}
	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}
	public int getPriv() {
		return priv;
	}
	public void setPriv(int priv) {
		this.priv = priv;
	}
	public float[] getCompanyAvg() {
		return companyAvg;
	}
	public void setCompanyAvg(float[] companyAvg) {
		this.companyAvg = companyAvg;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public int getSectionId() {
		return sectionId;
	}
	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}
}
	