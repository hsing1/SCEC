package com.employee.model;

import java.util.*;

public interface EmployeeTrainingViewDAO_interface {
	public List<EmployeeTrainingViewVO> findByPrimaryKey(Integer empId);
	public List<EmployeeTrainingViewVO> getAll();
	//萬用複合查詢(傳入參數型態Map)(回傳 List)
	public List<EmployeeTrainingViewVO> getAll(Map<String, String[]> map);
}
