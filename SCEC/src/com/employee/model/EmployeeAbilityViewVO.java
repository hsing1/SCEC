package com.employee.model;

import java.sql.Date;
import java.util.*;

public class EmployeeAbilityViewVO implements java.io.Serializable{
	private Integer empId;
	private String empName;
	private String deptName; 
	private Integer deptId;
	private String titleName; 
	private Integer titleId;
	private String subjectName;
	private Integer subjectId;
	private Float grade;
	private Float requiredGrade;
	private Integer categoryId;
	private String categoryName;
	private Integer classId;
	private String className;
	private float companyAvg; //公司應備平均
	private float companyAvgD; //公司具備平均
	private float deptAvg; //部門應備平均
	private float deptAvgD; //部門具備平均
	private float titleAvg; //同職位應備平均(跨部門）
	private float titleAvgD; //同職位具備平均
	private float standard;
	private boolean isRequired;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Float getGrade() {
		return grade;
	}
	public void setGrade(Float grade) {
		this.grade = grade;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getClassId() {
		return classId;
	}
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public float getCompanyAvg() {
		return companyAvg;
	}
	public void setCompanyAvg(float companyAvg) {
		this.companyAvg = companyAvg;
	}
	public float getDeptAvg() {
		return deptAvg;
	}
	public void setDeptAvg(float deptAvg) {
		this.deptAvg = deptAvg;
	}
	public Integer getTitleId() {
		return titleId;
	}
	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}
	public float getStandard() {
		return standard;
	}
	public void setStandard(float standard) {
		this.standard = standard;
	}
	public Float getRequiredGrade() {
		return requiredGrade;
	}
	public void setRequiredGrade(Float requiredGrade) {
		this.requiredGrade = requiredGrade;
	}
	public float getCompanyAvgD() {
		return companyAvgD;
	}
	public void setCompanyAvgD(float companyAvgD) {
		this.companyAvgD = companyAvgD;
	}
	public float getDeptAvgD() {
		return deptAvgD;
	}
	public void setDeptAvgD(float deptAvgD) {
		this.deptAvgD = deptAvgD;
	}
	public float getTitleAvg() {
		return titleAvg;
	}
	public void setTitleAvg(float titleAvg) {
		this.titleAvg = titleAvg;
	}
	public float getTitleAvgD() {
		return titleAvgD;
	}
	public void setTitleAvgD(float titleAvgD) {
		this.titleAvgD = titleAvgD;
	}
	public boolean isRequired() {
		return isRequired;
	}
	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}
}