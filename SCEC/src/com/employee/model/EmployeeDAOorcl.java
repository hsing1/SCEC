package com.employee.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class EmployeeDAOorcl implements EmployeeDAO_interface {
	// 一個應用程式中,針對一個資料庫 ,共用一個DataSource即可
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/SC_PROJECT");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO EMPLOYEE (emp_id, first_name, last_name, dept_id, title_id, account_id) VALUES ( EMPLOYEE_seq.NEXTVAL, ?,?,?,?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT emp_id,first_name,last_name,dept_id,title_id,account_id  FROM EMPLOYEE order by emp_id";
	private static final String GET_ONE_STMT = 
		"SELECT emp_id,first_name,last_name,dept_id,title_id,account_id  FROM EMPLOYEE where emp_id = ?";
	private static final String DELETE = 
		"DELETE FROM EMPLOYEE where emp_id = ?";
	private static final String UPDATE = 
		"UPDATE EMPLOYEE  set first_name=?,last_name = ?,dept_id=? ,title_id = ?,account_id = ? where emp_id=?";

	@Override
	public void insert(EmployeeVO employeeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
		
			pstmt.setString(1, employeeVO.getFirstName());
			pstmt.setString(2, employeeVO.getLastName());
			pstmt.setInt(3, employeeVO.getDeptId());
			pstmt.setInt(4, employeeVO.getTitleId());
			pstmt.setInt(5, employeeVO.getAccountId());
				
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(EmployeeVO employeeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, employeeVO.getFirstName());
			pstmt.setString(2, employeeVO.getLastName());
			pstmt.setInt(3, employeeVO.getDeptId());
			pstmt.setInt(4, employeeVO.getTitleId());
			pstmt.setInt(5, employeeVO.getAccountId());
			pstmt.setInt(6, employeeVO.getEmpId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer empId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, empId);

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public EmployeeVO findByPrimaryKey(Integer empId) {

		EmployeeVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				employeeVO = new EmployeeVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setFirstName(rs.getString("first_name"));
				employeeVO.setLastName(rs.getString("last_name"));
				employeeVO.setDeptId(rs.getInt("dept_id"));
				employeeVO.setTitleId(rs.getInt("title_id"));
				employeeVO.setAccountId(rs.getInt("account_id"));
				
				
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return employeeVO;
	}

	@Override
	public List<EmployeeVO> getAll() {
		List<EmployeeVO> list = new ArrayList<EmployeeVO>();
		EmployeeVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				employeeVO = new EmployeeVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setFirstName(rs.getString("first_name"));
				employeeVO.setLastName(rs.getString("last_name"));
				employeeVO.setDeptId(rs.getInt("dept_id"));
				employeeVO.setTitleId(rs.getInt("title_id"));
				employeeVO.setAccountId(rs.getInt("account_id"));
				list.add(employeeVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<EmployeeVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

}