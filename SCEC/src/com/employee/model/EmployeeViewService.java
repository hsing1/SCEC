package com.employee.model;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import com.department.model.DepartmentService;
import com.title.model.*;

public class EmployeeViewService {

	private EmployeeViewDAO_interface dao;

	public EmployeeViewService() {
		dao = new EmployeeViewDAO();
	}

	public EmployeeViewVO getOneEmployee(Integer empId) {
		return dao.findByPrimaryKey(empId);
	}

	public List<EmployeeViewVO> getAll() {
		return dao.getAll();
	}
	
	//get employee by deptId
	public List<EmployeeViewVO> getAll(Integer deptId) {
		return dao.getAll(deptId);
	}
	
	public List<EmployeeViewVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	// get employee by account or account_id
	public EmployeeViewVO getOneEmpByAccount(String account, int accountId) {
		return dao.getOneEmpByAccount(account, accountId);
	}
	
	//get employee by deptId and titleId
	public List<EmployeeViewVO> getEmpsByDeptIdAndTitleId(Integer deptId, Integer titleId) {
		return dao.getEmpsByDeptIdAndTitleId(deptId, titleId);
	}
}
