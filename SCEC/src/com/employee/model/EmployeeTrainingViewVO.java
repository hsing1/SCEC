package com.employee.model;

import java.sql.Date;

public class EmployeeTrainingViewVO implements java.io.Serializable{
	private Integer empId  ;
	private String empName; 
	private Date onboard;
	private Date leaveDate;
	private String deptName; 
	private String titleName; 
	private String seriesName; 
	private String levelName; 
	private String subjectName; 
	private Integer grade;
	private Integer trainingHour;
	private Integer fee;
	private Date trainingDate;
	private Date guarantee;
	private Date transfer;
	private String certificate;
	private String note;
	private int trainingType;
	private double compensate;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Date getOnboard() {
		return onboard;
	}
	public void setOnboard(Date onboard) {
		this.onboard = onboard;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public Integer getTrainingHour() {
		return trainingHour;
	}
	public void setTrainingHour(Integer trainingHour) {
		this.trainingHour = trainingHour;
	}
	public Date getTrainingDate() {
		return trainingDate;
	}
	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}
	public Integer getFee() {
		return fee;
	}
	public void setFee(Integer fee) {
		this.fee = fee;
	}
	public Date getGuarantee() {
		return guarantee;
	}
	public void setGuarantee(Date guarantee) {
		this.guarantee = guarantee;
	}
	public Date getTransfer() {
		return transfer;
	}
	public void setTransfer(Date transfer) {
		this.transfer = transfer;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getTrainingType() {
		return trainingType;
	}
	public void setTrainingType(int trainingType) {
		this.trainingType = trainingType;
	}
	public double getCompensate() {
		return compensate;
	}
	public void setCompensate(double compensate) {
		this.compensate = compensate;
	}
}
