package com.employee.model;

import java.sql.Connection;
import java.util.*;

public interface EmployeeAbilityViewDAO_interface {
	public List<EmployeeAbilityViewVO> findByPrimaryKey(Integer empId, Integer catId);
	public List<EmployeeAbilityViewVO> getAll();
	
	// calculate average
	public float [] calAverage(Integer empId);
	public void getEmployeeAverage(EmployeeViewVO empVO);
	public float [] getCompanyAvg();
	public float getCompanyAvgBySubjectId(Connection con, int subjectId);
    public float getAvgByDeptId(Connection con, int deptId, int subjectId);
    public float getAvgByDeptId(Connection con, int deptId);
    public float getAvgDByDeptId(Connection con, int deptId, int subjectId);
    public float getAvgByTitleId(Connection con, int titleId, int subjectId);
    public float getAvgDByTitleId(Connection con, int titleId, int subjectId);
    public float getAvgByDeptIdCatId(Connection con, int deptId, int catId);
    public float getAvgByTitleIdCatId(int titleId, int catId);
    
	public List<EmployeeAbilityViewVO> getAll(Map<String, String[]> map);
	public List<EmployeeAbilityViewVO> findBySubjectIdGrade(EmployeeViewVO myEmpVO, Integer subjectId, Float grade, String condition, int required);
	public List<EmployeeAbilityViewVO> findSubjectByEmpAndGrade(Integer empId, Float grade, String condition);
	/*
	 * 辦訓科目:find number of employee that is unqualified on a subject
	 * level: 0 company average, 1 : 1 ~ 2, 2: 2 ~ 3 , 3: 3 ~ 4, 4: 4
	 */
	public Map<Integer, Integer> findUnqualified(EmployeeViewVO myEmpVO, int level);
	public List<List<EmployeeAbilityViewVO>> getReview(Integer empId);
	public List<EmployeeAbilityViewVO> getAll(Integer empDI);
}
