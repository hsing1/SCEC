package com.employee.model;

import java.sql.Connection;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.account.model.*;
import com.department.model.DepartmentService;
import com.devision.model.DevisionService;
import com.section.model.SectionService;
import com.title.model.*;

public class EmployeeService {
	public static LinkedHashMap<Integer, String> empMap; //(empId, empName)
	public static LinkedHashMap<String, Integer> empMapInv; //(empName, empId)
	public static LinkedHashMap<Integer, String> empToDeptMap; //(empId, deptName)
	public static LinkedHashMap<Integer, Integer> empToDeptIdMap; //(empId, deptId)
	public static LinkedHashMap<Integer, String> empToSectionMap; //(empId, sectionName)
	public static LinkedHashMap<Integer, Integer> empToSectionIdMap; //(empId, sectionId)
	public static LinkedHashMap<Integer, String> empToTitleMap; //(empId, titleName)
	public static LinkedHashMap<Integer, Integer> empToTitleIdMap; //(empId, titleId)
	public static LinkedHashMap<Integer, String> empToDevisionMap; //(empId, devisionName)
	public static LinkedHashMap<Integer, Integer> empToDevisionIdMap; //(empId, devisionId)
	public static EmployeeDAO daoS = new EmployeeDAO();
	private EmployeeDAO_interface dao;
	
	static {
		if (empMap == null)
			empMap = new LinkedHashMap<Integer, String>();
		
		if (empMapInv == null)
			empMapInv = new LinkedHashMap<String, Integer>();
		
		if (empToDeptMap == null)
			empToDeptMap = new LinkedHashMap<Integer, String>();
		
		if (empToDeptIdMap == null)
			empToDeptIdMap = new LinkedHashMap<Integer, Integer>();
		
		if (empToSectionMap == null)
			empToSectionMap = new LinkedHashMap<Integer, String>();
		
		if (empToSectionIdMap == null)
			empToSectionIdMap = new LinkedHashMap<Integer, Integer>();
		
		if (empToTitleMap == null)
			empToTitleMap = new LinkedHashMap<Integer, String>();
		
		if (empToTitleIdMap == null)
			empToTitleIdMap = new LinkedHashMap<Integer, Integer>();
		
		if (empToDevisionMap == null)
			empToDevisionMap = new LinkedHashMap<Integer, String>();
		
		if (empToDevisionIdMap == null)
			empToDevisionIdMap = new LinkedHashMap<Integer, Integer>();
	}

	public EmployeeService() {
		dao = new EmployeeDAO();
	}

	public EmployeeVO addEmployee(int empId, String devision, String dept, String section, String title, String name, 
			java.util.Date uonboard, java.util.Date uleaveDate, String identity, String account, int priv) {
		DepartmentService deptSvc = new DepartmentService();
		TitleService titleSvc = new TitleService();
		AccountService accSvc = new AccountService();
		int accountId = 0;
		AccountVO accVO = new AccountVO();
		Integer deptId = deptSvc.getOneDept(dept).getDeptId();
		TitleVO titleVO = titleSvc.getOneTitle(title);
		Integer titleId = titleVO.getTitleId();
		Integer defPriv = titleVO.getPriv();
		Date onboardDate = null, leaveDate = null;
		Integer sectionId = SectionService.sectionMapInv.get(section);
		Integer devisionId = DevisionService.devisionMapInv.get(devision);
		
		if (uonboard != null)
			onboardDate = new Date(uonboard.getTime());
		if (uleaveDate != null)
			leaveDate = new Date(uleaveDate.getTime());
		
		if (priv == 0)
			priv = defPriv;
		
		accountId = accSvc.addAccount(account, priv).getAccountId();
		
		/*
		if ((onboardStr != null) && (!onboardStr.isEmpty()))
			onboardDate = Date.valueOf(onboardStr);
		
		if ((leaveDateStr!= null) && (!leaveDateStr.isEmpty()))
			leaveDate = Date.valueOf(leaveDateStr);
			*/
		
		EmployeeVO employeeVO = new EmployeeVO();	
		employeeVO.setEmpId(empId);
		employeeVO.setName(name);
		employeeVO.setDevisionId(devisionId);
		employeeVO.setDeptId(deptId);
		employeeVO.setSectionId(sectionId);
		employeeVO.setTitleId(titleId);
		employeeVO.setOnboard(onboardDate);
		employeeVO.setLeaveDate(leaveDate);
		employeeVO.setAccountId(accountId);
		dao.insert(employeeVO);

		return employeeVO;
	}

	public EmployeeVO updateTitle(Integer empId, String firstName, String lastName, Integer deptId, Integer titleId, Integer accountId) {

		EmployeeVO employeeVO = new EmployeeVO();	
		employeeVO.setEmpId(empId);
		employeeVO.setFirstName( firstName);
		employeeVO.setLastName(lastName);
		employeeVO.setDeptId( deptId);
		employeeVO.setTitleId( titleId);
		employeeVO.setAccountId(accountId);
	
		dao.update(employeeVO);

		return employeeVO;
	}

	public void deleteTitle(Integer empId) {
		dao.delete(empId);
	}

	public EmployeeVO getOneEmp(Integer empId) {
		return dao.findByPrimaryKey(empId);
	}

	public List<EmployeeVO> getAll() {
		return dao.getAll();
	}
	
	public List<EmployeeVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public List<EmployeeVO> getEmpsByDeptIdAndTitleId(Integer deptId, Integer titleId) {
		return dao.getEmpsByDeptIdAndTitleId(deptId, titleId);
	}
	
	public List<EmployeeVO> getEmpsByDeptIdAndTitleId(Integer [] deptArr, Integer [] titleArr) {
		return dao.getEmpsByDeptIdAndTitleId(deptArr, titleArr);
	}
	
	public int getNumOfEmpByDeptIdAndTitleId(Connection con, int deptId, int titleId) {
		return dao.getNumOfEmpByDeptIdAndTitleId(con, deptId, titleId);
	}
	
	public int getNumOfEmpByDeptIdAndSectionIdAndTitleId(Connection con, int deptId, int sectionId, int titleId) {
		return dao.getNumOfEmpByDeptIdAndSectionIdAndTitleId(con, deptId, sectionId, titleId);
	}
	
	public static void setEmpMap() {
		List<EmployeeVO> list = daoS.getAll();
		
		for (EmployeeVO empVO : list) {
			updateEmpMap(empVO);
		}
	}
	
	public static void updateEmpMap(EmployeeVO empVO) {
		int empId = 0, titleId = 0, sectionId = 0, deptId = 0, devisionId = 0;
		String empName = null, titleName = null, sectionName = null, deptName = null, devisionName = null;
		
		empId = empVO.getEmpId();
		empName = empVO.getName();
		empMap.put(empId, empName);
		empMapInv.put(empName, empId); //might be duplicate
		
		titleId = empVO.getTitleId();
		titleName = TitleService.titleMap.get(titleId);
		empToTitleMap.put(empId, titleName);
		empToTitleIdMap.put(empId, titleId);
		
		sectionId = empVO.getSectionId();
		sectionName = SectionService.sectionMap.get(sectionId);
		empToSectionMap.put(empId, sectionName);
		empToSectionIdMap.put(empId, sectionId);
		
		deptId = empVO.getDeptId();
		deptName = DepartmentService.deptMap.get(deptId);
		empToDeptMap.put(empId, deptName);
		empToDeptIdMap.put(empId, deptId);
		
		devisionId = empVO.getDevisionId();
		devisionName = DevisionService.devisionMap.get(devisionId);
		empToDevisionMap.put(empId, devisionName);
		empToDevisionIdMap.put(empId, devisionId);
	}
}
