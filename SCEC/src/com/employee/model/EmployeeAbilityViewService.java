package com.employee.model;

import java.sql.Date;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import com.department.model.DepartmentService;
import com.title.model.*;

public class EmployeeAbilityViewService {

	private EmployeeAbilityViewDAO_interface dao;

	public EmployeeAbilityViewService() {
		dao = new EmployeeAbilityViewDAO();
	}

	//get all abilities of one employee
	public List<EmployeeAbilityViewVO> getOneEmployee(Integer empId, Integer catId) {
		return dao.findByPrimaryKey(empId, catId);
	}

	public List<EmployeeAbilityViewVO> getAll() {
		return dao.getAll();
	}
	
	public List<EmployeeAbilityViewVO> getAll(Integer empId) {
		return dao.getAll(empId);
	}
	
	public float [] getAverage(Integer empId) {
		return dao.calAverage(empId);
	}
	
	public float [] getCompanyAvg() {
		return dao.getCompanyAvg();
	}
	
	public float getCompanyAvgBySubjetId(Connection con, int subjectId) {
		return dao.getCompanyAvgBySubjectId(con, subjectId);
	}

	public void getEmployeeAverage(EmployeeViewVO empVO) {
		dao.getEmployeeAverage(empVO);
	}
	
	public List<EmployeeAbilityViewVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public float getAvgByDeptIdCatId(Connection con, int deptId, int catId) {
		return dao.getAvgByDeptIdCatId(con, deptId, catId);
	}
	
	public float getAvgByDeptId(Connection con, int deptId) {
		return dao.getAvgByDeptId(con, deptId);
	}
	
	public float getAvgByTitleId(Connection con, int titleId, int subjectId) {
		return dao.getAvgByTitleId(con, titleId, subjectId);
	}

	public float getAvgDByTitleId(Connection con, int titleId, int subjectId) {
		return dao.getAvgDByTitleId(con, titleId, subjectId);
	}
	
	public List<EmployeeAbilityViewVO> getEmpBySubjectIdGrade(EmployeeViewVO myEmpVO, Integer subjectId, Float grade, String condition, int required) {
		return dao.findBySubjectIdGrade(myEmpVO, subjectId, grade, condition, required);
	}
	
	/*
	 * 辦訓科目:find number of employee that is unqualified on a subject
	 */
	public Map<Integer, Integer> getUnqualified(EmployeeViewVO myEmpVO, int level) {
		return dao.findUnqualified(myEmpVO, level);
	}
	
	public List<EmployeeAbilityViewVO> getSubjectByEmpAndGrade(Integer empId, Float grade, String condition) {
		return dao.findSubjectByEmpAndGrade(empId, grade, condition);
	}
	
	public List<List<EmployeeAbilityViewVO>> getReview(Integer empId) {
		return dao.getReview(empId);
	}
}
