package com.employee.model;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import com.department.model.DepartmentService;
import com.title.model.*;

public class EmployeeTrainingViewService {

	private EmployeeTrainingViewDAO_interface dao;

	public EmployeeTrainingViewService() {
		dao = new EmployeeTrainingViewDAO();
	}

	public List<EmployeeTrainingViewVO> getOneEmployee(Integer empId) {
		return dao.findByPrimaryKey(empId);
	}

	public List<EmployeeTrainingViewVO> getAll() {
		return dao.getAll();
	}
	
	public List<EmployeeTrainingViewVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
