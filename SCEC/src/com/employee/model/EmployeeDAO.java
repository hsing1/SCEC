package com.employee.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.department.model.DepartmentService;
import com.util.SCECcommon;

public class EmployeeDAO implements EmployeeDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO EMPLOYEE (emp_id, devision_id, dept_id, section_id, title_id, name, onboard_date, leave_date, account_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT * FROM EMPLOYEE order by emp_id";
	private static final String GET_ONE_STMT = 
		"SELECT * FROM EMPLOYEE where emp_id = ?";
	private static final String DELETE = 
		"DELETE FROM EMPLOYEE where emp_id = ?";
	private static final String UPDATE = 
		"UPDATE EMPLOYEE set first_name = ?, last_name = ?, dept_id = ?, title_id = ?, account_id = ? where emp_id = ?";
	private static final String GET_EMPS_BY_DEPTID_TITLEID = 
		"SELECT * FROM EMPLOYEE where dept_id = ? and title_id = ?";
	private static final String GET_NUM_OF_EMP_BY_DEPT_TITLE = 
		"SELECT count(emp_id) num_emp FROM EMPLOYEE where dept_id = ? and title_id = ?";
	private static final String GET_NUM_OF_EMP_BY_DEPT_SECTION_TITLE = 
		"SELECT count(emp_id) num_emp FROM EMPLOYEE where dept_id = ? and section_id = ? and title_id = ?";

	@Override
	public void insert(EmployeeVO employeeVO) {
		int empCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer deptId = null;
		HashMap<Integer, Integer> map = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
		
			pstmt.setInt(1, employeeVO.getEmpId());
			pstmt.setInt(2, employeeVO.getDevisionId());
			deptId = employeeVO.getDeptId();
			pstmt.setInt(3, deptId);
			pstmt.setInt(4, employeeVO.getSectionId());
			pstmt.setInt(5, employeeVO.getTitleId());
			pstmt.setString(6, employeeVO.getName());
			pstmt.setDate(7, employeeVO.getOnboard());
			pstmt.setDate(8, employeeVO.getLeaveDate());
			pstmt.setInt(9, employeeVO.getAccountId());
				
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, null, pstmt, true);
			
			DepartmentService.numOfEmpCompany++;
			
			if (DepartmentService.numOfEmpDept.containsKey(deptId)) 
				empCount = DepartmentService.numOfEmpDept.get(deptId);
			else
				empCount = 0;
			DepartmentService.numOfEmpDept.put(deptId, ++empCount);
		}
	}

	@Override
	public void update(EmployeeVO employeeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, employeeVO.getFirstName());
			pstmt.setString(2, employeeVO.getLastName());
			pstmt.setInt(3, employeeVO.getDeptId());
			pstmt.setInt(4, employeeVO.getTitleId());
			pstmt.setInt(5, employeeVO.getAccountId());
			pstmt.setInt(6, employeeVO.getEmpId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer empId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, empId);

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public EmployeeVO findByPrimaryKey(Integer empId) {

		EmployeeVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				employeeVO = new EmployeeVO();
				setVO(employeeVO, rs);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}

		return employeeVO;
	}

	@Override
	public List<EmployeeVO> getAll() {
		List<EmployeeVO> list = new ArrayList<EmployeeVO>();
		EmployeeVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				employeeVO = new EmployeeVO();
				setVO(employeeVO, rs);
				list.add(employeeVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<EmployeeVO> getEmpsByDeptIdAndTitleId(Integer deptId, Integer titleId) {
		List<EmployeeVO> list = new ArrayList<EmployeeVO>();
		EmployeeVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_EMPS_BY_DEPTID_TITLEID);
			pstmt.setInt(1, deptId);
			pstmt.setInt(2, titleId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setName(rs.getString("name"));
				employeeVO.setFirstName(rs.getString("first_name"));
				employeeVO.setLastName(rs.getString("last_name"));
				employeeVO.setDeptId(rs.getInt("dept_id"));
				employeeVO.setTitleId(rs.getInt("title_id"));
				employeeVO.setAccountId(rs.getInt("account_id"));
				list.add(employeeVO); // Store the row in the list
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<EmployeeVO> getEmpsByDeptIdAndTitleId(Integer [] deptArr, Integer [] titleArr) {
		int deptId = 0, titleId = 0, i = 0, j = 0;
		List<EmployeeVO> list = new ArrayList<EmployeeVO>();
		EmployeeVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_EMPS_BY_DEPTID_TITLEID);
			for (i = 0; i < deptArr.length; i++) {
				deptId = deptArr[i];
				for (j = 0; j < titleArr.length; j++) {
					titleId = titleArr[j];
					pstmt.setInt(1, deptId);
					pstmt.setInt(2, titleId);
					rs = pstmt.executeQuery();
		
					while (rs.next()) {
						employeeVO = new EmployeeVO();
						employeeVO.setEmpId(rs.getInt("emp_id"));
						employeeVO.setName(rs.getString("name"));
						employeeVO.setFirstName(rs.getString("first_name"));
						employeeVO.setLastName(rs.getString("last_name"));
						employeeVO.setDeptId(rs.getInt("dept_id"));
						employeeVO.setTitleId(rs.getInt("title_id"));
						employeeVO.setAccountId(rs.getInt("account_id"));
						list.add(employeeVO); // Store the row in the list
					}
				}
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public int getNumOfEmpByDeptIdAndSectionIdAndTitleId(Connection con, int deptId, int sectionId, int titleId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int numOfEmp = 0;
		boolean closeCon = false;

		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			pstmt = con.prepareStatement(GET_NUM_OF_EMP_BY_DEPT_SECTION_TITLE);
			pstmt.setInt(1, deptId);
			pstmt.setInt(2, sectionId);
			pstmt.setInt(3, titleId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				numOfEmp = rs.getInt("num_emp");
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		return numOfEmp;
	}
	
	@Override
	public int getNumOfEmpByDeptIdAndTitleId(Connection con, int deptId, int titleId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int numOfEmp = 0;
		boolean closeCon = false;

		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			pstmt = con.prepareStatement(GET_NUM_OF_EMP_BY_DEPT_TITLE);
			pstmt.setInt(1, deptId);
			pstmt.setInt(2, titleId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				numOfEmp = rs.getInt("num_emp");
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		return numOfEmp;
	}
	
	@Override
	public List<EmployeeVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void setVO(EmployeeVO employeeVO, ResultSet rs) throws SQLException {
		employeeVO.setEmpId(rs.getInt("emp_id"));
		employeeVO.setFirstName(rs.getString("first_name"));
		employeeVO.setLastName(rs.getString("last_name"));
		employeeVO.setName(rs.getString("name"));
		employeeVO.setDevisionId(rs.getInt("section_id"));
		employeeVO.setDeptId(rs.getInt("dept_id"));
		employeeVO.setSectionId(rs.getInt("section_id"));
		employeeVO.setTitleId(rs.getInt("title_id"));
		employeeVO.setAccountId(rs.getInt("account_id"));
		employeeVO.setOnboard(rs.getDate("onboard_date"));
		employeeVO.setLeaveDate(rs.getDate("leave_date"));
	}

}