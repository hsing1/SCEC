package com.employee.model;

import java.util.*;
import java.sql.*;

public class EmployeeJDBCDAO implements EmployeeDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO EMPLOYEE (emp_id,first_name,last_name,dept_id,title_id,account_id) VALUES ( EMPLOYEE_seq.NEXTVAL, ?,?,?,?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT emp_id,first_name,last_name,dept_id,title_id,account_id  FROM EMPLOYEE order by emp_id";
	private static final String GET_ONE_STMT = 
		"SELECT emp_id,first_name,last_name,dept_id,title_id,account_id  FROM EMPLOYEE where emp_id = ?";
	private static final String DELETE = 
		"DELETE FROM EMPLOYEE where emp_id = ?";
	private static final String UPDATE = 
		"UPDATE EMPLOYEE  set first_name=?,last_name = ?,dept_id=? ,title_id = ?,account_id = ? where emp_id=?";

	@Override
	public void insert(EmployeeVO employeeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
		
			pstmt.setString(1, employeeVO.getFirstName());
			pstmt.setString(2, employeeVO.getLastName());
			pstmt.setInt(3, employeeVO.getDeptId());
			pstmt.setInt(4, employeeVO.getTitleId());
			pstmt.setInt(5, employeeVO.getAccountId());
				
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(EmployeeVO employeeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
		
			pstmt.setString(1, employeeVO.getFirstName());
			pstmt.setString(2, employeeVO.getLastName());
			pstmt.setInt(3, employeeVO.getDeptId());
			pstmt.setInt(4, employeeVO.getTitleId());
			pstmt.setInt(5, employeeVO.getAccountId());
			pstmt.setInt(6, employeeVO.getEmpId());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer empId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, empId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public EmployeeVO findByPrimaryKey(Integer empId) {

		EmployeeVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				employeeVO = new EmployeeVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setFirstName(rs.getString("first_name"));
				employeeVO.setLastName(rs.getString("last_name"));
				employeeVO.setDeptId(rs.getInt("dept_id"));
				employeeVO.setTitleId(rs.getInt("title_id"));
				employeeVO.setAccountId(rs.getInt("account_id"));
				
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return employeeVO;
	}

	@Override
	public List<EmployeeVO> getAll() {
		List<EmployeeVO> list = new ArrayList<EmployeeVO>();
		EmployeeVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				employeeVO = new EmployeeVO();
				employeeVO.setEmpId(rs.getInt("emp_id"));
				employeeVO.setFirstName(rs.getString("first_name"));
				employeeVO.setLastName(rs.getString("last_name"));
				employeeVO.setDeptId(rs.getInt("dept_id"));
				employeeVO.setTitleId(rs.getInt("title_id"));
				employeeVO.setAccountId(rs.getInt("account_id"));
				list.add(employeeVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<EmployeeVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		EmployeeJDBCDAO dao = new EmployeeJDBCDAO();

		// 新增
		
//		EmployeeVO employeeVO = new EmployeeVO();
//		employeeVO.setFirstName("hsiming");
//		employeeVO.setLastName("chan");
//		employeeVO.setDeptId(1);
//		employeeVO.setTitleId(1);
//		employeeVO.setAccountId(3);
//		dao.insert(employeeVO);	
//		System.out.println("insert successdsful");
	
		// 修改
//		EmployeeVO employeeVO2 = new EmployeeVO();
//		employeeVO2.setFirstName("ming");
//		employeeVO2.setLastName("lin");
//		employeeVO2.setDeptId(1);
//		employeeVO2.setTitleId(1);
//		employeeVO2.setAccountId(3);
//		employeeVO2.setEmpId(3);
//		dao.update(employeeVO2);
//		System.out.println(employeeVO2.getEmpId());
//		System.out.println(employeeVO2.getFirstName());
//		System.out.println(employeeVO2.getLastName());
//		System.out.println(employeeVO2.getDeptId());
//		System.out.println(employeeVO2.getTitleId());
//		System.out.println(employeeVO2.getAccountId());
//		System.out.println("update sucessful");
	
		// 刪除
//		dao.delete(2);
//		System.out.println("delete sucessful");

		// 查詢
//		EmployeeVO employeeVO3 = dao.findByPrimaryKey(3);
//		System.out.println(employeeVO3.getEmpId());
//		System.out.println(employeeVO3.getFirstName());
//		System.out.println(employeeVO3.getLastName());
//		System.out.println(employeeVO3.getDeptId());
//		System.out.println(employeeVO3.getTitleId());
//		System.out.println(employeeVO3.getAccountId());
//		System.out.print("select sucessful");

		// 查詢
//		List<EmployeeVO> list = dao.getAll();
//		for (EmployeeVO EmployeeVO5 : list) {
//			System.out.println(EmployeeVO5.getEmpId());
//			System.out.println(EmployeeVO5.getFirstName());
//			System.out.println(EmployeeVO5.getLastName());
//			System.out.println(EmployeeVO5.getDeptId());
//			System.out.println(EmployeeVO5.getTitleId());
//			System.out.println(EmployeeVO5.getAccountId());
//			System.out.println("SUCCEEDS");
//		}
	}


}