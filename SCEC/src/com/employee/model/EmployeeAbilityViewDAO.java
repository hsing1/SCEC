package com.employee.model;

import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.category.model.*;
import com.standard.model.StandardService;
import com.subject.model.*;
import com.training.model.TrainingService;
import com.util.SCECcommon;

public class EmployeeAbilityViewDAO implements EmployeeAbilityViewDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String GET_ALL_STMT = "SELECT * FROM EMPLOYEE_ABILITY_VIEW";
	private static final String COUNT_EMP_STMT = "SELECT count(emp_id) count FROM EMPLOYEE_ABILITY_VIEW";
	private static final String GET_ONE_STMT = "SELECT * FROM EMPLOYEE_ABILITY_VIEW where emp_id = ?";
	private static final String GET_ONE_STMT_BY_CATID = "SELECT * FROM EMPLOYEE_ABILITY_VIEW where emp_id = ? and category_id = ?";
	private static final String AVG_BY_DEPT_STMT = "SELECT avg(required_grade) average FROM EMPLOYEE_ABILITY_VIEW where dept_id = ? and subject_id = ? and required = true";
	private static final String AVG_BY_DEPT_CAT_STMT = "SELECT avg(required_grade) average FROM EMPLOYEE_ABILITY_VIEW where dept_id = ? and category_id = ? and required = true";
	private static final String AVG_BY_TITLE_CAT_STMT = "SELECT avg(required_grade) average FROM EMPLOYEE_ABILITY_VIEW where title_id = ? and category_id = ? and required = true";
	private static final String AVG_BY_SUBJECT_STMT= "SELECT avg(required_grade) average FROM EMPLOYEE_ABILITY_VIEW where subject_id = ? and required = true";
	private static final String AVG_BY_DEPT = "SELECT avg(required_grade) avg FROM EMPLOYEE_ABILITY_VIEW where dept_id = ? and required = true";
	
	//部門應備總合
	private static final String SUM_OF_DEPT = "SELECT sum(required_grade) sum FROM EMPLOYEE_ABILITY_VIEW where dept_id = ? and subject_id = ? and required = true";
	//部門所有科目應備總合
	private static final String SUM_OF_DEPT_TOTAL = "SELECT sum(required_grade) sum FROM EMPLOYEE_ABILITY_VIEW where dept_id = ? and required = true";
	//部門具備總合
	private static final String SUMD_OF_DEPT = "SELECT sum(grade) sum FROM EMPLOYEE_ABILITY_VIEW where dept_id = ? and subject_id = ? and required = false";
	//公司應備總合
	private static final String SUM_OF_CMP = "SELECT sum(required_grade) sum FROM EMPLOYEE_ABILITY_VIEW where subject_id = ? and required = true";
	//公司具備總合
	private static final String SUMD_OF_CMP = "SELECT sum(grade) sum FROM EMPLOYEE_ABILITY_VIEW where subject_id = ? and required = false";
	//同職位應備總合
	private static final String SUM_OF_SAME_TITLE = "SELECT sum(required_grade) sum FROM EMPLOYEE_ABILITY_VIEW where title_id = ? and subject_id = ? and required = true";
	//同職位應備人數
	private static final String NUM_OF_EMP_OF_SAME_TITLE = "SELECT count(emp_id) empNum FROM EMPLOYEE_ABILITY_VIEW where title_id = ? and subject_id = ? and required = true";
	//同職位具備總合
	private static final String SUMD_OF_SAME_TITLE = "SELECT sum(grade) sum FROM EMPLOYEE_ABILITY_VIEW where title_id = ? and subject_id = ? and required = false";
	//同職位具備人數
	private static final String NUM_OF_EMP_OF_SAME_TITLE_D = "SELECT count(emp_id) empNum FROM EMPLOYEE_ABILITY_VIEW where title_id = ? and subject_id = ? and required = false";
	//公司平均
	private static final String GET_COMPANY_AVG = "SELECT avg(required_grade) cat_avg FROM EMPLOYEE_ABILITY_VIEW where category_id = ? and required = true";
	private static final String GET_COMPANY_AVG_BY_SUBJECT_ID = "SELECT avg(required_grade) subject_avg FROM EMPLOYEE_ABILITY_VIEW where subject_id = ? and required = true";

	@Override
	public float getCompanyAvgBySubjectId(Connection con, int subjectId) {
		float avg = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DecimalFormat twoDForm = new DecimalFormat("#.#"); 

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COMPANY_AVG_BY_SUBJECT_ID);
			pstmt.setInt(1, subjectId);
			
			rs = pstmt.executeQuery();
			while (rs.next()) {
				avg = rs.getFloat("subject_avg");
				avg = Float.valueOf(twoDForm.format(avg));
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return avg;
	}

	@Override
	public float [] getCompanyAvg() {
		int numOfCat = CategoryService.numOfCat;
		float [] avgArr = new float[numOfCat];
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DecimalFormat twoDForm = new DecimalFormat("#.#"); 

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COMPANY_AVG);
			
			for (int i = 0; i < numOfCat; i++) {
				pstmt.setInt(1, i+1);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					avgArr[i] = rs.getFloat("cat_avg");
					avgArr[i] = Float.valueOf(twoDForm.format(avgArr[i]));
				}
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return avgArr;
	}

	/*
	 * 員工總平均及職能類別平均
	 */
	@Override
	public void getEmployeeAverage(EmployeeViewVO empVO) {
		int empId = empVO.getEmpId();
		int numOfCat = CategoryService.numOfCat;
		int [] numOfSubArr = SubjectService.numOfSubArr;
		float total = 0, totalD = 0, totalAvg = 0, totalAvgD = 0;
		//TOTO:check issue #89
		float [] totalGrade = new float[numOfCat]; // total grade of each category
		float [] totalGradeD = new float[numOfCat]; // total development grade of each category
		float grade = 0, gradeD = 0;
		int catId = 0, classId = 0;
		float [] avgArr = new float[numOfCat]; 
		float [] avgArrD = new float[numOfCat]; 
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeAbVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DecimalFormat twoDForm = new DecimalFormat("#.#"); 

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeAbVO = new EmployeeAbilityViewVO();
				catId = rs.getInt("category_id");
				classId = rs.getInt("class_id");
				grade = rs.getFloat("required_grade");
				gradeD = rs.getFloat("grade");
				totalGrade[catId - 1] += grade;
				totalGradeD[catId - 1] += gradeD;
				total += grade;
				totalD += gradeD;
			}
			totalAvg = total / SubjectService.totalSub;
			totalAvgD = totalD / SubjectService.totalSub;
			empVO.setTotalAvg(totalAvg);
			empVO.setTotalAvgD(totalAvgD);
			
			// calculate average of each category
			for(int i = 0; i < numOfCat; i++) {
				if (numOfSubArr[i+1] == 0)
					continue;
				avgArr[i] = totalGrade[i]/numOfSubArr[i+1];
				avgArrD[i] = totalGradeD[i]/numOfSubArr[i+1];
				avgArr[i] = Float.valueOf(twoDForm.format(avgArr[i]));
				avgArrD[i] = Float.valueOf(twoDForm.format(avgArrD[i]));
			}
			empVO.setAverage(avgArr);
			empVO.setAverageD(avgArrD);
			
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
	}
	
	@Override
	public float [] calAverage(Integer empId) {
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		int numOfCat = CategoryService.numOfCat;
		int [] numOfSubArr = SubjectService.numOfSubArr;
		ResultSet rs = null;
		//TOTO:check issue #89
		float [] totalGrade = new float[numOfCat]; // total grade of each category
		float [] totalGradeD = new float[numOfCat]; // total development grade of each category
		float grade = 0, gradeD = 0;
		int catId = 0, classId = 0;
		float [] avgArr = new float[numOfCat]; 
		float [] avgArrD = new float[numOfCat]; 
		DecimalFormat twoDForm = new DecimalFormat("#.#"); 

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, empId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeAbilityViewVO();
				catId = rs.getInt("category_id");
				classId = rs.getInt("class_id");
				grade = rs.getFloat("required_grade");
				gradeD = rs.getFloat("grade");
				totalGrade[catId - 1] += grade;
				totalGradeD[catId - 1] += gradeD;
			}
			
			// calculate average of each category
			for(int i = 0; i < numOfCat; i++) {
				if (numOfSubArr[i+1] == 0)
					continue;
				avgArr[i] = totalGrade[i]/numOfSubArr[i+1];
				avgArrD[i] = totalGradeD[i]/numOfSubArr[i+1];
				avgArr[i] = Float.valueOf(twoDForm.format(avgArr[i]));
				avgArrD[i] = Float.valueOf(twoDForm.format(avgArrD[i]));
			}
			
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return avgArr;
	}
	
	/*
	 * 計算同職位應備平均
	 * related : listOneEmpAbility.jsp
	 */
	@Override 
	public float getAvgByTitleId(Connection con, int titleId, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0, sum = 0;
		TrainingService trnSvc = new TrainingService();
		int numOfEmp = 0; //number of employee with the same title that required the specified ability
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(SUM_OF_SAME_TITLE);
			pstmt.setInt(1, titleId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sum = rs.getFloat("sum");
			}

			pstmt = con.prepareStatement(NUM_OF_EMP_OF_SAME_TITLE);
			pstmt.setInt(1, titleId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				numOfEmp = rs.getInt("empNum");
			}
			
			if (numOfEmp == 0)
				average = 0;
			else
				average = sum/numOfEmp;
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	public float getAvgByTitleId_org(Connection con, int titleId, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0, sum = 0;
		TrainingService trnSvc = new TrainingService();
		int numOfEmp = 0; //number of employee with the same title that required the specified ability
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(SUM_OF_SAME_TITLE);
			pstmt.setInt(1, titleId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sum = rs.getFloat("sum");
			}
			numOfEmp = trnSvc.getNumOfEmpByTitle(con, titleId, subjectId);
			
			if (numOfEmp == 0)
				average = 0;
			else
				average = sum/numOfEmp;
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}

	/*
	 * 計算同職位具備平均
	 */
	@Override 
	public float getAvgDByTitleId(Connection con, int titleId, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0, sum = 0;
		TrainingService trnSvc = new TrainingService();
		int numOfEmp = 0; //number of employee with the same title that required the specified ability
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(SUMD_OF_SAME_TITLE);
			pstmt.setInt(1, titleId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sum = rs.getFloat("sum");
			}

			pstmt = con.prepareStatement(NUM_OF_EMP_OF_SAME_TITLE_D);
			pstmt.setInt(1, titleId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				numOfEmp = rs.getInt("empNum");
			}
			
			if (numOfEmp == 0)
				average = 0;
			else
				average = sum/numOfEmp;
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	/*
	 * 計算某部門某項職能的平均 ＝ sum(required_grade(dept_id, subject_id)) / sum(employee(dept_id, required = 1))
	 */
	@Override 
	public float getAvgByDeptId(Connection con, int deptId, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0, sum = 0;
		TrainingService trnSvc = new TrainingService();
		int numOfEmp = 0; //number of employee in a department that required the specified ability
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(SUM_OF_DEPT);
			pstmt.setInt(1, deptId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sum = rs.getFloat("sum");
			}
			//TODO:should use hashmap to reduce overhead
			numOfEmp = trnSvc.getNumOfEmp(con, deptId, subjectId);
			
			if (numOfEmp == 0)
				average = 0;
			else
				average = sum/numOfEmp;
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	
	/*
	 * 計算某部門某項職能的平均 ＝ sum(grade(dept_id, subject_id)) / sum(employee(dept_id, required = 0))
	 */
	@Override 
	public float getAvgDByDeptId(Connection con, int deptId, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0, sum = 0;
		TrainingService trnSvc = new TrainingService();
		int numOfEmp = 0; //number of employee in a department that required the specified ability
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(SUMD_OF_DEPT);
			pstmt.setInt(1, deptId); 
			pstmt.setInt(2, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sum = rs.getFloat("sum");
			}
			//TODO:should use hashmap to reduce overhead
			numOfEmp = trnSvc.getNumOfEmpD(con, deptId, subjectId);
			
			if (numOfEmp == 0)
				average = 0;
			else
				average = sum/numOfEmp;
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	/*
	 * 計算某部門的總平均(所有科目一起平均)
	 */
	@Override 
	public float getAvgByDeptId(Connection con, int deptId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0;
		TrainingService trnSvc = new TrainingService();
		int numOfEmp = 0; //number of employee in a department that required the specified ability
		boolean closeCon = false;
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(AVG_BY_DEPT);
			pstmt.setInt(1, deptId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				average = rs.getFloat("avg");
				average = Float.valueOf(twoDForm.format(average));
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	//公司應備平均
	public float getAvgBySubjectId(Connection con, int subjectId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0, sum = 0;
		DecimalFormat twoDForm = new DecimalFormat("#.#"); 
		int numOfEmp = 0;
		TrainingService trnSvc = new TrainingService();
		boolean closeCon = false;
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(SUM_OF_CMP);
			pstmt.setInt(1, subjectId); 
			rs = pstmt.executeQuery();
			while (rs.next())
				sum = rs.getFloat("sum");
			
			numOfEmp = trnSvc.getNumOfEmp(con, subjectId);
			
			if (numOfEmp == 0)
				average = 0;
			else {
				average = sum / numOfEmp;
				average = Float.valueOf(twoDForm.format(average));
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	@Override 
	public float getAvgByDeptIdCatId(Connection con, int deptId, int catId) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0;
		DecimalFormat twoDForm = new DecimalFormat("#.#"); 
		boolean closeCon = false;
		
		
		try {
			if (con == null) {
				con = ds.getConnection();
				closeCon = true;
			}
			
			pstmt = con.prepareStatement(AVG_BY_DEPT_CAT_STMT);
			pstmt.setInt(1, deptId); 
			pstmt.setInt(2, catId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				average = rs.getFloat("average");
				average = Float.valueOf(twoDForm.format(average));
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, closeCon);
		}
		
		return average;
	}
	
	@Override 
	public float getAvgByTitleIdCatId(int titleId, int catId) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float average = 0;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(AVG_BY_TITLE_CAT_STMT);
			pstmt.setInt(1, titleId); 
			pstmt.setInt(2, catId); 
			rs = pstmt.executeQuery();
			while (rs.next()) {
				average = rs.getFloat("average");
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		return average;
	}

	/*
	 * find ability of specified employee, if categoryId = 0 mean find all ability
	 */
	@Override
	public List<EmployeeAbilityViewVO> findByPrimaryKey(Integer empId, Integer categoryId) {
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;
		TrainingService trnSvc = new TrainingService();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		float [] totalGrade = new float[CategoryVO.numOfCat];
		float grade = 0, deptAvg = 0, companyAvg = 0, standard = 0;
		int catId = 0, classId = 0, deptId = 0, subjectId = 0, titleId = 0;
		List<Float> avgList = new ArrayList<Float>();

		try {
			con = ds.getConnection();
			if (categoryId == 0) {
				pstmt = con.prepareStatement(GET_ONE_STMT);
				pstmt.setInt(1, empId);
			} else {
				pstmt = con.prepareStatement(GET_ONE_STMT_BY_CATID);
				pstmt.setInt(1, empId);
				pstmt.setInt(2, categoryId);
			}
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeAbilityViewVO();
				setVO(employeeVO, rs, con);
				list.add(employeeVO);
			}
			
			/*
			for(int i=0; i < CategoryVO.numOfCat; i++) {
				avgList.add(totalGrade[i]/4);
			}
			*/
			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	/*
	 * get list for optional ability and required (must) ability
	 */
	@Override
	public List<List<EmployeeAbilityViewVO>> getReview(Integer empId) {
		List<List<EmployeeAbilityViewVO>> listArr = new ArrayList<List<EmployeeAbilityViewVO>>();
		List<EmployeeAbilityViewVO> mustList = new ArrayList<EmployeeAbilityViewVO>();
		List<EmployeeAbilityViewVO> optionList = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			pstmt.setInt(1, empId);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeAbilityViewVO();
				setVO(employeeVO, rs, con);
				if (employeeVO.getGrade() > 0)
					optionList.add(employeeVO);
				else
					mustList.add(employeeVO);
			}
			listArr.add(mustList);
			listArr.add(optionList);
			
			/*
			for(int i=0; i < CategoryVO.numOfCat; i++) {
				avgList.add(totalGrade[i]/4);
			}
			*/
			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return listArr;
	}
	
	//filter required_grade by condition
	@Override
	public List<EmployeeAbilityViewVO> findSubjectByEmpAndGrade(Integer empId, Float grade, String condition) {
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String queryStmt = null;

		if (condition.equals("gt"))
			queryStmt = GET_ONE_STMT + " AND required_grade > ?";
		else if (condition.equals("ge"))
			queryStmt = GET_ONE_STMT + " AND required_grade >= ?";
		else if (condition.equals("eq"))
			queryStmt = GET_ONE_STMT + " AND required_grade = ?";
		else if (condition.equals("le"))
			queryStmt = GET_ONE_STMT + " AND required_grade <= ?";
		else if (condition.equals("lt"))
			queryStmt = GET_ONE_STMT + " AND required_grade < ?";
			
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(queryStmt);
			pstmt.setInt(1, empId);
			pstmt.setFloat(2, grade);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeAbilityViewVO();
				setVO(employeeVO, rs, con);
				list.add(employeeVO);
			}
			
			/*
			for(int i=0; i < CategoryVO.numOfCat; i++) {
				avgList.add(totalGrade[i]/4);
			}
			*/
			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<EmployeeAbilityViewVO> findBySubjectIdGrade(EmployeeViewVO myEmpVO, Integer subjectId, 
		Float grade, String condition, int required) {
		
		int priv = myEmpVO.getPriv();
		int myDeptId = myEmpVO.getDeptId(), myEmpId = myEmpVO.getEmpId();
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String queryStmt = null;
		String privStmt = null;
	

		try {
			con = ds.getConnection();
			
			//search required subject
			if (required == 1 || required == 2) {
				if (condition.equals("gt"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND required_grade > ?";
				else if (condition.equals("ge"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND required_grade >= ?";
				else if (condition.equals("eq"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND required_grade = ?";
				else if (condition.equals("le"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND required_grade <= ?";
				else if (condition.equals("lt"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND required_grade < ?";
				
				switch (priv) {
				case 1:
				case 2:
					privStmt = "";
					break;
				case 3:
					privStmt = " AND dept_id = ?";
					break;
				case 4:
					privStmt = " AND emp_id = ?";
					break;
				}
			
				queryStmt = queryStmt + privStmt + " AND required = true";
				pstmt = con.prepareStatement(queryStmt);
				pstmt.setInt(1, subjectId);
				pstmt.setFloat(2, grade);
				
				if (priv == 3)
					pstmt.setInt(3, myDeptId);
				
				if (priv == 4)
					pstmt.setInt(3, myEmpId);
				
				rs = pstmt.executeQuery();
	
				while (rs.next()) {
					employeeVO = new EmployeeAbilityViewVO();
					setVO(employeeVO, rs, con);
					//totalGrade[catId - 1] += grade;
					list.add(employeeVO);
				}
			}
			
			//search development subject (具備)
			if (required == 0 || required == 2) {
				if (condition.equals("gt"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND grade > ?";
				else if (condition.equals("ge"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND grade >= ?";
				else if (condition.equals("eq"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND grade = ?";
				else if (condition.equals("le"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND grade <= ?";
				else if (condition.equals("lt"))
					queryStmt = GET_ALL_STMT + " where subject_id = ? AND grade < ?";
				
				switch (priv) {
				case 1:
				case 2:
					privStmt = "";
					break;
				case 3:
					privStmt = " AND dept_id = ?";
					break;
				case 4:
					privStmt = " AND emp_id = ?";
					break;
				}
				queryStmt = queryStmt + privStmt + " AND required = false";
				
				pstmt = con.prepareStatement(queryStmt);
				pstmt.setInt(1, subjectId);
				pstmt.setFloat(2, grade);
				
				if (priv == 3)
					pstmt.setInt(3, myDeptId);
				
				if (priv == 4)
					pstmt.setInt(3, myEmpId);
				
				rs = pstmt.executeQuery();
	
				while (rs.next()) {
					employeeVO = new EmployeeAbilityViewVO();
					setVO(employeeVO, rs, con);
					//totalGrade[catId - 1] += grade;
					list.add(employeeVO);
				}
			}
			
			/*
			for(int i=0; i < CategoryVO.numOfCat; i++) {
				avgList.add(totalGrade[i]/4);
			}
			*/
			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public Map<Integer, Integer> findUnqualified(EmployeeViewVO myEmpVO, int level) {
		int priv = myEmpVO.getPriv(), count = 0;
		int myDeptId = myEmpVO.getDeptId(), myEmpId = myEmpVO.getEmpId();
		float companyAvg = 0;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		SubjectVO subjectVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String queryStmt = null;
		String privStmt = null;
	
		try {
			con = ds.getConnection();
			
			switch (level) {
			case -1:
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade < ?";
				break;
			case 0:
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade >= 0 AND required_grade < 1";
				break;
			case 1:
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade >= 1 AND required_grade < 2";
				break;
			case 2:
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade >= 2 AND required_grade < 3";
				break;
			case 3:
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade >= 3 AND required_grade < 4";
				break;
			case 4:
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade >= 4 AND required_grade < 5";
				break;
			default: //compare with company average
				queryStmt = COUNT_EMP_STMT + " where subject_id = ? AND required_grade < ?";
				break;
			}
			
			switch (priv) {
			case 1:
			case 2:
				privStmt = "";
				break;
			case 3:
				privStmt = " AND dept_id = ?";
				break;
			case 4:
				privStmt = " AND emp_id = ?";
				break;
			}
			//search required subject
			queryStmt = queryStmt + privStmt + " AND required = true";
			pstmt = con.prepareStatement(queryStmt);
			if (priv == 3)
				if (level == -1)
					pstmt.setInt(3, myDeptId);
				else
					pstmt.setInt(2, myDeptId);
			
			if (priv == 4)
				if (level == -1)
					pstmt.setInt(3, myEmpId);
				else
					pstmt.setInt(2, myEmpId);
			
			for (Integer subjectId : SubjectService.subjectMap.keySet() ) {
				pstmt.setInt(1, subjectId);
				
				if (level == -1) { //compare with company average
					companyAvg = SubjectService.subjectCmpAvgMap.get(subjectId);
					pstmt.setFloat(2, companyAvg);
				}
					
				rs = pstmt.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
					map.put(subjectId, count);
				}
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return map;
	}

	@Override
	public List<EmployeeAbilityViewVO> getAll() {
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeAbilityViewVO();
				setVO(employeeVO, rs, con);
				//totalGrade[catId - 1] += grade;
				list.add(employeeVO);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	
	@Override
	public List<EmployeeAbilityViewVO> getAll(Integer empId) {
		List<EmployeeAbilityViewVO> list = new ArrayList<EmployeeAbilityViewVO>();
		EmployeeAbilityViewVO employeeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			pstmt.setInt(1, empId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				employeeVO = new EmployeeAbilityViewVO();
				setVO(employeeVO, rs, con);
				list.add(employeeVO);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
		}
		return list;
	}
	
	@Override
	public List<EmployeeAbilityViewVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void setVO(EmployeeAbilityViewVO employeeVO, ResultSet rs, Connection con) throws SQLException {
		TrainingService trnSvc = new TrainingService();
		int deptId = 0, titleId = 0, subjectId = 0, catId = 0, classId = 0;
		float standard = 0, deptAvg = 0, companyAvg = 0, titleAvg = 0, grade = 0;
		
		employeeVO.setEmpId(rs.getInt("emp_id"));
		employeeVO.setEmpName(rs.getString("emp_name"));
		employeeVO.setDeptName(rs.getString("dept_name"));
		deptId = rs.getInt("dept_id");
		employeeVO.setDeptId(deptId);
		employeeVO.setTitleName(rs.getString("title_name"));
		titleId = rs.getInt("title_id");
		employeeVO.setTitleId(titleId);
		employeeVO.setSubjectName(rs.getString("subject_name"));
		subjectId = rs.getInt("subject_id");
		employeeVO.setSubjectId(subjectId);
		catId = rs.getInt("category_id");
		classId = rs.getInt("class_id");
		employeeVO.setCategoryId(catId);
		employeeVO.setClassId(classId);
		grade = rs.getFloat("grade");
		employeeVO.setGrade(grade);
		grade = rs.getFloat("required_grade");
		employeeVO.setRequiredGrade(grade);
		employeeVO.setRequired(rs.getBoolean("required"));
		
		//get standard of specified subject
		//TODO:need add sectionId
		standard = trnSvc.getStandard(con, deptId, titleId, subjectId);
		employeeVO.setStandard(standard);
		
		employeeVO.setCategoryName(rs.getString("cat_name"));
		employeeVO.setClassName(rs.getString("class_name"));
		employeeVO.setRequired(rs.getBoolean("required"));
		
		//部門應備平均
		deptAvg = getAvgByDeptId(con, deptId, subjectId);
		employeeVO.setDeptAvg(deptAvg);
		
		//部門具備平均
		deptAvg = getAvgDByDeptId(con, deptId, subjectId);
		employeeVO.setDeptAvgD(deptAvg);
		
		//公司應備平均
		companyAvg = getAvgBySubjectId(con, subjectId);
		employeeVO.setCompanyAvg(companyAvg);
		
		//同職位應備平均
		titleAvg = getAvgByTitleId(con, titleId, subjectId);
		employeeVO.setTitleAvg(titleAvg);

		//同職位具備平均
		titleAvg = getAvgDByTitleId(con, titleId, subjectId);
		employeeVO.setTitleAvgD(titleAvg);
	}
}
