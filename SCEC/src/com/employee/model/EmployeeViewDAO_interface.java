package com.employee.model;

import java.util.*;

public interface EmployeeViewDAO_interface {
	public EmployeeViewVO findByPrimaryKey(Integer empId);
	public List<EmployeeViewVO> getAll();
	public List<EmployeeViewVO> getAll(Integer deptId);
	public List<EmployeeViewVO> getAll(Map<String, String[]> map);
	public EmployeeViewVO getOneEmpByAccount(String account, int accountId);
	public List<EmployeeViewVO> getEmpsByDeptIdAndTitleId(int deptId, int titleId);
}
