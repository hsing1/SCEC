package com.employee.controller;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONArray;

import com.department.model.*;
import com.deptTitle.model.DeptTitleService;
import com.employee.model.*;
import com.promotion.model.*;
import com.subject.model.SubjectService;
import com.title.model.TitleVO;
import com.category.model.*;
import com.account.model.*;
import com.util.*;

public class EmpServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		HttpSession session = null;
		String account = null;
		int priv = 0;
		String contentType = req.getContentType();
		String encoding = req.getCharacterEncoding();
		String endodingO = res.getCharacterEncoding();
		String contentTypeO = res.getContentType();
	
		req.setCharacterEncoding("UTF-8");
		//req.setCharacterEncoding("Big5");
		res.setContentType("text/html; charset=UTF-8");
		PrintWriter out = res.getWriter();
		String action = req.getParameter("action");
		
		if ("listAbilityDiff".equals(action)) {
			String newDeptiIdStr = req.getParameter("newDeptId");
			String newTitleIdStr = req.getParameter("newTitleId");
		}
		
		if ("updateEmpInfo".equals(action)) {
			boolean pass = true, pass2 = true;
			String password = req.getParameter("password");
			String newPasswd1 = req.getParameter("newPassword1");
			String newPasswd2 = req.getParameter("newPassword2");
			String myPassword = null, url = null;
			List<String> errorPasswdMsgs = new LinkedList<String>();
			List<String> newPasswd1Err = new LinkedList<String>();
			List<String> newPasswd2Err = new LinkedList<String>();
			List<String> statusMsgs = new LinkedList<String>();
			EmployeeViewVO empVO = getMyEmpVO(req);
			AccountService accountSvc = new AccountService();
			AccountVO accountVO = null;
			
			account = empVO.getAccount();
			accountVO = accountSvc.getAccount(account);
			myPassword = accountVO.getPassword();
			
			req.setAttribute("errorPasswdMsgs", errorPasswdMsgs);
			req.setAttribute("newPasswd1Err", newPasswd1Err);
			req.setAttribute("newPasswd2Err", newPasswd2Err);
			req.setAttribute("statusMsgs", statusMsgs);
			
			try {
				if (password == null || (password.trim().length() == 0)) {
					errorPasswdMsgs.add("請輸入密碼");
					pass = false;
				} else {
					if (!password.equals(myPassword)) {
						errorPasswdMsgs.add("密碼錯誤");
						pass = false;
					}
				}
				
				if (pass) {
					if (newPasswd1 != null && (newPasswd1.trim().length() != 0)) {
						if (newPasswd2 == null || (newPasswd2.trim().length() ==0)) {
							newPasswd2Err.add("請再次輸入新密碼");
							pass2 = false;
						} else {
							if (!newPasswd1.equals(newPasswd2)) {
								newPasswd2Err.add("密碼錯誤，請再次輸入");
								pass2 = false;
							} else {
								try {
									accountSvc.updatePassword(account, newPasswd1);
								} catch (Exception e) {
									statusMsgs.add(e.getMessage());
								}
							}
						}
					} else {
						if (newPasswd2 != null && (newPasswd2.trim().length() != 0)) {
							newPasswd1Err.add("請輸入新密碼");
							pass2 = false;
						}
					}
				}
			} catch (Exception e) {
				statusMsgs.add(e.getMessage());
			}
			
			url = "/employee/empInfo.jsp";
			checkPrivAndForward(req, res, url);
		}
		
		if ("getEmpInfo".equals(action)) {
			String url = "/employee/empInfo.jsp";
			EmployeeViewVO empVO = getMyEmpVO(req);
			req.setAttribute("empVO", empVO);
			checkPrivAndForward(req, res, url);
		}
			
		
		/*
		 * 查詢 -> 異動記錄
		 * request from selectEmpShowPromotion.jsp
		 */
		if ("compareEmps".equals(action)) {
			String url = "/employee/selectEmps.jsp";
			session = req.getSession();
			session.setAttribute("compareEmps", true);
			checkPrivAndForward(req, res, url);
		}
		
		/*
		 * 查詢 -> 異動記錄
		 * request from selectEmpShowPromotion.jsp
		 */
		if ("selectEmpShowPromotion".equals(action)) {
			String url = "/employee/selectEmpShowPromotion.jsp";
			session = req.getSession();
			session.setAttribute("selectEmpShowPromotion", true);
			checkPrivAndForward(req, res, url);
		}
		
		if ("listAllEmpPromotion".equals(action)) {
			String [] deptStr = req.getParameterValues("selectDept1");
			String [] titleStr = req.getParameterValues("selectTitle1");
			String [] empStr = req.getParameterValues("selectEmp1");
			String startDateStr = req.getParameter("startDate");
			Integer [] empArr = new Integer[empStr.length];
			Timestamp startDate = Timestamp.valueOf(startDateStr + " 00:00:00");
			PromotionViewService proSvc = null;
			List<PromotionViewVO> list = null;
			EmployeeViewVO myEmpVO = null;
			
			try {
				for (int i = 0; i < empStr.length; i++)
					empArr[i] = Integer.valueOf(empStr[i]);
				
				proSvc = new PromotionViewService();
				list = proSvc.getAll(empArr, startDate);
				//myEmpVO = getMyEmpVO(req);
				//req.setAttribute("myEmpVO", myEmpVO);
				req.setAttribute("listAllEmpPromotion", list);
				String url = "/employee/selectEmpShowPromotion.jsp";
				//TODO: need polish for multiple selection
				checkPrivAndForward(req, res, url, deptStr[0], titleStr[0], empStr[0], startDate);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		/*
		 * 報表 -> 個人報表
		 * request from selectEmpAndGrade.jsp 
		 */
		if ("selectEmpAndGrade".equals(action)) {
			String url = "/employee/selectEmpAndGrade.jsp";
			checkPrivAndForward(req, res, url);
		}
		
		if ("listOneEmpReview".equals(action)) {
			String empIdStr = req.getParameter("selectEmp1");
			Integer empId = Integer.valueOf(empIdStr);
			EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
			List<List<EmployeeAbilityViewVO>> listArr = null;
			
			try {
				listArr = empSvc.getReview(empId);
				req.setAttribute("listOneEmpReview", listArr);
				String url = "/employee/selectOneEmpReview.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listUnqualifiedEmp".equals(action)) {
			try {
				float cmpAvg = 0;
				int required = 1; //0:development 1:required 2:both
				String subjectStr = req.getParameter("subjectId");
				Integer subjectId = Integer.valueOf(subjectStr);
				EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
				List<EmployeeAbilityViewVO> list = null;
				EmployeeViewVO myEmpVO = null;
				
				try {
					myEmpVO = SCECcommon.getMyEmp(req, res);
					cmpAvg = SubjectService.subjectCmpAvgMap.get(subjectId);
					list = empSvc.getEmpBySubjectIdGrade(myEmpVO, subjectId, cmpAvg, "lt", required);
					req.setAttribute("hasBootstrap", true);
					req.setAttribute("listEmpBySubjectAndGrade", list);
					req.setAttribute("cmpAvgMap", SubjectService.subjectCmpAvgMap);
					String url = "/employee/listEmpBySubjectAndGrade.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url);
					successView.forward(req, res);
				} catch (Exception e) {
					throw new ServletException(e);
				}
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listEmpBySubjectAndGrade".equals(action)) {
			try {
				String subjectStr = req.getParameter("selectSubject");
				Integer subjectId = Integer.valueOf(subjectStr);
				String condition = req.getParameter("selectCond");
				String gradeStr = req.getParameter("grade");
				Float grade = Float.valueOf(gradeStr);
				EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
				List<EmployeeAbilityViewVO> list = null;
				EmployeeViewVO myEmpVO = null;
				
				try {
					myEmpVO = SCECcommon.getMyEmp(req, res);
					list = empSvc.getEmpBySubjectIdGrade(myEmpVO, subjectId, grade, condition, 2);
					req.setAttribute("listEmpBySubjectAndGrade", list);
					String url = "/employee/selectSubjectAndGrade.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url);
					successView.forward(req, res);
				} catch (Exception e) {
					throw new ServletException(e);
				}
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listSubjectByEmpAndGrade".equals(action)) {
			String deptIdStr = req.getParameter("selectDept1");
			String titleIdStr = req.getParameter("selectTitle1");
			String empIdStr = req.getParameter("selectEmp1");
			String gradeStr = req.getParameter("grade");
			String condStr = req.getParameter("selectCond");
			Integer empId = Integer.valueOf(empIdStr);
			Integer titleId = Integer.valueOf(titleIdStr);
			Integer deptId = Integer.valueOf(deptIdStr), myDeptId = null;
			int myEmpId = 0;
			Float grade = Float.valueOf(gradeStr);
			List<EmployeeAbilityViewVO> list = null;
			List<EmployeeVO> empList = null;
			List<EmployeeViewVO> empViewList = null;
			List<TitleVO> titleList = null;
			EmployeeAbilityViewService abSvc = new EmployeeAbilityViewService();
			EmployeeService empSvc = new EmployeeService();
			EmployeeViewService empVSvc = new EmployeeViewService();
			DeptTitleService deptSvc = new DeptTitleService();
			EmployeeViewVO empVO = null;
			
			try {
				session = req.getSession();
				account = (String)session.getAttribute("account"); 
				
				if (account == null) {
					session.setAttribute("prevPage", req.getRequestURI());
					res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
				}
				
				empVSvc = new EmployeeViewService();
				empVO = empVSvc.getOneEmpByAccount(account, 0);
				myEmpId = empVO.getEmpId();
				myDeptId = empVO.getDeptId();
				
				priv = empVO.getPriv();
				switch (priv) {
				case 1:
					list = abSvc.getSubjectByEmpAndGrade(empId, grade, condStr);
					empViewList = empVSvc.getEmpsByDeptIdAndTitleId(deptId, titleId);
					break;
				case 2:
					list = abSvc.getSubjectByEmpAndGrade(empId, grade, condStr);
					empViewList = empVSvc.getEmpsByDeptIdAndTitleId(deptId, titleId);
					break;
				case 3:
					if (deptId == myDeptId) {
						list = abSvc.getSubjectByEmpAndGrade(empId, grade, condStr);
						empViewList = empVSvc.getEmpsByDeptIdAndTitleId(myDeptId, titleId);
					}
					break;
				case 4:
					if (empId == myEmpId) {
						list = abSvc.getSubjectByEmpAndGrade(empId, grade, condStr);
						empViewList = empVSvc.getEmpsByDeptIdAndTitleId(myDeptId, titleId);
					}
					break;
				}
				titleList = deptSvc.getAll(deptId);
				
				req.setAttribute("listSubjectByEmpAndGrade", list);
				req.setAttribute("titleList", titleList);
				//req.setAttribute("empList", empList);
				req.setAttribute("empViewList", empViewList);
				req.setAttribute("currentDept", deptIdStr);
				req.setAttribute("currentTitle", titleIdStr);
				req.setAttribute("currentEmp", empIdStr);
				req.setAttribute("currentCond", condStr);
				req.setAttribute("currentGrade", gradeStr);
				req.setAttribute("myEmpVO", empVO);
	
				String url = "/employee/selectEmpAndGrade.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		/*
		 * request from /employee/selectEmps.jsp
		 * forward to selectEmps.js : showChartByEmpId(), showChart()
		 */
		if ("getOneEmpJSON".equals(action)) {
			String empIdStr = req.getParameter("empId1");
			Integer empId1 = null, empId2 = null;
			EmployeeViewService empSvc = new EmployeeViewService();
			CategoryService catSvc = new CategoryService();
			JSONArray empList = new JSONArray();
			EmployeeViewVO empVO = null;
			Map<Integer, String> categoryMap = null;
			float [] companyAvg = CategoryService.companyAvg;
			
			categoryMap = catSvc.getAllName();
			empList.put(categoryMap); //[0]
			
			empId1 = Integer.valueOf(empIdStr); 
			empIdStr = req.getParameter("empId2");
			empId2 = Integer.valueOf(empIdStr);
			
			empVO = empSvc.getOneEmployee(empId1); 
			empList.put(empVO.getEmpId()); //[1]
			empList.put(empVO.getEmpName()); //[2]
			empList.put(empVO.getAverage()); //[3]
			
			empVO = empSvc.getOneEmployee(empId2); 
			empList.put(empVO.getEmpId()); //[3]
			empList.put(empVO.getEmpName()); //[5]
			empList.put(empVO.getAverage()); //[6]
			
			//category average of company
			empList.put(companyAvg); //[7]
			
			System.out.println(empList);
			out.println(empList);
		}
		
		//request from listAllEmp.jsp to all employee's abilities
		if ("listOneEmpAbility".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String requestPath = req.getRequestURI();
			String empIdStr = req.getParameter("empId");
			String catIdStr = req.getParameter("catId");
			Integer empId = Integer.valueOf(empIdStr);
			Integer catId = Integer.valueOf(catIdStr);

			try {
				EmployeeAbilityViewService empAbSvc = new EmployeeAbilityViewService();
				List<EmployeeAbilityViewVO> list = empAbSvc.getOneEmployee(empId, catId);
				EmployeeService empSvc = new EmployeeService();
				EmployeeVO empVO = null;

				empVO = empSvc.getOneEmp(empId);
				req.setAttribute("empName", empVO.getName());
				req.setAttribute("catName", catId);
				req.setAttribute("listOneEmpAbility", list);
				req.setAttribute("requestPath", requestPath);
				req.setAttribute("hasBootstrap", true);

				String url = "/employee/listOneEmpAbility.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		//request from listAllEmp.jsp 
		if ("listOneEmpTraining".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String empIdStr = req.getParameter("empId");
			Integer empId = Integer.valueOf(empIdStr);

			try {
				EmployeeTrainingViewService empSvc = new EmployeeTrainingViewService();
				List<EmployeeTrainingViewVO> list = empSvc.getOneEmployee(empId);

				req.setAttribute("listOneEmpTraining", list);
				req.setAttribute("hasBootstrap", true);

				String url = "/employee/listOneEmpTrainingRecord.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}

		//request from listAllEmp.jsp 
		if ("listAllEmp".equals(action)) {
			AccountVO accountVO = null;
			List<String> errorMsgs = new LinkedList<String>();
			int deptId = 0, empId = 0;
			EmployeeViewService empSvc = null;
			List<EmployeeViewVO> list = null;
			EmployeeViewVO empVO = null;
			String url = null;

			req.setAttribute("errorMsgs", errorMsgs);
			try {
				session = req.getSession();
				account = (String)session.getAttribute("account"); 
				
				if (account == null) {
					session.setAttribute("prevPage", req.getRequestURI());
					res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
				}

				empSvc = new EmployeeViewService();
				empVO = empSvc.getOneEmpByAccount(account, 0);
				empId = empVO.getEmpId();
				deptId = empVO.getDeptId();
				
				priv = empVO.getPriv();
				switch (priv) {
				case 1:
					list = empSvc.getAll();
					break;
				case 2:
					list = empSvc.getAll();
					break;
				case 3:
					list = empSvc.getAll(deptId);
					break;
				case 4:
					list = new ArrayList<EmployeeViewVO>();
					list.add(empVO);
					break;
				}
				req.setAttribute("listAllEmp", list); 

				url = "/employee/listAllEmp.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listAllEmpByDeptId".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				Integer deptId = new Integer(req.getParameter("deptId"));

				DepartmentService deptSvc = new DepartmentService();
				Set<EmployeeVO> set = deptSvc.getEmpsByDeptId(deptId);

				req.setAttribute("listEmpsByDeptId", set);

				String url = null;
				if ("listEmps_ByDeptno_A".equals(action))
					url = "/dept/listEmps_ByDeptno.jsp";
				else if ("listEmpsByDeptId_B".equals(action))
					url = "/dept/listAllDept.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listEmpByDeptAndTitle".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				Integer deptId = new Integer(req.getParameter("deptId"));
				Integer titleId = new Integer(req.getParameter("titleId"));

				EmployeeService empSvc = new EmployeeService();
				List<EmployeeVO> list = empSvc.getEmpsByDeptIdAndTitleId(deptId, titleId);
				JSONArray empList = new JSONArray();
				
				for (EmployeeVO empVO : list) {
					empList.put(empVO.getEmpId());
					empList.put(empVO.getName());
				}
				System.out.println(empList);
				out.println(empList);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("listEmpByDeptAndTitleMulti".equals(action)) {
			String [] selectDept = req.getParameterValues("deptArr[]");
			String [] selectTitle = req.getParameterValues("titleArr[]");
			int i = 0, deptSize = selectDept.length, titleSize = selectTitle.length;
			Integer [] deptArr = new Integer[deptSize], titleArr = new Integer[titleSize];
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				for (i = 0; i < deptSize; i++) 
					deptArr[i] = Integer.valueOf(selectDept[i]);
				
				for (i = 0; i < titleSize; i++) 
					titleArr[i] = Integer.valueOf(selectTitle[i]);
				
				EmployeeService empSvc = new EmployeeService();
				List<EmployeeVO> list = empSvc.getEmpsByDeptIdAndTitleId(deptArr, titleArr);
				JSONArray empList = new JSONArray();
				
				for (EmployeeVO empVO : list) {
					empList.put(empVO.getEmpId());
					empList.put(empVO.getName());
				}
				System.out.println(empList);
				out.println(empList);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("delete_Dept".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.�����ШD�Ѽ�***************************************/
				Integer deptno = new Integer(req.getParameter("deptno"));
				
				/***************************2.�}�l�R�����***************************************/
				DepartmentService deptSvc = new DepartmentService();
				//deptSvc.deleteDept(deptno);
				
				/***************************3.�R������,�ǳ����(Send the Success view)***********/
				String url = "/dept/listAllDept.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// �R�����\��, ���\��� �^�� /dept/listAllDept.jsp
				successView.forward(req, res);
				
				/***************************��L�i�઺��~�B�z***********************************/
			} catch (Exception e) {
				errorMsgs.add("�R����ƥ���:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/dept/listAllDept.jsp");
				failureView.forward(req, res);
			}
		}

	}
	
	private EmployeeViewVO getMyEmpVO(HttpServletRequest req) {
		String account = null;
		HttpSession session = null;
		EmployeeViewService empSvc = null;
		EmployeeViewVO empVO = null;
		
		session = req.getSession();
		account = (String)session.getAttribute("account");
		empSvc = new EmployeeViewService();
		empVO = empSvc.getOneEmpByAccount(account, 0);
		
	    return empVO;	
	}
	
	private void checkPrivAndForward(HttpServletRequest req, HttpServletResponse res, String url) 
		throws ServletException {
		
		int myEmpId = 0, myDeptId = 0, myTitleId = 0, priv = 0;
		String account = null;
		EmployeeViewService empSvc = null;
		DeptTitleService deptSvc = null;
		EmployeeViewVO empVO = null;
		List<EmployeeViewVO> empViewList = null;
		List<TitleVO> titleList = null;
		List<String> errorMsgs = new LinkedList<String>();
		HttpSession session = null;
		req.setAttribute("errorMsgs", errorMsgs);
		
		try {
			session = req.getSession();
			account = (String)session.getAttribute("account"); 
			
			if (account == null) {
				session.setAttribute("prevPage", req.getRequestURI());
				res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
			}
			
			empSvc = new EmployeeViewService();
			deptSvc = new DeptTitleService();
			empVO = empSvc.getOneEmpByAccount(account, 0);
			myEmpId = empVO.getEmpId();
			myDeptId = empVO.getDeptId();
			myTitleId = empVO.getTitleId();
			
			priv = empVO.getPriv();
			switch (priv) {
			case 1:
			case 2:
			case 3:
				titleList = deptSvc.getAll(myDeptId);
			    empViewList = empSvc.getEmpsByDeptIdAndTitleId(myDeptId, myTitleId); 
				break;
			case 4:
				break;
			}
			
			req.setAttribute("titleList", titleList);
			req.setAttribute("currentDept", myDeptId);
			req.setAttribute("currentTitle", myTitleId);
			req.setAttribute("currentEmp", myEmpId);
			req.setAttribute("myEmpVO", empVO);
			req.setAttribute("empViewList", empViewList);

			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
	private void checkPrivAndForward(HttpServletRequest req, HttpServletResponse res, String url, String selectedDept,
									String selectedTitle, String selectedEmp, Timestamp startDate) 
		throws ServletException {
		
		int myEmpId = 0, myDeptId = 0, myTitleId = 0, priv = 0, idx = 0;
		Integer selectedDeptId = Integer.valueOf(selectedDept);
		Integer selectedTitleId = Integer.valueOf(selectedTitle);
		Integer selectedEmpId = Integer.valueOf(selectedEmp);
		String account = null, startDateStr = null;
		Boolean isVisted = false;
		EmployeeViewService empSvc = null;
		DeptTitleService deptSvc = null;
		EmployeeViewVO empVO = null;
		List<EmployeeViewVO> empViewList = null;
		List<TitleVO> titleList = null;
		List<String> errorMsgs = new LinkedList<String>();
		HttpSession session = null;
		req.setAttribute("errorMsgs", errorMsgs);
		
		try {
			if (startDate != null) {
				startDateStr = startDate.toString();
				idx = startDateStr.indexOf(" ");
				startDateStr = startDateStr.substring(0, idx);
			}
			
			session = req.getSession();
			account = (String)session.getAttribute("account"); 
			isVisted = (Boolean)session.getAttribute("selectEmpShowPromotion");
			
			if (account == null) {
				session.setAttribute("prevPage", req.getRequestURI());
				res.sendRedirect(req.getContextPath()+ "/login/login.jsp");
			}
			
			empSvc = new EmployeeViewService();
			deptSvc = new DeptTitleService();
			empVO = empSvc.getOneEmpByAccount(account, 0);
			myDeptId = empVO.getDeptId();
			myTitleId = empVO.getTitleId();
			priv = empVO.getPriv();
			
			switch (priv) {
			case 1:
			case 2:
			case 3:
				if (isVisted) {
					titleList = deptSvc.getAll(selectedDeptId);
					empViewList = empSvc.getEmpsByDeptIdAndTitleId(selectedDeptId, selectedTitleId); 
				} else {
					titleList = deptSvc.getAll(myDeptId);
					empViewList = empSvc.getEmpsByDeptIdAndTitleId(myDeptId, myTitleId); 
				}
				break;
			case 4:
				break;
			}
			
			
			req.setAttribute("titleList", titleList);
			req.setAttribute("currentDept", selectedDept);
			req.setAttribute("currentTitle", selectedTitle);
			req.setAttribute("currentEmp", selectedEmp);
			req.setAttribute("myEmpVO", empVO);
			req.setAttribute("empViewList", empViewList);
			req.setAttribute("startDate", startDateStr);

			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
