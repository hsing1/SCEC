package com.department.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.department.model.*;
import com.deptTitle.model.*;
import com.title.model.*;
import com.employee.model.*;
import org.json.JSONArray;
import com.section.model.*;

public class DeptServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=Big5");
		PrintWriter out = res.getWriter();
		String action = req.getParameter("action");

		if ("listEmps_ByDeptno_A".equals(action) || "listEmpsByDeptId_B".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				Integer deptId = new Integer(req.getParameter("deptId"));

				EmployeeViewService empSvc = new EmployeeViewService();
				List<EmployeeViewVO> list = empSvc.getAll(deptId);

				req.setAttribute("listEmpsByDeptId", list);

				String url = null;
				if ("listEmps_ByDeptno_A".equals(action))
					url = "/dept/listEmps_ByDeptno.jsp";
				else if ("listEmpsByDeptId_B".equals(action))
					url = "/dept/listAllDept.jsp";

				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("getSection".equals(action)) {
			int sectionId = 0;
			String [] selectedArr = req.getParameterValues("selectedArr[]");
			int length = selectedArr.length;
			DeptTitleService titleSvc = null;
			List<String> errorMsgs = new LinkedList<String>();
			List<SectionVO> list = null;
			JSONArray sectionList = new JSONArray();
			Integer [] deptIdArr = new Integer[length]; 
			req.setAttribute("sectionErrMsg", errorMsgs);

			try {
				for (int i = 0; i < length; i++)
					deptIdArr[i] = Integer.valueOf(selectedArr[i]);
				
				titleSvc = new DeptTitleService();
				//List<SectionVO> list = titleSvc.getAll(deptId);
				list = titleSvc.getAllSection(deptIdArr[0]);
				
				for (SectionVO sectionVO : list) {
					sectionList.put(sectionVO.getSectionId());
					sectionList.put(sectionVO.getName());
				}
				System.out.println(sectionList);
				out.println(sectionList);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("getTitleByDeptAndSec".equals(action)) {
			String [] deptArr = req.getParameterValues("deptArr[]");
			String [] sectionArr = req.getParameterValues("sectionArr[]");
			int deptLength = 0, sectionLength = 0;
			List<String> errorMsgs = new LinkedList<String>();
			Integer [] deptIdArr = null, sectionIdArr = null;
			DeptTitleService titleSvc = null;
			List<TitleVO> list = null;
			req.setAttribute("errorMsgs", errorMsgs);

			if (deptArr == null)
				throw new RuntimeException("No department specified");
			deptLength = deptArr.length;
			
			if (sectionArr != null)
				sectionLength = sectionArr.length;
			
			deptIdArr = new Integer[deptLength];
			sectionIdArr = new Integer[sectionLength];
			
			try {
				for (int i = 0; i < deptLength; i++)
					deptIdArr[i] = Integer.valueOf(deptArr[i]);
				
				for (int i = 0; i < sectionLength; i++)
					sectionIdArr[i] = Integer.valueOf(sectionArr[i]);
				
				titleSvc = new DeptTitleService();
				list = titleSvc.getAll(deptIdArr, sectionIdArr);

				JSONArray titleList = new JSONArray();
				
				for (TitleVO titleVO : list) {
					titleList.put(titleVO.getTitleId());
					titleList.put(titleVO.getName());
				}
				System.out.println(titleList);
				out.println(titleList);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("getTitle".equals(action)) {

			String [] selectedArr = req.getParameterValues("selectedArr[]");
			int length = selectedArr.length;
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				//Integer deptId = new Integer(req.getParameter("deptId"));
				Integer [] deptIdArr = new Integer[length]; 
				
				for (int i = 0; i < length; i++)
					deptIdArr[i] = Integer.valueOf(selectedArr[i]);

				DeptTitleService titleSvc = new DeptTitleService();
				//List<TitleVO> list = titleSvc.getAll(deptId);
				List<TitleVO> list = titleSvc.getAll(deptIdArr);

				JSONArray titleList = new JSONArray();
				
				for (TitleVO titleVO : list) {
					titleList.put(titleVO.getTitleId());
					titleList.put(titleVO.getName());
				}
				System.out.println(titleList);
				out.println(titleList);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		if ("delete_Dept".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				Integer deptno = new Integer(req.getParameter("deptno"));
				
				DepartmentService deptSvc = new DepartmentService();
				//deptSvc.deleteDept(deptno);
				
				String url = "/dept/listAllDept.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
			} catch (Exception e) {
				errorMsgs.add("�R����ƥ���:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/dept/listAllDept.jsp");
				failureView.forward(req, res);
			}
		}

	}
}
