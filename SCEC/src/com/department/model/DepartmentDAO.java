package com.department.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.employee.model.*;
import com.util.SCECcommon;

public class DepartmentDAO implements DepartmentDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
		"INSERT INTO DEPARTMENT (dept_id,name ) VALUES ( DEPARTMENT_seq.NEXTVAL, ?)";
	private static final String INSERT_STMT_MYSQL = 
		"INSERT INTO DEPARTMENT (name) VALUES (?)";
	private static final String GET_ALL_STMT = 
		"SELECT dept_id,name FROM DEPARTMENT order by dept_id";
	private static final String GET_ONE_STMT = 
		"SELECT dept_id,name  FROM DEPARTMENT where dept_id = ?";
	private static final String DELETE = 
		"DELETE FROM DEPARTMENT where dept_id = ?";
	private static final String UPDATE = 
		"UPDATE DEPARTMENT set name = ? where dept_id=?";
	private static final String FIND_BY_DEPT = 
		"SELECT dept_id, name FROM DEPARTMENT where name = ?";
	private static final String GET_EMPS_BY_DEPT_ID =
		"SELECT emp_id, dept_id, name, title_id, onboard_date FROM EMPLOYEE where dept_id = ?"; 
	private static final String GET_NUM_OF_EMP_OF_DEPT = 
			"SELECT count(emp_id) num_of_emp FROM EMPLOYEE where dept_id = ?";
	private static final String GET_NUM_OF_DEPT = 
			"SELECT count(dept_id) num_of_dept FROM DEPARTMENT";
	private static final String GET_NUM_OF_COMPANY = 
			"SELECT count(emp_id) num_of_emp FROM EMPLOYEE";

	@Override
	public int findNumOfEmpOfCompany() {
		Connection con = null;
		PreparedStatement pstmt = null;
		HashMap<Integer, Integer> numOfEmpDept = DepartmentService.numOfEmpDept;
		ResultSet rs = null;
		int numOfEmp = 0;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_NUM_OF_COMPANY);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				numOfEmp = rs.getInt("num_of_emp");
				DepartmentService.numOfEmpCompany = numOfEmp;
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		return numOfEmp;
	}
	
	@Override
	public int findNumOfDept() {
		Connection con = null;
		PreparedStatement pstmt = null;
		HashMap<Integer, Integer> numOfEmpDept = DepartmentService.numOfEmpDept;
		ResultSet rs = null;
		int numOfDept = 0;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_NUM_OF_DEPT);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				numOfDept = rs.getInt("num_of_dept");
				DepartmentService.numOfDept = numOfDept;
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		return numOfDept;
	}
	
	@Override
	public void findNumOfEmpOfDept() {
		Connection con = null;
		PreparedStatement pstmt = null;
		int numOfDept = DepartmentService.numOfDept;
		HashMap<Integer, Integer> numOfEmpDept = DepartmentService.numOfEmpDept;
		ResultSet rs = null;
		int numOfEmp = 0, count = 0;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_NUM_OF_EMP_OF_DEPT);
			for (int deptId = 1; deptId <= numOfDept; deptId++) {
				pstmt.setInt(1, deptId);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					numOfEmp = rs.getInt("num_of_emp");
					numOfEmpDept.put(deptId, numOfEmp);
				}
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	@Override
	public void insert(DepartmentVO departmentVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			//pstmt = con.prepareStatement(INSERT_STMT);
			pstmt = con.prepareStatement(INSERT_STMT_MYSQL);

			pstmt.setString(1, departmentVO.getName());

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(DepartmentVO departmentVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			
			pstmt.setString(1, departmentVO.getName());
			pstmt.setInt(2, departmentVO.getDeptId());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer deptId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, deptId);

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public DepartmentVO findByDeptName(String dept) {
		DepartmentVO departmentVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean recordNotFound = true;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_DEPT);

			pstmt.setString(1, dept);
	
			rs = pstmt.executeQuery();
			while (rs.next()) {
				recordNotFound = false;
				departmentVO = new DepartmentVO();
				departmentVO.setDeptId(rs.getInt("dept_id"));
				departmentVO.setName(rs.getString("name"));
			}
			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			SCECcommon.close(con, rs, pstmt, true);
			if (recordNotFound)
				throw new RuntimeException("Cannot find " + dept + " in database.");
		}
		return departmentVO;
	}

	@Override
	public DepartmentVO findByPrimaryKey(Integer deptId) {
		DepartmentVO departmentVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptId);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				departmentVO = new DepartmentVO();
				departmentVO.setDeptId(rs.getInt("dept_id"));
				departmentVO.setName(rs.getString("name"));
			
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return departmentVO;
	}

	@Override
	public List<DepartmentVO> getAll() {
		List<DepartmentVO> list = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				departmentVO = new DepartmentVO();
				departmentVO.setDeptId(rs.getInt("dept_id"));
				departmentVO.setName(rs.getString("name"));
				list.add(departmentVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public Set<EmployeeVO> getEmpsByDeptId(Integer deptId) {
		Set<EmployeeVO> set = new LinkedHashSet<EmployeeVO>();
		EmployeeVO empVO = null;
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		try {
	
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_EMPS_BY_DEPT_ID);
			pstmt.setInt(1, deptId);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				empVO = new EmployeeVO();
				empVO.setEmpId(rs.getInt("emp_id"));
				empVO.setName(rs.getString("name"));
				empVO.setOnboard(rs.getDate("onboard_date"));
				empVO.setDeptId(rs.getInt("dept_id"));
				set.add(empVO); // Store the row in the vector
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	@Override
	public List<DepartmentVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		DepartmentDAO dao = new DepartmentDAO();

		// �s�W
//		DepartmentVO departmentVO = new DepartmentVO();
//		departmentVO.setDeptId(1);
//		departmentVO.setName("�޲z��");
//		dao.insert(departmentVO);
//		System.out.println("insert sucessful");
	
		// �ק�
//		DepartmentVO departmentVO1= new DepartmentVO();
//		departmentVO1.setDeptId(1);
//		departmentVO1.setName("��o��");
//		dao.update(departmentVO1);
//		System.out.println(departmentVO1.getDeptId());
//		System.out.println(departmentVO1.getName());
//		System.out.println("update sucessful");
	
		// �R��
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// �d��
//		DepartmentVO departmentVO4 = dao.findByPrimaryKey(1);
//		System.out.print(departmentVO4.getDeptId() + ",");
//		System.out.print(departmentVO4.getName() + ",");
//		System.out.println("select sucessful");

		// �d��
//		List<DepartmentVO> list = dao.getAll();
//		for (DepartmentVO departmentVO5 : list) {
//			System.out.print(departmentVO5.getDeptId() + ",");
//			System.out.print(departmentVO5.getName() + ",");
//			System.out.println();
//		}
	}


}