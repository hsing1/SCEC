package com.department.model;

public class DepartmentVO implements java.io.Serializable{
	private Integer deptId ;
	private String  name;
	
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
