package com.department.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.employee.model.*;
import com.util.*;
import org.apache.log4j.*;

public class DepartmentService {

	private DepartmentDAO dao;
	private static DepartmentDAO daoS;
	public static int numOfEmpCompany; //count at EmployeeDAO.insert()
	public static HashMap<Integer, Integer> numOfEmpDept; // count at EmployeeDAO.insert()
	public static int numOfDept;
	public static LinkedHashMap<Integer, String> deptMap;
	public static LinkedHashMap<String, Integer> deptMapInv;
	private static Logger logger = Logger.getLogger(SCECworkbookUtil.class);
	
	static {
		if (daoS == null)
			daoS = new DepartmentDAO();
		
		numOfEmpDept = new HashMap<Integer, Integer>();
		if (SCECstatus.status == SCECstatusEnum.SCEC_DB_MODE) {
			//getNumOfDept();
			setDeptMap();
			getNumOfEmpOfDept();
			getNumOfEmpOfCompany();
		}
	}

	public DepartmentService() {
		dao = new DepartmentDAO();
	}

	public DepartmentVO addDepartment(String name) {

		logger.debug("BEGIN:addDepartment");
		logger.debug("Dept=" + name);
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setName(name);
		dao.insert(departmentVO);

		logger.debug("END:addDepartment");
		return departmentVO;
	}

	public DepartmentVO updateSeries(Integer deptId,String  name) {

		DepartmentVO departmentVO = new DepartmentVO();

		departmentVO.setDeptId(deptId);
		departmentVO.setName(name);
	
		dao.update(departmentVO);

		return departmentVO;
	}

	public void deleteLevels(Integer deptId) {
		dao.delete(deptId);
	}

	public DepartmentVO getOneDept(Integer deptId) {
		return dao.findByPrimaryKey(deptId);
	}
	
	public DepartmentVO getOneDept(String dept) {
		return dao.findByDeptName(dept);
	}

	public List<DepartmentVO> getAll() {
		return dao.getAll();
	}
	
	public List<DepartmentVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public Set<EmployeeVO> getEmpsByDeptId(Integer deptId) {
		return dao.getEmpsByDeptId(deptId);
	}
	
	public static void getNumOfEmpOfDept() {
		daoS.findNumOfEmpOfDept();
	}
	
	public static int getNumOfDept() {
		return daoS.findNumOfDept();
	}
	
	public static int getNumOfEmpOfCompany() {
		return daoS.findNumOfEmpOfCompany();
	}
	
	public static void setDeptMap() {
		int deptId = 0;
		String name = null;
		List<DepartmentVO> list = daoS.getAll();
		deptMap = new LinkedHashMap<Integer, String>();
		deptMapInv = new LinkedHashMap<String, Integer>();
		
		numOfDept = list.size();
		for (DepartmentVO deptVO : list) {
			deptId = deptVO.getDeptId();
			name = deptVO.getName();
			deptMap.put(deptId, name);
			deptMapInv.put(name, deptId);
		}
	}
}
