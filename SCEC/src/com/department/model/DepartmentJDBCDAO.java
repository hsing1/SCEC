package com.department.model;

import java.util.*;
import java.sql.*;

public class DepartmentJDBCDAO {
	//String driver = "oracle.jdbc.driver.OracleDriver";
	String driver = "com.mysql.jdbc.Driver";
	//String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String url = "jdbc:mysql://localhost:3306/SCEC";
	String userid = "scec";
	String passwd = "";

	private static final String INSERT_STMT = 
		"INSERT INTO DEPARTMENT (name ) VALUES (?)";
	private static final String GET_ALL_STMT = 
		"SELECT dept_id,name FROM DEPARTMENT order by dept_id";
	private static final String GET_ONE_STMT = 
		"SELECT dept_id,name  FROM DEPARTMENT where dept_id = ?";
	private static final String DELETE = 
		"DELETE FROM DEPARTMENT where dept_id = ?";
	private static final String UPDATE = 
		"UPDATE DEPARTMENT set name = ? where dept_id=?";

	public void insert(DepartmentVO departmentVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, departmentVO.getName());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	public void update(DepartmentVO departmentVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			
			pstmt.setString(1, departmentVO.getName());
			pstmt.setInt(2, departmentVO.getDeptId());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	public void delete(Integer deptId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, deptId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	public DepartmentVO findByPrimaryKey(Integer deptId) {

		DepartmentVO departmentVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, deptId);
	
			rs = pstmt.executeQuery();


			while (rs.next()) {
				
				departmentVO = new DepartmentVO();
				departmentVO.setDeptId(rs.getInt("dept_id"));
				departmentVO.setName(rs.getString("name"));
			
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return departmentVO;
	}

	public List<DepartmentVO> getAll() {
		List<DepartmentVO> list = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				departmentVO = new DepartmentVO();
				departmentVO.setDeptId(rs.getInt("dept_id"));
				departmentVO.setName(rs.getString("name"));
				list.add(departmentVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	public List<DepartmentVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		DepartmentJDBCDAO dao = new DepartmentJDBCDAO();

		// 嚙編嚙磕
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setName("創新部");
		//departmentVO.setName("�菜���);
		dao.insert(departmentVO);
		System.out.println("insert sucessful");
	
		// 嚙論改蕭
//		DepartmentVO departmentVO1= new DepartmentVO();
//		departmentVO1.setDeptId(1);
//		departmentVO1.setName("嚙踝蕭o嚙踝蕭");
//		dao.update(departmentVO1);
//		System.out.println(departmentVO1.getDeptId());
//		System.out.println(departmentVO1.getName());
//		System.out.println("update sucessful");
	
		// 嚙磋嚙踝蕭
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// 嚙範嚙踝蕭
//		DepartmentVO departmentVO4 = dao.findByPrimaryKey(1);
//		System.out.print(departmentVO4.getDeptId() + ",");
//		System.out.print(departmentVO4.getName() + ",");
//		System.out.println("select sucessful");

		// 嚙範嚙踝蕭
//		List<DepartmentVO> list = dao.getAll();
//		for (DepartmentVO departmentVO5 : list) {
//			System.out.print(departmentVO5.getDeptId() + ",");
//			System.out.print(departmentVO5.getName() + ",");
//			System.out.println();
//		}
	}


}