package com.department.model;

import java.util.*;
import com.employee.model.*;

public interface DepartmentDAO_interface {
	public void insert(DepartmentVO departmentVO);
	public void update(DepartmentVO departmentVO);
	public void delete(Integer deptId);
	public DepartmentVO findByPrimaryKey(Integer deptId);
	public DepartmentVO findByDeptName(String dept);
	public void findNumOfEmpOfDept();
	public int findNumOfEmpOfCompany();
	public int findNumOfDept();
	public List<DepartmentVO> getAll();
	public List<DepartmentVO> getAll(Map<String, String[]> map); 
	public Set<EmployeeVO> getEmpsByDeptId(Integer deptId);
}
