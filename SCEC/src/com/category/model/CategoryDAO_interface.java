package com.category.model;

import java.util.*;

public interface CategoryDAO_interface {
	public void insert(CategoryVO categoryVO);
	public void update(CategoryVO categoryVO);
	public void delete(Integer categoryId);
	public CategoryVO findByPrimaryKey(Integer categoryId);
	public CategoryVO findByCategoryName(String category);
	public List<CategoryVO> getAll();
	public Map<Integer, String> getAllName();
	public List<CategoryVO> getAll(Map<String, String[]> map); 
}
