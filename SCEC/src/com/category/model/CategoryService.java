package com.category.model;

import java.util.List;
import java.util.Map;

import com.util.SCECstatus;
import com.util.SCECstatusEnum;
import com.employee.model.*;

public class CategoryService {

	private CategoryDAO_interface dao;
	public static int numOfCat; //number of category
	public static Map<Integer, String> catList; //TODO:need implement this, current use dynamic query
	public static float [] companyAvg;
	
	static {
		if ((SCECstatus.status == SCECstatusEnum.SCEC_DB_MODE) && (numOfCat == 0)) {
			setNumOfCategory();
			setCompanyAvg();
		}
	}

	public CategoryService() {
		dao = new CategoryDAO();
	}

	public CategoryVO addCategory(String  name) {

		CategoryVO categoryVO = new CategoryVO();
		
		categoryVO.setName(name);
	
		dao.insert(categoryVO);

		return categoryVO;
	}

	public CategoryVO updateSeries(Integer categoryId,String  name) {

		CategoryVO categoryVO = new CategoryVO();

		categoryVO.setCategoryId(categoryId);
		categoryVO.setName(name);
	
		dao.update(	categoryVO);

		return categoryVO;
	}

	public void deleteLevel(Integer categoryId) {
		dao.delete(categoryId);
	}

	public CategoryVO getOneCategory(Integer categoryId) {
		return dao.findByPrimaryKey(categoryId);
	}
	
	public CategoryVO getOneCategory(String category) {
		return dao.findByCategoryName(category);
	}

	public List<CategoryVO> getAll() {
		return dao.getAll();
	}
	
	public Map<Integer, String> getAllName() {
		return dao.getAllName();
	}
	
	public List<CategoryVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public static void setNumOfCategory() {
		numOfCat = CategoryDAO.numberOfCategory();
	}
	
	public static void setCompanyAvg() {
		EmployeeAbilityViewService empSvc = new EmployeeAbilityViewService();
		companyAvg = empSvc.getCompanyAvg();
	}
}
