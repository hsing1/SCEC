package com.category.model;

public class CategoryVO implements java.io.Serializable{
	private Integer categoryId ;
	private String  name;
	public static int numOfCat;
	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
