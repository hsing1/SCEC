package com.category.model;

import java.util.*;
import java.sql.*;

public class CategoryJDBCDAO implements CategoryDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO CATEGORY (category_id ,name ) VALUES ( CATEGORY_seq.NEXTVAL, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT category_id  ,name  FROM CATEGORY order by category_id";
	private static final String GET_ONE_STMT = 
		"SELECT  category_id  ,name  FROM CATEGORY where category_id = ?";
	private static final String DELETE = 
		"DELETE FROM CATEGORY where category_id = ?";
	private static final String UPDATE = 
		"UPDATE CATEGORY set  name=? where category_id = ?";

	@Override
	public void insert(CategoryVO categoryVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, categoryVO.getName());
		

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(CategoryVO categoryVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, categoryVO.getName());
			pstmt.setInt(2, categoryVO.getCategoryId());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer categoryId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, categoryId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public CategoryVO findByPrimaryKey(Integer categoryId) {

		CategoryVO categoryVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, categoryId);

			rs = pstmt.executeQuery();
			
//			System.out.println(seriesVO.getName());
//			System.out.println(seriesVO.getSeriesId());

			while (rs.next()) {
				
				categoryVO = new CategoryVO();
				categoryVO.setCategoryId(rs.getInt("category_id"));
				categoryVO.setName(rs.getString("name"));
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return categoryVO;
	}

	@Override
	public List<CategoryVO> getAll() {
		List<CategoryVO> list = new ArrayList<CategoryVO>();
		CategoryVO categoryVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO 也稱為 Domain objects
				categoryVO = new CategoryVO();
				categoryVO.setCategoryId(rs.getInt("category_id"));
				categoryVO.setName(rs.getString("name"));
				list.add(categoryVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<CategoryVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		CategoryJDBCDAO dao = new CategoryJDBCDAO();

		// 新增
//		CategoryVO categoryVO = new CategoryVO();
//		categoryVO.setName("軟體類");
//		dao.insert(categoryVO);
//		categoryVO.setName("知識類");
//		dao.insert(categoryVO);
//		categoryVO.setName("態度類");
//		dao.insert(categoryVO);
//       System.out.println("insert sucessful");
	
		// 修改
//       CategoryVO categoryVO2 = new CategoryVO();
//       categoryVO2.setCategoryId(1);
//       categoryVO2.setName("其他類");
//		dao.update(categoryVO2);
//		System.out.println(categoryVO2.getCategoryId());
//		System.out.println(categoryVO2.getName());
//		System.out.println("update sucessful");
	
//		 刪除
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// 查詢
//		CategoryVO categoryVO4 = dao.findByPrimaryKey(2);
//		System.out.print(categoryVO4.getCategoryId() + ",");
//		System.out.print(categoryVO4.getName() + ",");
//		System.out.println("select sucessful");

		// 查詢
//		List<CategoryVO> list = dao.getAll();
//		for (CategoryVO seriesVO : list) {
//			System.out.print(seriesVO.getCategoryId() + ",");
//			System.out.print(seriesVO.getName() + ",");
//			System.out.println();
//		}
	}


}