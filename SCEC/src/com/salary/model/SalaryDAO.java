package com.salary.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class SalaryDAO implements SalaryDAO_interface {
	private static DataSource ds = null;
	
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO SALARY (title_id, salary_level, salary) " +
			"VALUES (?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT salary_id, title_id, salary_level, salary FROM SALARY order by salary";
	private static final String GET_ONE_STMT = 
		"SELECT * FROM SALARY where title_id = ? AND salary_level = ?";
	private static final String DELETE = 
		"DELETE FROM TITLE where title_id = ?";
	private static final String UPDATE = 
		"UPDATE TITLE set series_id = ?, level_id = ? where title_id = ? and salary_level = ?";
	private static final String SETUP = 
		"UPDATE TITLE set series_id = ? , level_id = ? where title_id = ? and salary_level = ?";
	private static final String FIND_BY_TITLE = 
		"SELECT title_id, name, series_id, level_id FROM TITLE where name = ?";

	@Override
	public void insert(SalaryVO salaryVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, salaryVO.getTitleId());
			pstmt.setInt(2, salaryVO.getSalaryLevel());
			pstmt.setInt(3, salaryVO.getSalary());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	

	@Override
	public SalaryVO findByPrimaryKey(Integer titleId, Integer salaryLevel) {

		SalaryVO salaryVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, titleId);
			pstmt.setInt(2, salaryLevel);
	
			rs = pstmt.executeQuery();

			while (rs.next()) {
				salaryVO = new SalaryVO();
				salaryVO.setSalaryId(rs.getInt("salary_id"));
				salaryVO.setTitleId(rs.getInt("title_id"));
				salaryVO.setSalaryLevel(rs.getInt("salary_level"));
				salaryVO.setSalary(rs.getInt("salary"));
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return salaryVO;
	}

	@Override
	public List<SalaryVO> getAll() {
		List<SalaryVO> list = new ArrayList<SalaryVO>();
		SalaryVO salaryVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				salaryVO = new SalaryVO();
				salaryVO.setSalaryId(rs.getInt("salary_id"));
				salaryVO.setTitleId(rs.getInt("title_id"));
				salaryVO.setSalaryLevel(rs.getInt("salary_level"));
				salaryVO.setSalary(rs.getInt("salary"));
				list.add(salaryVO);
			}

			// Handle any driver errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	

}