package com.salary.model;

import java.util.*;

public interface SalaryDAO_interface {
	public void insert(SalaryVO titleVO);
	public SalaryVO findByPrimaryKey(Integer titleId, Integer salaryLevel);
	public List<SalaryVO> getAll();
}
