package com.salary.model;

import java.util.List;
import java.util.Map;

import com.level.model.*;
import com.series.model.*;

public class SalaryService {

	private SalaryDAO_interface dao;

	public SalaryService() {
		dao = new SalaryDAO();
	}

	
	public SalaryVO addSalary(Integer titleId, Integer salaryLevel, Integer salary) {
		SalaryVO salaryVO = new SalaryVO();
		
		salaryVO.setTitleId(titleId);
		salaryVO.setSalaryLevel(salaryLevel);
		salaryVO.setSalary(salary);
	
		dao.insert(salaryVO);

		return salaryVO;
	}
	
	public SalaryVO getOneTitle(Integer titleId, Integer salaryLevel) {
		return dao.findByPrimaryKey(titleId, salaryLevel);
	}
	

	public List<SalaryVO> getAll() {
		return dao.getAll();
	}
	
}
