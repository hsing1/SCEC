package com.series.model;

import java.util.List;
import java.util.Map;

public class SeriesService {

	private SeriesDAO_interface dao;

	public SeriesService() {
		dao = new SeriesDAO();
	}

	public SeriesVO addSeries(String  name) {

		SeriesVO seriesVO = new SeriesVO();

		seriesVO.setName(name);
	
		dao.insert(seriesVO);

		return seriesVO;
	}

	public SeriesVO updateSeries(Integer seriesId,String  name) {

		SeriesVO seriesVO = new SeriesVO();

		seriesVO.setSeriesId(seriesId);
		seriesVO.setName(name);
	
		dao.update(seriesVO);

		return seriesVO;
	}

	public void deleteLevels(Integer seriesId) {
		dao.delete(seriesId);
	}

	public SeriesVO getOneSeries(Integer seriesId) { 
		return dao.findByPrimaryKey(seriesId);
	}
	
	public SeriesVO getOneSeries(String series) { 
		return dao.findBySeriesName(series);
	}

	public List<SeriesVO> getAll() {
		return dao.getAll();
	}
	
	public List<SeriesVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
