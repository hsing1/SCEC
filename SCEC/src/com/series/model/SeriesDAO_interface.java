package com.series.model;

import java.util.*;

public interface SeriesDAO_interface {
          public void insert(SeriesVO seriesVO);
          public void update(SeriesVO seriesVO);
          public void delete(Integer seriesId);
          public SeriesVO findByPrimaryKey(Integer seriesId);
          public SeriesVO findBySeriesName(String series);
          public List<SeriesVO> getAll();
          //萬用複合查詢(傳入參數型態Map)(回傳 List)
          public List<SeriesVO> getAll(Map<String, String[]> map); 
}
