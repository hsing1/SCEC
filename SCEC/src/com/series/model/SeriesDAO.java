package com.series.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.util.SCECcommon;

public class SeriesDAO implements SeriesDAO_interface {

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(SCECcommon.ctxStr);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO SERIES (series_id  ,name ) VALUES ( SERIES_seq.NEXTVAL, ?)";
	private static final String INSERT_STMT_MYSQL = "INSERT INTO SERIES(name) VALUES (?)";
	private static final String GET_ALL_STMT = 
		"SELECT series_id  ,name  FROM SERIES order by series_id";
	private static final String GET_ONE_STMT = 
		"SELECT  series_id  ,name  FROM SERIES where series_id = ?";
	private static final String DELETE = 
		"DELETE FROM SERIES where series_id = ?";
	private static final String UPDATE = 
		"UPDATE SERIES set  name=? where series_id = ?";
	private static final String FIND_BY_SERIES = "SELECT series_id, name FROM SERIES where name = ?";

	@Override
	public void insert(SeriesVO seriesVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT_MYSQL);

			pstmt.setString(1, seriesVO.getName());	

			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(SeriesVO seriesVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, seriesVO.getName());
			pstmt.setInt(2, seriesVO.getSeriesId());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer seriesId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, seriesId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public SeriesVO findBySeriesName(String series) {

		SeriesVO seriesVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_BY_SERIES);

			pstmt.setString(1, series);

			rs = pstmt.executeQuery();
			
//			System.out.println(seriesVO.getName());
//			System.out.println(seriesVO.getSeriesId());

			while (rs.next()) {
				seriesVO = new SeriesVO();
				seriesVO.setSeriesId(rs.getInt("series_id"));
				seriesVO.setName(rs.getString("name"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return seriesVO;
	}
	@Override
	public SeriesVO findByPrimaryKey(Integer seriesId) {

		SeriesVO seriesVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, seriesId);

			rs = pstmt.executeQuery();
			
//			System.out.println(seriesVO.getName());
//			System.out.println(seriesVO.getSeriesId());

			while (rs.next()) {
				
				seriesVO = new SeriesVO();
				seriesVO.setSeriesId(rs.getInt("series_id "));
				seriesVO.setName(rs.getString("name"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return seriesVO;
	}

	@Override
	public List<SeriesVO> getAll() {
		List<SeriesVO> list = new ArrayList<SeriesVO>();
		SeriesVO seriesVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO �]�٬� Domain objects
				seriesVO = new SeriesVO();
				seriesVO.setSeriesId(rs.getInt("series_id"));
				seriesVO.setName(rs.getString("name"));
				list.add(seriesVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<SeriesVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

//		SeriesJDBCDAO dao = new SeriesJDBCDAO();

		// �s�W
//		SeriesVO seriesVO = new SeriesVO();
//		seriesVO.setSeriesId(1);
//		seriesVO.setName("�޲z��");
//		dao.insert(seriesVO);
//		seriesVO.setSeriesId(2);
//		seriesVO.setName("��o��");
//		dao.insert(seriesVO);
//       System.out.println("insert sucessful");
	
		// �ק�
//		SeriesVO seriesVO2 = new SeriesVO();
//		seriesVO2.setSeriesId(4);
//		seriesVO2.setName("��o��");
//		dao.update(seriesVO2);
//		System.out.println(seriesVO2.getSeriesId());
//		System.out.println(seriesVO2.getName());
//		System.out.println("update sucessful");
	
//		 �R��
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// �d��
//		SeriesVO seriesVO = dao.findByPrimaryKey(1);
//		System.out.print(seriesVO.getSeriesId() + ",");
//		System.out.print(seriesVO.getName() + ",");
//		System.out.println("select sucessful");

		// �d��
//		List<SeriesVO> list = dao.getAll();
//		for (SeriesVO seriesVO : list) {
//			System.out.print(seriesVO.getSeriesId() + ",");
//			System.out.print(seriesVO.getName() + ",");
//			System.out.println();
//		}
	}


}