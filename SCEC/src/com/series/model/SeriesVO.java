package com.series.model;

public class SeriesVO implements java.io.Serializable{
	private Integer seriesId ;
	private String  name;
	
	public Integer getSeriesId() {
		return seriesId;
	}
	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
