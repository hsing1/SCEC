package com.grade.model;

import java.util.List;
import java.util.Map;

public class GradeService {

	private GradeDAO_interface dao;

	public GradeService() {
		dao = new  GradeDAO();
	}

	public GradeVO addGrade(Integer gradeId,Integer  grade) {

		GradeVO gradeVO = new GradeVO();

		gradeVO.setGradeId(gradeId);
		gradeVO.setGrade(grade);
	
		dao.insert(gradeVO);

		return gradeVO;
	}

	public GradeVO updateGrade(Integer gradeId, Integer  grade) {

		GradeVO gradeVO = new GradeVO();

		gradeVO.setGradeId(gradeId);
		gradeVO.setGrade(grade);
	
		dao.update(gradeVO);

		return gradeVO;
	}

	public void deleteLevels(Integer levelId) {
		dao.delete(levelId);
	}

	public GradeVO getOneEmp(Integer gradeId) {
		return dao.findByPrimaryKey(gradeId);
	}

	public List<GradeVO> getAll() {
		return dao.getAll();
	}
	
	public List<GradeVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
}
