package com.grade.model;

public class GradeVO implements java.io.Serializable{
	private Integer gradeId;
	private Integer  grade;
	public Integer getGradeId() {
		return gradeId;
	}
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	

	
}
