package com.grade.model;

import java.util.*;
import java.sql.*;

public class GradeJDBCDAO implements GradeDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	String userid = "HR";
	String passwd = "HR";

	private static final String INSERT_STMT = 
		"INSERT INTO GRADE (grade_id ,grade ) VALUES ( GRADE_seq.NEXTVAL, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT grade_id ,grade FROM GRADE order by grade_id";
	private static final String GET_ONE_STMT = 
		"SELECT  grade_id ,grade  FROM GRADE where grade_id = ?";
	private static final String DELETE = 
		"DELETE FROM GRADE where grade_id = ?";
	private static final String UPDATE = 
		"UPDATE GRADE set  grade=? where grade_id = ?";

	@Override
	public void insert(GradeVO gradeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setInt(1, gradeVO.getGrade());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(GradeVO gradeVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setInt(1, gradeVO.getGrade());
			pstmt.setInt(2, gradeVO.getGradeId());
			
		

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer gradeId) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, gradeId);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public GradeVO findByPrimaryKey(Integer gradeId) {

		GradeVO gradeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, gradeId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo 也稱為 Domain objects
				gradeVO = new GradeVO();
				gradeVO.setGradeId(rs.getInt("grade_id"));
				gradeVO.setGrade(rs.getInt("grade"));
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return gradeVO;
	}

	@Override
	public List<GradeVO> getAll() {
		List<GradeVO> list = new ArrayList<GradeVO>();
		GradeVO gradeVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO 也稱為 Domain objects
				gradeVO = new GradeVO();
				gradeVO.setGradeId(rs.getInt("grade_id"));
				gradeVO.setGrade(rs.getInt("grade"));
				list.add(gradeVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<GradeVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

		GradeJDBCDAO dao = new GradeJDBCDAO();

		// 新增
		GradeVO gradeVO = new GradeVO();
		gradeVO.setGrade(6);
		dao.insert(gradeVO);
		gradeVO.setGrade(7);
		dao.insert(gradeVO);
		System.out.println("insert sucessful");
		// 修改
//		GradeVO gradeVO2 = new GradeVO();
//		gradeVO2.setName(6);
//		gradeVO2.setGradeId(1);
//		dao.update(gradeVO2);
//		System.out.println("update  sucessful");

		// 刪除
//		dao.delete(1);
//		System.out.println("delete sucessful");

		// 查詢
//		GradeVO gradeVO3 = dao.findByPrimaryKey(3);
//		System.out.print(gradeVO3.getGradeId() + ",");
//		System.out.print(gradeVO3.getName() + ",");
//		System.out.println(" sucessful");

		// 查詢
//		List<GradeVO> list = dao.getAll();
//		for (GradeVO levelVO : list) {
//			System.out.print(levelVO.getGradeId() + ",");
//			System.out.print(levelVO.getName() + ",");
//			System.out.println();
//		}
	}


}