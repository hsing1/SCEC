package com.grade.model;

import java.util.*;

public interface GradeDAO_interface {
          public void insert(GradeVO gradeVO);
          public void update(GradeVO gradeVO);
          public void delete(Integer gradeId);
          public GradeVO findByPrimaryKey(Integer gradeId);
          public List<GradeVO> getAll();
          //萬用複合查詢(傳入參數型態Map)(回傳 List)
         public List<GradeVO> getAll(Map<String, String[]> map); 
}
